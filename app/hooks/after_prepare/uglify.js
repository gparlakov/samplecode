#!/usr/bin/env node

//var fs = require('fs');
var path = require('path');

var fs = require('fs-extra');

//trqbva da e po drug nachin za ios


var rootDir = process.argv[2];
var platformPath = path.join(rootDir, 'platforms');
var platform = process.env.CORDOVA_PLATFORMS;
var cliCommand = process.env.CORDOVA_CMDLINE;

//var configurationsToOptimize = ['RELEASE' , 'TESTBANK', 'TESTLOCAL'];
var configurationsToOptimize = ['RELEASE' , 'TESTBANK', 'TESTLOCAL', 'TESTVRBB2008'];

var requirejs = require(path.join(rootDir, 'r.js'));



switch (platform) {
    case 'android':
        platformPath = path.join(platformPath, platform, 'assets', 'www');
        break;
    case 'ios':
        platformPath = path.join(platformPath, platform, 'www');
        break;
    default:
        console.log('this hook only supports android and ios currently');
        break;
        //return;
}
var internalLibsPath = path.join(platformPath, 'Scripts', 'libs');

var buildPath = path.join(platformPath, '../www-build');

var viewModelsPath = path.join(platformPath, 'Scripts', 'Mobile', 'ViewModels');
var viewModelsBuildPath = path.join(buildPath, 'Scripts', 'Mobile', 'ViewModels');


var _ = require(path.join(internalLibsPath, 'underscore', 'underscore-1.6.0'))._;

var buildConfiguration = process.env.Configuration; 

if(_.any(configurationsToOptimize, function(el){ return el == process.env.Configuration} )){

    var optimizeConfig = {
        appDir: platformPath,
        baseUrl: "./Scripts",
        modules: [
            {
                name: "main"
            }
        ],
        dir: buildPath,
        mainConfigFile: './www/Scripts/main.js',
        skipDirOptimize: false,
        optimize: 'uglify2',
        nodeRequire: require,
        fileExclusionRegExp: /ViewModels/,
        uglify2: {
            compress: {
                global_defs: {
                    Configuration: buildConfiguration
                }
            }
        }
    }

    //TODO da se naprvi s promisi ?? primerno s  fs-extra-promise  ili Bluebird ili druga 
    requirejs.optimize (optimizeConfig, function (){
        
        fs.copy(viewModelsPath, viewModelsBuildPath, function(error) {
            if (error) {
                console.log(error);
            }
            else{
                fs.remove(platformPath, function(err) {
                    if(err) {
                        console.log(err);   
                    }else {
                        fs.renameSync(buildPath, platformPath);
                    }
                });
            }

        });
    });

}


