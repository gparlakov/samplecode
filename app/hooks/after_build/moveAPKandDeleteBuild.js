#!/usr/bin/env node

var platform = process.env.CORDOVA_PLATFORMS;
var buildConfiguration = process.env.Configuration;

function ApkPath(apkName, apkPath, rootDir) {
    this.FileName = apkName;
    this.SourcePath = path.join(apkPath, apkName);
    this.DestinationPath = path.join(rootDir, apkName);

    return this;
}

//only for android and if buildConfiguration is not set
if (platform == 'android' && buildConfiguration) {
    console.log('Startin cleanup for ' + platform + ' ' + buildConfiguration);
    var path = require('path');

    var rootDir = process.argv[2];
    var platformsPath = path.join(rootDir, 'platforms');
    var buildPath = path.join(platformsPath, platform, 'build');
    var apkPath = path.join(buildPath, 'outputs', 'apk');
    
    var buildTarget = process.env.CORDOVA_CMDLINE.indexOf('--release') > 0 ? 'release-unsigned' : 'debug';

    var fs = require('fs-extra');
    var replaceOptions = { clobber: true };
    var apks = [
        new ApkPath('android-armv7-' + buildTarget + '.apk', apkPath, rootDir),
        new ApkPath('android-x86-' + buildTarget + '.apk', apkPath, rootDir)
    ];
    
    //console.log("moving " + apks[0].FileName + " to " + rootDir);
    fs.move(apks[0].SourcePath, apks[0].DestinationPath, replaceOptions, function (error1) {
        if (error1) {
            console.log(error1);
        }
        else {
            //console.log("moving " + apks[1].FileName + " to " + rootDir);
            fs.move(apks[1].SourcePath, apks[1].DestinationPath, replaceOptions, function (error2) {
                if (error2) {
                    console.log(error2);
                }
                else {
                    //console.log("deleting " + buildPath);
                    fs.delete(platformsPath, function (error3) {
                        if (error3) {
                            console.log(error3);
                        }
                    });
                }
            });
        }
    });
}