﻿define(
    [
        "async!https://maps.googleapis.com/maps/api/js?v=3.12&libraries=places&language=bg&sensor=true!callback",
    ],
    function () {
        // Enable the visual refresh
        window.google.maps.visualRefresh = true;
        return window.google.maps;
    }
)
