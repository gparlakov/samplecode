﻿define([
    'jquery',
    'underscore',
    'knockout',

    'Mobile/ViewModels/Base/PopupPageViewModel',
    'text!Mobile/Templates/Accounts/Movements/Partials/DatesPaymentSystem.html'

], function ($, _, ko, PopupPageViewModel) {

    function BankStructureHelperPopUp(parent, options) {

        var self = this;

        //PopupPageViewModel.apply(this, arguments);
        PopupPageViewModel.apply(this, arguments);

        this.path = "BankBranches/ViewBankStructure/BankStructureTemplate";

        this.BankStructure = ko.observable();

        this.loadData = function () {
            var result =
                $.whenEx(
                    options.load()
                )
                .done(function (bankStructure) {
                    self.BankStructure(bankStructure);
                });

            return result;
        };
    }

    return BankStructureHelperPopUp;
});