﻿define([
  'jquery',
  'underscore',
  'knockout',
  'services',
  'validator',
  'productscache',
  'Mobile/ViewModels/Common/AccountLabelBase',
  'text!Mobile/Templates/Common/AccountsListHelperTemplate.html'
], function ($, _, ko, services, validator, productscache, AccountLabelBase, AccountsListHelperTemplate)
{
    function AccountsListHelper(parent, options) {
        var self = this;

        validator.validatableModel.call(this, {
            properties: {
                "SelectedAccount": {
                    rules: {
                        NotNull: !options.showOptionAll
                    },
                    messages: {
                        NotNull: "TMPL_VAL_SPC_CHOOSE_BENEFICIARY"
                    }
                }
            }
        });

        this.showOptionAll = false;

        if (options.showOptionAll) {
            this.showOptionAll = true;
            this.ClearAccount = function () {
                self.SelectedAccount(null);
            }
        }

        AccountLabelBase.apply(this);

        this.APAccounts = ko.observableArray();
        this.SelectedAccount = ko.observable(options.SelectedAccount);

        this.SelectAccount = function (apa) {
            var key = new services.Objects.BankAccountKey(apa.BankAccount);
            self.SelectedAccount(key);
        }

        this.loadData = function () {
            var result =
                $.whenEx(
                    options.load()
                )
                .done(function (accounts) {
                    self.APAccounts.removeAll();
                    var grouped = _.groupBy(accounts, function (apa) {
                        if (options.GroupByBankClient) {
                            return apa.BankAccount.BankClient.BankClientID
                        } else {
                            return null;
                        }
                    });
                    var apas = _.map(grouped, function (group, key) {
                        return {
                            BankClient: productscache.GetBankClientByID(key),
                            Items: group
                        };
                    });
                    if (options.FirstBankClient) {
                        var pos;
                        for (var i = 0; i < apas.length; i++) {
                            if (apas[i].BankClient.BankClientID == options.FirstBankClient.BankClientID) {
                                pos = i;
                            }
                        }
                        var element = apas[pos];
                        apas.splice(pos, 1);
                        apas.splice(0, 0, element);
                    }
                    self.APAccounts(apas);
                }).done(function() {
                    //scroll to selected account after render
                    var element = $("div.select-payee li.selected");
                    if (element.length == 1) {
                        $('body').animate({ scrollTop: element.offset().top - 100 }, 400);
                    }
                });

            return result;
        };
    }

    return AccountsListHelper;
});