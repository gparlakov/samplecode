﻿define([
  'underscore',
  'knockout',
  'backbone',
  'validator',
  'services',
  'utils',
  'Mobile/ViewModels/Payments/PaymentWizardBaseViewModel',

  'Mobile/ViewModels/Common/AccountsHelperPopUp'
], function (_, ko, backbone, validator, services, utils, PaymentWizardBaseViewModel, AccountsHelperPopUp) {

    function PaymentWizardChoosePayerAmountViewModel(model) {

        validator.validatableModel.call(this,
            {
                properties: {
                    "amount": {
                        rules: {
                            RegEx: validator.RegExUtils.Amount,
                            NotNullOrEmpty: true,
                            DeciamlMinValue: { value: 0, inclusive: false }
                        },
                        messages: {
                            RegEx: "TMPL_VAL_SPC_AMOUNT"
                        }
                    },
                    "selectedCCY": {
                        rules: {
                            NotNullOrEmpty: true
                        }
                    },
                    "selectedAccount": {
                        rules: {
                                NotNullOrEmpty: true
                        }
                    }
                }
            }
        );

        var self = this;
        
        PaymentWizardBaseViewModel.call(this, model);

        this.GetRequestModelForNext = function () {
            return new services.Objects.PaymentWizardChoosePayerAndAmountInput({
                State: self.Model.State,
                Payer: { BankAccount: { BankAccountID: self.selectedAccount().BankAccountID } },
                Amount: { Amount: self.amount(), CCY: self.selectedCCY() }
            });
        }

        this.GetRequestModelForBack = function () {
            throw "Not supported";
        }

        var ccyFromState = self.Model.State.Amount ? self.Model.State.Amount.CCY : null;
        var ccyFromStateSWIFTCode = ccyFromState ? ccyFromState.SWIFTCode : null;
        var ccyFromList = _.find(self.Model.AllowedCCYs, function (ccy) { return ccy.SWIFTCode == ccyFromStateSWIFTCode; });

        this.selectedCCY = ko.observable(ccyFromList);

        var selectedAccountID = self.Model.State.Payer ? self.Model.State.Payer.BankAccount.BankAccountID : null;
        var selectedAccountFromList = _.find(self.Model.AllowedAccounts, function (acc) { return acc.BankAccount.BankAccountID == selectedAccountID })
        this.selectedAccount = ko.observable(selectedAccountFromList ? selectedAccountFromList.BankAccount : null);

        this.amount = ko.observable(self.Model.State.Amount && self.Model.State.Amount.Amount >0 ? self.Model.State.Amount.Amount : null);


        this._getAPAccounts = function () {
            return self.Model.AllowedAccounts;
        }

        this.AccountsHelperPopUp = new AccountsHelperPopUp(
                                    self,
                                    { load: self._getAPAccounts }
                        );


        this.GetAPAccountsHandler = function () {
            self.AccountsHelperPopUp
                .Open()
                .done(function (account) {
                    self.selectedAccount(account.BankAccount);
                });
        }

        this.MoveBack = function (actionModel) {
            utils.navigateBack();
        }

        this.Init = function (initCompletedCallback) {
            
            self.selectedAccount.subscribe(function (newValue) { //по подразбиране валутата да се сменя с тази на избраната сметка
                
                var fromCCYList = _.find(self.Model.AllowedCCYs, function (fromList) {
                    return fromList.SWIFTCode == newValue.CCY.SWIFTCode
                });
                if (fromCCYList) {
                    self.selectedCCY(fromCCYList);
                }
            });

            if (initCompletedCallback) {
                initCompletedCallback.call(this);
            }
        }
    };

    return PaymentWizardChoosePayerAmountViewModel;
});