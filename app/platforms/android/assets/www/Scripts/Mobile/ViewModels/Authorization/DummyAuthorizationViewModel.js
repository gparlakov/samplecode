﻿define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'services',
  'utils',
  'servicesObjects',
  'events',
  'uimodels',
  'i18n!Mobile/nls/strings'
], function ($, _, backbone, ko, komapping, services, utils, servicesObjects, events, uimodels, strings) {

    //това трябва да стане част от другия Auth
    var DummyAuthorizationViewModel = function () {
   
        var self = this;

        this.Process = function () {
            events.trigger(self, "success", null);
        };

        this.Cancel = function () {
            events.trigger(self, "cancel", null);
        };
    };

    return DummyAuthorizationViewModel;
});