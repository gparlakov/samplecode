define([
  'utils',
  'jquery',
  'services',
  'i18n!Mobile/nls/strings',
  'Mobile/ViewModels/Repository/ActionButtonTypes',
  'servicesObjects',
  'knockout',
  'deviceapi'

], function (utils, $, services, strings, buttonTypes, servicesObjects, ko, deviceapi) {

    var actionButtons = {};

    var statusActive = servicesObjects.EnumNotificationCorrespondentSubscriptionStatus.Active;
    var statusInactive = servicesObjects.EnumNotificationCorrespondentSubscriptionStatus.Inactive;

    actionButtons.ActivateSubscription = function (subscription) {

        $.extend(this, buttonTypes.green);

        this.enabled = ko.computed(function () {
            return subscription.Status() == statusInactive;
        });

        this.label = strings.ID_STR_ONLINE_NOTIF_ACTIVATE;
        this.click = function () {
            changeStatus(subscription, statusActive, function () {
                subscription.IsActive(true);               
            });
        }
    };

    actionButtons.DeactivateSubscription = function (subscription) {

        $.extend(this, buttonTypes.red);

        this.enabled = ko.computed(function () {
            return subscription.Status() == statusActive;
        });
           
        this.label = strings.ID_STR_ONLINE_NOTIF_DEACTIVATE;
        this.click = function () {

            var dialog = deviceapi.notification.Confirm(
                "title",
                "message",
                [{ ID: "YES", text: "Sure" }, { ID: "NO", text: "No!" }]
            );

            $.whenEx(
                dialog
            )
            .then(function (dialogres) {
                if (dialogres.ID == "YES") {
                    changeStatus(subscription, statusInactive, function () {
                        subscription.IsActive(false);
                    });
                }
            })
        }
    };

    var changeStatus = function (subscription, newStatus, callback) {

        var newStatus = servicesObjects.ChangeSubscriptionStatus({
            Subscription: subscription,
            SubscriptionStatus: newStatus
        });

        services.OnlineNotificationsService.ChangeSubscriptionStatus(newStatus, function () {
            subscription.Status(newStatus);
            callback();
        });
    }

    return actionButtons;  
});