﻿define([    
    'jquery',   
    'services'    
], function ($, services) {

    function OnlineNotificationViewModel(notification) {
        this.Notification = notification;
        this.FullDataIsSet = notification.FullDataIsSet || false;

        var self = this;

        this.getNotification = function () {
            var notifKey = new services.Objects.OnlineNotificationKey({ ID: notification.ID });

            var notfSrv = new services.Services.OnlineNotificationsService();
            var res = notfSrv.GetOnlineNotification(notifKey)
                    .done(function (data) {
                        self.Notification = data.OnlineNotification;
                        self.Notification.FullDataIsSet = true;                        
                    });

            return res;
        }

    }

    return OnlineNotificationViewModel;
});