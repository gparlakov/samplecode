﻿define([
  'jquery',
  'knockout',
  'services',
  'servicesObjects',
  'validator'
], function ($, ko, services, servicesObjects, validator) {
    function CaptchaViewModel() {
        var self = this;

        this.CaptchaRequired = ko.observable(false);
        this.ImageSrc = ko.observable(null);
        this.CaptchaCode = ko.observable(null);
        this.HashKey = null;

        validator.validatableModel.call(this, function () {
            return {
                properties: {
                    "CaptchaCode": {
                        rules: {
                            NotNullOrEmpty: ko.computed({
                                read: function () {
                                    return self.CaptchaRequired();
                                }
                            })
                        }
                    }
                }
            }
        });

        this.GetModel = function () {
            var self = this;

            if (self.CaptchaRequired()) {
                var model = {
                    CaptchaKey: { ID: self.HashKey },
                    CaptchaValue: self.CaptchaCode()
                };
                return new servicesObjects.AntiBotProtectionCaptcha(model);
            } else {
                return null;
            }
        }

        this.LoadCaptcha = function () {
            var self = this;

            self.CaptchaRequired(true);

            var res = services.AntiBotProtectionService
                .RequestAntiBotProtection()
                .done(function (result) {
                    if (result.Status.StatusCode == 0) { //success
                        self.CaptchaCode(null);
                        self.HashKey = result.AntiBotProtectionTokenData.CaptchaKey.ID;

                        var src = "data:image/" +
                            result.AntiBotProtectionTokenData.CaptchaImage.ImageFormat +
                            "{0};base64," +
                            result.AntiBotProtectionTokenData.CaptchaImage.Data.$value;

                        self.ImageSrc(src);
                    }
                });
        }
    }
    return CaptchaViewModel;
});