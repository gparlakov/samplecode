define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'services',
  'productscache',
  'validator',
  'events',
  'Mobile/ViewModels/Common/MovementsViewModel',
  'text!Mobile/Templates/Cards/CardMovementAdditionalInfoTemplate.html',
  'Mobile/Models/Filters/MovementType'

], function ($, _, bakcbone, ko, komapping, services, productscache, validator, events, CommonMovementsViewModel, CardMovementAdditionalInfoTemplate, movementTypeOptions) {

    var MovementsViewModel = function (cardKey) {
        var filter = new services.Objects.BankCardMovementsFilterForPeriod({
            BankCard: cardKey,
            Paging: new services.Objects.Paging({ ResultsForPage: 10 }),
            Period: new services.Objects.Period()
        });

        CommonMovementsViewModel.call(this, filter);
        var self = this;

        validator.validatableModel.call(this, function () {
            return {
                properties: {
                    "MovementsFilter": {
                        properties: {
                            "Period": {
                                properties: {
                                    "DateStart": {
                                        rules: {
                                            NotNullOrEmpty: true
                                        }
                                    },
                                    "DateEnd": {
                                        rules: {
                                            NotNullOrEmpty: true
                                        }
                                    }
                                }
                            }
                        }
                    }//,
                    //"password": {
                    //    rules: {
                    //        NotNullOrEmpty: true
                    //    }
                    //}
                }
            }
        });

        this.MovementTypeSelect = movementTypeOptions();
    };

    return MovementsViewModel;
});