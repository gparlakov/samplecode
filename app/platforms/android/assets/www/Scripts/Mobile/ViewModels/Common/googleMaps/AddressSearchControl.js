﻿define([
    'jquery',
    'knockout',
    'i18n!Mobile/nls/googleMaps',
    'mapBaseControl',
    'events',
    
    'text!Mobile/Templates/Common/googleMaps/AddressSearchControlTemplate.html',
    
], function ($, ko, googleMaps, baseControl, events, template) {

    var boundsHelperConstrain = new googleMaps.LatLngBounds(
        new googleMaps.LatLng(41.173302153445874, 22.35083908264164), 
        new googleMaps.LatLng(44.28731145416453, 28.65699142639164)
    );

    function SearchAddress(map) {

        var self = this;

        baseControl.call(this, template);

        this.map = map;
        
        this.getPlace = function () {

            var autoCompl = self.searchAutocomplete;


            //hakeriisko, no api-to za autocomplete ne poddarja vzimane na parviq prediction 
            //autoCompl.gm_accessors_.place.Gc.predictions [0] - e drugiq variant 
            if ($('.pac-selected').length <= 0) {

                var firstResult = $(".pac-container .pac-item:first").text();
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({ "address": firstResult }, function (results, status) {

                    if (status == google.maps.GeocoderStatus.OK) {
                        self.map.setCenter(results[0].geometry.location);
                        self.map.setZoom(13);
                    }
                });
            }
            return false;
        }

        this.eventTypes = {};
        this.eventTypes.showSearch = 'showSearch';
        this.eventTypes.hideSearch = 'hideSearch';

        this.visible = ko.observable(false);

        this.show = function (contolModel, event) {

            self.visible(true);

            //self.uiBtnShowSearch.hide();
            events.trigger(self, self.eventTypes.showSearch);

            //self.uiSearchDiv.show();
            //self.uiSearchInput.focus();

        }

        this.hide = function () {
            
            events.trigger(self, self.eventTypes.hideSearch);
            self.visible(false);
            //self.uiSearchDiv.hide();

            //self.uiBtnShowSearch.show();
        }
        this.enableHideOnBodyClick();

        this.create();

        //this.uiSearchDiv = $('div.searchDiv', self.control);
        this.uiSearchInput = $('input[type=search]', self.control);        
        //this.uiBtnShowSearch = $('#showSearch', self.control);
        


        this.searchAutocompleteOptions = {
            bounds: boundsHelperConstrain,
            componentRestrictions: { country: 'bg' },
            //types: ['geocode' , 'regions']
        }

        this.searchAutocomplete = new googleMaps.places.Autocomplete(this.uiSearchInput[0], this.searchAutocompleteOptions);
        
        googleMaps.event.addListener(this.searchAutocomplete, 'place_changed', function () {
            var result = self.searchAutocomplete.getPlace();
            if (!result.geometry) {
                //new e namereno ??? 
                //nqkakav class na imput-a moje bi ? 
                return;
            }

            self.map.setCenter(result.geometry.location);
            self.map.setZoom(13);
        });

      
    }

    return SearchAddress;
});