﻿define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'knockoutDais',
  'services',
  'utils',
  'i18n!Mobile/nls/strings',
  'events',
  'uimodels',
  'Mobile/Models/Payments/OperationStatusToMessageFormater',
  'Mobile/ViewModels/Common/HeaderViewModel',
], function ($, _, backbone, ko, komapping, knockoutDais, services, utils, strings, events, uimodels, operationStatusFormater, HeaderViewModel) {

    
    function ProcessPaymentDeleteViewModel(paymentsKeys) {
        this.HeaderViewModel = new HeaderViewModel({ titleStringID: "ID_STR_DELETE" });
        uimodels.Notifications.UINotificationContainer.call(this);

        this._operationStatusFormatter = new operationStatusFormater();

        
        var self = this;
    
        this.Types = {
            State: {
                Confirmation: 1,
                Completed: 3,
                Error: 4
            }
        };

        this.State = ko.observable(this.Types.State.Confirmation);

        this.PaymentsKeys = paymentsKeys;
        this.Payments = [];

        this.Init = function (initCompletedCallback) {
            self._getPayemtns(function () {
                initCompletedCallback.call(this);
            });
        };

        this.Delete = function () {
            self._delete();
        }

        this.Back = function () {
            utils.navigateBack();
        }

        this.Cancel = function () {
            self.Back();
        };

        this._getPayemtns = function (getPaymentsCompletedCallback) {
            services.PaymentOrderService.GetPaymentOrdersPending(
                    {
                        PaymentKeys: self.PaymentsKeys,
                        Paging: {
                            CurrentPage: 1,
                            ResultsForPage: -1
                        }
                    },
                    function (payments) {
                        _.each(payments.PendingPayments, function (payment) {
                            self.Payments.push(payment);
                        });

                        getPaymentsCompletedCallback.call(this);
                    }
                );
        };

        this._delete = function () {
            services.PaymentService.DeletePayments(
                self.PaymentsKeys,
                function (statuses) {
                    if (statuses && statuses.OperationStatuses) {
                        _.each(statuses.OperationStatuses, function (status) {
                            self.AddNotification(self._operationStatusFormatter.FormatOperationsStatus(status));
                        });
                        self.State(self.Types.State.Completed);
                    }
                }
            );
        }
    };

    return ProcessPaymentDeleteViewModel;
});