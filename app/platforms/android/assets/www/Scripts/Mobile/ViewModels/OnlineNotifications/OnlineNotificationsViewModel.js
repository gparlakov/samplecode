﻿define([    
    'jquery',   
    'Mobile/ViewModels/Repository/SubMenu/OnlineNotificationsSubmenu',
    'Mobile/ViewModels/Common/HeaderViewModel',
    
], function ($, onlineNotifSubmenu, headerViewModel) {
    function OnlineNotificationsViewModel() {
        var self = this;

        this.Init = function () {
            this.HeaderViewModel = new headerViewModel({ titleStringID: "ID_STR_ONLINE_NOTIF" });
        }

        this.Init();

        this.Submenu = onlineNotifSubmenu();
    }



    return OnlineNotificationsViewModel;
});