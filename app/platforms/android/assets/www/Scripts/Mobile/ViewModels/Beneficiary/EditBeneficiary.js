﻿define([
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'utils',
  'services',
  'validator',
  'typeVisitor',
  'Common/ufnomenclatures/UFNmFactory',
  'validator',
  'Mobile/ViewModels/Payments/Beneficiaries/BeneficiariesHelperPopUp'
], function (_, Backbone, ko, komapping, utils, services, validator, typeVisitor, ufnmFactory, validator, BeneficiariesHelperPopUp) {

    var AccountTypes = 
    {
        Home: 1,
        Foreign: 2
    }

    function EditBeneficiaryHomeAccount(account)
    {
        validator.validatableModel.call(this, {
            properties: {
                "Account": {
                    properties: {
                        "IBAN": {
                            rules: {
                                NotNullOrEmpty: true,
                                HomeIBAN: true
                            }
                        }
                    }
                }
            }
        });

        this.Account = account;
        this.IBANtoUpper = function (viewmodel) {
            var iban = ko.utils.unwrapObservable(viewmodel.Account.IBAN);
            if (iban && iban != '') {
                viewmodel.Account.IBAN(iban.toUpperCase());
            }
        }

    }

    function EditBeneficiaryForeignAccount(account)
    {
        var self = this;

        validator.validatableModel.call(this, function() { return {
            properties: {
                "Account": {
                    properties: {
                        "AccountNumber": {
                            rules: {
                                NotNullOrEmpty: true,
                                InternationalIBANForCountry: ko.dependentObservable({ read: function () { return self.SelectedCountry() != null ? self.SelectedCountry().Value : null; } }),
                                RegEx: "^[a-zA-Z0-9\" ()-\\/,.]{0,35}$",
                            },
                            messages: {
                                RegEx: "TMPL_VAL_SPC_INVALIDACCOUNTNUMBER"
                            }
                        },
                        "BankSWIFT" : {
                            rules: {
                                InternationalSWIFTForCountry: ko.dependentObservable({ read: function () { return self.SelectedCountry() != null ? self.SelectedCountry().Value : null; } }),
                                ThisOrOtherGroupMustBeFilled: { properties: ["BankName", "BankAddress", "BankCity", "BankCountry"] },
                                BankSWIFT: true
                            },
                            messages: {
                                ThisOrOtherGroupMustBeFilled: "TMPL_VAL_SPC_FOREIGNBENEFACCOUN_SWIFT_OR_BANKINFO"
                            }
                        },
                        "BankName": {
                            rules: {
                                MaxLength: 35,
                                SWIFTSymbols: true
                            }
                        },
                        "BankAddress": {
                            rules: {
                                MaxLength: 35,
                                SWIFTSymbols: true
                            }
                        },
                        "BankCity": {
                            rules: {
                                MaxLength: 35,
                                SWIFTSymbols: true
                            }
                        }
                    }
                },
                "SelectedCountry": {
                    rules: {
                        //NotNull: true
                    }
                }
            }
        }});


        this.Account = account;

        this.ViewData = {};
        this.ViewData.Countries = ko.observable(null);

        
        this.SelectedCountry = ko.computed({
            read: function () {
                if (self.Account.BankCountry() != null && self.ViewData.Countries() != null)
                {
                    return _.find(
                            self.ViewData.Countries(),
                            function (item) {
                                return item.Value.Code == self.Account.BankCountry().Code;
                            });
                }
                else
                {
                    return null;
                }
        
            },
            write: function (newValue) {
                if (newValue) {
                    self.Account.BankCountry(
                            new services.Objects.CountryKey(newValue.Value)
                        );
                }
                else {
                    self.Account.BankCountry(null);
                }
            }
        });

        var nmFactory = new ufnmFactory();
        $.whenEx(
            nmFactory.GetUfNomenclature(nmFactory.ufNomenclatureTypes.NomenclatureCountry)
        ).then(function(nm) {
            var nomenklature = nm.GetNomenclature()
            var toShow = _.map(nomenklature, function (item) {
                return {
                    Name: $.GetLocalizedString(item.Name),
                    Value: item
                }
            });
            self.ViewData.Countries(toShow);
        });
    }


  

    function EditBeneficiary(parent, options) {
        validator.validatableModel.call(this,
            {
                properties: {
                    "BeneficiaryInput": {
                        rules: {
                            NotNull: true
                        },
                        properties: {
                            "BeneficiaryName": {
                                rules: {
                                    MaxLength: 70,
                                    NotNullOrEmpty: true
                                }
                            }
                            //,"BeneficiaryAddress": {
                            //    rules: {
                            //        MaxLength: 70,
                            //    }
                            //}
                        }
                    },
                    "BeneficiaryAccountInput" : {
                        rules : {
                            NotNullOrEmpty: true
                        }
                    },
                    "AccountViewModel" : {
                        rules : {
                            SubModel: true
                        }
                    },
                    "AccountType": {
                        rules: {
                            NotNullOrEmpty: true
                        }
                    }
                }
            }
        );


        var self = this;

        this.Types = {
            AccountTypes: AccountTypes
        }

        this.BeneficiaryInput = ko.observable(komapping.fromJS(options.SelectedBeneficiary.Beneficiary));
        this.BeneficiaryAccountInput = ko.observable();

        this.AccountType = ko.dependentObservable({
            read: function () {
                if (self.BeneficiaryAccountInput() != null) {
                    return self._getTypeByAccountInstance(self.BeneficiaryAccountInput());
                }
            },
            write: function (newValue) {
                
                var account = self.BeneficiaryAccountInput();
                var type = self._getTypeByAccountInstance(account)

                if (type == newValue)
                    return;

                if (newValue == AccountTypes.Home)
                {
                    account = new services.Objects.BeneficiaryAccountHomeCountryInput();
                }
                if (newValue == AccountTypes.Foreign)
                {
                    account = new services.Objects.BeneficiaryAccountForeignCountryInput();
                }
                //var oldAcc = self.BeneficiaryAccountInput();
                var newAcc = komapping.fromJS(account);
                //komapping.fromJS(komapping.toJS(oldAcc), newAcc);
                self.BeneficiaryAccountInput(newAcc);
            }
        });

        this._getTypeByAccountInstance = function(account)
        {
            var account = ko.utils.unwrapObservable(account);
            if (account == null)
                return null;
            return typeVisitor.visit(account, {
                BeneficiaryAccountForeignCountryInput: AccountTypes.Foreign,
                BeneficiaryAccountHomeCountryInput: AccountTypes.Home
            });
        }

        this.Init = function(callback)
        {
            //init beneficiary
            var cleanBeneficiary = new services.Objects.BeneficiaryInput();
            self.BeneficiaryInput(komapping.fromJS(cleanBeneficiary));

            //init account
            var cleanAccount = new services.Objects.BeneficiaryAccountHomeCountryInput();
            self.BeneficiaryAccountInput(komapping.fromJS(cleanAccount));

            if ($.isFunction(callback))
                callback.call(self);
        }

        this.AccountViewModel = ko.computed({
            read: function () {
                var account = self.BeneficiaryAccountInput();
                var type = self._getTypeByAccountInstance(account)
                if (type == AccountTypes.Home) {
                    return new EditBeneficiaryHomeAccount(account);
                }
                if (type == AccountTypes.Foreign) {
                    return new EditBeneficiaryForeignAccount(account);
                }
            }

        })

        this.SetAccount = function (acc) {
            self.BeneficiaryAccountInput(
                komapping.fromJS(acc, {
                    "BankCountry": {
                        create: function (options) {
                            return ko.observable(options.data);
                        }
                    } //iskame observable-a da e na nivo BankCountry a ne na listata
                })
            );
        }
        this.SetAccount(options.SelectedBeneficiary.Account);

        this.SelectBeneficiary = function (benef)
        {
            var beneficiaryService = new services.Services.BeneficiaryService();
            $.whenEx(
                beneficiaryService.GetBeneficiaryAccountAsInput(new services.Objects.BeneficiaryAccountKey(benef))
            )
            .done(function (response) {
                self.BeneficiaryInput(response.BeneficiaryInput);
                self.SetAccount(response.BeneficiaryAccountInput);
            });
        }

        this.BeneficiariesHelperPopUp = new BeneficiariesHelperPopUp(self, options.WizardState);

        this.GetBeneficiariesHandler = function () {
            self.BeneficiariesHelperPopUp
                .Open()
                .done(function (data) {
                    self.SelectBeneficiary(data);
                });
        }
	};



    return EditBeneficiary;
});