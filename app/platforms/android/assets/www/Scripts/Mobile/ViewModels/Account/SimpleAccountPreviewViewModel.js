define([
  'underscore',
  'knockout',
  'komapping',
  'services',
  'productscache'
], function (_, ko, komapping, services, productscache) {


    var SimpleAccountPreviewViewModel = function (model) {
        var apa = productscache.GetAPAccountByID(model.BankAccountID);
        
        var viewModel = _.extend(model, { APAccount: apa });

        return viewModel;
    };

    return SimpleAccountPreviewViewModel;
});