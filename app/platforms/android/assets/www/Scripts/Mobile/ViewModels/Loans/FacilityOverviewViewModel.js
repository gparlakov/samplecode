define([
  'underscore',
  'productscache',
  'Mobile/ViewModels/Common/HeaderViewModel',
  'Mobile/ViewModels/Repository/SubMenu/FacilitiesSubmenu'
], function (_, productscache, headerViewModel, facilitySubmenu) {


    var FacilityOverviewViewModel = function (apFacility) {

        this.BankFacility = apFacility.BankFacility;
        //this.Init = function () {
        //    this.APAccount = productscache.GetAPAccountByID(model.BankAccountID);
        //}
        //this.Init();

        this.Submenu = facilitySubmenu([apFacility.BankFacility.BankClient.BankClientID, apFacility.BankFacility.FacilityType.ID, apFacility.BankFacility.FacilitySequence]);
        this.HeaderViewModel = new headerViewModel({ titleStringID: "ID_STR_LOAN_DETAILS" });

    };

    return FacilityOverviewViewModel;
});