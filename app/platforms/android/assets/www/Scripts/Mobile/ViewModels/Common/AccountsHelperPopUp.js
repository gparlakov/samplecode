﻿define([
  'jquery',
  'underscore',
  'knockout',
  'services',
  'Mobile/ViewModels/Common/AccountsListHelper',
   'Mobile/ViewModels/Base/PopupPageViewModel'

], function ($, _, ko, services, AccountsListHelper, PopupPageViewModel)
{
    function AccountsHelperPopUp(parent, options) {
        var self = this;

        PopupPageViewModel.apply(this, arguments);
        AccountsListHelper.apply(this, arguments);

        if (options.showOptionAll) {
            this.ClearAccount = function () {
                //a moje i s null ? 
                self["return"]({ dummyForOptionAll: null });
            }
        }
        
        this.SelectAccount = function (account) {
            self["return"](account);
        }

        this.path = "Common/PayeeAccountsHelperPopUpTemplate";
    }

    return AccountsHelperPopUp;
});