define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'services',
  'validator',
  'events'

], function ($, _, bakcbone, ko, komapping, services, validator, events) {

    var MovementsViewModel = function (filter) {
        var self = this;

        this.MovementsFilter = komapping.fromJS(filter);
        this.Movements = ko.observableArray([]);

        this.GetNonobservableFilter = function () {
            return komapping.toJS(self.MovementsFilter);
        }

        this.isLoaded = ko.observable(false);
        this.find = function () {
            if (validator.getValidator(self).validate()) {
                self.MovementsFilter.Paging.CurrentPage(1);
                this._findInternal(self, true);
                self.isLoaded(true);
            }
        }

        this.findMore = function () {
            var def = new $.DeferredEx(function (def) {
                if (self.MovementsFilter.Paging.TotalPages() > self.MovementsFilter.Paging.CurrentPage() && validator.getValidator(self).validate()) {
                    self.MovementsFilter.Paging.CurrentPage(self.MovementsFilter.Paging.CurrentPage() + 1);
                    def.bindTo(self._findInternal(false));
                } else {
                    def.fail();
                }
            });
            return def;
        }

        this.pipe = $.DeferredEx();
        this.pipe.resolve();

        this._findInternal = function (shouldClearOldResults) {
            var res = $.DeferredEx(function (res) {

                var action = function () {
                    services.BankProductInfoService.GetProductMovements(self.GetNonobservableFilter(), function (data) {

                        if (shouldClearOldResults) {
                            self.Movements.removeAll();
                        }

                        for (var movt in data.Movements) {
                            self.Movements.push(data.Movements[movt]);
                        }

                        komapping.fromJS(data.MovementsFilter, self.MovementsFilter);

                    })
                    .done(function () {
                        res.resolve();
                    });
                }

                self.pipe.pipe(action);
            });
            return res;
        }

        this.initialize = function () {
            events.one(self, 'rendered', function () {
                $(window).daisScrollV2('register', self.findMore);
                events.bind(self, "findresulsfinished", function () {
                    $(window).daisScrollV2('reset', self.findMore);
                });
            });
        }

        this.findresulsfinished = function () {

        }

        this.unload = function () {

            $(window).daisScrollV2('unregister', self.findMore);
        }

    };

    return MovementsViewModel;
});