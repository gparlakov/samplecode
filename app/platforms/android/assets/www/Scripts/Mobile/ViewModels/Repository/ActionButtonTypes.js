define(
 function () {
    var buttonTypes = {
        green: { type: 'btn-green' },
        greenLight: { type: 'btn-green-light' },
        blue: { type: 'btn-gray' },
        gray: { type: 'btn-blue' },
        red: { type: 'btn-red' }
    };
    return buttonTypes;
 });