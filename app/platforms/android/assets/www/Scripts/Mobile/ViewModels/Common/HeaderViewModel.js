﻿define([
    'i18n!Mobile/nls/strings'
], function (strings) {
    //args = {
    //    //by priority
    //    titleStringID: "strings.StringID",
    //    titleStringValue : "value as string" 
    //}
    function HeaderViewModel(headerArgs) {
        this.HeaderTemplate = headerArgs.headerTemplate || "text!Mobile/Templates/Common/PanelHeader.html";
        this.Title = '';
        if (headerArgs.titleStringID) {
            this.Title = strings[headerArgs.titleStringID];
        } else{
            if (headerArgs.titleStringValue) {
                this.Title = headerArgs.titleStringValue;
            }
        }
    };

    return HeaderViewModel;
})