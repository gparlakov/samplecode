﻿define([   
    'jquery',
    'knockout',
    'services',
    'Mobile/ViewModels/Repository/SubscriptionsActionButtons',
    'servicesObjects',
    'underscore'
], function ($, ko, services, subscriptionsActionButtons, servicesObjects, _) {
    function OnlineNotificationsListViewModel() {
        var self = this;

        this.Subscriptions = ko.observableArray();
        this.Correspondent = null;

        this.Init = function () {


            services.OnlineNotificationsService.RetreiveBankCorrespondentForAP(function (data) {
                self.Correspondent = data.BankCorrespondent;

                services.OnlineNotificationsService.RetrieveSubscriptions(data.BankCorrespondent, function (data) {
                    _.map(data.Subscriptions, function (subscription) {
                        
                        var subscr = {};
                        _.extend(subscr, subscription);

                        subscr.IsActive = ko
                            .observable(subscription.Status == servicesObjects.EnumNotificationCorrespondentSubscriptionStatus.Active);
                        subscr.Status = ko.observable(subscription.Status);                        

                        subscr.actionButtons = [];
                        subscr.actionButtons.push(
                            new subscriptionsActionButtons.ActivateSubscription(subscr));

                        subscr.actionButtons.push(
                             new subscriptionsActionButtons.DeactivateSubscription(subscr));                        

                        self.Subscriptions.push(subscr);
                    })
                })
            });
        };

        this.Init();

        this.click = function (event) {
            //event.stopPropagation();
        }

        
    }

    return OnlineNotificationsListViewModel;
});