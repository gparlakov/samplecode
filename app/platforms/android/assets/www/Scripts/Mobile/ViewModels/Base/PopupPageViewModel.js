﻿define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'services',
  'validator',
  'productscache',
  'session',
  'uimodels',
  'events',
  'utils',
], function ($, _, Backbone, ko, services, validator, productscache, session, uimodels, events, utils) {


    function PopupPageViewModel(
            parentViewModel
        ) {
        var self = this;

        this.path = "";
        this.parent = parentViewModel;
        this.element = null;

        this.loadData = function () { };
        this["return"] = function (data) {
            self.currentDeferred.resolve(data);
            self._close();
        }
        //return метода трябва да се махне защото 2.3 се чупи заради него
        this._return = this["return"];

        this.cancel = function () {
            self.currentDeferred.reject();
            self._close();
        }

        this.currentDeferred = null;
        //връща директно резултата като промис
        this.Open = function (foreceRefresh) {

            self.currentDeferred = $.Deferred(function () {

                $.whenEx(
                    self.loadData()
                )
                .done(function () {
                    //self.element.daispopup("open");

                    //self.element.closest(".page").children()
                    self.openScrollPosition = $("body").scrollTop();
                    self.opend = true;
                    self.elements.page.append(self.elements.popup);

                    self.elements.page.children(":visible").data("wasVisible", true).hide();
                    self.elements.popup.show();

                    //self._oldScrollHandlers = $.data(window, "events").scroll;
                    //$.data(window, "events").scroll = [];
                    $(utils.getCurrentPageContainer()).daisScrollV2("pause");

                    //$("body").scrollTop(0)

                    if (foreceRefresh) {
                        self.refresh();
                    }
                });
            });

            return self.currentDeferred;
        }

        this.Close = function () {
            this.cancel();
        }

        self._opened = function () {
            //return self.element.daispopup("opened");
            return self.opend;
        }
        self._close = function () {
            //self.element.daispopup("close");
            self.elements.container.append(self.elements.popup);
            self.elements.popup.hide();

            self.elements.page.children(":data(wasVisible)").removeData("wasVisible").show();

            $("body").scrollTop(self.openScrollPosition);
            self.opend = false;

            $(utils.getCurrentPageContainer()).daisScrollV2("resume");

            //$.data(window, "events").scroll = self._oldScrollHandlers;
            //self._oldScrollHandlers = [];
        }

        this._backButtonClose = function () {
            if (self._opened()) {
                self._close();
                return true;
            }
            else {
                return false;
            }
        }
        this.rendered = function () {
            events.handlers.add(utils.getCurrentPageViewModel(), "handlebackbutton", self._backButtonClose);
        }


        this.unload = function () {
            events.handlers.remove(utils.getCurrentPageViewModel(), "handlebackbutton", self._backButtonClose);
        }

        events.one(this.parent, 'rendered', function (data) {
            var page = utils.getCurrentPageContainer();

            self.elements = {};
            self.elements.page = page;
            self.elements.container = $(data.element);

            self.elements.popup = $('<div class="daispopup">popup</div>').hide();
            //self.elements.popup.css("margin-top", "-" + self.elements.page.css("padding-top"));
            //self.elements.popup.css("margin-bottom", "-" + self.elements.page.css("padding-bottom"));

            self.elements.container.append(self.elements.popup);

            self.elements.popup.attr("data-bind", "partial: { path: '{0}' }".format(self.path));
            ko.applyBindings(self, self.elements.popup[0]);

            //self.element = $('<div class="daispopup" style="overflow: scroll; -webkit-overflow-scrolling: touch;">POPUP</div>');
            //$(data.element).append(self.element);
            //self.element.attr("data-bind", "partial: { path: '" + self.path + "' }");
            //ko.applyBindings(self, self.element[0]);
            //self.element.daispopup({ history: false });
        });

        this.refresh = function () {
            //self.elements.popup[0].trigger("create");;
            self.elements.popup.trigger("create");;
        }
    };

    return PopupPageViewModel;
});