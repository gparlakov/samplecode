﻿define([
  'jquery',
  'underscore',
  'knockout',
  'komapping',
  'services',
  'validator',

  'Mobile/ViewModels/Payments/PaymentDocsDetails/DetailsBaseViewModel'
], function ($, _, ko, komapping, services, validator, baseViewModel) {

    function DocCP(original) {

        _.extend(this, komapping.fromJS(original));    
    }    

    function DocCPDetailsViewModel(model, wizardOutputModel) {

        validator.validatableModel.call(this, function () {
            return {
                properties: {
                    "Details": {
                        properties: {
                            "Details1": {
                                rules: {
                                    NotNullOrEmpty: true,
                                    MaxLength: 35,
                                    BISERASymbols: true
                                }
                            },
                            "Details2": {
                                rules: {
                                    MaxLength: 35,
                                    BISERASymbols: true
                                }
                            },
                            "DirtyMoney": {
                                rules: {
                                    NotNullOrEmpty: function () { return self.EspeciallyNeedsDirtyMoneyDeclaration; }
                                }
                            }
                        }
                    }
                }
            }
        });

        var self = this;
        
        baseViewModel.call(this, model, wizardOutputModel);

        this.initialize = function () {
            var result = self.initializeBase();
            return result;
        }

        this.Details = new DocCP(model);
        
        //this.AdditionalViewData = {};
        this.AdditionalViewData.isRings = ko.computed({
            read: function () {
                return this.Details.RINGS();

            },
            write: function (value) {
                this.Details.RINGS(value);
            },
            owner: this
        });
        
    
    };

    return DocCPDetailsViewModel;
});