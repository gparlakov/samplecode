define([
  'i18n!Mobile/nls/strings'

], function (strings) {

    var CCYRatesSubmenu = function (params) {

        return {
            Items: [
                {
                    Label: strings.ID_STR_CURRENCY_RATES,
                    Controller: 'CCYRates',
                    Action: 'ccyRates',
                    //IsActive: false,
                    liCss: 'ccy-rates',
                    Params: params
                }, {
                    Label: strings.ID_STR_CURRENCY_CONVERTER,
                    Controller: 'CCYRates',
                    Action: 'ccyConverter',
                    //IsActive: false,
                    liCss: 'ccy-converter',
                    Params: params
                }
            ]
        };
    }
    return CCYRatesSubmenu;
});