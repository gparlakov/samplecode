define([
  'Mobile/Models/Submenu/SubmenuItem',
  'Mobile/Models/Submenu/Submenu',
  'i18n!Mobile/nls/strings'

], function (submenuItem, submenu, strings) {

    var PaymentsSubmenu = function (params) {

        //return {
        //    Items: [
        //        new submenuItem(strings.ID_STR_PENDING_PAYMENT, 'ViewPayments', 'viewPendingPayments', params),

        //        new submenuItem(strings.ID_STR_PAYMENT_ARCHIVE, 'ViewPayments', 'viewArchivePayments', params)
        //    ]
        //};
        return {
            Items: [
                {
                    Label: strings.ID_STR_PENDING,
                    Controller: 'ViewPayments',
                    Action: 'viewPendingPayments',
                    //IsActive: false,
                    liCss: 'pmt-pending',
                    Params: params
                }, {
                    Label: strings.ID_STR_ARCHIVE,
                    Controller: 'ViewPayments',
                    Action: 'viewArchivePayments',
                    //IsActive: false,
                    liCss: 'pmt-archive',
                    Params: params
                }
            ]
        };
    }
    return PaymentsSubmenu;
});