define([
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'services',
  'productscache',
  'Mobile/ViewModels/Common/HeaderViewModel',
  'i18n!Mobile/nls/strings'
], function (_, bakcbone, ko, komapping, services, productscache, headerViewModel, strings) {

	var DepositDetailsViewModel = function (model) {
		var viewModel = model; 
		viewModel.HeaderViewModel = new headerViewModel({ titleStringID: "ID_STR_DEPOSIT_DETAILS" });
		//var viewModel = komapping.fromJS(intModel);
			
		viewModel.ProductDetails = {
		    heading: { label: strings.ID_STR_DEPOSIT_DETAILS },
		    details: []
		};
		viewModel.ProductDetails.details.push({ label: strings.ID_STR_TYPE, value: $.GetLocalizedString(viewModel.BankTimeDeposit.DepositType.FunctionalType.Description) });
		viewModel.ProductDetails.details.push({ label: strings.ID_STR_DEPOSIT_ID, value: viewModel.BankTimeDeposit.BankDepositID });
		viewModel.ProductDetails.details.push({ label: strings.ID_STR_CCY, value: viewModel.BankTimeDeposit.BankDepositCCY });
		viewModel.ProductDetails.details.push({ label: strings.ID_STR_DEPOSIT_OPENED_AT, value: $.format.date(viewModel.BankTimeDeposit.BankDepositDealDate) });
		if (!viewModel.BankTimeDeposit.ExtendedProperties.IsAccountDeposit)
		    viewModel.ProductDetails.details.push({ label: strings.ID_STR_DEPOSTI_DUE_DATA, value: $.format.date(viewModel.BankTimeDeposit.FinalMaturityDate) });
		viewModel.ProductDetails.details.push({ label: strings.ID_STR_DEPOSIT_INTERESET_RATE, value: $.format.number(viewModel.BankTimeDeposit.InterestRate) });


		viewModel.DepositBalance = {
		    heading: { label: strings.ID_STR_TOTAL_AMOUNTS },
		    details: []
		};
		viewModel.DepositBalance.details.push({ label: strings.ID_STR_AMOUNT, value: $.format.number(viewModel.BankTimeDeposit.Principal) });
		viewModel.DepositBalance.details.push({ label: strings.ID_STR_DEPOSIT_ACCRUED_INTEREST, value: $.format.number(viewModel.BankTimeDeposit.InterestAccrual) });
		viewModel.DepositBalance.details.push({ label: strings.ID_STR_DEPOSIT_INTEREST_AT_MATURITY, value: $.format.number(viewModel.BankTimeDeposit.Interest) });
		
		return viewModel;
	};

	return DepositDetailsViewModel;
});