define([
  'underscore',
  'knockout',
  'komapping',
  'services',
  'utils',
  'backbone',
  'productscache',
  'i18n!Mobile/nls/strings',
  'Mobile/ViewModels/Common/HeaderViewModel',
  'typeVisitor'
], function (_, ko, komapping, services, utils, backbone, productscache, strings, headerViewModel, typeVisitor) {


    var AccountOverviewViewModel = function (cardKey) {

        var self = this;

        this.CardKey = cardKey;
        
        var cards = productscache.GetAPerson().APCards;

        this.APBankCard = _.find(cards, function (card) { return card.BankCard.BankCardID == cardKey.BankCardID });
        this.BankCard = this.APBankCard.BankCard;
        productscache.SetBankAccountToBankCard(this.BankCard);
        this.HeaderViewModel = new headerViewModel({ titleStringID: "ID_STR_CARD_DETAILS" });

        this.Submenu = {
            submenuItems: {
                Items: [
                    {
                        Label: strings.ID_STR_DETAILS,
                        Controller: 'CardDetails',
                        Action: typeVisitor.visit(self.BankCard, {
                                    BankCardCredit: 'BankCardCredit',
                                    BankCardRaiCard: 'BankCardRaiCard',
                                    BankCardDebit: 'BankCardDebit',
                                    Other: 'index'
                                }),
                        Params: [self.BankCard.BankCardID],
                        liCss: 'card-details'
                    }
                ]
            }
        }

        var  additionalTabForMovements ={
            Label: strings.ID_STR_ACCOUNT_MOVEMENTS,
            Controller: 'CardDetails',
            Action: 'BankCardMovements',
            Params: [self.BankCard.BankCardID],
            liCss: 'card-movements'
        }

        typeVisitor.visit(self.BankCard, {
            BankCardCredit: function() {} ,
            BankCardRaiCard: function () { self.Submenu.submenuItems.Items.push(additionalTabForMovements); },
            BankCardDebit: function () { self.Submenu.submenuItems.Items.push(additionalTabForMovements); },
            Other: 'index'
            
        })

    };

    return AccountOverviewViewModel;
});