﻿define([
  'jquery',
  'underscore',
  'knockout',
  'komapping',
  'services',
  'productscache',
  'validator',
  'Mobile/ViewModels/Payments/PaymentDocsDetails/DetailsBaseViewModel'

], function ($, _, ko, komapping, services, productscache, validator, baseViewModel) {

    function DocFX(original) {

        _.extend(this, komapping.fromJS(original, this));

        //this.ThereIsTicket = this.ThereIsTicket.extend({ boolean: true });
    }

    function DocFXDetailsViewModel(model, wizardOutputModel) {
        validator.validatableModel.call(this, function () {
            return {
                properties: {
                    "Details": {
                        properties: {
                            "DirtyMoney": {
                                rules: {
                                    NotNullOrEmpty: function () { return self.EspeciallyNeedsDirtyMoneyDeclaration; }
                                }
                            },
                            "Reference": {
                                rules: {
                                    NotNullOrEmpty: self.Details.ThereIsTicket,
                                    RegEx:'^[a-zA-Z0-9\"" ()-\\/,.]{0,35}$'
                                }
                            },
                            "FinalRate": {
                                rules: {
                                    NotNullOrEmpty: self.Details.ThereIsTicket,
                                    RegEx: '^[0-9]{1,5}(([.|,][0-9]{1,6}){0,1}|.{0})$',
                                    DeciamlMinValue: { value: 0, inclusive: false }
                                },
                                messages: {
                                    RegEx: "TMPL_VAL_SPC_RATIO"
                                }
                            }
                        }
                    }
                }
            }
        });

        var self = this;
        
        baseViewModel.call(this, model, wizardOutputModel);
        
        this.initialize = function () {
            var result = self.initializeBase();
            return result;
        }
        
        this.Details = new DocFX(model);
        
        var payerApa = productscache.GetAPAccountByID(wizardOutputModel.State.Payer.BankAccount.BankAccountID);
        var payeeApa = productscache.GetAPAccountByID(wizardOutputModel.State.Payee.BankAccount.BankAccountID);

        var payerCCY = payerApa.BankAccount.CCY;
        var payeeCCY = payeeApa.BankAccount.CCY;

        this.AdditionalViewData.RateTypeOptions = [];
        
        // za sega sas zabita tuk konstanta . Da se napravi da se vzima ot server-a 
        var homeCCY = {SWIFTCode: 'BGN'};
        

        if (payeeCCY.SWIFTCode != homeCCY.SWIFTCode) {
            this.AdditionalViewData.RateTypeOptions.push({ value: payeeCCY.SWIFTCode + payerCCY.SWIFTCode, text: payeeCCY.SWIFTCode + '/' + payerCCY.SWIFTCode });
        }

        if (payerCCY.SWIFTCode != homeCCY.SWIFTCode) {
            this.AdditionalViewData.RateTypeOptions.push({ value: payerCCY.SWIFTCode + payeeCCY.SWIFTCode, text: payerCCY.SWIFTCode + '/' + payeeCCY.SWIFTCode });
        }

        this.AdditionalViewData.SelectedRateType = ko.computed({
            read: function () {
                if (self.Details.RateType()) {
                    return _.find(self.AdditionalViewData.RateTypeOptions,
                        function (item) {
                            return item.value == self.Details.RateType();
                        });
                } else {
                    return null;
                }

            },

            write: function (newValue) {
                self.Details.RateType(newValue.value);
            }

        });

        this.AdditionalViewData.thereIsTicketComp = ko.computed({
            read: function () {
                return this.Details.ThereIsTicket();

            },
            write: function (value) {
                var resValue = (value);
                if (resValue) {
                    //clear all. Ne iskame da im se puska validaciqta 
                    this.Details.FinalRate(null);
                    this.Details.Reference(null);
                    
                }
                this.Details.ThereIsTicket(value);

            },
            owner: this
        });
    };

    return DocFXDetailsViewModel;
});