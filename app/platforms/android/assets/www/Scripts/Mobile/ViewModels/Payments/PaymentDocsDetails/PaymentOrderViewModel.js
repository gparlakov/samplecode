﻿define([
  'jquery',
  'underscore',
  'knockout',
  'komapping',
  'services',
  'validator',
  'dateUtils'
], function ($, _, ko, komapping, services, validator, dateUtils) {

    function PaymentOrderViewModel(original) {
        var self = this;

        validator.validatableModel.call(this, function () {
            return {
                properties: {
                    "ExecuteDate": {
                        rules: {
                            NotNullOrEmpty: true,
                            MinDate: { value: dateUtils.today(), inclusive: true },
                            MaxDate: { value: dateUtils.addDays(30, dateUtils.today()), inclusive: false  } // v online ne e inclusive 
                        }
                    }
                }
            }
        });

        _.extend(this, komapping.fromJS(original));

        this.GetModel = function () {
            return komapping.toJS(self);
        }
    }    

    

    return PaymentOrderViewModel;
});