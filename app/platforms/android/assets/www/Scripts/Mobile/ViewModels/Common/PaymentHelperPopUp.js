﻿define([
  'jquery',
  'underscore',
  'knockout',
  'services',

   'Mobile/ViewModels/Base/PopupPageViewModel',
   'Mobile/ViewModels/Payments/ViewPaymentViewModel'

], function ($, _, ko, services, PopupPageViewModel, ViewPaymentViewModel) {

    function PaymentHelperPopUp(parent, options) {

        var self = this;

        //PopupPageViewModel.apply(this, arguments);
        PopupPageViewModel.apply(this, arguments);

        this.path = "Payments/ViewPaymentHelperPopupTemplate";
        
        this._paymentViewModel = new ViewPaymentViewModel(null, options.paymentTypes);

        this.PaymentViewModel = ko.observable();
        //this.Incarnation = ko.observable();
        //this.SelectAccount = function (account) {
        //    self["return"](account);
        //}

        //this.ClearAccount = function () {
        //    //a moje i s null ? 
        //    self["return"]({dummyForOptionAll: null});
        //}

        this.setPaymentKey = function (key) {
            self._paymentViewModel._paymentKey = key;
        }

        this.loadData = function () {
            var result =
                $.whenEx(
                    self._paymentViewModel.retrievePayment()
                    //options.load.call(self)
                )
                .done(function (paymentViewModel) {
                    self.PaymentViewModel(self._paymentViewModel);
                  
                });

            return result;
        };
    }

    return PaymentHelperPopUp;
});