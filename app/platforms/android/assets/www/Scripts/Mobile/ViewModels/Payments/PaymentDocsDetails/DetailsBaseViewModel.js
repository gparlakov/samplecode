﻿/// <reference path="../../../../libs/require.intellisense.js" />
define([
  'jquery',
  'underscore',
  'knockout',
  'komapping',
  'services',
  'validator',
  'Common/ufnomenclatures/UFNmFactory',
  'validator',
   
  'text!Mobile/Templates/Common/DirtyMoneyPicker.html',
  

], function ($, _, ko, komapping, services, validator, ufnmFactory, validator) {

        
    function DetailsBaseViewModel(model, wizardOutputModel) {
       
        var self = this;

        //this.Details = new DocFCCYFull(model);
        this.EspeciallyNeedsDirtyMoneyDeclaration = wizardOutputModel.EspeciallyNeedsDirtyMoneyDeclaration;
       
        this.AdditionalViewData = {};
        var AdditionalViewData = this.AdditionalViewData;

        //this.AdditionalViewData.AdditionalViewData = this.AdditionalViewData;

        this.AdditionalViewData.DirtyMoneyOptions = ko.observable(null);

        this.initializeBase = function () {
       
            if (!this.EspeciallyNeedsDirtyMoneyDeclaration) {
                this.Details.DirtyMoney(null);
            }

            var nmFactory = new ufnmFactory();
             

            var result =
                //$.when(
                //    nmFactory.GetUfNomenclature(nmFactory.ufNomenclatureTypes.NomenclatureCountry)
                //)
                //.done(function (nm) {
                //    var nomenklature = nm.GetNomenclature();
                //    var toShow = _.map(nomenklature, function (item) {
                //        return {
                //            Name: $.GetLocalizedString(item.Name),
                //            Value: item
                //        }
                //    });
                //    self.AdditionalViewData.CountriesOptions(toShow);
                //})
                $.whenEx(
                    nmFactory.GetUfNomenclature(nmFactory.ufNomenclatureTypes.NomenclatureDirtyMoneyReasons)
                ).done(function (nm) {
                        var nomenclature = nm.GetNomenclature();

                        var toShow = _.map(nomenclature, function (item) {
                            return {
                                Name: $.GetLocalizedString(item.Value),
                                Value: $.GetLocalizedString(item.Value)
                            }
                        });

                        self.AdditionalViewData.DirtyMoneyOptions(toShow);
                        var dirtyMoney = ko.utils.unwrapObservable(self.Details.DirtyMoney());
                        if (dirtyMoney) {
                            var selectedOption = _.find(self.AdditionalViewData.DirtyMoneyOptions(), function (opt) {
                                return opt.Value == dirtyMoney;
                            });
                            // na nqkoi mesta  (v obrazcite naprimer) ima zapisani stoinosti na deklaciqta, koito ne sa vspisaka. 
                            if (selectedOption) {
                            self.Details.DirtyMoney(selectedOption.Value);
                            }

                        }

                    });

            return result;
        }
        
        this.GetModel = function () {
            return komapping.toJS(this.Details);
        }
    };

    return DetailsBaseViewModel;
});