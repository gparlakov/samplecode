﻿/// <reference path="../../../../libs/require.intellisense.js" />
define([
  'jquery',
  'underscore',
  'knockout',
  'komapping',
  'services',
  'servicesObjects',
  'productscache',
  'Common/ufnomenclatures/UFNmFactory',

], function ($, _, ko, komapping, services, objects,productscache, ufnmFactory) {


    function OpCodesViewModel(model, wizardOutputModel) {

        var self = this;

        if (!this.AdditionalViewData) {
            this.AdditionalViewData = {};
        }

        this.AdditionalViewData.OpModesOptions = ko.observable(null);

        this.AdditionalViewData.SelectedOpMode = ko.observable(null);

        var bankAccountKey = wizardOutputModel.State.Payer.BankAccount.BankAccountID;
        var bankAccount = productscache.GetAPAccountByID(bankAccountKey).BankAccount;
        var segment = bankAccount.BankClient.BUSINESSSEGMENT;
        //this.AdditionalViewData.CreditNumber = ko.observable(null);

        this.AdditionalViewData.CreditNumberVisible = ko.pureComputed(function () {
            return segment == objects.EnumBankClientSegment.Corporate;
        }, this);

        this.AdditionalViewData.CreditNumberRequiered = ko.pureComputed(function () {
            if (this.AdditionalViewData.SelectedOpCode()) {

                var isCreditNumberRequired = this.AdditionalViewData.SelectedOpCode().IsBNBCodeRequired;
                return this.AdditionalViewData.CreditNumberVisible() && isCreditNumberRequired;
            } else {

                return false;
            }
        }, this);

        this.AdditionalViewData._selectedOpCode = ko.observable(null);
        this.AdditionalViewData.SelectedOpCode = ko.pureComputed({
            read: function () {
                return this.AdditionalViewData._selectedOpCode();
            },
            write: function (value) {
                if (value) {
                    this.AdditionalViewData._selectedOpCode(value);
                    this.Details.OpCode(new services.Objects.OpCodeKey(value));
                }
                else {
                    this.Details.OpCode(null);
                }
            },
            owner: this
        });

        this.AdditionalViewData.OpCodesOptions = ko.pureComputed(function () {
            var selecterOpMode = this.AdditionalViewData.SelectedOpMode()
            if (selecterOpMode) {
                return selecterOpMode.OpCodes;
            }
        }, this);



        this.initializeOpcodes = function () {

            

            var nmFactory = new ufnmFactory();


            var result =

                $.whenEx(
                    nmFactory.GetUfNomenclature(nmFactory.ufNomenclatureTypes.NomenclatureOpModes)
                ).done(function (nm) {
                    var nomenclature = nm.GetNomenclature();

                    var toShow = _.each(nomenclature, function (item) {

                        item.Opcodes = _.each(item.OpCodes, function (opcode) {
                            opcode.CodeName = $.GetLocalizedString(opcode.Name);

                        });

                        item.Name = $.GetLocalizedString(item.ModeName);
                    });

                    self.AdditionalViewData.OpModesOptions(toShow);

                    var opCode = ko.utils.unwrapObservable(self.Details.OpCode);

                    if (opCode) {

                        var selectedCodeAndMode = null;
                        _.each(toShow, function (opModeCurrent) {
                            var selectCodeIntenal = _.find(opModeCurrent.OpCodes, function (opCodeCurrent) {
                                return opCodeCurrent.Code == opCode.Code;
                            });

                            if (selectCodeIntenal) {
                                selectedCodeAndMode = { opMode: opModeCurrent, opCode: selectCodeIntenal };
                            }
                        });

                        if (selectedCodeAndMode) {

                            self.AdditionalViewData.SelectedOpMode(selectedCodeAndMode.opMode);
                            self.AdditionalViewData.SelectedOpCode(selectedCodeAndMode.opCode);
                        }
                    }
                });
            return result;
        }
    };

    return OpCodesViewModel;
});