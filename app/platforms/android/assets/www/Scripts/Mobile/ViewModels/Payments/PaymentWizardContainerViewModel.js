﻿define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'services',
  'typeVisitor',
  'events',
  'Mobile/ViewModels/Common/HeaderViewModel',
  'Mobile/ViewModels/Payments/PaymentWizardChoosePayerAmountViewModel',
  'Mobile/ViewModels/Payments/PaymentWizardChoosePayeeViewModel',
  'Mobile/ViewModels/Payments/PaymentWizardFillDetailsViewModel',
  'Mobile/ViewModels/Payments/PaymentWizardSummaryViewModel'

], function (
        $, _, backbone, ko, komapping, services, typeVisitor, events, headerViewModel,

        PaymentWizardChoosePayerAmountViewModel, PaymentWizardChoosePayeeViewModel, PaymentWizardFillDetailsViewModel, PaymentWizardSummaryViewModel
    ) {

    
    var WizardRequestObjectsMappings = {
        PaymentWizardChoosePayerAndAmountOutput: function (wizardOutputModel) {
            return {
                viewModel: new PaymentWizardChoosePayerAmountViewModel(wizardOutputModel)
            };
        },

        PaymentWizardChoosePayeeOutput: function (wizardOutputModel) {
            return {

                viewModel: new PaymentWizardChoosePayeeViewModel(wizardOutputModel)
            };
        },

        PaymentWizardFillDetailsOutput: function (wizardOutputModel) {
            return {
                viewModel: new PaymentWizardFillDetailsViewModel(wizardOutputModel)
            };

        },

        PaymentWizardSummaryOutput: function (wizardOutputModel) {
            return {
                viewModel: new PaymentWizardSummaryViewModel(wizardOutputModel)
            }
        }
    };

    var PaymentWizardContainerViewModel = function PaymentWizardContainerViewModel(firstStepOutputData) {

        var self = this;
        self.HeaderViewModel = new headerViewModel({ titleStringID: "ID_STR_CREATE_NEW_PAYMENT" });
        

        this.StepViewModel = ko.observable(null);

        //this.Init = function (callback)
        //{
        //    if (firstStepOutputData) {
        //        self._setSubModel(firstStepOutputData, function () { callback.call(self, self); });
        //    }
        //    else {
        //        callback.call(self, self);
        //    }
        //}

        this.initialize = function () {
            var res = 
                services.PaymentWizardService.PaymentWizardInit(firstStepOutputData)
                .pipe(function(paymentWizardInitresponse) {
                    return self.SetStepOutputModel(paymentWizardInitresponse.PaymentWizardStepOutput);
                });

            return res;
        }

        //modela na stapkata, callback kogato vsichko e gotovo
        this.SetStepOutputModel = function(firstStepOutputData)
        {
            return self._setSubModel(firstStepOutputData);
        }

        this.Next = function () {
            var requestModel = self.StepViewModel().GetRequestModelForNext();

            services.PaymentWizardService.PaymentWizardNext(requestModel, function (data) {
                self._setSubModel(data.PaymentWizardStepOutput);
            });
        }

        this.Back = function () {
            var requestModel = self.StepViewModel().GetRequestModelForBack();

            services.PaymentWizardService.PaymentWizardBack(requestModel, function (data) {
                self._setSubModel(data.PaymentWizardStepOutput);
            });
        }

        this.handlebackbutton = function () {
            var viewModel = this.StepViewModel();
            if (viewModel && viewModel.MoveBack)
            {
                viewModel.MoveBack.call(viewModel, viewModel);
            }
        }

        this._setSubModel = function (stepOutput, callback) {
            var viewModelAndTemplate = typeVisitor.visit(stepOutput, WizardRequestObjectsMappings);

            var viewModel = viewModelAndTemplate.viewModel;

            viewModel.Init(function () {
                    self.StepViewModel(viewModel);

                    events.bind(viewModel, "next", self.Next);
                    events.bind(viewModel, "back", self.Back);
                });
        }

    };

    return PaymentWizardContainerViewModel;
});
