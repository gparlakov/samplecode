﻿define([
    "backbone",
    "utils"
], function (backbone, utils) {
    var Submenu = function (id, submenuitems) {
        var model = this;

        model.Items = submenuitems;
        model.ID = id;

        model.ClickHandler = function (data) {
            var paramsAsQueryString = "";

            for (idx in data.Params) {
                paramsAsQueryString += "/" + data.Params[idx];
            }

            utils.navigate(data.Controller + "/" + data.Action + paramsAsQueryString, { trigger: true });
            //alert(data.Controller + "/" + data.Action + paramsAsQueryString);
        }


        model.AlertL = function (val) {
            alert(JSON.stringify(val));
        }

        return model;
    }
    return Submenu;
});