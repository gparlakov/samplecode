﻿define([
  'jquery',
  'underscore',
  'i18n!Mobile/nls/strings',
  'servicesObjects',
  'uimodels',
  'jsrender'
], function ($, _, strings, servicesObjects, uimodels, jsrender) {

  
    var PaymentOrderAllowedOperationReasonToMessageFormater = function () {

    }

    PaymentOrderAllowedOperationReasonToMessageFormater.prototype.FormatAsUINotificationContainer = function (operationReason) {
        var self = this;

        if (!$.isArray(operationReason))
            operationReason = [operationReason];

        
        var result = new uimodels.Notifications.UINotificationContainer();

        operationReason.forEach(function (opR) {

            result.AddNotification(self.FormatAsUINotification(opR));

        });

        return result;
    }


    PaymentOrderAllowedOperationReasonToMessageFormater.prototype.FormatAsUINotification = function (opR) {
        
        var message = this.FormatAsText(opR)

        var res = new uimodels.Notifications.UINotification({
            Type: uimodels.Notifications.UINotificationType.Error,
            Message: message
        });

        return res;
    }

    PaymentOrderAllowedOperationReasonToMessageFormater.prototype.FormatAsUINotificationCollection = function (opRs) {

        var self = this;
        return $.map(opRs, function (opR) { return self.FormatAsUINotification(opR); });
    }

    PaymentOrderAllowedOperationReasonToMessageFormater.prototype.FormatAsText = function (opR) {
        var resString = "ID_TMPL_PAYMENTORDERALLOWEDOPERATIONREASON[" + servicesObjects.Utils.GetShortTypeNameByObject(opR) + "]";
        var message = "ERROR";
        if (strings[resString]) {
            var compiledTemplates = jsrender(strings[resString]);
            var text = compiledTemplates.render(opR)
            message = text;
        }

        return message;
    }


    PaymentOrderAllowedOperationReasonToMessageFormater.prototype.FormatAsTextCollection = function (opRs) {
        var self = this;
        return $.map(opRs, function (item) { return self.FormatAsText(item); });
    }


    return PaymentOrderAllowedOperationReasonToMessageFormater;
});