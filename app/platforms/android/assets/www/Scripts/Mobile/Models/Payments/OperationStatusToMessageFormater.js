﻿define([
  'underscore',
  'i18n!Mobile/nls/strings',
  'servicesObjects',
  'uimodels',
  'jsrender'
], function (_, strings, servicesObjects, uimodels, jsrender) {

    var messages = {
        'PaymentOperationStatusSend': {
            'PaymentOrder': {
                'Success': strings.ID_TMPL_PAYMENTOPERATIONSTATUSSEND_PAYMENTORDER_SUCCESS,
                'Error': strings.ID_TMPL_PAYMENTOPERATIONSTATUSSEND_PAYMENTORDER_ERROR
            }
        },
        'PaymentOperationStatusSign': {
            'PaymentOrder': {
                'Success': strings.ID_TMPL_PAYMENTOPERATIONSTATUSSIGN_PAYMENTORDER_SUCCESS,
                'Error': strings.ID_TMPL_PAYMENTOPERATIONSTATUSSIGN_PAYMENTORDER_ERROR
            }
        }
        ,
        'PaymentOperationStatusSave': {
            'PaymentOrder': {
                'Success': strings.ID_TMPL_PAYMENTOPERATIONSTATUSSAVE_PAYMENTORDER_SUCCESS,
                'Error': strings.ID_TMPL_PAYMENTOPERATIONSTATUSSAVE_PAYMENTORDER_ERROR
            }
        },
        'PaymentOperationStatusDelete': {
            'PaymentOrder': {
                'Success': strings.ID_TMPL_PAYMENTOPERATIONSTATUSDELETE_PAYMENTORDER_SUCCESS,
                'Error': strings.ID_TMPL_PAYMENTOPERATIONSTATUSDELETE_PAYMENTORDER_ERROR
            }
        }

    }

    var incarnationMapping = {
        '1': 'PaymentOrder'
    }

    var statusMapping = {}
    statusMapping[true] = 'Success';
    statusMapping[false] = 'Error';

    var PaymentOperationStatusFormatter = function () {
        this.FormatOperationsStatus = function (operationStatus) {
            var type = servicesObjects.Utils.GetShortTypeNameByObject(operationStatus);
            var incarnation = incarnationMapping[operationStatus.PaymentIncarnation.PaymentIncarnationType]
            var status = statusMapping[operationStatus.Success];

            var template = messages[type][incarnation][status];
            var compiledTemplates = jsrender(template);
            var text = compiledTemplates.render(operationStatus)
            
            var notificationmessage = text;
            var notificationtype = operationStatus.Success ? uimodels.Notifications.UINotificationType.Success : uimodels.Notifications.UINotificationType.Error;

            return new uimodels.Notifications.UINotification({ Type: notificationtype, Message: notificationmessage });
        }
    }

    return PaymentOperationStatusFormatter;
});