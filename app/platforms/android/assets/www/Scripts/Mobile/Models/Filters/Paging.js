﻿define(
 function () {

     var PagingFilter = function () {

         var filter = {

            CurrentPage: 1,
            ResultsForPage: 10,

            clearPaging: function () {
                CurrentPage = 1;
                ResultsForPage = 10;
            }
        }
         return filter;
     }

     return PagingFilter;
 });