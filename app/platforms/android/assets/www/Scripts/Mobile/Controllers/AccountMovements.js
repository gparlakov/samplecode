define([
  'jquery',
  'Mobile/ViewModels/Account/MovementsViewModel',
  'text!Mobile/Templates/Accounts/Movements/MovementsTemplate.html',
  'services',
  'utils',
  'mvc',
  'knockoutDais',
  'i18n!Mobile/nls/strings',

  'Mobile/ViewModels/Account/MovementsBlocksViewModel',
  'text!Mobile/Templates/Accounts/MovementsBlocksTemplate.html',

  'Mobile/ViewModels/Account/MovementDocViewModel',
  'text!Mobile/Templates/Accounts/Movements/MovementDocumentTemplate.html'

], function ($, MovementsViewModel, template,
             services, utils, mvc, knockoutDais, strings,
             blocUnblockViewModel, blockUnblockTemplate,
             movementDocViewModel, movementDocTemplate) {

    return mvc.Controller.extend({
        routes: {
            "AccountMovements/index/:accountID": "index",
            "AccountMovements/blocks/:accountID": "blocks",
            "AccountMovements/showMovementDocument/:documentRefence/:archiveSuffix/:bankAccountID": "showMovementDocument"
        },

        index: function (accountID) {

            var filter = new services.Objects.AccountMovementsFilterForPeriod({ BankAccount: { BankAccountID: accountID } });
            var movementsViewModel = new MovementsViewModel(filter);
            movementsViewModel.strings = strings;

            var movmentsView = new mvc.ViewResult(template, function (dataAvailableHandler) {
                dataAvailableHandler(movementsViewModel);
            }
            , function (el, data) {
                //data.AfterRender();
            });

            return movmentsView;
        },

        blocks: function (accountID) {
            var bankAccountKey = new services.Objects.BankAccountKey({ BankAccountID: accountID });

            var ViewModelType = require('Mobile/ViewModels/Account/MovementsBlocksViewModel');
            var Template = require('text!Mobile/Templates/Accounts/MovementsBlocksTemplate.html');

            var viewModel = new ViewModelType(bankAccountKey);

            return new mvc.ViewResult(
                    Template,
                    function (dataAvailableHandler) {
                        dataAvailableHandler(viewModel);
                    }
                );
        },

        showMovementDocument: function (documentRefence, archiveSuffix, bankAccountID) {
            
            var bankAccountKey = new services.Objects.BankAccountKey({ BankAccountID: bankAccountID });

            var viewResult = new mvc.ViewResult(movementDocTemplate, function (dataAvailableHandler) {

                        var viewModel = new movementDocViewModel(documentRefence, archiveSuffix, bankAccountKey);
                        dataAvailableHandler(viewModel);
                }
                , function (el, data) {

                });
            
            return viewResult;
        }
    });

});
