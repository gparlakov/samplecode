define([
  'Mobile/ViewModels/Cards/CardDetailsViewModel',
  'text!Mobile/Templates/Cards/CardDetailsCommonTemplate.html',
  'text!Mobile/Templates/Cards/DebitCardDetailsTemplate.html',
  'text!Mobile/Templates/Cards/CreditCardDetailsTemplate.html',
  'text!Mobile/Templates/Cards/RaiCardDetailsTemplate.html',

  'services',
  'productscache',
  'utils',
  'mvc',
  'underscore',
  'jquery',
  
    "Mobile/ViewModels/Cards/MovementsViewModel",
    "text!Mobile/Templates/Cards/MovementsTemplate.html",

    "Mobile/ViewModels/Cards/CardOverviewViewModel",
    "text!Mobile/Templates/Cards/CardOverviewTemplate.html"


], function (CardDetailsViewModel, cardPartialTemplate, debitCardTemplate, creditCardTemplate, raiCardTemplate, services, productscache, utils, mvc, _, $) {

    return mvc.Controller.extend({
        routes: {
            "CardDetails/overview/:cardID": "overview",
            "CardDetails/:cardID": "index",
            "CardDetails/BankCardDebit/:cardID": "BankCardDebit",
            "CardDetails/BankCardCredit/:cardID": "BankCardCredit",
            "CardDetails/BankCardRaiCard/:cardID": "BankCardRaiCard",
            "CardDetails/Movements/:cardID": "BankCardMovements"
        },

        overview: function (cardID) {
            var ViewModelType = require("Mobile/ViewModels/Cards/CardOverviewViewModel");
            var Template = require("text!Mobile/Templates/Cards/CardOverviewTemplate.html");

            var bankCardKey = new services.Objects.BankCardKey({ BankCardID: cardID });

            var viewModel = new ViewModelType(bankCardKey);

            return new mvc.ViewResult(
                Template,
                function (dataAvailableHandler) {
                    dataAvailableHandler(viewModel);
                }
            );
        },

        index: function (cardID) {

            //var ap = productscache.GetAPerson();
            var cards = productscache.GetAPerson().APCards;

            var apCard = _.find(cards, function (card) { return card.BankCard.BankCardID == cardID });

            var cardViewModel = new CardDetailsViewModel(apCard);
            //cardViewModel.strings = strings;

            var cardView = new mvc.ViewResult(template, function (dataAvailableHandler) {
                dataAvailableHandler(cardViewModel);
            });
            return cardView;
            //return depositViewModel;
        },

        _showCardCommon: function (cardID, cardTemplate) {

            //var ap = productscache.GetAPerson();
            var cards = productscache.GetAPerson().APCards;

            var apCard = $.extend(true, {}, _.find(cards, function (card) { return card.BankCard.BankCardID == cardID }));

            var cardViewModel = new CardDetailsViewModel(apCard);
            
            var cardView = new mvc.ViewResult(cardTemplate, function (dataAvailableHandler) {
                dataAvailableHandler(cardViewModel);
            });
            return cardView;
            //return depositViewModel;
        },

        BankCardDebit: function (cardID) {

            return this._showCardCommon(cardID, debitCardTemplate);
        },
        BankCardCredit: function (cardID) {

            return this._showCardCommon(cardID, creditCardTemplate);
        },
        BankCardRaiCard: function (cardID) {

            return this._showCardCommon(cardID, raiCardTemplate);
        },

        BankCardMovements: function (cardID)
        {
            var ViewModelType = require("Mobile/ViewModels/Cards/MovementsViewModel");
            var Template = require("text!Mobile/Templates/Cards/MovementsTemplate.html");

            var bankCardKey = new services.Objects.BankCardKey({ BankCardID: cardID });

            var viewModel = new ViewModelType(bankCardKey);

            return new mvc.ViewResult(
                Template,
                function (dataAvailableHandler) {
                    dataAvailableHandler(viewModel);
                }
            );
        }
    });

});
