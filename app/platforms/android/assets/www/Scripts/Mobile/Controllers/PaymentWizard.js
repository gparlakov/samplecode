﻿define([
  'jquery',
  'utils',
  'mvc',
  'services',
  'typeVisitor',

  'Mobile/ViewModels/Payments/PaymentWizardBaseViewModel',

  'Mobile/ViewModels/Payments/PaymentWizardContainerViewModel',
  'text!Mobile/Templates/Payments/Wizard/PaymentWizardContainerTemplate.html'

], function ($, utils, mvc, services, typeVisitor, WizardBaseViewModel) {

    return mvc.Controller.extend({

        routes: {
            "PaymentWizard/index" : "newPaymentOrder",
            "PaymentWizard/newPayment/:incarnationType": "newPayment",
            "PaymentWizard/newPaymentFromBankAccount/:incarnationType/:paymentID": "newPaymentFromBankAccount",
            "PaymentWizard/newPaymentOrder": "newPaymentOrder",

            "PaymentWizard/cereteLikePayment/:incarnationType/:sourceIncarnationType/:paymentID": "cereteLikePayment",
            "PaymentWizard/editPayment/:incarnationType/:bankAccountID": "editPayment"
        },

        _getViewResult: function(initData)
        {
            var viewModel = new (require('Mobile/ViewModels/Payments/PaymentWizardContainerViewModel'))(initData);
            var template = require('text!Mobile/Templates/Payments/Wizard/PaymentWizardContainerTemplate.html');

            var viewResult = new mvc.ViewResult(
                    template,
                    viewModel
                );

            return viewResult;  
        },

        //nov prevod po incarnationType
        newPayment: function(incarnationType) {
            return this.newPaymentFromBankAccount(incarnationType);
        },

        newPaymentFromBankAccount: function (incarnationType, bankAccountID)
        {
            return this._getViewResult(
                    new services.Objects.PaymentWizardInitDataNewPayment({
                        PaymentIncarnation: incarnationType,
                        BankAccount: bankAccountID ? new services.Objects.BankAccountKey({ BankAccountID: bankAccountID }) : null
                    })
                );
        },

        cereteLikePayment: function (incarnationType, sourceIncarnationType, paymentID) {
            return this._getViewResult(
                    new services.Objects.PaymentWizardInitDataCreateLikePayment({
                        PaymentIncarnation: incarnationType,
                        Payment: new services.Objects.PaymentKey({ ID: paymentID, PaymentIncarnationType: sourceIncarnationType })
                    })
                );
        },

        editPayment: function (incarnationType, paymentID) {
            return this._getViewResult(
                    new services.Objects.PaymentWizardInitDataEditPayment({
                        Payment: new services.Objects.PaymentKey({ ID: paymentID, PaymentIncarnationType: incarnationType })
                    })
                );
        },

        //pravi nov PaymentOrder
        newPaymentOrder: function () {
            return this.newPayment(services.Objects.EnumPaymentIncarnationType.PaymentOrder);
        }
    });

});
