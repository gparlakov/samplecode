define([
  'Mobile/ViewModels/Account/BalancesViewModel',
  'text!Mobile/Templates/Accounts/BalancesTemplate.html',
  'services',
  'utils',
  'mvc',
  'jquery'


], function (BalancesViewModel, template, services, utils, mvc, $) {

    return mvc.Controller.extend({
        routes: {
            "AccountOverview/index/:accountID": "index"
        },

        index: function (accountID) {
            var filter =  new services.Objects.BankAccountBalancesFilterCurrent({
                Paging: {
                    CurrentPage: 1,
                    ResultsForPage: 10
                },

                Period: {},

                BankAccount: { BankAccountID: accountID }
            });

            var svc = new services.Services.BankProductInfoService();
            svc.Behaiviours.Remove(new services.Behaviours.Types.HandleServiceError);
            svc.Behaiviours.Add(new services.Behaviours.Types.HandleServiceError(['DAIS.Nightingale.Security.CNightingaleUserHasNoRightsForAccount']));
            //DAIS.Nightingale.Security.CNightingaleUserHasNoRightsForAccount

            var res =
                
                svc.GetProductBalances(filter)
                .pipe(function (data) {
                    if (!data.BalancesFilter) {
                        //metnalo e greshka, naprimer za prava po balansi :( 
                        data.BalancesFilter = filter; //dummy
                    }

                    return new BalancesViewModel(data, this);
                })
                .pipe(function (viewModel) {
                    return new mvc.ViewResult(template, viewModel);
                });

           
            return res;
        },
    });

});
