﻿define([
  'jquery',
  'utils',
  'mvc',
  'services',

  'Mobile/ViewModels/Beneficiary/EditBeneficiary',
  'text!Mobile/Templates/Beneficiary/EditBeneficiary.html',

], function ($, utils, mvc, services, EditBeneficiary, EditBeneficiaryTemplate) {


    return mvc.Controller.extend({
        routes: {
            "Beneficiary/edit": "edit",
        },

        index: function (payments) {
        },

        edit: function(parent, options)
        {
            var self = this;

            var viewModel = new EditBeneficiary(parent, options);

            return new mvc.ViewResult(
                        EditBeneficiaryTemplate,
                        function (dataAvailableHandler) {
                            dataAvailableHandler(viewModel);
                        },
                        function (el, data) {

                        }
                );
        }
    });

});
