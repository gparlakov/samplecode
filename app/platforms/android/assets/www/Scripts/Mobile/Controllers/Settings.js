define([
    'mvc',
    'Common/cache/language',
    'Mobile/ViewModels/Settings/SettingsViewModel',
    'text!Mobile/Templates/Settings/Index.html',
    'text!Mobile/Templates/Settings/Contacts.html'
    
], function (mvc, language,viewModel, template, contactsTemplate) {

    return mvc.Controller.extend({
        routes: {
            "Settings/index": "index",
            "Settings/contacts" : "contacts"
            
        },

        index: function () {
            var settingsDummyViewModel = new viewModel();

            var settingsView = new mvc.ViewResult(template, settingsDummyViewModel);

            return settingsView;
        },

        contacts: function () {
            var settingsDummyViewModel = new viewModel({ skipHideOsHeader: true });

            var settingsView = new mvc.ViewResult(contactsTemplate, settingsDummyViewModel);

            return settingsView;

        }

    });

});
