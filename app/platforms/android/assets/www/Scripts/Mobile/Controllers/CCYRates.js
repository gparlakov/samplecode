﻿define([
  'events',
  'utils',
  'mvc',

  'Mobile/ViewModels/Currency/CCYsInformationViewModel',
  'text!Mobile/Templates/Currency/CCYsInformationTemplate.html',

  'Mobile/ViewModels/Currency/CCYCalculatorViewModel',
  'text!Mobile/Templates/Currency/CCYCalculatorTemplate.html',

  'Mobile/ViewModels/Currency/CCYRatesViewModel',
  'text!Mobile/Templates/Currency/CCYRatesTemplate.html',

], function (events, utils, mvc, CCYsInformationViewModel, templateCCYsInformation, CCYCalculatorViewModel, templateConverter, CCYRatesViewModel, templateRates) {

    var CCYRates = null;

    return mvc.Controller.extend({

        index: function () {
            var ccysInformationViewModel = new CCYsInformationViewModel();

            events.bind(ccysInformationViewModel, "rendered", function (el, data) {
                var self = this;

                CCYRates = self.CCYRates;
                var res = $.DeferredEx(function (res) {

                    $(el.element).find('#submenu').submenu({
                        submenuItems: self.Submenu,
                        initialized: function () {
                            res.resolve();
                        }
                    });

                });
                return res;
            });

            return new mvc.ViewResult(templateCCYsInformation, ccysInformationViewModel);
        },

        ccyRates: function () {
            var ccyRatesViewModel = new CCYRatesViewModel(CCYRates);

            var ccyRatesView = new mvc.ViewResult(templateRates, ccyRatesViewModel);

            return ccyRatesView;
        },

        ccyConverter: function () {

            var ccyCalcViewModel = new CCYCalculatorViewModel(CCYRates);

            var ccyCalcView = new mvc.ViewResult(templateConverter, ccyCalcViewModel);

            return ccyCalcView;
        }
    });
});
