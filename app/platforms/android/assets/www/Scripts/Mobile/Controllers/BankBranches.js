﻿define([
  'Mobile/ViewModels/BankBranches/BankBranchesViewModel',
  'text!Mobile/Templates/BankBranches/ViewBankStructuresTemplate.html',
  'utils',
  'mvc'

], function (BranchesViewModel, template, utils, mvc) {

    return mvc.Controller.extend({
        index: function () {

            var branchesViewModel = new BranchesViewModel();

            var branchesView = new mvc.ViewResult(template, branchesViewModel);

            //utils.changePage(productsView);
            return branchesView;
        }
    });
});
