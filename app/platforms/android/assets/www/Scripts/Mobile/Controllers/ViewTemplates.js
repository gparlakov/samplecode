﻿define([
  'jquery',
  'utils',
  'mvc',

  'Mobile/ViewModels/Payments/Templates/ViewTemplatesViewModel',
  'text!Mobile/Templates/Payments/Templates/ViewPaymentTemplates.html',
  //'Mobile/Models/Filters/PaymentsListFilter',
  //'text!Mobile/Templates/Payments/Templates/ViewPaymnetTemplatesFilter.html'

], function ($, utils, mvc, ViewTemplatesViewModel, template) { //, filterTemplate

    return mvc.Controller.extend({
        routes: {
            "ViewTemplates/index": "index"
        },

        index: function () {

            var viewTemplatesView = new mvc.ViewResult(template, function (dataAvailableHandler) {

                var viewTemplatesViewModel = new ViewTemplatesViewModel();
                dataAvailableHandler(viewTemplatesViewModel);


            }, function (el, data) {
                
            });
            

            return viewTemplatesView;
        }
    });

});
