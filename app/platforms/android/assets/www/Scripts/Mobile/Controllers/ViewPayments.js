﻿define([
  'jquery',
  'utils',
  'mvc',
  'services',

  'events',

  'Mobile/ViewModels/Payments/ViewPaymentsViewModel',
  'text!Mobile/Templates/Payments/ViewPendingPaymentsTemplate.html',
  
  'text!Mobile/Templates/Payments/ViewPaymentsFilterTemplate.html',

  'Mobile/ViewModels/Payments/PaymentOverviewViewModel',
  'text!Mobile/Templates/Payments/PaymentsOverviewTemplate.html',

  'Mobile/ViewModels/Payments/ViewPaymentsArchiveViewModel',
  'text!Mobile/Templates/Payments/ViewArchivePaymentsTemplate.html'

], function ($, utils, mvc, services, events,
            ViewPaymentsViewModel, template,
            filterTemplate,
            PaymentOverviewViewModel, overviewTemplate,
            ViewPaymentsArchiveViewModel, artchiveTemplate) {

    return mvc.Controller.extend({
        routes: {
            "ViewPayments/index": "index"
        },

        index: function () {

            var paymentOverviewViewModel = new PaymentOverviewViewModel();
            //displayed
            events.bind(paymentOverviewViewModel, "rendered", function (el, data) {
                var self = this;

                var res = $.DeferredEx(function (res) {

                    $(el.element).find('#submenu').submenu({
                        submenuItems: self.Submenu,
                        initialized: function () {
                            res.resolve();
                        }
                    });
                });
                return res;
            });

            return new mvc.ViewResult(overviewTemplate, paymentOverviewViewModel);
        },

        viewPendingPayments: function () {

            var viewPaymentsView = new mvc.ViewResult(template, function (dataAvailableHandler) {

                var filter = new services.Objects.PaymentOrdersFilter();
                var viewPaymentViewModel = new ViewPaymentsViewModel(filter);

                dataAvailableHandler(viewPaymentViewModel);

            }, function (el, data) {
                data.AfterRender();
            });

            return viewPaymentsView;
        },

        viewArchivePayments: function () {

            var viewPaymentsView = new mvc.ViewResult(artchiveTemplate, function (dataAvailableHandler) {

                var filter = new services.Objects.PaymentOrdersFilter();
                var viewPaymentViewModel = new ViewPaymentsArchiveViewModel(filter);

                dataAvailableHandler(viewPaymentViewModel);

            }, function (el, data) {
                data.AfterRender();
            });

            return viewPaymentsView;
        }
    });

});
