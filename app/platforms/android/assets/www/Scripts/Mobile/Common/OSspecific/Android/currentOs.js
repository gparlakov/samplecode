﻿define([
    'utils',
    'Common/nomenclatures/enumOSType'
],function (utils, enumOsType) {

        var Android = {
            Consts: {
                MarketLink: "market://details?id=com.raiffeisen.online.mobile"
            },
            app: {
                terminate: function () {
                    utils.navigate("Login", { trigger: true });

                }
            },
            osTypeName: enumOsType.Android,
            closeAppIsAvailable: true,
            pushNotification_applicationId: '508795191701'
        }
		
        document.addEventListener("deviceready", function () {
            Android.app.terminate = navigator.app.exitApp;

        }, false);
		
        return Android;
    });