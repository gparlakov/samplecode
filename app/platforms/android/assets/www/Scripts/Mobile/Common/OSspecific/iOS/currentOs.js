﻿define([
    'utils',
    'Common/nomenclatures/enumOSType'
], function (utils, enumOsType) {
    
    var iOS = {
        Consts: {
            MarketLink: "market://details?id=com.raiffeisen.online.mobile"
        },
        app: {
            terminate: function () {
                utils.navigate("Login", { trigger: true });
                
            }
        },
        closeAppIsAvailable: false,

        osTypeName: enumOsType.iOS,

        pushNotification_applicationId: ''
    }
	
    return iOS;
});