﻿({
    appDir: "../",
    baseUrl: "Scripts",
    modules: [
        {
            name: "main"
        }
    ],
    dir: "../../www-build",
    mainConfigFile: 'main.js',
    skipDirOptimize: false,
    optimize: 'uglify2',
    fileExclusionRegExp: /ViewModels/,
    uglify2: {
        compress: {
            global_defs: {
                Configuration: "DEBUG"
            }
        }
    }
})
