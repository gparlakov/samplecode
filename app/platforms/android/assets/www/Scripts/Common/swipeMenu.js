﻿define([
  'jquery',
  'utils',
], function ($, utils) {

    var res = {};
    
    $.widget("ui.swipeMenu", {
        options: {
            initialSlide: 1, //create closed
            resistanceRatio: 0, //no offset blank space
            slidesPerView: 'auto',
            speed: 100
        },

        _create: function () {
            var self = this;
            self.overlay = self.element.find(".navig-overlay");

            self.options.onReachBeginning = function (swiper) {
                self.overlay.addClass("active");
                $('body').addClass('scroll-stop');
            }
            self.options.onReachEnd = function (swiper) {
                self.overlay.removeClass("active");
                $('body').removeClass('scroll-stop');
            }

            self.overlay.on("click", function () {
                self.hide();
            });

            self.element.swiper(self.options);
        },

        isVisible: function() {
            return this.element[0].swiper.isBeginning;
        },

        show: function () {
            if (!this.isVisible()) {
                this.element[0].swiper.slidePrev();
            }
        },

        hide: function () {
            if (this.isVisible()) {
                this.element[0].swiper.slideNext();
            }
        },

        toggle: function () {
            if (this.isVisible()) {
                this.hide();
            } else {
                this.show();
            }
        },

        destroy: function () {
            this.element[0].swiper.destroy();
            $.Widget.prototype.destroy.call(this);
        }
    });

    return res;
});

