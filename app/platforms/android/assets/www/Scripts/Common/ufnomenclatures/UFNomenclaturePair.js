﻿define([
    'underscore',
    'i18n!Mobile/nls/strings'

], function (_, strings) {

    var UFNomenclaturePair = function (id, valueStringID, additional) {

        additional = additional || {};

        var res = {};
        res.ID = id;
        res.Value = strings[valueStringID];

        _.extend(res, additional);

        return res;
    }

    return UFNomenclaturePair;
});