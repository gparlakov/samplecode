﻿define(
    function () {

        var ValueDate = [
            { ID: 0, Value: "SPOT" },
            { ID: 1, Value: "SAME" },
            { ID: 2, Value: "NEXT" }
        ]

        return ValueDate;
    });