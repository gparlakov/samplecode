﻿define([
    "Common/ufnomenclatures/UFNomenclaturePair"
], function (ufPair) {
    var DirType = {
        A: 0,
        D: 1
    };

    var ValueDate = [
        new ufPair(0, "ID_STR_PAYMENT_DIRECTION_ABROAD", { Direction: DirType.A, BenefType: DirType.A }),
        new ufPair(2, "ID_STR_PAYMENT_DIRECTION_DOMESTIC_LOCAL", { Direction: DirType.D, BenefType: DirType.D }),
        new ufPair(1, "ID_STR_PAYMENT_DIRECTION_DOMESTIC_FOREING", { Direction: DirType.D, BenefType: DirType.A })
    ];

    return ValueDate;
});