﻿define([
  'jquery',
  'underscore',
  'events',
  'config',
  'deviceapi',
  'session'
], function ($, _, events, config, deviceapi, session) {

    var services  = function() {
        return require("services");
    }

    var debug = {
            warning: function(message, params)
            {
                if (config.Warning) {
                    console.warn(message, params);
                }
            },
            write: function (message, params) {
                if (config.Debug) {
                    console.log(message, params);
                }
            }
    }

    diagnostics = {};

    diagnostics.Error = function (error)
    {
        try
        {
            var errorAsText = JSON.stringify(error);
            var ds = new (services().Services).DiagnosticsService();

            var s = session.GetSession();
            if (s) {
                var deviceData = new (services().Objects).AdditionalUserDataMobileDevice({
                    UserDeviceID: deviceapi.device.DeviceID,
                    UserDeviceName: deviceapi.device.DeviceName,
                    UserDevicePlatform: deviceapi.device.DevicePlatform,
                    UserDeviceVersion: deviceapi.device.DeviceVersion,
                    UserDeviceModel : deviceapi.device.DeviceModel
                });

                var error = new (services().Objects).DiagnosticsError({
                    AdditionalUserData: deviceData,
                    Message: errorAsText
                });

                console.warn("diagnostics.Error", error);
                var result = ds.LogError(error);

                return result;
            }
            else {
                diagnostics.Debug("No session to log error");
            }
        }
        catch (e)
        {
            diagnostics.Debug("Excetion in diagnostics.Error: " + e);
            return null;
        }
    }

    diagnostics.Debug = function (message)
    {
        debug.write(message);
    }

    diagnostics.Time = function (message) {
        if (config.Debug) {
            return (function () {
                var startTime = new Date();
                return function () {
                    var endTime = new Date();
                    console.log("'{0}' take {1} sec. to complete".format(message, (endTime - startTime) / 1000));
                }
            })()
        }
        else {
            return $.noop;
        }
    }

    return diagnostics;
});