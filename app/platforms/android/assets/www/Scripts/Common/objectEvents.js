﻿define([
  'jquery',
  'underscore'
], function ($, _) {

  
    function evetsContainer()
    {
        var a = {
        };
        return function getEvets() {
            return a;
        }
    }
    function getObjectEvets(object)
    {
        if (object)
        {
            if (!object.__events)
            {
                object.__events = evetsContainer();
            }
            return object.__events();
        }
        return {};
    }
    


    var objectEvents = {
        trigger: function (object, event, args) {
            
            var functions = [];

            var events = getObjectEvets(object);
            if (object[event] && $.isFunction) {
                functions.push(function () { return object[event].call(object, args) });
                
            }
            if (events[event]) {
                _.each(events[event], function (handler) {
                    functions.push(function () { return handler.call(object, args) });
                });
            }
            
            var results = [];
            for (var f in functions)
            {
                results.push(functions[f].call(this));
            }

            return $.whenEx.apply($.when, results);
        },
        bind: function (object, event, callback) {
            var events = getObjectEvets(object);
            if (!events[event])
                events[event] = [];
            events[event].push(callback);
        },
        one: function (object, event, callback) {
            var events = getObjectEvets(object);
            if (!events[event])
                events[event] = [];
            var f = null;
            f = function () {
                callback.apply(this, arguments);
                objectEvents.unbind(object, event, f);
            };
            events[event].push(f);
        },
        unbind: function (object, event, callback) {
            var events = getObjectEvets(object);
            if (!events[event])
                events[event] = [];
            if (callback)
                events[event] = _.without(events[event], callback);
            else
                events[event] = [];
        }
    }

    function handlersContainer()
    {
        var a = {};
        return function getHanders() {
            return a;
        }
    }
    function getObjectsHandlers(object)
    {
        if (object)
        {
            if (!object.__handlers)
            {
                object.__handlers = handlersContainer();
            }
            return object.__handlers();
        }
        return {};
    }

    var hadleStackUtils = {

        trigger: function (object, event, params)
        {
            var functions = [];

            var handlers = getObjectsHandlers(object);
            if (object[event] && $.isFunction) {
                functions.push(object[event]);

            }
            if (handlers[event]) {
                _.each(handlers[event], function (handler) {
                    functions.push(handler);
                });
            }
            functions.reverse();

            var results = [];
            for (var f in functions) {
                var res = functions[f].apply(object, params);
                if (res != false)
                    return true;
            }

            return false;
        },
        add: function (object, event, callback)
        {
            var handlers = getObjectsHandlers(object);
            if (!handlers[event])
                handlers[event] = [];
            handlers[event].push(callback);
        },
        remove: function (object, event, callback)
        {
            var handlers = getObjectsHandlers(object);
            if (!handlers[event])
                handlers[event] = [];
            if (callback)
                handlers[event] = _.without(handlers[event], callback);
            else
                handlers[event] = [];
        }
    }
    return $.extend({}, objectEvents, { handlers: hadleStackUtils });
});
