define([
  'daisJson',
  'Common/cache/objectStorage'
],
 function (daisJson, objectStorage) {

     var Cache = {
         //_storage: window.localStorage, 
         _storage: objectStorage,
         GetObject: function (localStorageKey) {
             var strobjValue = this._storage.getItem(localStorageKey);
             return strobjValue;
//             	        if (strobjValue != null && strobjValue != '') {
//             	          var myglobaldata = daisJson.daisJsonDeserialize(strobjValue);
//             	          return myglobaldata;
//             	          
//             	    	} else {
//             	    		return null;
//             	    	}
         },

         SetObject: function (localStorageKey, value) {
             //this._storage.setItem(localStorageKey, daisJson.daisJsonSerialize(value));
             this._storage.setItem(localStorageKey, value);
         },

         Clear: function () {
             this._storage.clear();
         },

         RemoveObject: function (localStorageKey) {
             this._storage.removeItem(localStorageKey);
         }

     }

     return Cache;
 });