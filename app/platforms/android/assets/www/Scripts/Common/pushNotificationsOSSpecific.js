﻿// android

define([
    'jquery',
    'services',
    'servicesObjects',
    'underscore',
    'diagnostics',
    'session',
    'osManager',

], function ($, services, servicesObjects, _, diagnostics, session, osManager) {

    var pushNotificationsOSSpecific = {
        Register: function () { },
        Unregister: function () { },
    };

    function register(options) {
        var defaults = {
            "senderID": osManager.CurrentOS.pushNotification_applicationId,
            "ecb": "window.OnReceivedPushNotification"
        };

        options = _.extend(defaults, options);

        window.plugins.pushNotification.register(onRegisterSuccess, onRegisterError, options);
    };

    function onRegisterSuccess(status) {
        //log successful register
        diagnostics.Debug('Push Notification -> registered successfully on android');
    }

    function onRegisterError(error) {
        //log error register
        diagnostics.Error(error);
    }

    function onNotification(e) {
        //remove after test
        diagnostics.Debug('Push Notifications -> Event RECEIVED:' + e.event);

        switch (e.event) {
            case 'registered':
                if (e.regid.length > 0) {                    
                    // Your GCM push server needs to know the regID before it can push to this device
                    // here is where you might want to send it the regID for later use.
                    diagnostics.Debug('Push Notifications -> regID = ' + e.regid);

                    var apId = session.GetSession().UserID;

                    var request = new (servicesObjects.PushRegistrationData)(
                    {
                        APerson: {
                            APersonID: apId
                        },
                        RegistrationToken: e.regid,
                        DeviceType: osManager.CurrentOS.osTypeName.id
                    });

                    services.PushNotificationsService.SavePushRegistration(request);                    
                }
                break;

            case 'message':
                // if this flag is set, this notification happened while we were in the foreground.
                // you might want to play a sound to get the user's attention, throw up a dialog, etc.
                if (e.foreground) {
                    diagnostics.Debug('Push Notifications ->--INLINE NOTIFICATION--');

                    diagnostics.Debug('Push Notifications ->' + e.msg);
                    diagnostics.Debug('Push Notifications ->' + e.msgcnt);
                }
                else {  // otherwise we were launched because the user touched a notification in the notification tray.
                    if (e.coldstart) {
                        diagnostics.Debug('Push Notifications ->--COLDSTART NOTIFICATION--');
                    }
                    else {
                        diagnostics.Debug('Push Notifications ->--BACKGROUND NOTIFICATION--');
                    }
                }

                //console.log('<li>MESSAGE -> payload.message: ' + e.payload.message + '</li>');
                //console.log('<li>MESSAGE -> payload.msgcnt: ' + e.payload.msgcnt + '</li>');
                ////$status.append('<li>MESSAGE -> data: ' + e.payload.data.message + '</li>');

                //console.log('<li>MESSAGE -> payload.title: ' + e.payload.title + '</li>');
                break;

            case 'error':
                diagnostics.Error(e);                
                break;

            default:
                diagnostics.Debug('Push Notification -> Unknown, an event was received and we do not know what it is');
                break;
        }
    };

    function onDeviceReady() {
        //when an event is received it does not know about this scope so it needs a global var
        window.OnReceivedPushNotification = onNotification;

        pushNotificationsOSSpecific.Register = register;

        pushNotificationsOSSpecific.Unregister = window.plugins.pushNotification.unregister;
    }

    document.addEventListener("deviceready", onDeviceReady, false);

    return pushNotificationsOSSpecific;
});