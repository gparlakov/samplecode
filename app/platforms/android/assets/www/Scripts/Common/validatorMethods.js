﻿define([
  'underscore',
  'require',
  'jquery',
  'jsrender',
  'knockout',
  'jsrender',
  'utils',
  'validator',
  'i18n!Mobile/nls/validation',
], function (_, require, $, $templates, ko, jsrender, utils, Validator, validationMessages) {

    var STR_LIMIT_NO_SWIFT_SYMBOLS = "[^ /?:()\\-.,'+0-9A-Za-z]";
    var STR_LIMIT_SWIFTN = /^[A-Za-z]{6}[A-Za-z0-9]{2}([A-Za-z0-9]{3})?$/;
    var STR_LIMIT_NO_BISERA_SYMBOLS = "[^ /?:()\\-.,'+0-9A-Za-z]";

    function IBANCheckDefinition(pISOCode, pCountryName, pIBANPrefix, pIBANLength) {
        this.ISOCode = pISOCode;
        this.IBANLength = pIBANLength;
        this.CountryName = pCountryName;
        this.IBANPrefix = pIBANPrefix;
    }

    var countryIBANDefByPref = {};
    var countryIBANDefByISO = {};

    countryIBANDefByISO.BG = countryIBANDefByPref.BG = new IBANCheckDefinition('BG', 'Bulgaria', 'BG', 22);

    function CheckIBAN(sIBAN) {
        return CheckIBANGeneral(sIBAN, countryIBANDefByPref.BG);
    }

    function CheckIBANGeneral(sIBAN, IBANFormat) {
        sIBAN = sIBAN.toUpperCase();

        if (sIBAN.length == 0) return true;

        if (sIBAN.length != IBANFormat.IBANLength) return false;
        if (sIBAN.substring(0, 2) != IBANFormat.IBANPrefix) return false;

        var arrLettersToNumbers = new Array();
        arrLettersToNumbers[65] = 10;
        arrLettersToNumbers[66] = 11;
        arrLettersToNumbers[67] = 12;
        arrLettersToNumbers[68] = 13;
        arrLettersToNumbers[69] = 14;
        arrLettersToNumbers[70] = 15;
        arrLettersToNumbers[71] = 16;
        arrLettersToNumbers[72] = 17;
        arrLettersToNumbers[73] = 18;
        arrLettersToNumbers[74] = 19;
        arrLettersToNumbers[75] = 20;
        arrLettersToNumbers[76] = 21;
        arrLettersToNumbers[77] = 22;
        arrLettersToNumbers[78] = 23;
        arrLettersToNumbers[79] = 24;
        arrLettersToNumbers[80] = 25;
        arrLettersToNumbers[81] = 26;
        arrLettersToNumbers[82] = 27;
        arrLettersToNumbers[83] = 28;
        arrLettersToNumbers[84] = 29;
        arrLettersToNumbers[85] = 30;
        arrLettersToNumbers[86] = 31;
        arrLettersToNumbers[87] = 32;
        arrLettersToNumbers[88] = 33;
        arrLettersToNumbers[89] = 34;
        arrLettersToNumbers[90] = 35;

        modifiedIBAN = sIBAN.substring(4, sIBAN.length) + sIBAN.substring(0, 4);

        transformedIBAN = "";

        for (var k = 0; k < sIBAN.length; k++) {
            var c = modifiedIBAN.charAt(k);
            if (c >= 'A' && c <= 'Z') {
                //letter
                transformedIBAN += arrLettersToNumbers[modifiedIBAN.charCodeAt(k)];
            }
            else if (c >= '0' && c <= '9') {
                //number
                transformedIBAN += c;
            }
            else {
                return false;
            }
        }

        strbase = transformedIBAN.substring(0, transformedIBAN.length - 2) + "00";

        //ostatyk za base (bez controlno chislo)

        k = DAISModule(strbase, 97);

        var ctrlNumber = new Number(sIBAN.substring(2, 4))

        if ((98 - k) != ctrlNumber) {
            return false;//controlno chislo ne sywpada s izchislenoto
        }

        //ostatyk za IBAN
        k = DAISModule(transformedIBAN, 97)

        if (k != 1) {
            return false;//newalidno kontrolno chislo
        }

        return true;
    }

    function CheckIBANInternational(sIBAN) {
        if (sIBAN.length == 0) return true;
        var arrLettersToNumbers = new Array();
        arrLettersToNumbers[65] = 10;
        arrLettersToNumbers[66] = 11;
        arrLettersToNumbers[67] = 12;
        arrLettersToNumbers[68] = 13;
        arrLettersToNumbers[69] = 14;
        arrLettersToNumbers[70] = 15;
        arrLettersToNumbers[71] = 16;
        arrLettersToNumbers[72] = 17;
        arrLettersToNumbers[73] = 18;
        arrLettersToNumbers[74] = 19;
        arrLettersToNumbers[75] = 20;
        arrLettersToNumbers[76] = 21;
        arrLettersToNumbers[77] = 22;
        arrLettersToNumbers[78] = 23;
        arrLettersToNumbers[79] = 24;
        arrLettersToNumbers[80] = 25;
        arrLettersToNumbers[81] = 26;
        arrLettersToNumbers[82] = 27;
        arrLettersToNumbers[83] = 28;
        arrLettersToNumbers[84] = 29;
        arrLettersToNumbers[85] = 30;
        arrLettersToNumbers[86] = 31;
        arrLettersToNumbers[87] = 32;
        arrLettersToNumbers[88] = 33;
        arrLettersToNumbers[89] = 34;
        arrLettersToNumbers[90] = 35;

        //първите 4 знака се преместват на края
        modifiedIBAN = sIBAN.substring(4, sIBAN.length) + sIBAN.substring(0, 4);

        transformedIBAN = "";
        //буквите се заменят с числа v transformedIBAN
        for (var k = 0; k < sIBAN.length; k++) {
            var c = modifiedIBAN.charAt(k);
            if (c >= 'A' && c <= 'Z') {
                //letter
                transformedIBAN += arrLettersToNumbers[modifiedIBAN.charCodeAt(k)];
            }
            else if (c >= '0' && c <= '9') {
                //number
                transformedIBAN += c;
            }
            else
                return false;
        }

        strbase = transformedIBAN.substring(0, transformedIBAN.length - 2) + "00";

        //ostatyk za base (bez controlno chislo)

        k = DAISModule(strbase, 97);

        var ctrlNumber = new Number(sIBAN.substring(2, 4))

        if ((98 - k) != ctrlNumber)
            return false; //controlno chislo ne sywpada s izchislenoto

        //ostatyk za IBAN
        k = DAISModule(transformedIBAN, 97)

        if (k != 1)
            return false; //newalidno kontrolno chislo

        return true;
    }

    function DAISModule(strBigNumber, SmallNumber) {
        k = 0;
        for (var i = 0; i < strBigNumber.length; i++) {
            k = (10 * k + new Number(strBigNumber.charAt(i))) % SmallNumber;

        }
        return k;
    }

    function TrimString(sInString) {
        sInString = sInString.replace(/^\s+/g, "");// strip leading
        return sInString.replace(/\s+$/g, "");// strip trailing
    }

    function StringValidater(str, strRegExpr) {
        eval("var re = " + strRegExpr);

        str = TrimString(str);

        var bRes = re.test(str);
        if (bRes) {
            var strRes = str.replace(re, "");
            bRes = (strRes == "");
        }
        return bRes;
    };

    function CheckIbanForCountry(iban, checkIbanStruct, ibanStructure, checkIbanControl) {
        if (iban != null) {

            if (checkIbanStruct) {
                if (!StringValidater(iban, ibanStructure)) {
                    return false;
                }
            }

            if (checkIbanControl) {
                if (!CheckIBANInternational(iban.toUpperCase())) {
                    return false;
                }
            }

        }

        return true;
    }

    function CheckSwiftContryIsoCode(swiftCode, countryIsoCode) {
        if (swiftCode != null && swiftCode.length != 0 && TrimString(countryIsoCode).length != 0) {
            var swiftIsoCode = swiftCode.substring(4, 6).toUpperCase();

            if (swiftIsoCode == countryIsoCode) {
                return true;
            }
            else {
                return false;
            }
        }
        return true;
    }

    function GetInvalidBISERASymbols(str2) {
        var str = str2.toUpperCase();
        var l = str.length;
        var res = [];


        for (var k = 0; k < l; k++) {
            var c = str.charAt(k);
            var c2 = str2.charAt(k);

            if (!(0 == k && "-" == c) &&
                (" " == c || "!" == c || '"' == c || "%" == c || "&" == c || "'" == c || "(" == c || ")" == c ||
                "*" == c || "+" == c || "," == c || "-" == c || "." == c || "/" == c || ":" == c || ";" == c ||
                "<" == c || "=" == c || ">" == c || "?" == c || (c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'А' && c <= 'Я'))
              ) {
            }
            else {
                res.push(c);
            }
        }
        return res;
    }

    function CheckEGN(sEGN) {
        if (sEGN.length != 10) return false;

        var arrWeightVector = new Array(2, 4, 8, 5, 10, 9, 7, 3, 6);
        var sum = 0;

        for (var k = 0; k < 9; k++) {
            sum += new Number(sEGN.charAt(k).valueOf()) * arrWeightVector[k];
        }

        sum = (sum % 11) % 10;

        if ((new String(sum)) != sEGN.charAt(9)) return false;

        var nDay = new Number(sEGN.substring(4, 6));
        var nMonth = new Number(sEGN.substring(2, 4));
        var nYear = new Number(sEGN.substring(0, 2));

        if (31 < nDay) return false;

        if (!((nMonth >= 1 && nMonth <= 12) || (nMonth >= 21 && nMonth <= 32) || (nMonth >= 41 && nMonth <= 52))) return false

        return true;
    }

    function CheckTaxNumber(sTaxNumber) {
        if (sTaxNumber.length != 10) return false;

        var arrWeightVector = new Array(4, 3, 2, 7, 6, 5, 4, 3, 2);
        var sum = 0;

        for (var k = 0; k < 9; k++) {
            sum += new Number(sTaxNumber.charAt(k).valueOf()) * arrWeightVector[k];
        }

        sum = (11 - sum % 11) % 11;

        return ((new String(sum)) == sTaxNumber.charAt(9));
    }

    function CheckBULSTAT_9(sBULSTAT) {
        if (sBULSTAT.length != 9) return false;

        var arrWeightVector = new Array(1, 2, 3, 4, 5, 6, 7, 8);
        var arrWeightVector1 = new Array(3, 4, 5, 6, 7, 8, 9, 10);
        var sum = 0;

        for (var k = 0; k < 8; k++) {
            sum += new Number(sBULSTAT.charAt(k).valueOf()) * arrWeightVector[k];
        }

        sum = sum % 11;

        if (10 == sum) {
            sum = 0;
            for (var k = 0; k < 8; k++) {
                sum += new Number(sBULSTAT.charAt(k).valueOf()) * arrWeightVector1[k];
            }

            sum = (sum % 11) % 10;
        }

        return ((new String(sum)) == sBULSTAT.charAt(8));
    }

    function CheckBULSTAT_13(sBULSTAT) {
        if (sBULSTAT.length != 13) return false;

        if (!CheckBULSTAT_9(sBULSTAT.substring(0, 9))) return false;

        var arrWeightVector = new Array(2, 7, 3, 5);
        var arrWeightVector1 = new Array(4, 9, 5, 7);
        var sum = 0;

        for (var k = 8; k < 12; k++) {
            sum += new Number(sBULSTAT.charAt(k).valueOf()) * arrWeightVector[k - 8];
        }

        sum = sum % 11;

        if (10 == sum) {
            sum = 0;
            for (var k = 8; k < 12; k++) {
                sum += new Number(sBULSTAT.charAt(k).valueOf()) * arrWeightVector1[k - 8];
            }

            sum = (sum % 11) % 10;
        }

        return ((new String(sum)) == sBULSTAT.charAt(12));
    }

    function CheckBULSTAT(sBULSTAT) {
        if (CheckBULSTAT_9(sBULSTAT)) return true;
        if (CheckBULSTAT_13(sBULSTAT)) return true;
        return false;
    }


    Validator.methods.add({
        HomeIBAN: {
            method: function (value, parameters) {
                if (value != null) {
                    return Validator.methods.RegEx.method(value, Validator.RegExUtils.HomeIBAN) && CheckIBAN(value);
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_HOMEIBAN)
        },

        InternationalIBANForCountry: {
            method: function (value, parameters, context) {
                if (value != null && value != "" && parameters != null) {
                    var country = parameters;
                    return CheckIbanForCountry(value, country.CheckIbanStruct, country.IbanStructure, country.CheckIbanControl);
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_INTERNATIONALIBAN)
        },

        InternationalSWIFTForCountry: {
            method: function (value, parameters, context) {
                if (value != null && value != "" && parameters != null) {
                    var country = parameters;
                    return CheckSwiftContryIsoCode(value, country.ISOCode);
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_INTERNATIONALSWIFT)
        },

        ThisOrOtherGroupMustBeFilled: {
            method: function (value, parameters, context) {
                self = this;
                if (value == null || value == '') {
                    var parentValue = context.parentContext.getValue();

                    return _.all(parameters.properties, function (prop) {
                        var propValue = self.unWrapValueAndRegisterOnChange(parentValue[prop], context);
                        return propValue != null && propValue != '';
                    });
                }
                return true;
            },
            message: function (value, parameters, context, result) {
                var res = "This or all of following fields must be filled: ";
                res = res + parameters.properties.join(", ");
                return res;
            }
        },
        SWIFTSymbols: {
            method: function (value, parameters, context) {
                if (value != null && value != "") {
                    var re = new RegExp(STR_LIMIT_NO_SWIFT_SYMBOLS, "g");
                    var result = value.match(re);
                    return {
                        valid: result == null,
                        invalidValues: result,
                        invalidString: result != null ? _.map(_.unique(result), function (item) { return item }).join(", ") : ""
                    }
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_INVALIDSWIFTSYMBOLS)
        },
        BankSWIFT: {
            method: function (value, parameters, context) {
                if (value != null && value != "") {
                    return STR_LIMIT_SWIFTN.test(value);
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_INVALIDBANKSWIFT)
        },
        BISERASymbols: {
            method: function (value, parameters, context) {
                if (value != null && value != "") {
                    var result = GetInvalidBISERASymbols(value);
                    return {
                        valid: result == null || result.length == 0,
                        invalidValues: result,
                        invalidString: result != null ? _.map(_.unique(result), function (item) { return item }).join(", ") : ""
                    }
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_INVALIDBISERASYMBOLS)
        },
        EGN: {
            method: function (value, parameters, context) {
                if (value != null && value != "") {
                    return CheckEGN(value);
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_INVALIDEGN)
        },
        TaxNumber: {
            method: function (value, parameters, context) {
                if (value != null && value != "") {
                    return CheckTaxNumber(value);
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_INVALIDTAXNUMBER)
        },
        BULSTAT: {
            method: function (value, parameters, context) {
                if (value != null && value != "") {
                    return CheckBULSTAT(value);
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_INVALIDBULSTAT)
        }

    });

});
