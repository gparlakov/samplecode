﻿define([
// Load the original jQuery source file
  'libs/jquery.min/jquery-2.1.0.min'
], function () {
	// Tell Require.js that this module returns a reference to jQuery
	return jQuery;
});