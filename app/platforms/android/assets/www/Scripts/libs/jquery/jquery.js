﻿define([
// Load the original jQuery source file
  'libs/jquery/jquery-1.7.2'
], function () {
	// Tell Require.js that this module returns a reference to jQuery
	return jQuery;
});