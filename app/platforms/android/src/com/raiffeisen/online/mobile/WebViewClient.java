package com.raiffeisen.online.mobile;

import org.apache.cordova.engine.SystemWebViewEngine;

import android.util.Log;
import android.net.http.SslError;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;

public class WebViewClient extends org.apache.cordova.engine.SystemWebViewClient {

    public WebViewClient (SystemWebViewEngine ctx) {
        super(ctx);
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler,
    		SslError error) {
    	// TODO Auto-generated method stub
    	Log.i("app", "ssl Error");
    	handler.proceed();
    }
}
