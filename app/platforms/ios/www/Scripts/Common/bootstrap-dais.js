﻿define([
  'jquery',
  'osManager'
], function ($, osManager) {

    var res = {};
    var scrollControl = {
        prevent: function () {
            document.body.className += ' scroll-stop'
        },
        allow: function () {
            document.body.className = document.body.className.replace(' scroll-stop', '');
        },
        top: function () {
            $('body').css('overflow', 'visible');
            window.scrollTo(0, 0);
            //$('html, body').css('overflow', 'auto');
        },
        unfix: function () {
            var s = $(window).scrollTop();
            var fixedTitle = $('#navbar');
            fixedTitle.css('position', 'absolute');
            fixedTitle.css('top', s + 'px');
        }
    }

    daisui = {
        vars: {
            mainPage: document.getElementById('mainPage')
        },

        expand: function (li) {

            var sibling = document.querySelector('.list-group-item.active');
            if (sibling) sibling.className = sibling.className.replace(' active', '');

            li.className += ' active';
            li.setAttribute('tabindex', 0);
            li.focus();

            //li.on('click', function () {
            //    li.className.replace(' active', '');
            //});

        },
        toggle: function (target) {
       
            if (target.className.indexOf('active') > -1) { 
                target.className = target.className.replace(' active', '');
            }
            else {
                target.className += ' active';
            }

        },
        scrollObject: {
            x: 0,
            y: 0
        },
        getScrollPosition: function () {

            var a = daisui.scrollObject.y || 0;

            daisui.scrollObject = {
                x: window.pageXOffset,
                y: window.pageYOffset
            }

            var b = daisui.scrollObject.y;

            //if (b > 72 && document.body.className.indexOf('fixchilds') <=0) { document.body.className += ' fixchilds' }
            //if (b < 70) { document.body.className = document.body.className.replace(' fixchilds', ''); }

            if (a < b
                && document.body.className.indexOf('compact') <= 0
                && b > 72) {
                document.body.className += ' compact'
            }
            else if (a > b) {
                document.body.className = document.body.className.replace(' compact', '');
            }


        }
    };

    // gmaps tap on search result suggestions fix 
    $(document).on({
        'DOMNodeInserted': function () {
            $('.pac-item, .pac-item span', this).addClass('needsclick');
        }
    }, '.pac-container');

    //hak, za da se aktivira scroll-a, pri smqna na stranica ot menu-to
    $(document).on("create", function (e) {
        scrollControl.allow();
        scrollControl.top();
        
        // initial loading fix
        $('body').removeClass('loading-on');
    });

    function getAndroidVersion(ua) {
        var ua = ua || navigator.userAgent;
        var match = ua.match(/Android\s(([0-9]*\.)[0-9]*)/);
        return match ? match[1] : '';
    };

    var ginger = getAndroidVersion().replace('.', '-');
    $('body').addClass('android-' + ginger).addClass(osManager.CurrentOS.osTypeName.id);
    //document.body.className += 'android-' + ginger;
    //window events -->

    //   window.addEventListener('scroll', daisui.getScrollPosition);

    //<--window events 
    $("body").on("create", "#mainPage", function () {
        
        document.getElementById('mainPage').style.minHeight = screen.availHeight - 72 - 40;
        //scrollControl.top();
        
        //selectbox appearance fix for 4.2
        $(".android-4-2 select").removeAttr("class").css("width", "100%");
    });
    window.addEventListener('resize', function () {
        document.getElementById('mainPage').style.minHeight = screen.availHeight - 72 - 40;
    });

    //Това прави проблеми - не може да хване само бутоните които трябва да премести
    //$("body").on("create", "#mainPage", function () {
    //    $("#mainPage>* .footer").each(function () {
    //        $("#mainPage .footer").remove();
    //        $("#mainPage").append(this);
    //    });
    //});

    $("body").on("click", "a[target='_blank']", function (e) {
        e.preventDefault();
        window.open(this.href, '_system');
        //console.log('opening ' + e.target.href + ' in new window');
    });


    


    return res;
});

