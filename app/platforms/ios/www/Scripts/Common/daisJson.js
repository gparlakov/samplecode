﻿define([
    'jqueryformat',
    'dateUtils'

],function ($format, dateUtils) {
    var DaisJson = {
        initialize: function () {
            dateUtils.overrideToJSON();
        },
        daisJsonDeserialize: (function() {

            return function (text) {
                
                return JSON.parse(text, dateUtils.parseDateFromServer);
            }
        })(),

        daisJsonSerialize: (function () {
            
            return function (obj) {
               return JSON.stringify(obj);
            }
        })()
        
    }

    return DaisJson;
});