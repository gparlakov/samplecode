﻿define([
    'jqueryformat'

], function ($format) {
    var self = {
        //_date: new Date(),

        //DATE(2006-06-16T00:00:0)
        serverDateRegEx: new RegExp(/^DATE\((\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+)([+](\d*):(\d*))?\)$/),

        browserDateRegEx: new RegExp(/^(\d+)-(\d+)-(\d+)$/),
        
        nativeDateRegEx : new RegExp(/^(\d+)\.(\d+)\.(\d+)$/),

        sgebServiceDateRegEx: new RegExp(/^(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)/),

        Date: function (y, m, d) {
            return new Date(y,m,d);
        },

        _getDateTimeFromRegExMatch: function (match) {

            var year = match[1];
            var month = parseInt(match[2], 10) - 1;
            var day = match[3];
            var hour = match[4];
            var minute = match[5];
            var second = match[6];
            //narochno e taka 
            return new Date(year, month, day, hour, minute, second, 0);
        },
    }
    //Ne stasna chitavo 
    var dateUtils = {

        today: function () {
            var newDate = new Date();
            return self.Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate());
            
        },

        monthStart : function (){
            var newDate = new Date();
            return self.Date(newDate.getFullYear(), newDate.getMonth(), 1);
        },

        addDays: function (dayOffset, date) {

            var millisecondOffset = dayOffset * 24 * 60 * 60 * 1000;
            return new Date(date.setTime(date.getTime() + millisecondOffset)); 

        },

        overrideToJSON: function () {

            Date.prototype.toJSON = function () {

                return $format.date(this, "DATE(yyyy-MM-ddTHH:mm:ss)");
            }
        },

        Date: function (y, m, d) {
            return self.Date(y, m, d);
        },

        
        parseDateFromServer: function (key, value) {
            if (typeof (value) == "string") {
                if (value.substring(0, 5) == "DATE(") {
                    if (self.serverDateRegEx.test(value)) {
                        var result = self.serverDateRegEx.exec(value);
                        return self._getDateTimeFromRegExMatch(result);
                    }
                }
            }
            return value;
        },

       

        isBrowseDate: function (value) {
            return self.browserDateRegEx.test(value);
        },

        parseDateFromBrowser: function (value) {

            var result = self.browserDateRegEx.exec(value);
            year = result[1];
            month = parseInt(result[2], 10) - 1;
            day = result[3];

            return self.Date(year, month, day);
        },

        //moje da se naloji da se razdeli za Android i iPhone
        isNativeDate :function (value){
            return self.nativeDateRegEx.test(value);
        },

        //moje da se naloji da se razdeli za Android i iPhone
        parseDateFromNative: function (value) {
            
            var result = self.nativeDateRegEx.exec(value);
            var year = result[3];
            var month = parseInt(result[2], 10) - 1;
            var day = result[1];

            return self.Date(year, month, day);

        },

        parseFromSGEBService: function (value) {
            var result = self.sgebServiceDateRegEx.exec(value);
            return self._getDateTimeFromRegExMatch(result);
        }   

    }

    return dateUtils;
});