﻿define([
  'require',
  'jquery',
  'jsrender',
  'knockout',
  'jqueryformat',
  'deviceapi',
  'i18n!Mobile/nls/strings',
  'config',
], function (require, $, $templates, ko, $format,deviceapi ,strings, config) {

    $.views.helpers({
        format: $format,

        GetString: function GetString(name) {
            if (strings[name])
                return strings[name];
            else
                return "!!" + name + "!!";
        },

        GetLocalizedString: function (description) {
            return $.GetLocalizedString(description);
        },

        GetWeekDayByNumber : function (dayNumber){
            if (dayNumber == 1) {
                return strings['ID_STR_WEEKDAY_MON']
            } else if(dayNumber == 2) {
                return strings['ID_STR_WEEKDAY_TUE']
            } else if (dayNumber == 3) {
                return strings['ID_STR_WEEKDAY_WED']
            } else if (dayNumber == 4) {
                return strings['ID_STR_WEEKDAY_THU']
            } else if (dayNumber == 5) {
                return strings['ID_STR_WEEKDAY_FRI']
            } else if (dayNumber == 6) {
                return strings['ID_STR_WEEKDAY_SAT']
            } else if (dayNumber == 7) {
                return strings['ID_STR_WEEKDAY_SUN']
            }
        },

        RenderPartial: function (name, data) {
            var templ = require(name);

            $templates({ tmpl: templ });
            var resHtml = $.render.tmpl(data);
            return resHtml;
        },

        RenderOutOfPage: function (name, data) {
            var templ = require(name);

            $templates({ tmpl: templ });
            var resElement = $($.render.tmpl(data));
            ko.applyBindings(data, resElement[0]);
            $('body').append(resElement);


        },

        RenderSubmenu: function (id, data) {
            var el = $('<div></div>');
            var rednerd = el.submenu({ submenuItems: data });
            return rednerd.html();
        }
    });

    $.views.tags({
        Debug: function () {
            if (config.Debug) {
                return "<div class='debug'>{0}</div>".format(this.renderContent(this.view.data));
            }
        },
        RenderTemplateFromStrings: function () {
            var string = strings[this.tagCtx.props.name];
            this.tagCtx.tmpl = string;
            return this.tagCtx.render(this.tagCtx.view.data)
        },

        InputDate: function (data) {
            //value tata ne se prahvalqt... v props trqbva da ima attibute 'value'
            //neshto takova inp2.attr('value','1234');  
            var self = this;

            var input = $("<input />");
            
            //da ne overidenem class = 'xxx yyy' 
            for (var prop in this.tagCtx.props) {
                input.attr(prop.replace('_', '-'), self.tagCtx.props[prop]);
            }

            if (deviceapi.useNativeCalendar) {

                input.attr('type', 'text');
                input.addClass('nativedatepicker');
                
            } else {
                input.attr('type', 'date');

            }

            //for (var prop in this.tagCtx.props) {

            //    if ("class".toLocaleLowerCase() == prop.toLocaleLowerCase()) {
            //        input.addClass(self.tagCtx.props[prop]);
            //    } else {
            //        input.attr(prop.replace('_', '-'), self.tagCtx.props[prop]);
            //    }

            //}
            var parent = input.wrap('<div></div>').parent();
            parent.append('<span class="highlight"></span><span class="bar"></span>');
            return parent.html();
        }
    })
});