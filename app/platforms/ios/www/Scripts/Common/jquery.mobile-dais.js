﻿define([
  'jquery',
  'jquerymobile',
  'utils',
  'diagnostics'
], function ($, jquerymobile, utils, diagnostics) {

    //daispopup - popup wrapper:  remove popup element from page element if original container is removed
    jQuery.widget("mobile.daispopup", jQuery.mobile.popup, {
        _create: function () {
            jQuery.mobile.popup.prototype._create.apply(this, arguments);
        },
        _init: function () {
            var self = this;
            jQuery.mobile.popup.prototype._init.apply(this, arguments);
            this._ui.placeholder.one("remove", function () {
                diagnostics.Debug("mobile.daispopup: call destory on popup");
                self._destroy();
            });
        },
        opened: function () {
            return this._isOpen;
        }
    });

    jQuery(document).bind("pagecreate", function (e) {
        jQuery(document).trigger("daispopupbeforecreate");
        return jQuery(":jqmData(role='daispopup')", e.target).daispopup();
    });

    jQuery(document).bind("create", function (e) {
        jQuery(document).trigger("daispopupbeforecreate");
        return jQuery(":jqmData(role='daispopup')", e.target).daispopup();
    });


    
    $.widget("ui.loadingpanel", {
        options: {
            loadingPanelTemplate: function () {
                var progressPanel = $('<div class="ui-loadingpanel"></div>');
                var image = $('<span class="ui-loadingpanel-image"><img src="Content/images/loading.gif" /><span>');
                progressPanel.append(image);
                return progressPanel;
            },
            loadingPanelSelector: function ($element) {
                return $element.find(".ui-loadingpanel:first");
            },
            refreshOnResize: true,
            show: true,
        },
        _destoryFunctions: [],
        _create: function () {
            var self = this;

            self.$element = $(self.element);
            self.loadingpanel_count = 0;
            
            var panel = self.$element;

            var loadingPanel = $(this.options.loadingPanelTemplate.call(panel));

            loadingPanel.addClass("ui-loadingpanel-hidden");

            panel.append(loadingPanel);
            

            if (this.options.show)
            {
                this.show();
            }
        },
        _show: function () {
            var self = this;
            
            self.options.loadingPanelSelector(this.$element).removeClass("ui-loadingpanel-hidden");
            self.options.loadingPanelSelector(this.$element).addClass("ui-loadingpanel-active");
        },
        _refresh: function () {
            var self = this;

            if (self._refreshing)
            {
                return;
            }
            try
            {
                self._refreshing = true;

                var panel = $(this.element);

                var loadingPanel = self.options.loadingPanelSelector(this.$element);
            }
            finally
            {
                self._refreshing = false;
            }
        },
        show: function () {
            var self = this;

            if (self.loadingpanel_count == 0) {
                this._show();
                self.loadingpanel_count = 1;
            }
            else {
                self.loadingpanel_count += 1;
            }
            
            diagnostics.Debug("ui.loadingpanel: show "+ self.loadingpanel_count);

            this._trigger("onshow");

            return (function () {
                var hidecall = false;
                return function () {
                    if (!hidecall) {
                        self.hide();
                        hidecall = true;
                    }
                }
            })()
        },
        hide: function () {
            var self = this;

                diagnostics.Debug("ui.loadingpanel: hide " + self.loadingpanel_count);
                if (self.loadingpanel_count <= 0) {
                    return;
                }

                if (self.loadingpanel_count == 1) {
                    
                    self.options.loadingPanelSelector(this.$element).removeClass("ui-loadingpanel-active");
                    self.options.loadingPanelSelector(this.$element).addClass("ui-loadingpanel-hidden");

                    self.loadingpanel_count = 0;
                }
                else {
                    self.loadingpanel_count -= 1;
                };

                this._trigger("onhide");
        },
        destroy: function () {
            var self = this;

            self.hide();

            self._destoryFunctions.forEach(function (f) {
                f.call(self);
            });

            $.Widget.prototype.destroy.apply(this, arguments);
        },
    });
    
});