﻿define(
 function () {

     var ObjectStorage = {

         _storage: {},

         getItem: function (key) {
             return this._storage[key];
         },

         setItem: function (key, value) {
             this._storage[key] = value;

         },

         Clear: function () {
             this._storage = {};
         },

         removeItem: function (key) {
             delete this._storage[key];
         }

     }

     return ObjectStorage;
 });