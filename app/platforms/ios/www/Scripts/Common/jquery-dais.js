﻿define([
  'require',
  'jquery',
  'jquerymobile',
  'jsrender',
  'underscore',
  'text!Mobile/Templates/SubMenu/SubmenuTemplate.html',
  'knockout',
  'utils',
  'Common/cache/language',
  'i18n!Mobile/nls/strings'
],
    function (require, $, jquerymobile, $templates, _, submenuTemplate, ko, utils, language, strings) {
        $.GetLocalizedString = function (description) {

            var currentLang = language.GetCurrentLang();

            var localized = _.find(description, function (d) {return d.Lang == currentLang.id });
            return localized? localized.Value : '';
            

//            var res = _.find(description, function (objDescr) {
//                //return objDescr.Lang.toLowerCase() == navigator.language.toLowerCase();
//                
//                //            });
            if (res != null) {
                return res.Value;
            } else {
                if (description[0] != null) {
                    return description[0].Value;
                }
            }
            return '';
        }

        $.GetResString = function (resourceID) {
            return strings[resourceID];
        }

        $.widget("ui.loadingpanel", {
            options: {
                loadingPanelTemplate: function () {

                    var progressPanel = $('<div id="loading-indicator"><div class="loading-indicator"></div></div>');
                    //var image = $('<div class="loader"><div class="top"><span></span></div><div class="middle"></div><div class="bottom"><span></span></div></div>');
                    var image = $(' <div class="spinner"> <div class="bounce1"></div>  <div class="bounce2"></div>  <div class="bounce3"></div></div>');
                    //progressPanel.append(image);

                    return progressPanel;
                },
                loadingPanelSelector: function ($element) {
                    return $element.find("#loading-indicator:first");
                },
                refreshOnResize: true,
                show: true,
            },
            _destoryFunctions: [],
            _create: function () {
                var self = this;

                self.$element = $(self.element);
                self.loadingpanel_count = 0;

                var panel = self.$element;

                var loadingPanel = $(this.options.loadingPanelTemplate.call(panel));

                loadingPanel.addClass("loading-indicator-hidden");

                panel.append(loadingPanel);


                if (this.options.show) {
                    this.show();
                }
            },
            _show: function () {
                var self = this;

                self.options.loadingPanelSelector(this.$element).removeClass("loading-indicator-hidden");
                self.options.loadingPanelSelector(this.$element).addClass("ui-loadingpanel-active");
            },
            _refresh: function () {
                var self = this;

                if (self._refreshing) {
                    return;
                }
                try {
                    self._refreshing = true;

                    var panel = $(this.element);

                    var loadingPanel = self.options.loadingPanelSelector(this.$element);
                }
                finally {
                    self._refreshing = false;
                }
            },
            show: function () {
                var self = this;

                if (self.loadingpanel_count == 0) {
                    this._show();
                    self.loadingpanel_count = 1;
                }
                else {
                    self.loadingpanel_count += 1;
                }

                diagnostics.Debug("ui.loadingpanel: show " + self.loadingpanel_count);

                this._trigger("onshow");

                return (function () {
                    var hidecall = false;
                    return function () {
                        if (!hidecall) {
                            self.hide();
                            hidecall = true;
                        }
                    }
                })()
            },
            hide: function () {
                var self = this;

                diagnostics.Debug("ui.loadingpanel: hide " + self.loadingpanel_count);
                if (self.loadingpanel_count <= 0) {
                    return;
                }

                if (self.loadingpanel_count == 1) {

                    self.options.loadingPanelSelector(this.$element).removeClass("ui-loadingpanel-active");
                    self.options.loadingPanelSelector(this.$element).addClass("loading-indicator-hidden");

                    self.loadingpanel_count = 0;
                }
                else {
                    self.loadingpanel_count -= 1;
                };

                this._trigger("onhide");
            },
            destroy: function () {
                var self = this;

                self.hide();

                self._destoryFunctions.forEach(function (f) {
                    f.call(self);
                });

                $.Widget.prototype.destroy.apply(this, arguments);
            },
        });

        $.widget("mobile.loader", $.ui.loadingpanel, {});

        jQuery.extend({
            
            DeferredEx: (function () {
                var globalExceptions = jQuery.Callbacks();
                var _innerException = 0;
                deferreddefinition =
                 function (func) {
                    var tuples = [
                            // action, add listener, listener list, final state
                            ["resolve", "done", jQuery.Callbacks("once memory"), "resolved"],
                            ["reject", "fail", jQuery.Callbacks("once memory"), "rejected"],
                            ["rejectWithException", "exception", jQuery.Callbacks("once memory"), "rejectedWithException"],
                            ["notify", "progress", jQuery.Callbacks("memory")]
                    ],
                        state = "pending",
                        promise = {
                            state: function () {
                                return state;
                            },
                            always: function () {
                                //deferred.done(arguments).fail(arguments);
                                //return this;
                                deferred.done.apply(deferred, arguments).fail.apply(deferred, arguments);
                                return this;
                            },
                            then: function ( /* fnDone, fnFail, fnProgress */) {
                                var fns = arguments;
                                return jQuery.DeferredEx(function (newDefer) {
                                    jQuery.each(tuples, function (i, tuple) {
                                        var action = tuple[0],
                                            fn = fns[i];
                                        // deferred[ done | fail | progress ] for forwarding actions to newDefer
                                        deferred[tuple[1]](jQuery.isFunction(fn) ?
                                            function () {
                                                try
                                                {
                                                    var returned = fn.apply(this, arguments);
                                                }
                                                catch(e)
                                                {
                                                    newDefer.rejectWithException(e);
                                                }
                                                if (returned && jQuery.isFunction(returned.promise)) {
                                                    var tmppromise = returned.promise()
                                                    tmppromise.done(newDefer.resolve)
                                                    tmppromise.fail(newDefer.reject)
                                                    tmppromise.progress(newDefer.notify)
                                                    if (tmppromise.exception) {
                                                        tmppromise.exception(newDefer.rejectWithException);
                                                    }
                                                } else {
                                                    newDefer[action + "With"](this === deferred ? newDefer : this, [returned]);
                                                }
                                            } :
                                            newDefer[action]
                                        );
                                    });
                                    fns = null;
                                }).promise();
                            },
                            // Get a promise for this deferred
                            // If obj is provided, the promise aspect is added to the object
                            promise: function (obj) {
                                return obj != null ? jQuery.extend(obj, promise) : promise;
                            }
                        },
                        deferred = {};

                    // Keep pipe for back-compat
                    promise.pipe = promise.then;

                    // Add list-specific methods
                    jQuery.each(tuples, function (i, tuple) {
                        var list = tuple[2],
                            stateString = tuple[3];

                        // promise[ done | fail | progress ] = list.add
                        promise[tuple[1]] = list.add;

                        // Handle state
                        if (stateString) {
                            list.add(function () {
                                // state = [ resolved | rejected ]
                                state = stateString;

                                // [ reject_list | resolve_list ].disable; progress_list.lock
                            }, tuples[i ^ 1][2].disable, tuples[2][2].lock);
                        }

                        // deferred[ resolve | reject | notify ] = list.fire
                        deferred[tuple[0]] = list.fire;
                        deferred[tuple[0] + "With"] = list.fireWith;
                    });


                    // Make the deferred a promise
                    promise.promise(deferred);

                    deferred.exception(function (e) {
                        _innerException = _innerException + 1;
                        try
                        {
                            globalExceptions.fireWith(deferred, [e, _innerException > 1 ]);
                        }
                        finally
                        {
                            _innerException = _innerException -1;
                        }
                        deferred.reject();
                    });

                    deferred.catchException = function (f) {
                        if ($.isFunction(f))
                        {
                            try
                            {
                                f.call(deferred, deferred)
                            }
                            catch (eex)
                            {
                                deferred.rejectWithException(eex);
                                diagnostics.Debug(eex);
                            }
                        }
                    }

                    deferred.bindTo = function (source) {
                        source.done(deferred.resolve);
                        source.fail(deferred.reject);
                        source.progress(deferred.notify);
                        if (source.exception)
                            source.exception(deferred.rejectWithException);
                    }
                                                        

                    // Call given func if any
                    if (func) {
                        deferred.catchException(func);
                    }

                    // All done!
                    return deferred;
                 }
                deferreddefinition.exception = globalExceptions.add;
                return deferreddefinition;
            })()
        ,

            // Deferred helper
            whenEx: function (subordinate /* , ..., subordinateN */) {
                var i = 0,
                    resolveValues = Array.prototype.slice.call(arguments),
                    length = resolveValues.length,

                    // the count of uncompleted subordinates
                    remaining = length !== 1 || (subordinate && jQuery.isFunction(subordinate.promise)) ? length : 0,

                    // the master Deferred. If resolveValues consist of only a single Deferred, just use that.
                    deferred = remaining === 1 ? subordinate : jQuery.DeferredEx(),

                    // Update function for both resolve and progress values
                    updateFunc = function (i, contexts, values) {
                        return function (value) {
                            contexts[i] = this;
                            values[i] = arguments.length > 1 ? Array.prototype.slice.call(arguments) : value;
                            if (values === progressValues) {
                                deferred.notifyWith(contexts, values);
                            } else if (!(--remaining)) {
                                deferred.resolveWith(contexts, values);
                            }
                        };
                    },

                    progressValues, progressContexts, resolveContexts;

                // add listeners to Deferred subordinates; treat others as resolved
                if (length > 1) {
                    progressValues = new Array(length);
                    progressContexts = new Array(length);
                    resolveContexts = new Array(length);
                    for (; i < length; i++) {
                        if (resolveValues[i] && jQuery.isFunction(resolveValues[i].promise)) {
                            var tmppromise = resolveValues[i].promise()
                            tmppromise.done(updateFunc(i, resolveContexts, resolveValues));
                            tmppromise.fail(deferred.reject);
                            tmppromise.progress(updateFunc(i, progressContexts, progressValues));
                            if (tmppromise.exception) {
                                tmppromise.exception(deferred.rejectWithException);
                            }
                                
                        } else {
                            --remaining;
                        }
                    }
                }

                // if we're not waiting on anything, resolve the master
                if (!remaining) {
                    deferred.resolveWith(resolveContexts, resolveValues);
                }

                return deferred.promise();
            }
        });
        $.DeferredEx.exception(function () {   })
        
        $.widget("ui.submenu", {
            options: {
                submenuItems: null,
                submenuID: 'submenu'

            },
            _submenu: function (id, submenuitems) {
                var model = this;
                var self = this;

                model.Items = submenuitems.Items;
                model.ID = id;
                self.IsLoading = ko.observable(false);

                model.ClickHandler = function (data) {
                    if (!self.IsLoading()) {
                        self.IsLoading(true);
                        var res = $.DeferredEx();
                    
                        var controllerSourceFile = 'Mobile/Controllers/' + data.Controller;

                        require([controllerSourceFile], function (controller) {

                            var cntrlr = new controller();
                        
                            var params = [] ; 
                            if (data.Params){
                                if (Array.isArray(data.Params))
                                    params = data.Params;
                            }

                            var viewResult = cntrlr[data.Action].apply(cntrlr, params);

                            //var viewResult = cntrlr.handlePath(path);
                            var createTabProcess =
                                $.whenEx(
                                    viewResult
                                )
                                .then(function (viewResult) {
                                    return viewResult.execute();
                                })
                                .then(function (executedViewResult) {

                                    var $resultContainer = $(self.element).find('#{0}_container'.format(id));
                                    $resultContainer.addClass('daisui-submenucontainer');

                                    $resultContainer.html(executedViewResult.$el);
                                    executedViewResult.$el.addClass("controller-" + data.Controller.toLowerCase());
                                    executedViewResult.$el.addClass("action-" + data.Action.toLowerCase());

                                    utils.triggerMobileCreate({ data: executedViewResult.viewModel, selector: $resultContainer });

                                    $(self.element).find("a").removeClass("active");
                                    $(self.element).find("a.{0}".format(data.liCss)).addClass("active");

                                    //move footer from submenu as sibling of content div
                                    var contentContainer = $(".content").parent();
                                    contentContainer.children(".footer").remove();
                                    $(self.element).find(".footer").appendTo(contentContainer);
                                })
                                .always(function () {
                                    self.IsLoading(false);
                                });

                            res.bindTo(createTabProcess);
                        });

                        return res;
                    }
                }

                return model;
            },

            _create: function () {
                var self = this;

                var submenuItems = this.options['submenuItems'];
                var submenuID = this.options['submenuID'];
                var submenuViewModel = this._submenu(submenuID, submenuItems);

                this.element.addClass('daisui-submenu');

                $templates({ submenuTempl: submenuTemplate });
                var resHtml = $($.render.submenuTempl(submenuViewModel));

                this.element.append(resHtml);
                
                utils.applyBindingChildren({ data: submenuViewModel, selector: this.element });

                var hide = null;
                var donotshow = false;
                setTimeout(function () {
                    if (!donotshow)
                    {
                        hide = $("body").loadingpanel("show")
                    }
                }, 0)

                submenuViewModel
                    .ClickHandler(submenuViewModel.Items[0])
                    .always(function () {
                        if (hide)
                            hide();
                        donotshow = true;
                        self._trigger("initialized");
                    })
                    .done(function () {
                        //slagame class za da e izbran pavriq tab.
                        $(resHtml).find('li a:first').addClass('ui-btn-active');
                    });
            },

            destroy: function () {
                $.Widget.prototype.destroy.apply(this, arguments); // default destroy
                // now do other stuff particular to this widget

            }
        });

       

        var scrollHelpersV2 = {

            scrollHandler: function (element, action) {
                var b = $(element);
                var w = element == $("body")[0] ? $(window) : $(element);
    
                var pipe = $.DeferredEx();
                pipe.resolve();

                return function () {
                    
                    var innerHeight = $(element)[0].scrollHeight;
                    var loadedFor = $(element).data("daisScroll_loadedFor") || 0;

                    if ((b.scrollTop() + w.height() >= innerHeight - 100)
                        && loadedFor < b.scrollTop()) {
                        
                        var exec = function () {
                            var res = null;

                            if ($(element).data("paused"))
                                return;

                            var innerHeight = $(element)[0].scrollHeight;
                            var loadedFor = $(element).data("daisScroll_loadedFor") || 0;

                            if ((b.scrollTop() + w.height() >= innerHeight - 100)
                                && loadedFor < b.scrollTop()) {
                                $(element).data("daisScroll_loadedFor", b.scrollTop());
                                res = action.call(this);
                            }


                            if (!res) {
                                res = $.DeferredEx();
                                res.resolve();
                            }

                            return res;
                        }

                        pipe = pipe.pipe(exec);
                    }
                };
            },

            fill: function (element, action, oldHeight) {
                var b = $(element);
                var w = element == $("body")[0] ? $(window) : $(element);

                oldHeight = oldHeight || 0;

                if ($(element).data("paused"))
                    return;

                if (element.scrollHeight)
                {
                    if (b[0].scrollHeight < (w.innerHeight() + 150)
                        && $(element).height() >= oldHeight)
                    {
                        $.whenEx(
                                action.call(this)
                        )
                        .done(function () {
                            scrollHelpersV2.fill(element, action, $(element).height())
                        });
                    }
                }
            },

            reset: function (element, action)
            {
                $(element).data("daisScroll_loadedFor", 0);
                $(element).data("paused", false);

                scrollHelpersV2.fill(element, action)
            },

            register: function (element, action) {
                
                scrollHelpersV2.reset(element, action);

                var f = scrollHelpersV2.scrollHandler(element, action);
                action.__handler = f;

                var w = element == $("body")[0] ? $(window) : $(element);

                $(w).on('scroll', f);

            },

            unregister: function (element, action) {
                if (action.__handler) {

                    var w = element == $("body")[0] ? $(window) : $(element);

                    $(w).off('scroll', action.__handler);
                    $(element).data("paused",true);
                    delete action.__handler;
                }
            },
            pause: function (element) {
                $(element).data("paused", true);
            },
            resume: function (element)
            {
                $(element).data("paused", false);
                $(element).trigger('scroll');
            }
        }


        $.fn.daisScrollV2 = function (action, option) {
            //scrollHelpersV2[action].call(this[0], this[0], option);
            scrollHelpersV2[action].call($("body")[0], $("body")[0], option);
        }

        return $;
    }
);
