﻿define([
  "diagnostics",
  "cryptoCore",
  "session",
  "Mobile/Common/security/encryptor"
], function (diagnostics, cryptoCore, session, encryptoHelper) {

    function encryptor() {

    }


    function generatePassword() {
        if (currentStateContainer == null)
            return null;

        var res = cryptoCore.enc.Utf8.parse(currentStateContainer.SessionKey);
        res.concat(cryptoCore.enc.Utf8.parse(currentStateContainer.UserID));
        res.concat(currentStateContainer.PasswordHash);

        return res;
    }

    function generateKey() {
        if (currentStateContainer == null)
            return null;

        if (!currentStateContainer.calculated) {

            var password = generatePassword();
            if (password == null)
                return null

            var endkey = diagnostics.Time("Computating key");

            currentStateContainer.calculated = cryptoCore.PBKDF2(
                    password,
                    cryptoCore.enc.Base64.parse("KIX1GAUxWLhFwt/bef2Z/g=="),
                    { keySize: 256 / 32, iterations: 111 }
                );

            endkey();

        }

        return currentStateContainer.calculated;
    }

    encryptor.prototype._getBodyHASH = function (body) {
        return cryptoCore.SHA512(body);
    }

    encryptor.prototype._formatEncrypted = function (encrypted) {
        var enc = cryptoCore.enc.Base64.stringify(encrypted.ciphertext);
        var iv = cryptoCore.enc.Base64.stringify(encrypted.iv);
        return enc + "|" + iv;
    }

    var currentStateContainer = null

    encryptor.prototype.ClearState = function () {
        currentStateContainer = null;
    }

    encryptor.prototype.GetHashedPaswordAndSuccessStateHadler = function (userNameAndPassword) { //{Password: , LoginID:}
        this.ClearState();

        var passbytes = cryptoCore.enc.Utf16LE.parse(userNameAndPassword.Password);
        var passhash = cryptoCore.MD5(passbytes);

        var usernamebytes = cryptoCore.enc.Utf16LE.parse(encryptoHelper.TransformLoginForCryptoParse(userNameAndPassword.LoginID));

        var finalPass = cryptoCore.SHA512(passhash.clone().concat(usernamebytes));
        var finalPassText = cryptoCore.enc.Base64.stringify(finalPass);

        var result = {
            LoginID: userNameAndPassword.LoginID,
            Password: finalPassText,
            Success: function (sessionObject) {
                currentStateContainer = {};
                currentStateContainer.PasswordHash = passhash;
                currentStateContainer.SessionKey = sessionObject.SessionKey;
                currentStateContainer.UserID = sessionObject.UserID;
                //SignOnRs nqma property LoginID
                currentStateContainer.LoginID = userNameAndPassword.LoginID; //sessionObject.LoginID
            }
        }

        //var result = $.extend(
        //            {},
        //            userNameAndPassword,
        //            {
        //                Success: function (sessionObject) { }
        //            }
        //        );
        return result;
    }

    encryptor.prototype.Sign = function (requestBody) {

        var key = generateKey();

        if (key == null) {
            return null;
        }

        var endenc = diagnostics.Time("Computating encription");

        var data = this._getBodyHASH(cryptoCore.enc.Utf8.parse(requestBody));

        var encrypted = cryptoCore.AES.encrypt(data, key, { iv: cryptoCore.lib.WordArray.random(16) });
        var encryptedFormated = this._formatEncrypted(encrypted)

        endenc();

        return {
            $type: 'DAISSymmetricSignature',
            Signature: encryptedFormated
        };
    }

    encryptor.prototype.ResetPassword = function (newPassword) {
        var passbytes = cryptoCore.enc.Utf16LE.parse(newPassword);
        var passhash = cryptoCore.MD5(passbytes);
        var newState = {
            PasswordHash: passhash,
            SessionKey: currentStateContainer.SessionKey,
            UserID: currentStateContainer.UserID,
            LoginID: currentStateContainer.LoginID
        }
        currentStateContainer = newState;
    }

    encryptor.prototype.ResetLoginID = function (newLoginID) {
        var newState = {
            PasswordHash: currentStateContainer.PasswordHash,
            SessionKey: currentStateContainer.SessionKey,
            UserID: currentStateContainer.UserID,
            LoginID: newLoginID
        }
        currentStateContainer = newState;
    }

    encryptor.prototype.GetLoginID = function () {
        return currentStateContainer.LoginID;
    }

    return encryptor;
});