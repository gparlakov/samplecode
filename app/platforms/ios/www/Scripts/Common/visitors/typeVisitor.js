﻿define([
    'knockout',
    'underscore',
    'servicesObjects',
], function (ko, _, servicesObjects) {

    var TypeVisitor = {
        visit: function (obj, visitors, typeSelector) {

            var type = null;
            if (typeSelector != null) {
                type = typeSelector(obj).$type;
            } else {
                type = obj.$type;
            }

            if (ko.isObservable(type)) {
                type = type();
            }

            var resultType = null;

            if (type != null) {
                var t = type.split(',');

                resultType = servicesObjects.Utils.GetShortTypeNameByType(type);
            }

            var visitorCallResult = null;
            if (resultType != null && visitors[resultType]) {
                if (_.isFunction(visitors[resultType])) {
                    visitorCallResult = visitors[resultType].call(this, obj);
                }
                else {
                    visitorCallResult = visitors[resultType];
                }
            }
            else {
                if (visitors.Other) {
                    if (_.isFunction(visitors.Other)) {
                        visitorCallResult = visitors.Other.call(this, obj);
                    }
                    else {
                        visitorCallResult = visitors.Other;
                    }
                }
            }

            return visitorCallResult;
        }

        //    visitor.visit(obj, {
        //        On_CreditCard: function (cc) { },
        //        On_DevitCard: function (cc) { },
        //        On_CreditCard: function (cc) { },
        //        Other: function () { }
        //    })
    }
    return TypeVisitor;
});