﻿/// <reference path="libs/jquery/jquery-1.7.2.js" />
/// <reference path="libs/knockout/knockout-2.1.0.debug.js" />
/// <reference path="libs/knockout/knockout.mapping-2.2.4.js" />
/// <reference path="libs/knockout/knockout.mapping-2.2.4.js" />
/// <reference path="libs/underscore/underscore-1.3.3.js" />
/// <reference path="libs/backbone/backbone-0.9.2.js" />


var maps = {
    'knockout': ko,
    'underscore': _,
    'jquery': $,
    'backbone': Backbone,
    'komapping': ko.mapping
}



!function (window) {
    var defines = [],
        moduleUrls = [],
        oldDefine = window.define,
        oldRequire = window.require,
        oldLoad = requirejs.load;

    var loadEvent = document.createEvent("event");
    loadEvent.type = "load";

    // Ensure that we're only patching require/define
    // if RequireJS is the current AMD implementation
    //if (window.require !== window.requirejs)
    //    return;
  
    //unkonwn module
    var unkonwn = {};

    //do not use this
    var donot = {};

    window.define = function (deps, callback) {
        
        oldRequire.call(window, deps, callback);

        window.intellisense.logMessage("--------------------");
        
        window.intellisense.logMessage(typeof (window.symbolHelp));

        window.intellisense.logMessage("--------------------");
        var a = {};
        
        params = [];

        deps.forEach(function (depdef) {
            window.intellisense.logMessage(depdef);
            if (maps[depdef]) {
                params.push(maps[depdef]);
                window.intellisense.logMessage(depdef + " mapped");
            }
            else {
                params.push(unkonwn);
                window.intellisense.logMessage(depdef + " not mapped");
            }
        })
        window.intellisense.logMessage(params);
        //for (var def in deps)
        //{
        //    //window.intellisense.logMessage(a, "building intelisence for " + def);
        //    //if (maps[def]) {
        //    //    params.push(map[def]);
        //    //}
        //    //params.push(unkonwn);
        //}

        intellisense.setCallContext(callback, { thisArg: donot, args: params })
    }


    // Redirect all of the patched methods back to their originals
    // so Intellisense will use the previously defined annotations
    intellisense.redirectDefinition(requirejs.load, oldLoad);
    intellisense.redirectDefinition(window.define, oldDefine);
    intellisense.redirectDefinition(window.require, oldRequire);
}(this);

