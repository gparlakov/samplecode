﻿define(['jquery'], function ($) {
    $(document).bind("mobileinit", function () {
        $.mobile.ajaxEnabled = false;
        $.mobile.linkBindingEnabled = false;
        $.mobile.hashListeningEnabled = false;
        $.mobile.pushStateEnabled = false;
        $.mobile.defaultPageTransition = 'none';
        //$.mobile.trackPersistentToolbars = false;
        //$.mobile.defaultDialogTransition = 'pop';
//        $.mobile.pageContainer = $('#body_container');
        $.mobile.isPageRemoveAllowed = true;

        //$.extend($.mobile.fixedtoolbar.prototype.options, {
        //    updatePagePadding: false,
        //    tapToggle: false

        //})
        $('div[data-role="page"]').live('pagehide', function (event, ui) {
            if ($.mobile.isPageRemoveAllowed) {
                $(event.currentTarget).remove();
            }
        });
    });

});



