﻿// Filename: libs/underscore/underscore
// As above lets load the original underscore source code
define(['libs/underscore/underscore-1.6.0'], function () {
	// Tell Require.js that this module returns  a reference to Underscore
	return _;
});