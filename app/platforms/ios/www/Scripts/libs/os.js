define([
    'config'
], function (config) {

    var osLoader = {

        CommonPath: "Mobile/Common/OSspecific/",

        getPathToMobule: function (fileName) {
            return this.CommonPath + config.DeviceOSType + "/" + fileName

        },

        load: function (name, req, load, config) {

            var i, m, toLoad;

            toLoad = this.getPathToMobule(name);
            req([toLoad], load);
        }

     };

    return osLoader
});