﻿define([
// Load the original jQuery source file
  'libs/jquery.min/jquery.format-1.2'
], function () {
    // Tell Require.js that this module returns a reference to jQuery
    
        jQuery.format.locale({
            date: {
                format: 'dd.MM.yyyy',
                monthsFull: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                daysFull: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                timeFormat: 'hh:mm tt',
                shortDateFormat: 'dd.MM.yyyy',
                longDateFormat: 'dddd, MMMM dd, yyyy'
            },
            number: {
                // tova moje da se naloji da go zamenim s #,##0.00 pri update kam jqformat 1.3 
                // kam momenta #,##0.00 ne formatira pravilno.
                format: '#,###.00',
                groupingSeparator: ' ',
                decimalSeparator: '.'
            }
        });
    return jQuery.format;
});