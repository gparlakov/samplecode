﻿define(['jquery'], function ($) {
    $(document).bind("mobileinit", function () {
        $.mobile.ajaxEnabled = false;
        $.mobile.linkBindingEnabled = false;
        $.mobile.hashListeningEnabled = false;
        $.mobile.pushStateEnabled = false;
        $.mobile.defaultPageTransition = 'none';
        //$.mobile.autoInitializePage = false; //спира Global initialization of the library.
        //$.mobile.trackPersistentToolbars = false;
        //$.mobile.defaultDialogTransition = 'pop';
//        $.mobile.pageContainer = $('#body_container');
        $.mobile.isPageRemoveAllowed = true;

        //$.extend($.mobile.fixedtoolbar.prototype.options, {
        //    updatePagePadding: false,
        //    tapToggle: false

        //})
        $("body").on("pagehide", "div[data-role='page']", function (event, ui) {
            if ($.mobile.isPageRemoveAllowed) {
                $(event.currentTarget).remove();
            }
        })
        //$('div[data-role="page"]').on('pagehide', function (event, ui) {
        //    if ($.mobile.isPageRemoveAllowed) {
        //        $(event.currentTarget).remove();
        //    }
        //    alert(1);
        //});
    });

});



