﻿define([
    'jquery', 
    'daisJson'
    ],
function ($, daisJson) {

    $(document).bind("mobileinit", function () {
        $.ajaxSetup({
            converters: { "text json": daisJson.daisJsonDeserialize }
        })
    });

});



