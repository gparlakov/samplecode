﻿define([
// Load the original jQuery source file
  'libs/jquery.fake/jquery-ui-1.10.4.custom.min'
], function () {
	// Tell Require.js that this module returns a reference to jQuery
    return jQuery.ui;
});