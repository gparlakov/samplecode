define([
  'cache',
  'underscore',
  'typeVisitor',
  'diagnostics'
], function (cache, _, typeVisitor, diagnostics) {

    function CachePrecalculatedData(ap)
    {
        this.ap = ap;
    }

    CachePrecalculatedData.prototype._getMaps = function() {
        if (!this.ap.__maps)
            this.ap.__maps = {};
        return this.ap.__maps;
    }

    CachePrecalculatedData.prototype._createMapBankAccountToAPAccount = function() 
    {
        var maps = this._getMaps();
        var accountmap = {};
        maps["batoapa"] = accountmap;

        if (this.ap.APAccounts)
        {
            _.each(this.ap.APAccounts, function (account) {
                accountmap[account.BankAccount.BankAccountID] = account;
            })
        }
    }
    CachePrecalculatedData.prototype._getMapBankAccountToAPAccount = function () {
        var maps = this._getMaps();
        if (!maps["batoapa"])
            this._createMapBankAccountToAPAccount();

        return maps["batoapa"];
    }

    CachePrecalculatedData.prototype.GetAPAccountByBankAccountID = function(bankAccount)
    {
        return this._getMapBankAccountToAPAccount()[bankAccount];
    }

    CachePrecalculatedData.prototype._createMapBankClientToAPBankClient = function () {
        var maps = this._getMaps();
        var accountmap = {};
        maps["bctoapbc"] = accountmap;

        if (this.ap.APBankClients) {
            _.each(this.ap.APBankClients, function (apbankclient) {
                accountmap[apbankclient.BankClient.BankClientID] = apbankclient;
            })
        }
    }
    CachePrecalculatedData.prototype._getMapBankClientToAPBankClient = function () {
        var maps = this._getMaps();
        if (!maps["bctoapbc"])
            this._createMapBankClientToAPBankClient();

        return maps["bctoapbc"];
    }

    CachePrecalculatedData.prototype.GetAPBankClientByBankClientID = function(bankClientID)
    {
        return this._getMapBankClientToAPBankClient()[bankClientID];
    }


    var ProductsCache = {

        _apersonStorageKey: 'aperson',
        
        GetAPerson: function () {
            var ap = cache.GetObject(this._apersonStorageKey);
            return ap;
        },

        InitProductCache: function (aperson) {
            cache.SetObject(this._apersonStorageKey, aperson);
        },


        GetAPAccountByID: function (bankAccountID) {
            //var ap = this.GetAPerson();
            //var apAcc = _.find(ap.APAccounts, function (apa) { return apa.BankAccount.BankAccountID == bankAccountID; });

            //apAcc.BankAccount.BankClient = this.GetBankClientByID(apAcc.BankAccount.BankClient.BankClientID);

            //return apAcc;

            var ap = this.GetAPerson();
            var map = new CachePrecalculatedData(ap);

            var apa = $.extend({}, map.GetAPAccountByBankAccountID(bankAccountID));

            var apBnkClient =  map.GetAPBankClientByBankClientID(apa.BankAccount.BankClient.BankClientID);
            if(apBnkClient && apBnkClient.BankClient){
                $.extend(apa.BankAccount.BankClient, apBnkClient.BankClient);
            }
            return apa;
        },

        GetBankClientByID: function (bankClientID) {
            var ap = this.GetAPerson();
            var APbnkBcl = _.find(ap.APBankClients, function (apBnkBcl) { return apBnkBcl.BankClient.BankClientID == bankClientID });
            if (APbnkBcl)
                return APbnkBcl.BankClient;
            else
                return null;
        },

        SetBankAccountToBankCard: function (bankCard) {
            var targetBankAccount = this.GetAPAccountByID(bankCard.BankAccount.BankAccountID);
            if (targetBankAccount != null) {

                bankCard.BankAccount = targetBankAccount.BankAccount;
            }    
        },

        //popalva bankovi klienti v spisak ot APAccount-i .
        //ako APA-tata ni doidadat ot nqkoi serviz nepopalneni (naprimer beneficienti pri plashtaniq) 
        FillBankClientsForAPAccounts : function(apas) {
            var ap = this.GetAPerson();
            var map = new CachePrecalculatedData(ap);

            _.forEach(apas, function (apa) {
                var apBnkClient = map.GetAPBankClientByBankClientID(apa.BankAccount.BankClient.BankClientID);
                if (apBnkClient && apBnkClient.BankClient) {
                    $.extend(apa.BankAccount.BankClient, apBnkClient.BankClient);
                }
                return apa;
            });
        },

        RefreshBankAccountBalances: function (bankAccountBalancesList) {
            if (_.isArray(bankAccountBalancesList)) {
                var ap = this.GetAPerson();
                var map = new CachePrecalculatedData(ap);
                

                _.each(bankAccountBalancesList, function (balance) {
                    var apa = map.GetAPAccountByBankAccountID(balance.BankAccount.BankAccountID);
                    apa.BankAccount.BankAccountBalances = [];
                    apa.BankAccount.BankAccountBalances.push(balance);
                })
                diagnostics.Debug("RefreshBankAccountBalances")
            }
        },

        RefreshCacheByReturnedResponse: function (response) {
            var self = this;
            typeVisitor.visit(
                response,
                {
                    "PaymentWizardInitResponse": function (response) {
                        self.RefreshBankAccountBalances(
                            _.compact(
                                _.map(
                                    response.PaymentWizardStepOutput.AllowedAccounts, 
                                    function(acc) { return acc.BankAccount.BankAccountBalances ? acc.BankAccount.BankAccountBalances[0] : null }
                                )
                            )
                        );
                    },
                    "GetProductBalancesResponse": function (response) {
                        typeVisitor.visit(
                            response.BalancesFilter,
                            {
                                "BankAccountBalancesFilterCurrent": function (filter) {
                                    self.RefreshBankAccountBalances(response.Balances)
                                }
                            }
                        )
                    },
                    "PaymentWizardGetDataResponse": function (response) {
                        typeVisitor.visit(
                            response.PaymentWizardDetachedStepData,
                            {
                                "PaymentWizardChoosePayeeDataAPAccounts": function (stepData) {
                                    self.RefreshBankAccountBalances(
                                        _.compact(
                                            _.map(
                                                stepData.Accounts,
                                                function (acc) { return acc.BankAccount.BankAccountBalances ? acc.BankAccount.BankAccountBalances[0] : null }
                                            )
                                        )
                                    );
                                }
                            });
                    }
                }
            )
        }
    }

    return ProductsCache;
});