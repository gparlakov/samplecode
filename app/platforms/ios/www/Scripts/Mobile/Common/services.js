define([
    'exports',
  'underscore',
  'jquery',
  'session',
  'productscache',
  'daisJson',
  'servicesObjects',
  'events',
  'config',
  'diagnostics',
  'deviceapi',
  'encryptor',
  'osManager',
  'i18n!Mobile/nls/strings',
], function (exports, _, $, session, productscache, daisJson, objects, events, config, diagnostics, deviceapi, encryptor, osManager, strings) {

    var errorHandlers = {
        invalidSessionNotificationOpened: false,
        "DAIS.Nightingale.Security.Session.CNightingaleInvalidSession": function () {

            if (!errorHandlers.invalidSessionNotificationOpened) {
                session.Clear();
                errorHandlers.invalidSessionNotificationOpened = true;
                deviceapi.notification.Confirm(

                    strings.ID_STR_SESSION_EXPIRED_TITILE,
                    strings.ID_STR_SESSION_EXPIRED_MESSAGE,
                    [
                        { ID: 'login', text: strings.ID_STR_LOGIN_BUTTON }
                    ]
                ).done(function () {
                    errorHandlers.invalidSessionNotificationOpened = false;
                    require(["utils"], function (utils) {
                        utils.navigate("Login");
                    })
                });
            }
        },
        "DAIS.Nightingale.Security.Session.CNightingaleExpiredSession": function () {
            errorHandlers["DAIS.Nightingale.Security.Session.CNightingaleInvalidSession"]();
        }
    }

    

    var errorNotifications = {
        systemError: function (isSilent) {
            if (!isSilent) {
                deviceapi.notification.Alert(strings.ID_STR_ERROR_TITLE, strings.ID_STR_ERROR_SYSTEM_ERROR, strings.ID_STR_OK);
            }
        },
        applicationError: function (errors) {
            var messages = "";
            $.each(errors, function () {
                messages += $.GetLocalizedString(this.Messages);
            })
            deviceapi.notification.Alert(strings.ID_STR_ERROR_TITLE, messages, strings.ID_STR_OK);
        },
        timeoutError: function () {
            deviceapi.notification.Alert(strings.ID_STR_ERROR_TIMEOUT_TITLE, strings.ID_STR_ERROR_TIMEOUT_SYSTEM_ERROR, strings.ID_STR_OK);
        },

        noConnectionNotification : null,

        noConnection: function () {
            var buttons = osManager.CurrentOS.closeAppIsAvailable ?
                       [
                           { ID: 'exit', text: strings.ID_STR_LOGOUT_BUTTON },
                           { ID: 'retry', text: strings.ID_STR_RETRY }
                       ] :
                       [{ ID: 'retry', text: strings.ID_STR_RETRY }];

            //otvarqme dialoga samo ako nqma otvoren. Inaceh davame otvoreniq, koito go iska da go polzva. 
            if (errorNotifications.noConnectionNotification == null) {
                errorNotifications.noConnectionNotification = deviceapi.notification.Confirm(
                    strings.ID_STR_ERROR_NOCONNECTION_TITLE,
                    strings.ID_STR_ERROR_NOCONNECTION_ERROR,
                    buttons
                ).done(function () {
                    errorNotifications.noConnectionNotification = null;
                });
            }
            return errorNotifications.noConnectionNotification
        },
    }

    var DAIS = {};

    DAIS.eBank = {};
    DAIS.eBank.Contracts = {};

    DAIS.eBank.Contracts.Behaviours = {
        Types: {
            SetSessionBehaviour: function SetSessionBehaviour() {
                //uglify2 premahva imeto ne funciqta i posle ne mojem da razchitame 
                //na nego za obrabotvane na kolekciqta s behavirour- i ;( ;( 
                this.name = 'SetSessionBehaviour';

                this.beforeSend = function (jqXHR, settings) {
                    session.CheckSession();
                    settings.data.Session = session.GetSession();
                };
                this.isSystem = true;
                this.order = 900;
            },
            InternetConnectionChecker: function () {
                //uglify2 premahva imeto ne funciqta i posle ne mojem da razchitame 
                //na nego za obrabotvane na kolekciqta s behavirour- i ;( ;( 
                this.name = 'InternetConnectionChecker';

                var self = this;
                this._checkinternet = function () {
                    var res =
                    $.DeferredEx(function (res) {
                        if (!deviceapi.state.hasInternet()) {
                            var dialog = errorNotifications.noConnection();
                            $.whenEx(
                                dialog
                            ).then(function (dialogres) {
                                alert(dialogres);
                                if (dialogres.ID == 'exit') {
                                    deviceapi.app.terminate();
                                } else {
                                    res.bindTo(self._checkinternet());
                                }
                            });
                        }
                        else {
                            res.resolve();
                        }
                    });
                    return res;
                };
                this.initialize = function (settings) {
                    return self._checkinternet();
                };
                this.isSystem = true;
                this.order = 0;
            },
            ObjectToText: function ObjectToText() {
                //uglify2 premahva imeto ne funciqta i posle ne mojem da razchitame 
                //na nego za obrabotvane na kolekciqta s behavirour- i ;( ;( 
                this.name = 'ObjectToText';

                this.beforeSend = function (jqXHR, settings) {
                    settings.data = daisJson.daisJsonSerialize(settings.data);

                    var enc = new encryptor();

                    var signature = enc.Sign(settings.data);

                    var signatureText = daisJson.daisJsonSerialize(signature);

                    if (!settings.headers)
                        settings.headers = {};

                    jqXHR.setRequestHeader("DAISSignature", signatureText);
                }
                this.order = 1000;
                this.isSystem = true;
            },
            HandleFatalError: function HandleFatalError(options) {
                //uglify2 premahva imeto ne funciqta i posle ne mojem da razchitame 
                //na nego za obrabotvane na kolekciqta s behavirour- i ;( ;( 
                this.name = 'HandleFatalError';

                var options = options || {};
                options.silentError = options.silentError || false;

                this.error = function (e) {
                    if (e.statusText == "timeout") {
                        errorNotifications.timeoutError(options.silentError);
                    }
                    else {
                        require(["diagnostics"], function (diagnostics) {
                            diagnostics.Error({ type: "ajax request error", error: e });
                            errorNotifications.systemError(options.silentError);
                        });
                    }
                }
            },
            HandleServiceError: function HandleServiceError(ignoreErrors) {
                //uglify2 premahva imeto ne funciqta i posle ne mojem da razchitame 
                //na nego za obrabotvane na kolekciqta s behavirour- i ;( ;( 
                this.name = 'HandleServiceError';

                ignoreErrors = ignoreErrors == null ? [] : ignoreErrors;
                this.visit = function (jqXHR) {
                    var res = new $.DeferredEx(function (res) {

                        jqXHR.then(function (data) {
                            if (data.Status) {
                                if (data.Status.StatusCode != objects.EnumStatusCode.Success
                                    && !_.contains(ignoreErrors, data.Status.Errors[0].InternalType)) {

                                    if (errorHandlers[data.Status.Errors[0].InternalType]) {
                                        errorHandlers[data.Status.Errors[0].InternalType]();
                                    }
                                    else {
                                        errorNotifications.applicationError(data.Status.Errors);
                                    }
                                    require(["diagnostics"], function (diagnostics) {
                                        diagnostics.Error({ type: "ajax request service error", error: data.Status });
                                    });
                                    res.reject();
                                }
                                else {
                                    res.resolve();
                                }
                            }
                        });
                    });
                    return res;
                }
            },
            CustomHandleServiceSuccess: function CustomHandleServiceSuccess() {
                //uglify2 premahva imeto ne funciqta i posle ne mojem da razchitame 
                //na nego za obrabotvane na kolekciqta s behavirour- i ;( ;( 
                this.name = 'CustomHandleServiceSuccess';

                this.success = function (data) {
                    alert('ServiceSuccess');
                }
            },
            LoadingIndicator: function LoadingIndicator(setIndicatorFunction) {
                //uglify2 premahva imeto ne funciqta i posle ne mojem da razchitame 
                //na nego za obrabotvane na kolekciqta s behavirour- i ;( ;( 
                this.name = 'LoadingIndicator';

                this.beforeSend = function () {
                    $("body").loadingpanel("show");
                };
                this.complete = function () {
                    $("body").loadingpanel("hide");
                };
                this.order = 1000;
            },
            ResultObjectVisitor: function ResultObjectVisitor() {
                //uglify2 premahva imeto ne funciqta i posle ne mojem da razchitame 
                //na nego za obrabotvane na kolekciqta s behavirour- i ;( ;( 
                this.name = 'ResultObjectVisitor';

                this.visit = function (jqXHR) {
                    jqXHR.done(function (result) {
                        if (result.Status.StatusCode == objects.EnumStatusCode.Success)
                            productscache.RefreshCacheByReturnedResponse(result)

                    })
                    return jqXHR;
                },
                this.isSystem = true;
                this.order = 2000
            },

        },
        Collection: function () {
            this._behaviours = [];
            this.Add = function (behaviour) {
                if (_.isArray(behaviour)) {
                    this._behaviours = _.union(behaviour, this._behaviours);
                }
                else {
                    this._behaviours.push(behaviour);
                }
            };
            this.Clear = function () {
                this._behaviours = _.filter(this._behaviours, function (b) { return b.isSystem; });
            };
            this.Remove = function (type) {
                this._behaviours = _.filter(this._behaviours, function (b) { return b.name != type.name });
            };
            this.Get = function () {
                return _.sortBy(this._behaviours, function (b) { return b.order ? b.order : 100; });
            };
        },
        Utils: {
            _callFunctionOnArray: function (list, name, args) {
                _.each(list, function (item) {
                    if (item[name] && _.isFunction(item[name])) {
                        item[name].apply(item, args);
                    }
                })
            },
            _getCallFunctionOnArray: function (list, name) {
                return function () {
                    DAIS.eBank.Contracts.Behaviours.Utils._callFunctionOnArray(list, name, arguments);
                }
            },
            AttachBehaviourToSettings: function (behavioursCollection, settings) {
                var result = $.DeferredEx(function (result) {
                    var behaviours = behavioursCollection.Get();
                    var initlist = []
                    $.each(behaviours, function () { if ($.isFunction(this.initialize)) initlist.push(this.initialize(settings)); });
                    $.whenEx.apply($.whenEx, initlist)
                    .then(function () {
                        var list = _.union(behavioursCollection.Get(), settings);
                        var newSettings = _.extend({}, settings);
                        _.each(["beforeSend", "error", "success", "complete"], function (name) {
                            newSettings[name] = DAIS.eBank.Contracts.Behaviours.Utils._getCallFunctionOnArray(list, name);
                        })
                        result.resolve(newSettings);
                    }, function () {
                        result.fail();
                    });
                });

                return result;


            },
            AttachBehaviourToResult: function (behavioursCollection, jqXHR) {
                var res = new $.DeferredEx(function (res) {
                    if (jqXHR) {
                        var p = jqXHR;
                        $.each(behavioursCollection.Get(), function (index) {
                            if ($.isFunction(this["visit"])) {
                                var f = this["visit"];
                                p = p.pipe(
                                    function () {
                                        return f.call(this, jqXHR);
                                    },
                                    function () {
                                        res.reject();
                                    }
                                );
                            }
                        });

                        p = p.pipe(
                            function () {
                                jqXHR.then(
                                    function (data) {
                                        res.resolve(data)
                                    });
                            },
                            function () {
                                res.reject();
                            }
                        );
                    }
                    else {
                        res.reject();
                    }
                });
                return res;
            }
        }
    }

    var ajaxCounter = 0;
    DAIS.eBank.Contracts.Utils = {

        _ajax: function (service, param, callback, settingsVisitor, jqXHRVisitor) {
            var self = this;

            if (ajaxCounter < 0) ajaxCounter = 0;
            var hide = null;
            var settings =
            {
                url: DAIS.eBank.Contracts.Names.URL + service,
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: param,
                timeout: config.ServicesTimeout,
                cache: false,
                error: function (data) {
                },
                processData: false,
                beforeSend: function (jqXHR, settings) {
                    if (typeof (settings.data) != "string") {
                        settings.data = daisJson.daisJsonSerialize(settings.data);
                    }
                },

                complete: function (jqXHR, textStatus) {
                    //if (hide)
                    //    hide();
                }
            };

            settingsVisitor = settingsVisitor || function (input) { return input; };
            var newSettings = settingsVisitor(settings)
            var res =
                $.whenEx(
                    newSettings
                )
                .then(function (settings) {
                    var res = $.ajax(settings);

                    if ($.isFunction(jqXHRVisitor)) {
                        res = jqXHRVisitor(res);
                    }

                    res.then(function (data) {
                        if ($.isFunction(callback)) {
                            callback.call(self, data);
                        }
                    });

                    return res;
                });

            return res;
        },

        _ajaxSession: function (service, param, callback, settingsVisitor) {
            var self = this;

            session.CheckSession();

            var intParam = $.extend({}, param, { Session: session.GetSession() });
            return this._ajax(service, intParam, callback, settingsVisitor);
        },
    }

    DAIS.eBank.Contracts.Names = {


        URL: config.ServicesBaseURL,


        "SessionService": {
            "Login": "/SessionService/Login",
            "Logout": "/SessionService/Logout",
            "AddSessionDetail": "/SessionService/AddSessionDetail",
            "CheckSession": "/SessionService/CheckSession"
        },
        "BankProductsService": {
            "GetProducts": "/BankProductsService/GetProducts"
        },
        "BankInformationService": {
            "GetCCYRatesCurrent": "/BankInformationService/GetCCYRatesCurrent"
        },
        "BankProductInfoService": {
            "GetProductBalances": "/BankProductInfoService/GetProductBalances",
            "GetProductMovements": "/BankProductInfoService/GetProductMovements",
            "GetProductMovementDocument": "/BankProductInfoService/GetProductMovementDocument"
        },
        "PaymentOrderService": {
            "GetPaymentOrdersArchive": "/PaymentOrderService/GetPaymentOrdersArchive",
            "GetPaymentOrdersPending": "/PaymentOrderService/GetPaymentOrdersPending",
            "SignAndSendPaymentOrders": "/PaymentOrderService/SignAndSendPaymentOrders",
            "SignPaymentOrders": "/PaymentOrderService/SignPaymentOrders",
            "SendPaymentOrders": "/PaymentOrderService/SendPaymentOrders"
        },

        "PaymentWizardService": {
            "PaymentWizardInit": "/PaymentWizardService/PaymentWizardInit",
            "PaymentWizardNext": "/PaymentWizardService/PaymentWizardNext",
            "PaymentWizardBack": "/PaymentWizardService/PaymentWizardBack",
            "PaymentWizardGetData": "/PaymentWizardService/PaymentWizardGetData"
        },

        "PaymentService": {
            "SaveNewPayment": "/PaymentService/SaveNewPayment",
            "SavePayment": "/PaymentService/SavePayment",
            "GetPayment": "/PaymentService/GetPayment",
            "DeletePayment": "/PaymentService/DeletePayment",
            "DeletePayments": "/PaymentService/DeletePayments"
        },

        "AuthorizationService": {
            "RequestAuthorization": "/AuthorizationService/RequestAuthorization",
            "ProcessAuthorization": "/AuthorizationService/ProcessAuthorization"
        },

        "NomenclatureService": {
            "GetNomenclature": "/NomenclatureService/GetNomenclature"
        },

        "BeneficiaryService": {
            "GetBeneficiaryAsInput": "/BeneficiaryService/GetBeneficiaryAsInput",
            "GetBeneficiaryAccountAsInput": "/BeneficiaryService/GetBeneficiaryAccountAsInput"
        },

        "TemplateService": {
            "GetTemplates": "/TemplateService/GetTemplates"
        },

        "DiagnosticsService": {
            "LogError": "/DiagnosticsService/LogError"
        },

        "VersionService": {
            "CheckVersion": "/VersionService/CheckVersion"
        },

        "MessageService": {
            "GetMessage": "/MessageService/GetMessage",
            "GetMessages": "/MessageService/GetMessages",
            "MarkAsRead": "/MessageService/MarkAsRead"
        },
        "BankStructureItemsService": {
            "GetBankBranches": "/BankStructureItemsService/GetBankBranches/",
            "GetBankBranchesInBounds": "/BankStructureItemsService/GetBankBranchesInBounds/",
            "GetATMsInBounds": "/BankStructureItemsService/GetATMsInBounds/"
        },
        "AntiBotProtectionService": {
            "RequestAntiBotProtection": "/AntiBotProtectionService/RequestAntiBotProtection"
        },
        "FeedMessagesService": {
            "GetNewsFeedMessages": "/FeedMessagesService/GetNewsFeedMessages",
            "GetPromotionsFeedMessages": "/FeedMessagesService/GetPromotionsFeedMessages"           
        },
        "PushNotificationsService":{
            "SavePushRegistration": "/PushNotificationsService/SavePushRegistration"
        },
        "OnlineNotificationsService": {
            "GetOnlineNotifications": "/OnlineNotificationsService/GetOnlineNotifications",
            "GetOnlineNotification": "/OnlineNotificationsService/GetOnlineNotification",
            "GetOnlineNotificationServices": "/OnlineNotificationsService/GetOnlineNotificationServices",
            "RetreiveBankCorrespondentForAP": "/OnlineNotificationsService/RetreiveBankCorrespondentForAP",
            "SaveNewCorrespondentForAP": "/OnlineNotificationsService/SaveNewCorrespondentForAP",
            "RetrieveSubscriptions": "/OnlineNotificationsService/RetrieveSubscriptions",            
            "SaveNewCorrespondentSubscription": "/OnlineNotificationsService/SaveNewCorrespondentSubscription",
            "SaveCorrespondentSubscription": "/OnlineNotificationsService/SaveCorrespondentSubscription",
        },
    };

    DAIS.eBank.ContractsInstance = function () {
        var self = this;

        this._services = DAIS.eBank.Contracts.Names;
        this._baseUrl = DAIS.eBank.Contracts.Names.URL;

        this._ajax = DAIS.eBank.Contracts.Utils._ajax;

        this._ajaxSession = DAIS.eBank.Contracts.Utils._ajaxSession;

        var myglobaldata;

        this._localStorageOrAjax = function (localStorageKey, service, param, callback) {
            var strobjValue = window.localStorage.getItem(localStorageKey);
            if (strobjValue != null && strobjValue != '') {

                myglobaldata = daisJson.daisJsonDeserialize(strobjValue);
                callback.call(self, myglobaldata);

            } else {
                this._ajax(service, param, function (result) {
                    window.localStorage.setItem(localStorageKey, daisJson.daisJsonSerialize(result));
                    callback.call(self, result);
                });
            }
        };

        this.SessionService = new DAIS.eBank.Contracts.SessionService();

        this.BankInformationService = new DAIS.eBank.Contracts.BankInformationService();

        this.BankProductsService = new DAIS.eBank.Contracts.BankProductsService();

        this.PaymentOrderService = new DAIS.eBank.Contracts.PaymentOrderService();

        this.PaymentWizardService = new DAIS.eBank.Contracts.PaymentWizardService();

        this.PaymentService = new DAIS.eBank.Contracts.PaymentService();

        this.AuthorizationService = new DAIS.eBank.Contracts.AuthorizationService();

        this.NomenclatureService = new DAIS.eBank.Contracts.NomenclatureService();

        this.TemplateService = new DAIS.eBank.Contracts.TemplateService();

        this.BankProductInfoService = new DAIS.eBank.Contracts.BankProductInfoService();

        this.VersionService = new DAIS.eBank.Contracts.VersionService();

        this.MessageService = new DAIS.eBank.Contracts.MessageService();

        this.BankStructureItemsService = new DAIS.eBank.Contracts.BankStructureItemsService();

        this.AntiBotProtectionService = new DAIS.eBank.Contracts.AntiBotProtectionService();

        this.FeedMessagesService = new DAIS.eBank.Contracts.FeedMessagesService();

        this.PushNotificationsService = new DAIS.eBank.Contracts.PushNotificationsService();

        this.OnlineNotificationsService = new DAIS.eBank.Contracts.OnlineNotificationsService();

        this.Objects = objects;
    };

    DAIS.eBank.Contracts.BaseService = function () {
        this.Behaiviours = new DAIS.eBank.Contracts.Behaviours.Collection();

        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.HandleFatalError());
        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.HandleServiceError());
        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.LoadingIndicator());
        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.InternetConnectionChecker());
        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.ObjectToText());
        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.ResultObjectVisitor());

        var self = this;

        this.makeRequest = function (serviceName, data, successCallback) {
            var result = $.DeferredEx(function (result) {

                DAIS.eBank.Contracts.Utils._ajax(serviceName, data, successCallback,
                    function (settings) {
                        return DAIS.eBank.Contracts.Behaviours.Utils.AttachBehaviourToSettings(self.Behaiviours, settings);
                    },
                    function (req) {
                        return DAIS.eBank.Contracts.Behaviours.Utils.AttachBehaviourToResult(self.Behaiviours, req);
                    }
                )
                .then(function (resp) {
                    result.resolve(resp);
                })
                .fail(function (error) {
                    result.reject(error);
                });

            });

            return result;
        }
    }

    DAIS.eBank.Contracts.BankInformationService = function () {
        DAIS.eBank.Contracts.BaseService.call(this);
        var self = this;

        this.GetCCYRatesCurrent = function (callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.BankInformationService.GetCCYRatesCurrent,
                {},
                callback
            );
        }
    };

    DAIS.eBank.Contracts.PaymentOrderService = function () {
        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;

        this.GetPaymentOrdersArchive = function (filter, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.PaymentOrderService.GetPaymentOrdersArchive,
                { Filter: filter },
                callback
            );
        };

        this.GetPaymentOrdersPending = function (filter, callback) {

            return self.makeRequest(
                DAIS.eBank.Contracts.Names.PaymentOrderService.GetPaymentOrdersPending,
                { Filter: filter },
                callback
            );
        };

        this.SignAndSendPaymentOrders = function (payments, callback) {
            self.makeRequest(
                DAIS.eBank.Contracts.Names.PaymentOrderService.SignAndSendPaymentOrders,
                {
                    Payments: payments,
                    PaymentsAuthorizationData: null
                },
                function (result) {
                    callback.call(self, result.OperationStatuses);
                });
        };

        this.SignPaymentOrders = function (payments, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.PaymentOrderService.SignPaymentOrders,
                {
                    Payments: payments,
                    PaymentsAuthorizationData: null
                },
                callback);
        };

        this.SendPaymentOrders = function (payments, callback) {
            self.makeRequest(
                DAIS.eBank.Contracts.Names.PaymentOrderService.SendPaymentOrders,
                {
                    Payments: payments,
                    PaymentsAuthorizationData: null
                },
                function (result) {
                    //if (result.Status.StatusCode != objects.EnumStatusCode)
                    callback.call(self, result.OperationStatuses);
                });
        };
    }

    DAIS.eBank.Contracts.AuthorizationService = function () {
        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;

        this.RequestAuthorization = function (authorizationData, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.AuthorizationService.RequestAuthorization,
                { AuthorizationData: authorizationData },
                callback
            );
        };

        this.ProcessAuthorization = function (authorization, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.AuthorizationService.ProcessAuthorization,
                { Authorization: authorization },
                callback
            );
        };
    }

    DAIS.eBank.Contracts.SessionService = function () {
        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;

        this._nomenclatires = {
            LoginResultStatus: {
                Success: 0
            },
            LogoutResultStatus: {
                Success: 0
            }
        };

        self.LoginBehaiviours = new DAIS.eBank.Contracts.Behaviours.Collection();
        self.LoginBehaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.HandleFatalError());
        self.LoginBehaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.HandleServiceError());
        self.LoginBehaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.LoadingIndicator());
        self.LoginBehaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.ObjectToText());

        this.Login = function (loginRequest, callback) {
            return DAIS.eBank.Contracts.Utils._ajax(
                DAIS.eBank.Contracts.Names.SessionService.Login,
                loginRequest,
                callback,
                function (settings) {
                    return DAIS.eBank.Contracts.Behaviours.Utils.AttachBehaviourToSettings(self.LoginBehaiviours, settings);
                },
                function (req) {
                    return DAIS.eBank.Contracts.Behaviours.Utils.AttachBehaviourToResult(self.LoginBehaiviours, req);;
                }
            )
        };

        this.AddSessionDetail = function (sessiondetail, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.SessionService.AddSessionDetail,
                sessiondetail,
                callback
            )
        };

        this.Logout = function () {
            var res = self.makeRequest(
                DAIS.eBank.Contracts.Names.SessionService.Logout,
                    {}
                ).done(function (result) {
                    if (result.SignOffRs.Status != self._nomenclatires.LogoutResultStatus.Success) {
                        alert('not logged out');
                    }
                    session.Clear();
                });
            return res;
        };

        this.CheckSession = function (request) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.SessionService.CheckSession,
                request
            )
        };
    }

    DAIS.eBank.Contracts.BankProductsService = function () {
        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;

        this.GetProducts = function (callBack) {

            return self.makeRequest(
                DAIS.eBank.Contracts.Names.BankProductsService.GetProducts,
                {},
                callBack
            );

        }
    }

    DAIS.eBank.Contracts.NomenclatureService = function () {
        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;

        this.GetNomenclature = function (nomenclatureFilter, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.NomenclatureService.GetNomenclature,
                { NomenclatureFilter: nomenclatureFilter },
                 callback
            );
        };
    }

    DAIS.eBank.Contracts.BeneficiaryService = function () {
        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;

        this.GetBeneficiaryAsInput = function (beneficiaryKey, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.BeneficiaryService.GetBeneficiaryAsInput,
                { BeneficiaryKey: beneficiaryKey },
                callback
            );
        };
        this.GetBeneficiaryAccountAsInput = function (beneficiaryAccountKey, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.BeneficiaryService.GetBeneficiaryAccountAsInput,
                { BeneficiaryAccountKey: beneficiaryAccountKey },
                callback
            );
        };
    }

    DAIS.eBank.Contracts.PaymentService = function () {
        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;

        this.SaveNewPayment = function (paymentInput, callback) {
            self.makeRequest(
                DAIS.eBank.Contracts.Names.PaymentService.SaveNewPayment,
                 { PaymentInput: paymentInput },
                function (result) {
                    callback.call(self, result);
                }
            );
        };
        this.SavePayment = function (payment, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.PaymentService.SavePayment,
                {
                    PaymentInput: payment.PaymentInput,
                    PaymentKey: payment.PaymentKey
                },
                callback
            );
        };
        this.GetPayment = function (paymentKey, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.PaymentService.GetPayment,
                { PaymentKey: paymentKey },
                callback
            );
        };
        this.DeletePayment = function (paymentKey, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.PaymentService.DeletePayment,
                { PaymentKey: paymentKey },
                callback
            );
        };
        this.DeletePayments = function (paymentKeys, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.PaymentService.DeletePayments,
                { PaymentKeys: paymentKeys },
                callback
            );
        };
    }

    DAIS.eBank.Contracts.TemplateService = function () {
        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;

        this.GetTemplates = function (templatesFilter, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.TemplateService.GetTemplates,
                { Filter: templatesFilter },
                callback
            );
        };
    }

    DAIS.eBank.Contracts.BankProductInfoService = function () {

        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;

        this.GetProductBalances = function (filter, callback) {
            return self.makeRequest(DAIS.eBank.Contracts.Names.BankProductInfoService.GetProductBalances,
            { BalancesFilter: filter },
            callback);

        };

        this.GetProductMovements = function (filter, callback) {

            return self.makeRequest(
                DAIS.eBank.Contracts.Names.BankProductInfoService.GetProductMovements,
                { MovementsFilter: filter },
                callback);
        };

        this.GetProductMovementDocument = function (filter, callback) {

            return self.makeRequest(DAIS.eBank.Contracts.Names.BankProductInfoService.GetProductMovementDocument,
                { BankProductMovementDocumentKey: filter },
                callback);

        };

    };


    DAIS.eBank.Contracts.PaymentWizardService = function () {

        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;

        this.PaymentWizardInit = function (requestData, callback) {

            var res = self.makeRequest(
                            DAIS.eBank.Contracts.Names.PaymentWizardService.PaymentWizardInit,
                            { InitData: requestData },
                            callback
                        );
            return res;
        }

        this.PaymentWizardNext = function (nextRequest, callback) {

            return self.makeRequest(
                DAIS.eBank.Contracts.Names.PaymentWizardService.PaymentWizardNext,
                { PaymentWizardStepInput: nextRequest },
                callback);
        }

        this.PaymentWizardBack = function (backRequest, callback) {

            return self.makeRequest(
                DAIS.eBank.Contracts.Names.PaymentWizardService.PaymentWizardBack,
                { PaymentWizardStep: backRequest },
                callback);
        }

        this.PaymentWizardGetData = function (stepDataFilter, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.PaymentWizardService.PaymentWizardGetData,
                { PaymentWizardDetachedStepDataFilter: stepDataFilter },
                callback
            );
        }

    }


    DAIS.eBank.Contracts.DiagnosticsService = function () {
        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Clear();
        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;

        this.LogError = function (error, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.DiagnosticsService.LogError,
                { Error: error },
                callback
            );
        };
    }

    DAIS.eBank.Contracts.VersionService = function () {
        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Clear();

        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.LoadingIndicator());

        var self = this;

        this.CheckVersion = function (req) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.VersionService.CheckVersion,
                req
            );
        };
    }

    DAIS.eBank.Contracts.MessageService = function () {

        //"GetMessage":   "/MessageService/GetMessage",
        //"GetMessages":  "/MessageService/GetMessages",
        //"MarkAsRead":   "/MessageService/MarkAsRead"
        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;

        this.GetMessages = function (requestData, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.MessageService.GetMessages,
                { MessagesToUserFilter: requestData },
                callback
            );
        }

        this.GetMessage = function (requestData, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.MessageService.GetMessage,
                { MessageToUserKey: requestData },
                callback
            );
        }
        this.MarkAsRead = function (requestData, callback) {
            return self.makeRequest(
                    DAIS.eBank.Contracts.Names.MessageService.MarkAsRead,
                    { MessageToUserKey: requestData },
                    callback
                );
        }
    };


    DAIS.eBank.Contracts.BankStructureItemsService = function () {

        DAIS.eBank.Contracts.BaseService.call(this);

        //this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());
        this.Behaiviours.Remove(new DAIS.eBank.Contracts.Behaviours.Types.LoadingIndicator);

        var self = this;

        this.GetBankBranches = function (requestData, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.BankStructureItemsService.GetBankBranches,
                { BankBranchesFilter: requestData },
                callback
            );
        }

        this.GetBankBranchesInBounds = function (requestData, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.BankStructureItemsService.GetBankBranchesInBounds,
                { LatLngBoundsFilter: requestData },
                callback
            );
        }

        this.GetATMsInBounds = function (requestData, callback) {
            return self.makeRequest(
                DAIS.eBank.Contracts.Names.BankStructureItemsService.GetATMsInBounds,
                { LatLngBoundsFilter: requestData },
                callback
            );
        }
    }
    
    DAIS.eBank.Contracts.AntiBotProtectionService = function () {

        DAIS.eBank.Contracts.BaseService.call(this);

        //this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;
        this.RequestAntiBotProtection = function (type, callback) {

            var res = self.makeRequest(
                            DAIS.eBank.Contracts.Names.AntiBotProtectionService.RequestAntiBotProtection,
                            { AntiBotProtectionType: type },
                            callback
                        );
            return res;
        }
    }

    DAIS.eBank.Contracts.FeedMessagesService = function () {
        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Remove(new DAIS.eBank.Contracts.Behaviours.Types.LoadingIndicator());
        this.Behaiviours.Remove(new DAIS.eBank.Contracts.Behaviours.Types.HandleFatalError());
        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.HandleFatalError({ silentError: true }));

        var self = this;
        this.GetNewsFeedMessages = function (lang, callback) {
            var res = self.makeRequest(
                DAIS.eBank.Contracts.Names.FeedMessagesService.GetNewsFeedMessages,
                { FeedMessagesFilter: { Lang: lang } },
                callback
            );
            return res;
        }

        this.GetPromotionsFeedMessages = function (lang, callback) {
            var res = self.makeRequest(
                DAIS.eBank.Contracts.Names.FeedMessagesService.GetPromotionsFeedMessages,
                { FeedMessagesFilter: { Lang: lang } },
                callback
            );
            return res;
        }
    }


    DAIS.eBank.Contracts.PushNotificationsService = function () {
        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Remove(new DAIS.eBank.Contracts.Behaviours.Types.LoadingIndicator());
        this.Behaiviours.Remove(new DAIS.eBank.Contracts.Behaviours.Types.HandleFatalError());
        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.HandleFatalError({ silentError: true }));
        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;
        this.SavePushRegistration = function (savePushRegistrationRequest, callback) {
            var res = self.makeRequest(
                DAIS.eBank.Contracts.Names.PushNotificationsService.SavePushRegistration,
                { PushRegistrationData: savePushRegistrationRequest },
                callback
            );
            return res;
        }
    };

    DAIS.eBank.Contracts.OnlineNotificationsService = function () {
        DAIS.eBank.Contracts.BaseService.call(this);

        this.Behaiviours.Add(new DAIS.eBank.Contracts.Behaviours.Types.SetSessionBehaviour());

        var self = this;
        this.GetOnlineNotifications = function (getOnlineNotificationsRequest, callback) {
            var res = self.makeRequest(
                DAIS.eBank.Contracts.Names.OnlineNotificationsService.GetOnlineNotifications,
                { NotificationsFilter: getOnlineNotificationsRequest },
                callback
            );
            return res;
        };
        
        this.GetOnlineNotification = function (getOnlineNotificationRequest, callback) {
            var res = self.makeRequest(
                DAIS.eBank.Contracts.Names.OnlineNotificationsService.GetOnlineNotification,
                { OnlineNotificationKey: getOnlineNotificationRequest },
                callback
            );

            return res;
        };

        this.GetOnlineNotificationServices = function (callback) {
            var res = self.makeRequest(
                DAIS.eBank.Contracts.Names.OnlineNotificationsService.GetOnlineNotificationServices,
                {},
                callback
                );

            return res;
        };

        this.RetreiveBankCorrespondentForAP = function (callback) {
            var res = self.makeRequest(
                DAIS.eBank.Contracts.Names.OnlineNotificationsService.RetreiveBankCorrespondentForAP,
                {},
                callback
                );

            return res;
        };

        this.SaveNewCorrespondentForAP = function (callback) {
            var res = self.makeRequest(
                DAIS.eBank.Contracts.Names.OnlineNotificationsService.SaveNewCorrespondentForAP,
                { },
                callback
                );

            return res;
        };

        this.RetrieveSubscriptions = function (retrieveSubscriptionsRequest, callback) {
            var res = self.makeRequest(
                DAIS.eBank.Contracts.Names.OnlineNotificationsService.RetrieveSubscriptions,
                { BankCorrespondent: retrieveSubscriptionsRequest },
                callback
                );

            return res;
        };

        this.SaveNewCorrespondentSubscription = function (saveNewCorrespondentSubscriptionRequest,callback) {
            var res = self.makeRequest(
                DAIS.eBank.Contracts.Names.OnlineNotificationsService.SaveNewCorrespondentSubscription,
                { Subscription: saveNewCorrespondentSubscriptionRequest },
                callback
                );

            return res;
        };

        this.ChangeSubscriptionStatus = function (saveCorrespondentSubscriptionRequest, callback) {
            var res = self.makeRequest(
                DAIS.eBank.Contracts.Names.OnlineNotificationsService.SaveCorrespondentSubscription,
                { ChangeSubscriptionStatus: saveCorrespondentSubscriptionRequest },
                callback
            );

            return res;
        };
    };
    var services = new DAIS.eBank.ContractsInstance();

    services.Services = DAIS.eBank.Contracts;
    services.Behaviours = DAIS.eBank.Contracts.Behaviours;

    //return services;

    $.extend(exports, services);
});
