﻿define([
  'Mobile/ViewModels/OverviewViewModel',
  'text!Mobile/Templates/OverviewTemplate.html',
  'utils',
  'mvc'

], function (OverviewViewModel, OverviewTemplate, utils, mvc) {

    return mvc.Controller.extend({
        index: function () {

            var overviewViewModel = new OverviewViewModel()
            var view = new mvc.ViewResult(OverviewTemplate, overviewViewModel);

            return view;
        }
    });
});
