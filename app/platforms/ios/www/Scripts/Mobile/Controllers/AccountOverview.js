define([
  'services',
  'utils',
  'mvc',
  'jquery',

  'events',

  'Mobile/ViewModels/Account/AccountOverviewViewModel',
  'text!Mobile/Templates/Accounts/AccountOverviewTemplate.html',

  'Mobile/ViewModels/Account/SimpleAccountPreviewViewModel',
  'text!Mobile/Templates/Accounts/SimpleAccountPreviewTemplate.html',

  'Mobile/ViewModels/Cards/DebitCardsListViewModel',
  'text!Mobile/Templates/Cards/DebitCradsListTemplate.html',

  'Mobile/ViewModels/Common/AccountsListHelper',
  'text!Mobile/Templates/Common/AccountsListHelperTemplate.html',

], function (services, utils, mvc, $, events, AccountOverviewViewModel, template, SimpleAccountPreviewViewModel, SimpleAccountPreviewTemplate, DebitCardsListViewModel, DebitCradsListTemplate, AccountsListHelper, AccountsListHelperTemplate) {

    return mvc.Controller.extend({
        routes: {
            "AccountOverview/index/:accountID": "index",
            "AccountOverview/simpleAccountPreview/:accountID": "simpleAccountPreview"
        },

        index: function (accountID) {
            var accountOverviewViewModel = new AccountOverviewViewModel({ BankAccountID: accountID });

            events.bind(accountOverviewViewModel, "rendered", function (el, data) {
                var self = this;

                var res = $.DeferredEx(function (res) { 

                    $(el.element).find('#submenu').submenu({
                        submenuItems: self.Submenu,
                        initialized: function () {
                            res.resolve();
                        }
                    });

                });
                return res;
            });

            return new mvc.ViewResult(template, accountOverviewViewModel);
        },

        simpleAccountPreview: function (bankAccountID)
        {
            var simpleAccountPreviewViewModel = new SimpleAccountPreviewViewModel(new services.Objects.BankAccountKey({ BankAccountID: bankAccountID }));
                
            return new mvc.ViewResult(
               SimpleAccountPreviewTemplate,
                function(dataAvailableHandler) { 
                    dataAvailableHandler(simpleAccountPreviewViewModel);
                }
            );
        },

        AccountsList: function (parent, options) {
            var accountsListHelper = new AccountsListHelper(parent, options);
            accountsListHelper.loadData();

            return new mvc.ViewResult(
               AccountsListHelperTemplate,
                function (dataAvailableHandler) {
                    dataAvailableHandler(accountsListHelper);
                }
            );
        },

        DebitCardsList: function (bankAccountID) {

            var cardsView = new mvc.ViewResult(
                DebitCradsListTemplate,
                function (dataAvailableHandler) {
                    var debitCardsList = new DebitCardsListViewModel({ BankAccountID: bankAccountID });
                    dataAvailableHandler(debitCardsList);
                },
                function (el, data) {
                    
                }
            );

            return cardsView;

        }
    });

});
