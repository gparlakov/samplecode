define([
    'mvc',
    'Common/cache/language',    
    'events',
    'services',

    'Mobile/ViewModels/OnlineNotifications/OnlineNotificationsViewModel',
    'text!Mobile/Templates/OnlineNotifications/OnlineNotificationsTemplate.html',

    'Mobile/ViewModels/OnlineNotifications/OnlineNotificationsListViewModel',
    'text!Mobile/Templates/OnlineNotifications/OnlineNotificationsListTemplate.html',

    'Mobile/ViewModels/OnlineNotifications/OnlineNotificationsSubscriptionsListViewModel',
    'text!Mobile/Templates/OnlineNotifications/OnlineNotificationsSubscriptionsListTemplate.html',
    
    
], function (mvc, language, events, services,
    viewModel, indexTemplate,
    listViewModel, listTemplate,
    subscriptionsListViewModel, subscriptionsListTemplate) {

    return mvc.Controller.extend({
        routes: {
            "OnlineNotifications/index": "index",
            "OnlineNotifications/list": "onlineNotificaitonsList",
            "OnlineNotifications/subsriptionsList": "onlineNotificationsSubscriptionsList"
        },

        index: function () {
            var onlineNotificationsViewModel = new viewModel();

            var view = new mvc.ViewResult(indexTemplate, onlineNotificationsViewModel);

            events.bind(onlineNotificationsViewModel, "rendered", function (el, data) {
                var self = this;

                var res = $.DeferredEx(function (res) {

                    $(el.element).find('#submenu').submenu({
                        submenuItems: self.Submenu,
                        initialized: function () {
                            res.resolve();
                        }
                    });

                });
                return res;
            });

            return view;
        },
        onlineNotificationsList: function () {

            var filter = new services.Objects.NotificationsFilter({ Valid: true });
            var onlineNotificationsListViewModel = new listViewModel(filter);
            var view = new mvc.ViewResult(listTemplate, onlineNotificationsListViewModel);
    
            return view;
        },
        onlineNotificationsSubscriptionsList: function () {
            var onlineNotificationsSubscriptionsListViewModel = new subscriptionsListViewModel();
            var view = new mvc.ViewResult(
                subscriptionsListTemplate, onlineNotificationsSubscriptionsListViewModel);
            return view;
        }
    });

});
