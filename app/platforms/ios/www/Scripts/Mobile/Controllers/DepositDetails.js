define([
  'Mobile/ViewModels/Deposits/DepositDetailsViewModel',
  'text!Mobile/Templates/Deposits/DepositDetailsTemplate.html',

  'productscache',
  'utils',
  'mvc',
  'underscore'
  
  // 'Mobile/ViewModels/Repository/SubMenu/Account_Movements',
   // 'text!Mobile/Templates/SubMenu/SubmenuTemplate.html'

], function (DepositDetailsViewModel, template, productscache, utils, mvc, _) {

	return mvc.Controller.extend({
		routes: {
			"DepositDetails/:depositNumber": "index"
		},

		index: function(depositNumber) {
		
		//var ap = productscache.GetAPerson();
		var deposits = productscache.GetAPerson().APTimeDeposits;
		
		var apDeposit = _.find(deposits, function(apDep){return apDep.BankTimeDeposit.BankDepositID == depositNumber} );
		 
		var depositViewModel = new DepositDetailsViewModel(apDeposit);
		
		var depositView = new mvc.ViewResult(template, function (dataAvailableHandler){
							 dataAvailableHandler(depositViewModel);
					});
		return depositView;
		//return depositViewModel;
		}

	});

});
