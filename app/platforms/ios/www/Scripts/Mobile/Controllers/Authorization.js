﻿define([
  'jquery',
  'utils',
  'mvc',
  'services',
  'Mobile/ViewModels/Authorization/AuthorizationViewModel',
  'text!Mobile/Templates/Authorization/AuthorizationTemplate.html',

  'Mobile/ViewModels/Authorization/DummyAuthorizationViewModel',
  'text!Mobile/Templates/Authorization/DummyAuthorizationTemplate.html'

], function ($, utils, mvc, services, AuthorizationViewModel, AuthorizationTemplate, DummyAuthorizationViewModel, DummyAuthorizationTemplate) {


    AuthorizationDataPayments = function () {
        this.$type =  services.Objects.Utils.GetShortTypeNameByType('AuthorizationDataPayments');
        this.Payments = []; //list of SignablePayment
    }


    return mvc.Controller.extend({
        routes: {
            "Authorization/index": "index",
        },

        index: function (payments) {
            var self = this;
            
            var viewModel = new AuthorizationViewModel(payments);

            return new mvc.ViewResult(
                        AuthorizationTemplate,
                        function (dataAvailableHandler) {
                            dataAvailableHandler(viewModel);
                        },
                        function (el, data) {

                        }
                );

        },
        dummy: function()
        {
            var self = this;

            var viewModel = new DummyAuthorizationViewModel();

            return new mvc.ViewResult(
                        DummyAuthorizationTemplate,
                        function (dataAvailableHandler) {
                            dataAvailableHandler(viewModel);
                        },
                        function (el, data) {

                        }
                );
        }
    });

});
