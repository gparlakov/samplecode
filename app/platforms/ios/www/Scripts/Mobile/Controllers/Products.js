﻿define([
  'Mobile/ViewModels/ProductsViewModel',
  'text!Mobile/Templates/ProductsTemplate.html',
  'utils',
  'mvc'

], function (ProductsViewModel, template, utils, mvc) {

    return mvc.Controller.extend({
        index: function () {

            var productsViewModel = new ProductsViewModel()


            var productsView = new mvc.ViewResult(template, productsViewModel);

            //utils.changePage(productsView);
            return productsView;
        }
    });
});
