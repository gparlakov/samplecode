﻿define([
  'jquery',
  'utils',
  'mvc',
  'services',

  'Mobile/ViewModels/Payments/ViewPaymentViewModel',
  'text!Mobile/Templates/Payments/ViewPaymentTemplate.html',

], function ($, utils, mvc, services,
            ViewPaymentViewModel, ViewPaymentTemplate) {

    return mvc.Controller.extend({
        routes: {
            "Payments/index": "index",
            "Payments/view/:incarnation/:id": "view"
        },

        index: function () {

        },

        view: function (incarnation, id) {

            var viewPaymentView = new mvc.ViewResult(
                ViewPaymentTemplate,
                function (dataAvailableHandler) {

                    var paymentKey = new services.Objects.PaymentKey({ PaymentIncarnationType: incarnation, ID: id });
                    var viewPaymentViewModel = new ViewPaymentViewModel(paymentKey);

                    dataAvailableHandler(viewPaymentViewModel);
                    


                },
                function (el, data) {

                }
            );

            return viewPaymentView;
        },

    });
});
