﻿define([
  'require',
  'jquery',
  'underscore',
  'backbone',

  'utils'
], function (require, $, _, Backbone, utils) {

	var AppRouter = Backbone.Router.extend({

		routes: {
			"MobileTest/index.html": "index",
			"index.html": "index",
			"": "index",
			//			"login": "login",
			//			"products": "products",
			"*path": "allPaths"
		},

		//		login: function () {

		//			var router = this;

		//			require(['Mobile/Controllers/Login'], function (login) {
		//				login.initialize(router);
		//			});
		//		},

		//		products: function () {

		//			require(['Mobile/Controllers/Products'], function (products) {
		//				products.initialize();
		//			});
		//		},

		index: function () {
		    //this.navigate("Login", { trigger: true });
		    utils.navigate("Login", { trigger: true });
		},

		CONTROLLER_PATH:  'Mobile/Controllers/',

		getControllerNameAndActionFromPath: function (path) {
		    if (path.substr(0, 1) == "/") {
		        path = path.substr(1);
		    }
		    var pathElements = path.split('/'); //_.filter(path.split('/'), function (item) { return item != '' });

		    return {
		        ControllerName: pathElements[0],
		        ActionName: pathElements[1] || "index"
		    };
		},

		executePath: function (path) {

		    var self = this;
		    var result = $.DeferredEx(function (result) {
		        
		        var router = self;
		        if (path.substr(0, 1) == "/")
		        {
		            path = path.substr(1);
		        }
		        var controllerName = self.getControllerNameAndActionFromPath(path).ControllerName;

		        var controllerSourceFile = 'Mobile/Controllers/' + controllerName;

		        require([controllerSourceFile], function (controller) {

		            result.catchException(function () { 

		                var cntrlr = new controller();
		                var viewResult = cntrlr.handlePath(path);

		                var asyncviewResult = $.whenEx(viewResult);
		                result.bindTo(asyncviewResult);
		                //result.resolve(viewResult);
		            })
		        });
		    });
		    return result;
		},

		allPaths: function (path) {
		    var self = this;
		    var res =
                    $.whenEx(
                        this.executePath(path)
                    )
                    .pipe(function (viewResult) {
                        return viewResult.execute()
                    })
                    .pipe(function (executedViewResult) {
                        $.extend(executedViewResult, self.getControllerNameAndActionFromPath(path));
                        return utils.changePage(executedViewResult);
                    })
                    .done(function () {
                        require(["services", "session"], function (services, session) {
                            if (session.GetSession()) {
                                var sessionService = new services.Services.SessionService();
                                sessionService.Behaiviours.Remove(new services.Behaviours.Types.LoadingIndicator);

                                sessionService.AddSessionDetail(
                                    {
                                        SessionDetailInput: {
                                            Message: "Page: " + path
                                        }
                                    }
                                )
                            }
                        })
                    })
                    .done(function () {
                        $("body").data("currentPath", Backbone.history.fragment);
                    });
		    
            return res.promise();

		}
	});

	var result = {};

	var initialize = function () {
		$(function () {
			var app_router = new AppRouter;
			Backbone.history.start({
			    pushState: false,
			    hashChange: true,
				root: "/android_asset/www/"
				//root: "/MobileTest/"
			});
			result.Router = app_router;
		});
	};

	result.initialize = initialize;

	return result;
});
