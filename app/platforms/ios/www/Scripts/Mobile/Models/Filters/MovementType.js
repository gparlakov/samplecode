﻿define([
    'i18n!Mobile/nls/strings'
], function (strings) {

    var MovmentTypesOptions = function () {

        var optionsForSelect = [{ value: '', text: strings.ID_STR_ALL },
                                 { value: 0, text: strings.ID_STR_DT },
                                 { value: 1, text: strings.ID_STR_KT}];

        return optionsForSelect;
    }

    return MovmentTypesOptions;
});