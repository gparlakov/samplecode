define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'services',
  'productscache',
  'validator',
  'Mobile/Models/Filters/MovementType'

], function ($, _, bakcbone, ko, komapping, services, productscache, validator, movementTypeOptions) {

    var MovementsViewModel = function (bankAccountKey) {

        var self = this;

        this.bankAccountKey = bankAccountKey;

        this.Movements = ko.observableArray([]);
        this.Loaded = ko.observable(false);

        this.loadMovemetns = function () {
            var filter = new services.Objects.BankAccountMovementsBlockUnblockFilterCurrent({ BankAccount: bankAccountKey });

            services.BankProductInfoService.GetProductMovements(
                filter,
                function (data) {

                    self.Movements.removeAll();

                    for (var movt in data.Movements) {
                        self.Movements.push(data.Movements[movt]);
                    }
                    self.Loaded(true);
                }
            );
        }

        this.load = function () {
            self.loadMovemetns();
        }

        this.unload = function () {

        }

    };

    return MovementsViewModel;
});