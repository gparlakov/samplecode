define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'services',
  'productscache',
  'validator',
  'Mobile/Models/Filters/MovementType',
  'utils',
  'events',
  'Mobile/ViewModels/Common/MovementsViewModel',
  'text!Mobile/Templates/Accounts/Movements/AccountMovementAdditionalInfoTemplate.html',
  'Mobile/ViewModels/Common/MovementDocHelperPopUp',
  'Mobile/ViewModels/Common/AdvancedFilter'
  
], function ($, _, bakcbone, ko, komapping, services, productscache, validator, movementTypeOptions, utils, events,
    CommonMovementsViewModel, AccountMovementAdditionalInfoTemplate, movtDocHelperPopup, AdvancedFilter) {

    var MovementsViewModel = function (filter, model) {
        CommonMovementsViewModel.call(this, filter);
        AdvancedFilter.call(this, filter);
        var self = this;

        validator.validatableModel.call(this, function () {
            return {
                properties: {
                    "MovementsFilter": {
                        properties: {
                            "Period": {
                                properties: {
                                    "DateStart": {
                                        rules: {
                                            NotNullOrEmpty: true
                                        }
                                    },
                                    "DateEnd": {
                                        rules: {
                                            NotNullOrEmpty: true
                                        }
                                    }
                                }
                            }
                        }
                    }//,
                    //"password": {
                    //    rules: {
                    //        NotNullOrEmpty: true
                    //    }
                    //}
                }
            }
        });

        var movmentsAPA = productscache.GetAPAccountByID(filter.BankAccount.BankAccountID);

        this.APAccount = movmentsAPA;
        
        this.MovementTypeSelect = movementTypeOptions();

        this._getMovementDocument = function () {
            return services.BankProductInfoService.GetProductMovementDocument(self.MovementDocumentFilter);
        }

        this.MovtDocHelperPopup = new movtDocHelperPopup(
                  self,
                  { load: self._getMovementDocument }
        );

        this.showMovementDocumentHandler = function (movement) {
            var bankAccountKey = new services.Objects.BankAccountKey({ BankAccountID: movement.BankAccount.BankAccountID });

            self.MovementDocumentFilter = new services.Objects.BankAccountMovementDocumentKey(
                        { BankAccount: bankAccountKey,
                        DocumentReference: movement.DocumentReference, 
                        ArchiveSuffix: movement.PaymentDate.getFullYear().toString() +  (movement.PaymentDate.getMonth() + 1).toString() });
            self.MovtDocHelperPopup
                .Open()
                
            ;
            //var link = utils.buildLink("AccountMovements", "showMovementDocument", [movement.DocumentReference, movement.PaymentDate.getFullYear(), movement.BankAccount.BankAccountID]);
            //utils.navigate(link, { trigger: true });
        }
    };

    return MovementsViewModel;
});