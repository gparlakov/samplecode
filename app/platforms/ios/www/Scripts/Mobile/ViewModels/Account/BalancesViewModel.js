define([
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'services',
  'productscache',
  'i18n!Mobile/nls/strings'
], function (_, bakcbone, ko, komapping, services, productscache, strings) {

//    //ne e za tuk da se izkara    
//    var AccountsSubmenu = function (params) {
//        return new submenu([new submenuItem('Account', 'AccountOverview', 'index', params), new submenuItem('Movements', 'AccountOverview', 'Movements', params)]);
//    }

    var BalancesViewModel = function (model) {
        var balanceAPA = productscache.GetAPAccountByID(model.BalancesFilter.BankAccount.BankAccountID);
        var intModel = _.extend(model, { APAccount: balanceAPA });

        var viewModel = intModel;

        viewModel.ProductDetails = {
            heading: { label: strings.ID_STR_DETAILS },
            details: []
        };
        viewModel.ProductDetails.details.push({ label: strings.ID_STR_TYPE, value: $.GetLocalizedString(viewModel.APAccount.BankAccount.AccountProductType.Name) });
        viewModel.ProductDetails.details.push({ label: strings.ID_STR_IBAN, value: viewModel.APAccount.BankAccount.IBAN });
        viewModel.ProductDetails.details.push({ label: strings.ID_STR_CCY, value: viewModel.APAccount.BankAccount.CCY.SWIFTCode });
        viewModel.ProductDetails.details.push({ label: strings.ID_STR_OVERDRAFT, value: viewModel.Balances != null ? $.format.number(viewModel.Balances[0].Overdraft) : '' });
        viewModel.ProductDetails.details.push({ label: strings.ID_STR_BANK_CLIENT, value: viewModel.APAccount.BankAccount.BankClient.NAME });

        var hasBalances = viewModel.Balances != null && viewModel.Balances.length > 0;
        viewModel.AccountBalances = {
            heading: { label: strings.ID_STR_TOTAL_AMOUNTS, hidden: !hasBalances },
            details: []
        };
        if (hasBalances) {
            $.each(viewModel.Balances, function () {
                viewModel.AccountBalances.details.push({ label: strings.ID_STR_BALANCE, value: $.format.number(this.ActualBalance) });
                viewModel.AccountBalances.details.push({ label: strings.ID_STR_AVAILABLE_AMOUNT, value: $.format.number(this.AvailableBalance) });
            });
        }

        //viewModel.Submenu = accountsSubmenu([model.APAccount.BankAccount.BankAccountID]);
        return viewModel;
    };

    return BalancesViewModel;
});