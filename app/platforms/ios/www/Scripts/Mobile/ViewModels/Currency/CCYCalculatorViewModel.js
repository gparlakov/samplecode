﻿define([
  'underscore',
  'knockout',
  'services',
  'session',
  'utils',
  'jqueryformat',
  
], function (_, ko, services, session, utils, $format, ccyRateTemplate) {

    
    var CCYCalculatorViewModel = function CCYCalculatorViewModel(model) {
        var self = this;
        
        this.hasSeesion = session.GetSession() != null ;
        
        this.ccyRates = null;

        this.subModel = {}
        
        
        this.calculateAmountsFixRate = function (newValue) {
            var inLeva = newValue * this.CCYRate.FixRate;
            var me = this;
            _.each(self.subModel, function (ccyRate) {
                if (ccyRate.ID != me.ID) {
                    var newAmount = inLeva / ccyRate.CCYRate.FixRate;

                    ccyRate.AmountSubscription.dispose()
                    ccyRate.Amount($.format.number(newAmount, '0.0000'));
                    ccyRate.AmountSubscription = ccyRate.Amount.subscribe(self.calculateAmountsFixRate, ccyRate);
                }
            });
        }

        _.map(model.CCYRates, function (ccyRate) {
            //da ne promenqme dannite ot servera 
            var element = {};
            element.CCYRate = ccyRate;
            //shortcut 
            element.ID = ccyRate.CCY.SWIFTCode;

            element.Amount = ko.observable();
            element.AmountSubscription = element.Amount.subscribe(self.calculateAmountsFixRate, element);

            self.subModel[ccyRate.CCY.SWIFTCode] = element;
        });

    };

    return CCYCalculatorViewModel;
});