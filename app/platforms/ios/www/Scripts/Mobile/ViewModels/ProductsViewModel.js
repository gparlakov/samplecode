﻿define([
  'underscore',
  'jquery',
  'backbone',
  'utils',
  'knockout',
  'komapping',
  'services',
  'jqueryformat',
  'typeVisitor',
  'productscache',
  'i18n!Mobile/nls/strings',
  'Mobile/ViewModels/Common/AccountLabelBase',
  'Mobile/ViewModels/Common/HeaderWithOverviewLinkViewModel',
  'Mobile/ViewModels/Repository/ProductActionButtons',
], function (_, $, backbone, utils, ko, komapping, services, $format, typeVisitor, productscache, strings, accountLabel, headerWithOverviewLinkViewModel, actionButtons) {

    var ProductsViewModelKnockout = function ProductsViewModelKnockout() {

        var model = productscache.GetAPerson();

        var viewModel = {};
        viewModel.HeaderViewModel = new headerWithOverviewLinkViewModel({ titleStringID: "ID_STR_PRODUCTS" });

        viewModel.initialize = function () {

            var res =
                services.BankProductInfoService.GetProductBalances(
                    new services.Objects.BankAccountBalancesFilterCurrent(
                        { Paging: { CurrentPage: -1, ResultsForPage: -1 } })
                )

            viewModel.strings = strings;

            viewModel.BankCardCredit = [];
            viewModel.BankCardRaiCard = [];
            viewModel.BankCardDebit = [];

            $.extend(viewModel, model);
            _.extend(viewModel, new accountLabel());

            if (viewModel.APCards != null && viewModel.APCards != 'undefined' && viewModel.APCards.length > 0) {
                var cardType = {
                    BankCardCredit: function (rpcCard) {
                        viewModel.BankCardCredit.push($.extend(true, {
                            actionButtons: [new actionButtons.ViewCardDetails(rpcCard)]
                        }, rpcCard));
                    },
                    BankCardRaiCard: function (RaiCard) {
                        viewModel.BankCardCredit.push($.extend(true, {
                            actionButtons: [new actionButtons.ViewCardDetails(RaiCard)]
                        }, RaiCard));
                    },

                    Other: function () { }
                }
                for (var index in viewModel.APCards) {
                    typeVisitor.visit(viewModel.APCards[index], cardType, function (obj) { return obj.BankCard; });
                }
            }

            if (viewModel.APAccounts) {
                viewModel.APAccountsViewModel = _.map(viewModel.APAccounts, function (apa) {

                    var res = { actionButtons: [] };
                    res.actionButtons.push(new actionButtons.ViewAccountDetails(apa));
                    res.actionButtons.push(new actionButtons.NewPaymentFromAccount(apa));

                    $.extend(res, apa)
                    return res;
                });
            }

            viewModel.APTimeDepositsViewModel = _.map(viewModel.APTimeDeposits, function (apDeposit) {
                var res = {
                    actionButtons: [new actionButtons.ViewDepositDetails(apDeposit)]
                }
                $.extend(res, apDeposit);

                return res;
            });

            viewModel.ExtendedProperties.APFacilitiesViewModel = _.map(viewModel.ExtendedProperties.APFacilities, function (apFacility) {
                var res = {
                    actionButtons: [new actionButtons.ViewFacilityDetails(apFacility)]
                }

                $.extend(res, apFacility);
                return res;
            });
            // selector - funciq, koqto ot element ot spisaka, dava elemnta sadarjassht ConvertedAmount-a
            // koleckiq s elementa sadarjashata converted Amount-a 
            function calculateTotalAccmount(selector, collection) {
                var tmpResult = {};
                if (collection && collection.length > 0) {

                    var tmpResutl = selector(collection.shift());
                    // pravim rezultat da ima propertita s imeto na valutata, v koeto stoi amount-a (abe kato rechnik ) 
                    _.each(tmpResutl, function (convertedAmount) {
                        //var obj = {};
                        tmpResult[convertedAmount.CCY.SWIFTCode] = convertedAmount.Amount;

                    });

                    _.each(collection, function (element) {
                        _.each(selector(element), function (totalAmount) {
                            tmpResult[totalAmount.CCY.SWIFTCode] += totalAmount.Amount;
                        });


                    });

                    var resultAsArray = [];
                    for (var propName in tmpResult) {
                        if (tmpResult.hasOwnProperty(propName)) {
                            if (propName == 'BGN') { //trqbva da e parva :( :( 
                                resultAsArray.unshift({ SWIFTCode: propName, Amount: tmpResult[propName] });
                            } else {
                                resultAsArray.push({ SWIFTCode: propName, Amount: tmpResult[propName] });
                            }
                        }
                    }

                    return resultAsArray;

                }

            }

            return res.pipe(function () {
                viewModel.AccountsTotalAmounts = calculateTotalAccmount(
                    function (bal) { return bal.ExtendedProperties.ConvertedAvailableBalance },
                            //spisak ot vsichki balansi na smetki, koito imat balans 
                    _.chain(viewModel.APAccounts)
                     .filter(function (apa) { return apa.BankAccount.BankAccountBalances != null })
                     .map(function (apa) { return apa.BankAccount.BankAccountBalances[0] })
                     .value());

                viewModel.selectedAccTotalAmount = viewModel.AccountsTotalAmounts && viewModel.AccountsTotalAmounts.length > 0 ?
                    ko.observable(viewModel.AccountsTotalAmounts[0]) : ko.observable();


                viewModel.DepositTotalAmounts = calculateTotalAccmount(
                    function (bnkDeposit) { return bnkDeposit.ExtendedProperties.ConvertedPrincipal },
                    _.chain(viewModel.APTimeDeposits)
                     .map(function (apDeposit) { return apDeposit.BankTimeDeposit })
                     .value());

                viewModel.selectedDepTotalAmount = viewModel.DepositTotalAmounts && viewModel.DepositTotalAmounts.length > 0 ?
                                    ko.observable(viewModel.DepositTotalAmounts[0]) : ko.observable();
            });
        }

        viewModel.HasOverduedLoan = function (apFacility) {
            return _.any(apFacility.APLoans, function (loan) {
                return loan.BankLoan && loan.BankLoan.Overdued;

            });
        };

        viewModel.GetCardFormattedBalanceFromAccount = function (bankCard) {
            if (bankCard.BankCardStatus != null && bankCard.BankCardStatus.ID != 0 && !bankCard.BankCardStatus.isActive) {
                return;
            } else {
                if (bankCard.BankAccount != null && bankCard.BankAccount != 'undefined') {
                    productscache.SetBankAccountToBankCard(bankCard);

                    if (bankCard.BankAccount.BankAccountBalances != null)
                        return $format.number(bankCard.BankAccount.BankAccountBalances[0].AvailableBalance);
                    else
                        return;
                }
            }

        };

        return viewModel;
    };

    return ProductsViewModelKnockout;

});