﻿define([
  'underscore',
  'jquery',
  'backbone',
  //'utils',
  //'services',
  'productscache',
  'typeVisitor',
  'utils',
  'Mobile/ViewModels/Repository/ProductActionButtons'
], function (_, $, bakcbone, productscache, typeVisitor, utils, actionButtons) {

    var CardsOverviewViewModel = function (account) {

        var self = this;

        var ap = productscache.GetAPerson();

        this.BankCardDebit = [];
        
        if (ap.APCards != null && ap.APCards != 'undefined' && ap.APCards.length > 0) {

            var cardsForAccount = _.filter(ap.APCards, function (aPcard) {
                return aPcard.BankCard.BankAccount.BankAccountID == account.BankAccountID;
            });

            //ako vse pak ima neshto koeto ne e debitna karta 
            var cardType = {
                BankCardDebit: function (debit) {
                    self.BankCardDebit.push($.extend(true, {
                        actionButtons: [new actionButtons.ViewCardDetails(debit)]
                    }, debit));
                },
                Other: function () { }
            }

            for (var index in cardsForAccount) {
                typeVisitor.visit(cardsForAccount[index], cardType, function (obj) { return obj.BankCard; });
            }
        }

        this.GetCardFormattedBalanceFromAccount = function (bankCard) {
            if (bankCard.BankCardStatus != null && bankCard.BankCardStatus.ID != 0 && !bankCard.BankCardStatus.isActive) {
                return;
            } else {
                if (bankCard.BankAccount != null && bankCard.BankAccount != 'undefined') {
                    productscache.SetBankAccountToBankCard(bankCard);

                    if (bankCard.BankAccount.BankAccountBalances != null)
                        return $.format.number(bankCard.BankAccount.BankAccountBalances[0].AvailableBalance);
                    else
                        return;
                }
            }

        };
    }
    return CardsOverviewViewModel;

});