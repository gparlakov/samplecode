﻿define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'knockoutDais',
  'services',
  'utils',
  'i18n!Mobile/nls/strings',
  'events',
  'typeVisitor',
  'uimodels',
  'Mobile/Models/Payments/OperationStatusToMessageFormater',
  'Mobile/Models/Payments/PaymentOrderAllowedOperationReasonToMessageFormater',
  'Mobile/ViewModels/Common/HeaderViewModel',
], function ($, _, backbone, ko, komapping, knockoutDais, services, utils, strings, events, typeVisitor, uimodels, operationStatusFormater, PaymentOrderAllowedOperationReasonToMessageFormater, HeaderViewModel) {

    function OnlyServerSignableProcessor()
    {

    }

    function AuthorizationSignableProcessor()
    {

    }

    var State = {
        Sign: 1,
        Send: 2,
        Completed: 3,
        Error: 4
    }

    var ProcessPaymentSignViewModel = function ProcessPaymentSignViewModel(paymentsKeys) {
        this.HeaderViewModel = new HeaderViewModel({ titleStringID: "ID_STR_SEND" });
        uimodels.Notifications.UINotificationContainer.call(this);

        this._operationStatusFormatter = new operationStatusFormater();

        this.Types = {
            State: State
        }

        var self = this;
    
        this.State = ko.observable(State.Sign);

        this.PaymentsKeys = paymentsKeys;
        this.Payments = [];

        this.PaymentsToSign = [];
        this.PaymentsToAuthorize = [];
        this.PaymentsToSignOrAuthorize = [];

        this.SubPage = ko.observable(null);

        this.Init = function (initCompletedCallback) {
            self._getPayemtns(function () {
                initCompletedCallback.call(this);
            });
        };

        this.Back = function () {
            utils.navigateBack();
        }

        this.Cancel = function () {
            self.Back();
        };

        this.Sign = function () {
            self = this;
            if (_.isEmpty(self.PaymentsToSign)) {
                self.State(State.Send);
                self.Send();
            }
            else {
                services.PaymentOrderService.SignPaymentOrders(
                        _.map(self.PaymentsToSign, function (payment) { return new services.Objects.PaymentKey(payment) })
                ).done(function (resp) {
                    var statuses = resp.OperationStatuses;
                    if (statuses && !_.isEmpty(statuses)) {
                        _.each(statuses, function (status) {
                            self.AddNotification(self._operationStatusFormatter.FormatOperationsStatus(status));
                        });
                        if (_.all(statuses, function (status) { return !status.Success })) {
                            self.signFaild()
                        }
                        else {

                            self.State(State.Send);
                            _.each(statuses, function (status) {

                                typeVisitor.visit(status.PaymentAllowedOperations, {
                                    "PaymentOrderAllowedOperations" : function(ao) {
                                        if (!ao.ExtendedProperties.CanPerformCurrentOperation) {
                                            self.State(State.Error); //ако ще пращаме по няколко, трабвя да се сетва на Error само ако всички са !CanPerformCurrentOperation
                                            ao.ExtendedProperties.Reasons.forEach(function (reason) {
                                                var n = self.paymentOrderAllowedOperationReasonToMessageFormater.FormatAsUINotification(reason);
                                                self.AddNotification(n);
                                            });
                                        }
                                        else {
                                            //тук трябва да се добавят към колекциа за изпращане, но в момента поддържаме само 1
                                        }
                                    }

                                });
                            });
                        }
                    }
                    else {
                        self.signFaild()
                    }
                });
            }
        }

        this.Send = function () {
            self._send();
        };

        this.signFaild = function () {
            self.AddNotification(new uimodels.Notifications.UINotification({
                Message: strings.ID_STR_PAYMENTS_SIGN_ALL_ERROR,
                Type: uimodels.Notifications.UINotificationType.Error
            }));
            self.State(State.Error);
        };

        this.sendFaild = function () {
            self.AddNotification(new uimodels.Notifications.UINotification({
                Message: strings.ID_STR_PAYMENTS_SEND_ERROR,
                Type: uimodels.Notifications.UINotificationType.Error
            }));
            self.State(State.Error);
        };

        this._createAuth = function () {
            var self = this;
            var dummy = _.all(self.PaymentsToSignOrAuthorize, function (payment) { return payment.PaymentOrderAllowedOperations.ExtendedProperties.IsServerSideSignable; });
            if (dummy) {
                self.Sign();
            } else {
                this.SubPage(
                        {
                            controller: 'Authorization',
                            action: dummy ? 'dummy' : 'index',
                            params: [self.PaymentsToSignOrAuthorize],
                            viewModelAvailable: function (authViewModel) {

                                events.bind(authViewModel, "cancel", function () {
                                    self.Cancel();
                                });

                                events.bind(authViewModel, "success", function () {
                                    self.Sign();
                                });
                            }
                        }
                    );
            }
            
        }

        this._getPayemtns = function (getPaymentsCompletedCallback) {
            services.PaymentOrderService.GetPaymentOrdersPending(
                    {
                        PaymentKeys: self.PaymentsKeys,
                        Paging: {
                            CurrentPage: 1,
                            ResultsForPage: -1
                        }
                    },
                    function (payments) {
                        _.each(payments.PendingPayments, function (payment) {
                            self.Payments.push(payment);
                            //if (payment.PaymentOrderAllowedOperations.CanISign)
                            if (
                                payment.PaymentOrderAllowedOperations.ExtendedProperties.PendingOperation 
                                    == services.Objects.EnumPaymentOrderPendingOperation.Sign) { 
                                self.PaymentsToSign.push(payment);
                            }
                            if (payment.PaymentOrderAllowedOperations.ExtendedProperties.PendingOperation
                                    == services.Objects.EnumPaymentOrderPendingOperation.Send && payment.PaymentOrderAllowedOperations.ExtendedProperties.IsAuthorizationRequiredToSend) {
                                self.PaymentsToAuthorize.push(payment);
                            }
                            self.PaymentsToSignOrAuthorize = _.union(self.PaymentsToSign, self.PaymentsToAuthorize);
                            //Налага се да ги разделим на две, защото ни трабва да викаме подписването за тези за Authorize
                            //проблем ще е когато го направим да работи за повече преводи, защото след подписването овторицацията няма да свърши работа за изпращане
                        });

                        self._paymentsCreated();

                        getPaymentsCompletedCallback.call(this);
                    }
                );
        };

        this.paymentOrderAllowedOperationReasonToMessageFormater = new PaymentOrderAllowedOperationReasonToMessageFormater();
        this._paymentsCreated = function()
        {
            if (_.isEmpty(self.PaymentsToSignOrAuthorize))
            {
                self.State(State.Send);
            }
            else
            {
                if (_.all(self.PaymentsToSignOrAuthorize, function (p) { return !p.PaymentOrderAllowedOperations.ExtendedProperties.CanPerformCurrentOperation; })) {
                    self.State(State.Error);
                    self.PaymentsToSignOrAuthorize.forEach(function (p) {
                        p.PaymentOrderAllowedOperations.ExtendedProperties.Reasons.forEach(function (reason) {
                            var n = self.paymentOrderAllowedOperationReasonToMessageFormater.FormatAsUINotification(reason);
                            self.AddNotification(n);
                        })
                    });
                }
                else {
                    this._createAuth();
                }
            }
        }

        this._send = function () {
            services.PaymentOrderService.SendPaymentOrders(
                    self.PaymentsKeys,
                    function (statuses) {
                        if (statuses && !_.isEmpty(statuses)) {
                            _.each(statuses, function (status) {
                                self.AddNotification(self._operationStatusFormatter.FormatOperationsStatus(status));
                            });
                            self.State(State.Completed);
                        }
                        else {
                            self.sendFaild()
                        }
                    }
                );
        };
    };

    return ProcessPaymentSignViewModel;
});