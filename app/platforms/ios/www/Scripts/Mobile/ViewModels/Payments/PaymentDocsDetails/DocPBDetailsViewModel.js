﻿define([
  'jquery',
  'underscore',
  'knockout',
  'komapping',
  'services',
  'servicesObjects',
  'validator',
  'typeVisitor',
  'productscache',
  'Mobile/ViewModels/Payments/PaymentDocsDetails/DetailsBaseViewModel',
  'Common/ufnomenclatures/LiabilityPersonTypes',

  'i18n!Mobile/nls/strings',
  'text!Mobile/Templates/Payments/Wizard/Details/Partials/LiabilityPersonDetails.html'

], function ($, _, ko, komapping, services, servicesObjects, validator, typeVisitor, productscache, baseViewModel, LiabilityPersonTypes, strings, liabilityPersonTemplate) {

    var EnumLiabilityPersonTypes = {
        Local: "0",
        Foreing: "1",
        Company : "2"
    }
    
    var DocumentType = [
        { ID: 1, Desrciption: '1 - ' + strings.ID_STR_313_DOCTYPE_1, DocumentInfoNeeded: false, PeriodDatesNeeded: true },
        { ID: 2, Desrciption: '2 - ' + strings.ID_STR_313_DOCTYPE_2, DocumentInfoNeeded: true, PeriodDatesNeeded: true },
        { ID: 3, Desrciption: '3 - ' + strings.ID_STR_313_DOCTYPE_3, DocumentInfoNeeded: true, PeriodDatesNeeded: false },
        { ID: 4, Desrciption: '4 - ' + strings.ID_STR_313_DOCTYPE_4, DocumentInfoNeeded: false, PeriodDatesNeeded: true },
        { ID: 5, Desrciption: '5 - ' + strings.ID_STR_313_DOCTYPE_5, DocumentInfoNeeded: false, PeriodDatesNeeded: true },
        { ID: 6, Desrciption: '6 - ' + strings.ID_STR_313_DOCTYPE_6, DocumentInfoNeeded: true, PeriodDatesNeeded: false },
        //izbran po podrazbirane 
        { ID: 9, Desrciption: '9 - ' + strings.ID_STR_313_DOCTYPE_9, DocumentInfoNeeded: false, PeriodDatesNeeded: false },

    ];

    var paymentTypeForComputed = function (self2, propValueGetter, optionsContainer, propValueSetter) {
        return {
            read: function () {
                    
                var propValue = propValueGetter();
                if (propValue) {
                    var res = _.find(ko.utils.unwrapObservable(optionsContainer), function (payType) {
                        return ko.utils.unwrapObservable(payType.ID) == propValue;
                    });
    
                    return res;
                } else {
                    return null;
                }
            },
            write: function (value) {
                if (value) {
                    propValueSetter({ ID: ko.utils.unwrapObservable(value.ID) });
                } else {
                    propValueSetter(null);
                }
            },
            owner: self2
        };
    }


    function DocPB(original) {
        var self = this;

       
        _.extend(this, komapping.fromJS(original));
    }

    function DocPBDetailsViewModel(model, wizardOutputModel) {

        validator.validatableModel.call(this, function () {
            return {
                properties: {
                    "Details": {
                        properties: {
                            "Details1": {
                                rules: {
                                    NotNullOrEmpty: true,
                                    MaxLength: 35,
                                    BISERASymbols: true
                                }
                            },
                            "Details2": {
                                rules: {
                                    MaxLength: 35,
                                    BISERASymbols: true
                                }
                            },
                            "LiabilityPersonName": {
                                rules: {
                                    NotNullOrEmpty: true,
                                    MaxLength: 35,
                                    BISERASymbols: true
                                }
                            },
                            "LiabilityPersonEGN": {
                                rules: {
                                    NotNullOrEmpty: ko.computed({
                                        read: function () {
                                            return self.AdditionalViewData.selectedLiabilityPersonType().ID == EnumLiabilityPersonTypes.Local
                                        }
                                    }),
                                    EGN: true
                                }
                            },
                            "LiabilityPersonBULSTAT": {
                                rules: {
                                    NotNullOrEmpty: ko.computed({
                                        read: function () {
                                            return self.AdditionalViewData.selectedLiabilityPersonType().ID == EnumLiabilityPersonTypes.Company
                                        }
                                    }),
                                    BULSTAT: true
                                }
                            },
                            "LiabilityPersonID": {
                                rules: {
                                    NotNullOrEmpty: ko.computed({
                                        read: function () {
                                            return self.AdditionalViewData.selectedLiabilityPersonType().ID == EnumLiabilityPersonTypes.Foreing;
                                        }
                                    }),
                                    RegEx: "^[0-9]{10}$"
                                }
                            },
                            "LiabilityDocumentNumber": {
                                rules: {
                                    MaxLength: 17,
                                    NotNullOrEmpty: ko.computed({
                                        read: function () {
                                            return self.AdditionalViewData.selectedTypeOfDocument() == null ? null : self.AdditionalViewData.selectedTypeOfDocument().DocumentInfoNeeded;
                                        }
                                    })
                                }
                            },
                            "LiabilityDocumentDate": {
                                rules: {
                                    NotNullOrEmpty: ko.computed({
                                        read: function () {
                                            return self.AdditionalViewData.selectedTypeOfDocument() == null ? null : self.AdditionalViewData.selectedTypeOfDocument().DocumentInfoNeeded;
                                        }
                                    })
                                }
                            },
                            "PeriodStartDate": {
                                rules: {
                                    NotNullOrEmpty: ko.computed({
                                        read: function () {
                                            return self.AdditionalViewData.selectedTypeOfDocument() == null ? null : self.AdditionalViewData.selectedTypeOfDocument().PeriodDatesNeeded;
                                        }
                                    })
                                }
                            },
                            "PeriodEndDate": {
                                rules: {
                                    NotNullOrEmpty: ko.computed({
                                        read: function () {
                                            return self.AdditionalViewData.selectedTypeOfDocument() == null ? null : self.AdditionalViewData.selectedTypeOfDocument().PeriodDatesNeeded;
                                        }
                                    })
                                }
                            },
                            "DirtyMoney": {
                                rules: {
                                    NotNullOrEmpty: function () { return self.EspeciallyNeedsDirtyMoneyDeclaration; }
                                }
                            },
                            "PayerPaymentType": {
                                rules: {
                                    NotNullOrEmpty: ko.computed({
                                        read: function () {
                                            return (self.AdditionalViewData.PayerPaymentTypeOptions() == null || self.AdditionalViewData.PayerPaymentTypeOptions().length > 0);

                                        }
                                    })
                                }
                            },
                            "PayeePaymentType": {
                                rules: {
                                    NotNullOrEmpty: ko.computed({
                                        read: function () {
                                            return (self.AdditionalViewData.PayeePaymentTypeOptions() == null || self.AdditionalViewData.PayeePaymentTypeOptions().length > 0);
                                        }
                                    })
                                }
                            },
                        }
                    },
                    "AdditionalViewData": {
                        properties: {
                            "selectedPayerPaymentType": {
                                rules: {
                                }
                            },
                            "selectedPayeePaymentType": {
                                rules: {
                                }
                            },
                            "selectedLiabilityPersonType": {
                                rules: {
                                    NotNullOrEmpty: true
                                }
                            },
                            "selectedTypeOfDocument": {
                                rules: {
                                    NotNullOrEmpty: true
                                }
                            }
                        }
                    }
                }
            }
        });

        var self = this;

        baseViewModel.call(this, model, wizardOutputModel);
            
        this.Model = wizardOutputModel;

        this.initialize = function () {

            self.AdditionalViewData.liabilityDocumentTypeOptions = ko.observableArray(DocumentType);
            //self.Details.LiabilityDocumentType,
            self.AdditionalViewData.selectedTypeOfDocument = ko.computed(
                    paymentTypeForComputed(self,
                                   function (){
                                       var tmpDocType = ko.utils.unwrapObservable(self.Details.LiabilityDocumentType);
                                       return tmpDocType;
                                   },
                                   self.AdditionalViewData.liabilityDocumentTypeOptions,
                                   function (val) { self.Details.LiabilityDocumentType(val.ID); }
                ));

            var payerApa = productscache.GetAPAccountByID(self.Model.State.Payer.BankAccount.BankAccountID);
            var promise1 = services.NomenclatureService.GetNomenclature(new servicesObjects.NomenclatureNOIParagraphFilterByIBAN({ IBAN: payerApa.BankAccount.IBAN, Type: servicesObjects.EnumNOIParagraphDirection.IN }),
                function (nm) {
                    
                    self.AdditionalViewData.PayerPaymentTypeOptions = ko.observableArray(
                                    _.map(nm.NomenclatureCollection.NOIParagraphs, function (paragraph) {
                                        paragraph.localDecription = paragraph.ID + ' - ' + $.GetLocalizedString(paragraph.Description);
                                        return paragraph;
                                    }));
                    //tva ne che ne moje argumenta da e obekt, no haide
                    //self.Details.PayerPaymentType,
                    self.AdditionalViewData.selectedPayerPaymentType = ko.computed(
                        paymentTypeForComputed(self,
                            function () {
                                var tmpPayerPayType = ko.utils.unwrapObservable(self.Details.PayerPaymentType);
                                if (tmpPayerPayType && tmpPayerPayType.ID){
                                    return ko.utils.unwrapObservable(tmpPayerPayType.ID);
                                }
                                return null;
                            },
                            self.AdditionalViewData.PayerPaymentTypeOptions,
                            function (val) {
                                if (val) {
                                    self.Details.PayerPaymentType(komapping.fromJS(new servicesObjects.NOIParagraphKey(val)));
                                }
                                else {
                                    self.Details.PayerPaymentType(null);
                                }
                            }
                        )
                    );
                });


            var payeeIBAN = typeVisitor.visit(self.Model.State.Payee, {
                PaymentWizardPayeeBankAccount: function (payee) {
                    var payeeApa = productscache.GetAPAccountByID(payee.BankAccount.BankAccountID);
                    if (payeeApa && payeeApa.BankAccount)
                        return payeeApa.BankAccount.IBAN;
                    return null;

                },
                //spestqvame si sluchaq za foreign account 
                PaymentWizardPayeeNewBeneficiary: function (payee) {
                    if (payee.Account && payee.Account.IBAN)
                        return payee.Account.IBAN;
                }
            });

            //koq li ot dvete shte se varne posledna ??? 
            var promise2 = services.NomenclatureService.GetNomenclature(new servicesObjects.NomenclatureNOIParagraphFilterByIBAN({ IBAN: payeeIBAN, Type: servicesObjects.EnumNOIParagraphDirection.IN }),
                function (nm) {
                    self.AdditionalViewData.PayeePaymentTypeOptions = ko.observableArray(
                                    _.map(nm.NomenclatureCollection.NOIParagraphs, function (paragraph) {
                                        paragraph.localDecription = paragraph.ID + ' - ' + $.GetLocalizedString(paragraph.Description);
                                        return paragraph;
                                    }));
                    ////tva ne che ne moje argumenta da e obekt, no haide
                    self.AdditionalViewData.selectedPayeePaymentType = ko.computed(
                        paymentTypeForComputed(self,
                            function () {
                                var tmpPayeePayType = ko.utils.unwrapObservable(self.Details.PayeePaymentType);
                                if (tmpPayeePayType && tmpPayeePayType.ID) {
                                    return ko.utils.unwrapObservable(tmpPayeePayType.ID);
                                }
                                return null;
                            },
                            self.AdditionalViewData.PayeePaymentTypeOptions,
                                function (val) {
                                    if (val)
                                        self.Details.PayeePaymentType(komapping.fromJS(new servicesObjects.NOIParagraphKey(val)));
                                    else {
                                        self.Details.PayeePaymentType(null);
                                    }
                                }
                        )
                    );
                });

                    
            var result =
                $.whenEx(
                    promise1, promise2
                ).pipe(function(p){
                    return self.initializeBase();
                });

            return result;
        }

        
        this.Details = new DocPB(model);

        var AdditionalViewData = this.AdditionalViewData;

        self.AdditionalViewData.PayerPaymentTypeOptions = ko.observableArray([]);
        self.AdditionalViewData.PayeePaymentTypeOptions = ko.observableArray([]);
        
        self.AdditionalViewData.liabilityPersonTypeOptions = LiabilityPersonTypes;
        
        self.AdditionalViewData._liabilityPersonType = ko.observable();
        self.AdditionalViewData.selectedLiabilityPersonType = ko.computed({
            read: function () {
                return this.AdditionalViewData._liabilityPersonType();
            },
            write: function (value) {
                // clear all 
                if (value.ID == EnumLiabilityPersonTypes.Local) {
                    this.Details.LiabilityPersonBULSTAT(null);
                    this.Details.LiabilityPersonID(null);

                } else if (value.ID == EnumLiabilityPersonTypes.Foreing) {
                    this.Details.LiabilityPersonBULSTAT(null);
                    this.Details.LiabilityPersonEGN(null);

                } else if (value.ID == EnumLiabilityPersonTypes.Company) {
                    this.Details.LiabilityPersonEGN(null);
                    this.Details.LiabilityPersonID(null);
                } 
                

                this.AdditionalViewData._liabilityPersonType(value);
            },
            owner: this
        });


        self.AdditionalViewData.isRings = ko.computed({
            read: function () {
                return this.Details.RINGS();
            },
            write: function (value) {
                this.Details.RINGS(value === true);
            },
            owner: this

        });
    

        //kogato se initva 
        if (this.Details.LiabilityPersonEGN()) {
            AdditionalViewData.selectedLiabilityPersonType(LiabilityPersonTypes[0]);
        }
        else if (this.Details.LiabilityPersonID()) {
            AdditionalViewData.selectedLiabilityPersonType(LiabilityPersonTypes[1]);
        }
        else if (this.Details.LiabilityPersonBULSTAT()) {
            AdditionalViewData.selectedLiabilityPersonType(LiabilityPersonTypes[2]);
        }
        else {
            //default
            AdditionalViewData.selectedLiabilityPersonType(LiabilityPersonTypes[0]);
        }

        this.GetModel = function () {
            return komapping.toJS(this.Details);
        }

    };

    return DocPBDetailsViewModel;
});