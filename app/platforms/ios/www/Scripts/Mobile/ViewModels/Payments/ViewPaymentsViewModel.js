﻿define([
  'jquery',
  'underscore',
  'backbone',
  'services',
  'utils',
  'Mobile/ViewModels/Payments/ViewPaymentsBaseViewModel',
  'text!Mobile/Templates/Payments/PendingPaymentAdditionalInfoTemplate.html',
  'Mobile/ViewModels/Repository/PaymentActionButtons',
  'Mobile/Models/Payments/PaymentOrderAllowedOperationReasonToMessageFormater'

], function ($, _, backbone, services, utils, baseViewModel, PendingPaymentAdditionalInfoTemplate, actionButtons, PaymentOrderAllowedOperationReasonToMessageFormater) {

   function ViewPaymentsViewModel() {
        
        baseViewModel.call(this);

        var self = this;

        this.PaymentsOrdersGetFromServiceMethod = self.paymentOrderService.GetPaymentOrdersPending;
        this.GetPaymentsFromResult = function (data) {
            return data.PendingPayments;
        }

        this._operationReasonFormatter = new PaymentOrderAllowedOperationReasonToMessageFormater();
        var _wrapPayment = this._wrapPayment;
        this._wrapPayment = function (p)
        {
            p = _wrapPayment.call(this, p);
            if (p.PaymentOrderAllowedOperations)
            {
                p.PaymentOrderAllowedOperations._Messages = this._operationReasonFormatter.FormatAsTextCollection(p.PaymentOrderAllowedOperations.ExtendedProperties.Reasons)
            }
            p.actionButtons = [];
            p.actionButtons.push(new actionButtons.viewPayment(function (data, e) {
                self.viewPayment(p, e);
            }));
            p.actionButtons.push(new actionButtons.editPayment(p));
            p.actionButtons.push(new actionButtons.signPayment(p));
            p.actionButtons.push(new actionButtons.copyPayment(p));
            p.actionButtons.push(new actionButtons.deletePayment(p));

            return p;
        }

        this.newPayment = function (data, e) {
            e.stopPropagation();
            var link = utils.buildLink('PaymentWizard', 'newPaymentOrder');
            utils.navigate(link, { trigger: true });
            return false;
        }

        this.paymentTemplates = function (data, e) {
            e.stopPropagation();
            var link = utils.buildLink('ViewTemplates', 'index');
            utils.navigate(link, { trigger: true });
            return false;
        }
    };

    return ViewPaymentsViewModel;
});