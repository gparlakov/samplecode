﻿/// <reference path="../../../../libs/require.intellisense.js" />
define([
  'jquery',
  'underscore',
  'knockout',
  'komapping',
  'services',
  'validator',
  'Mobile/ViewModels/Payments/PaymentDocsDetails/DetailsBaseViewModel',
  'Mobile/ViewModels/Payments/Helpers/OpCodesViewModel',
  'text!Mobile/Templates/Payments/Wizard/Details/Partials/OpCodesTemplate.html',
  
], function ($, _, ko, komapping, services, validator, baseViewModel, opcodeViewModel) {

    
    function DocFCCYSimple(original, wizardOutputModel) {

        _.extend(
            this,
            komapping.fromJS(
                original,
                {
                    "OpCode": {
                        create: function (options) {
                            return ko.observable(options.data);
                        }
                    }
                }
            )
        );
    }

    function DocFCCYSimpleDetailsViewModel(model, wizardOutputModel) {
        
        validator.validatableModel.call(this, function () {
            return {
                properties: {
                    "Details": {
                        properties: {
                            "PayeeAddress": {
                                rules: {
                                    NotNullOrEmpty: true,
                                    MaxLength: 35,
                                    SWIFTSymbols: true
                                }
                            },
                            "PayerAddress": {
                                rules: {
                                    NotNullOrEmpty: true,
                                    MaxLength: 70,
                                    SWIFTSymbols: true
                                }
                            },
                            "Details1": {
                                rules: {
                                    NotNullOrEmpty: true,
                                    MaxLength: 35,
                                    SWIFTSymbols: true
                                }
                            },
                            "Details2": {
                                rules: {
                                    MaxLength: 35,
                                    SWIFTSymbols: true
                                }
                            },
                            "Details3": {
                                rules: {
                                    MaxLength: 35,
                                    SWIFTSymbols: true
                                }
                            },
                            "Details4": {
                                rules: {
                                    MaxLength: 35,
                                    SWIFTSymbols: true
                                }
                            },
                            "DirtyMoney": {
                                rules: {
                                    NotNullOrEmpty: function () { return self.EspeciallyNeedsDirtyMoneyDeclaration; }
                                }
                            },

                            "CreditNumber": {
                                rules: {
                                    NotNullOrEmpty: function () { return self.AdditionalViewData.CreditNumberRequiered(); },
                                    MaxLength:10,
                                    RegEx: '^[0-9]{1,10}$'
                                },
                            },
                            "OpCode": {
                                rules: {
                                    NotNullOrEmpty: true
                                }
                            }
                        }
                    }
                }
            }
        });

        var self = this;
        
        baseViewModel.call(this, model, wizardOutputModel);
        opcodeViewModel.call(this, model, wizardOutputModel);

        this.initialize = function () {
            var result = 
                $.whenEx(self.initializeBase())
                    .pipe(function (p) {
                        return self.initializeOpcodes();
                    });

            return result;
        }

        this.Details = new DocFCCYSimple(model);
        
    };

    return DocFCCYSimpleDetailsViewModel;
});