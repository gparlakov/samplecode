﻿/// <reference path="../../../../libs/require.intellisense.js" />
define([
  'jquery',
  'underscore',
  'knockout',
  'komapping',
  'services',
  'validator',

   'Common/ufnomenclatures/ValueDate',
   'Common/ufnomenclatures/PaymentDirection',
   'Common/ufnomenclatures/PaymentBankExpenses',
   'Common/ufnomenclatures/UFNmFactory',
   'Mobile/ViewModels/Payments/PaymentDocsDetails/DetailsBaseViewModel',
   'Mobile/ViewModels/Payments/Helpers/OpCodesViewModel',
   'text!Mobile/Templates/Payments/Wizard/Details/Partials/OpCodesTemplate.html',

   //vremenno
  'text!Mobile/Templates/Payments/Wizard/Details/Partials/PaymentBankCorrespondent.html',
  'text!Mobile/Templates/Payments/Wizard/Details/Partials/PaymentValueDate.html',
  'text!Mobile/Templates/Payments/Wizard/Details/DocFCCYSimpleDetailsViewModel.html',
  'text!Mobile/Templates/Payments/Wizard/Details/Partials/PaymentBankExpenses.html',

], function ($, _, ko, komapping, services, validator, valueDateOptions, paymentDiretionOptions, paymentBankExpensesOptions, ufnmFactory, baseViewModel, opcodeViewModel) {

    function DocFCCYFull(original) {
        var self = this;

        _.extend(
            this,
            komapping.fromJS(
                    original,
                    {
                        "CorrBankCountry": {
                            create: function (options) {
                                return ko.observable(options.data);
                            }
                        }, //iskame observable-a da e na nivo BankCountry a ne na listata
                        "OpCode": {
                            create: function(options){
                                return ko.observable(options.data);
                            }
                        }
                    } 
                )
        );
    }
    
    function DocFCCYFullDetailsViewModel(model, wizardOutputModel) {
       
        var self = this;

        validator.validatableModel.call(this, function () {
            return {
                properties: {
                    "Details": {
                        properties: {
                            "PayeeAddress": {
                                rules: {
                                    NotNullOrEmpty: true,
                                    MaxLength: 70,
                                    SWIFTSymbols: true
                                }
                            },
                            "PayerAddress": {
                                rules: {
                                    NotNullOrEmpty: true,
                                    MaxLength: 70,
                                    SWIFTSymbols: true
                                }
                            },
                            "Details1": {
                                rules: {
                                    NotNullOrEmpty: true,
                                    MaxLength: 35,
                                    SWIFTSymbols: true
                                }
                            },
                            "Details2": {
                                rules: {
                                    MaxLength: 35,
                                    SWIFTSymbols: true
                                }
                            },
                            "Details3": {
                                rules: {
                                    MaxLength: 35,
                                    SWIFTSymbols: true
                                }
                            },
                            "Details4": {
                                rules: {
                                    MaxLength: 35,
                                    SWIFTSymbols: true
                                }
                            },
                            "CorrBankName": {
                                rules: {
                                    MaxLength: 35,
                                    SWIFTSymbols: true
                                }
                            },
                            "CorrBankAddress": {
                                rules: {
                                    MaxLength: 35,
                                    SWIFTSymbols: true
                                }
                            },
                            "CorrBankCity": {
                                rules: {
                                    MaxLength: 35,
                                    SWIFTSymbols: true
                                }
                            },
                            "CorrBankSWIFT": {
                                rules: {
                                    BankSWIFT: true,
                                    InternationalSWIFTForCountry: ko.dependentObservable({
                                        read: function () { return self.AdditionalViewData.SelectedCorespondentCountry() != null ? self.AdditionalViewData.SelectedCorespondentCountry().Value : null; }
                                    }),
                                }
                            },
                            "DirtyMoney": {
                                rules: {
                                    NotNullOrEmpty: function () { return self.EspeciallyNeedsDirtyMoneyDeclaration; }
                                }
                            },
                            "CreditNumber": {
                                rules: {
                                    NotNullOrEmpty: function () { return self.AdditionalViewData.CreditNumberRequiered(); },
                                    MaxLength: 10,
                                    RegEx: '^[0-9]{1,10}$'
                                }
                            },
                            "OpCode": {
                                rules: {
                                    NotNullOrEmpty: true
                                }
                            }

                        }
                    }
                }
            }
        }); 

        baseViewModel.call(this, model, wizardOutputModel);
        opcodeViewModel.call(this, model, wizardOutputModel);

        this.Details = new DocFCCYFull(model);
        var AdditionalViewData = this.AdditionalViewData;

        //this.AdditionalViewData.AdditionalViewData = this.AdditionalViewData;

        this.AdditionalViewData.CountriesOptions = ko.observable(null);

        this.AdditionalViewData.SelectedCorespondentCountry = ko.computed({
            read: function () {
                if (self.Details.CorrBankCountry() != null && self.AdditionalViewData.CountriesOptions() != null) {
                    return _.find(
                            self.AdditionalViewData.CountriesOptions(),
                            function (item) {
                                return item.Value.Code == self.Details.CorrBankCountry().Code;
                            });
                }
                else {
                    return null;
                }

            },
            write: function (newValue) {
                if (newValue) {
                    self.Details.CorrBankCountry(
                            new services.Objects.CountryKey(newValue.Value)
                        );
                }
                else {
                    self.Details.CorrBankCountry(null);
                }
            }
        });

        this.initialize = function () {
            
            var nmFactory = new ufnmFactory();
             

            var result =
                $.whenEx(
                    nmFactory.GetUfNomenclature(nmFactory.ufNomenclatureTypes.NomenclatureCountry)
                )
                .done(function (nm) {
                    var nomenklature = nm.GetNomenclature();
                    var toShow = _.map(nomenklature, function (item) {
                        return {
                            Name: $.GetLocalizedString(item.Name),
                            Value: item
                        }
                    });
                    self.AdditionalViewData.CountriesOptions(toShow);
                }).pipe(function (p){
                    return self.initializeBase();
                }).pipe(function(p){
                    return self.initializeOpcodes();
                });

            return result;
        }

        
        AdditionalViewData.ValueDateOptions = valueDateOptions;

        AdditionalViewData.SelectedValueDate = ko.computed( {
            read : function () {
                if (this.Details.ValueDate) {
                    var tmpValDate = this.Details.ValueDate();
                    var res = _.find(this.AdditionalViewData.ValueDateOptions, function (valDate) {
                        return valDate.ID == tmpValDate;
                    });
                    return res;
                }
                else {
                    return null;
                }
            },
            write: function (value) {
                this.ValueDate(value.ID);
            },
            owner: this
        });

        AdditionalViewData.DiretionOptions = paymentDiretionOptions;

        AdditionalViewData.SelectedDirection = ko.computed({
            read: function () {
                if (this.Details.Direction) {
                    var tmpDirection = this.Details.Direction();
                    var res = _.find(this.AdditionalViewData.DiretionOptions, function (direction) {
                        return direction.ID == tmpDirection;
                    });
                    return res;
                }
                else {
                    return null;
                }
            },
            write: function (value) {
                this.Details.Direction(value.ID);
                this.Details.BenefType(value.BenefType);
            },
            owner: this
        });

        AdditionalViewData.ExpensesOptions = paymentBankExpensesOptions;

        AdditionalViewData.SelectedExpenses = ko.computed({
            read: function () {
                if (this.Details.PayerBankExpenses) {
                    var tmpExpense = this.Details.PayerBankExpenses();
                    var res = _.find(this.AdditionalViewData.ExpensesOptions, function (direction) {
                        return direction.ID == tmpExpense;
                    });
                    return res;
                }
                else {
                    return null;
                }
            },
            write: function (value) {
                this.Details.PayerBankExpenses(value.ID);
            },
            owner: this
        });
    };

    return DocFCCYFullDetailsViewModel;
});