﻿define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'services',
  'typeVisitor',
  'events',
  'validator', 
  'text!Mobile/Templates/Payments/Wizard/ButtonsTemplate.html',
  'Mobile/ViewModels/Payments/Helpers/PaymentWizardSteps',

  'i18n!Mobile/nls/strings',

  'text!Mobile/Templates/Payments/Wizard/ButtonsSummaryTemplate.html',
  
], function ($, _, backbone, ko, komapping, services, typeVisitor, events, validator, buttonsTemplate, wizardSteps, strings, buttonsSummaryTemplate) {

    

    var PaymentWizardBaseViewModel = function PaymentWizardBaseViewModel(modelInput) {
        var model = modelInput;
        if (model == null) {
            model = {};
        }

        this.Model = model;

        //var viewModel = komapping.fromJS(model, { 'ignore': ["State"] });

        //viewModel.State = model.State;
        this.strings = strings;

        this.Init = function (initCompletedCallback) {
            if (initCompletedCallback) {
                initCompletedCallback.call(this);
            }
        }

        //viewModel.Model = model;

        this.GetNonObservable = function (prop) {
            return komapping.toJS(prop);
        }
        //ne tryabva da vzima kato parametar nishto, tryabva da raboi varfu sebe si
        this.GetRequestModelForNext = function (actionModel) {
            throw "Each step specific view model must override this method";
        }

        this.validate = function ()
        {
            if (validator.isValidatable(this)) {
                return validator.getValidator(this).validate();
            }
            else {
                return true;
            }
        }
        this.MoveNext = function (actionModel, e) {
            
            if (actionModel.validate()) {
                events.trigger(actionModel, "next", actionModel);

            }
        }
        //ne tryabva da vzima kato parametar nishto, tryabva da raboi varfu sebe si
        this.GetRequestModelForBack = function (actionModel) {
            throw "Each step specific view model must override this method";
        }

        this.MoveBack = function (actionModel,e) {
            events.trigger(actionModel, "back", actionModel);
            //var requestModel = viewModel.GetRequestModelForBack(actionModel);
            //services.PaymentWizardService.PaymentWizardBack(requestModel, function (data) {
            //    paymentWizardCache.SetWizardOutput(data);
            //    window.history.back();
            //    // backbone.history.navigate("PaymentWizard/onMovenext/" + (new Date()).getTime(), { trigger: true });
            //});
        }

        this.AfterRender = function (el) {

        }

        this.StepsInformation = new wizardSteps();
        this.StepsInformation.MarkSteps(ko.utils.unwrapObservable(this.Model.$type));
    };

    return PaymentWizardBaseViewModel;
});
