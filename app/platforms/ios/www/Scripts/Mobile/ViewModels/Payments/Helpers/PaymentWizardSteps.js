﻿define([
    'underscore',
    
    'text!Mobile/Templates/Payments/Wizard/Steps/CurrentStepTemplate.html'
], function (_, currentTemplate) {

    var PaymentWizardStepsModel = [
         {
             $type: 'DAIS.eBank.Contracts.MultiLangString, DAIS.eBank.Contracts.Extended',
             name: 'PaymentWizardChoosePayerAndAmount',
             stepNumber: '1',
             description: [{
                 Lang: 1,
                 Value: 'Моля, въведете сметка на наредителя, сума и валута'
             },
            {
                Lang: 2,
                Value: "Please enter payer's account, amount and currency"

            }],

             tittle: [{
                 Lang: 1,
                 Value: 'Данни за наредител'
             }, {
                 Lang: 2,
                 Value: 'Payer details'
             }]
         },
         {
             $type: 'DAIS.eBank.Contracts.MultiLangString, DAIS.eBank.Contracts.Extended',
             name: 'PaymentWizardChoosePayee',
             stepNumber: '2',
             description: [{
                 Lang: 1,
                 Value: 'Моля, изберете сметката на получателя'
             },
            {
                Lang: 2,
                Value: "Please select payee's account"

            }],
             tittle: [{
                 Lang: 1,
                 Value: 'Получател'
             }, {
                 Lang: 2,
                 Value: 'Payeе'
             }]
         },
         {
             $type: 'DAIS.eBank.Contracts.MultiLangString, DAIS.eBank.Contracts.Extended',
             name: 'PaymentWizardFillDetails',
             stepNumber: '3',
             description: [{
                 Lang: 1,
                 Value: 'Моля, попълненете детайлите на превода'
             },
               {
                   Lang: 2,
                   Value: "Please fill transfer details"

               }],
             tittle: [{
                 Lang: 1,
                 Value: 'Детайли'
             }, {
                 Lang: 2,
                 Value: 'Details'
             }]
         },
         {
             $type: 'DAIS.eBank.Contracts.MultiLangString, DAIS.eBank.Contracts.Extended',
             name: 'PaymentWizardSummary',
             stepNumber: '4',
             description: [{
                 Lang: 1,
                 Value: 'Потвърждение на превода'
             },
              {
                  Lang: 2,
                  Value: 'Transfer confirmation'

              }],
             tittle: [{
                 Lang: 1,
                 Value: 'Преглед'
             }, {
                 Lang: 2,
                 Value: 'Preview'
             }]
         }];

    var PaymentWizardSteps = function () {

        var self = this;

        //this.wizardSteps = [{ name: 'PaymentWizardChoosePayerAndAmount' }, { name: 'PaymentWizardChoosePayee' }, { name: 'PaymentWizardFillDetails' }, { name: 'PaymentPreview' }];
        this.wizardSteps = PaymentWizardStepsModel;

        this.wizardStepsBefore = [];
        this.wizardStepsAfter = [];
        this.currentStep = null;

        this.MarkSteps = function (currentStepName) {
            var currPassed = false;
            _.each(self.wizardSteps, function (step) {
                if (currentStepName.indexOf(step.name) != -1) {
                    //if (step.name == currentStepName) {
                    step.position = 'current';
                    self.currentStep = step;
                    currPassed = true;

                } else {
                    if (!currPassed) {
                        step.position = 'beforeCurrent';
                        self.wizardStepsBefore.push(step);
                    } else {
                        step.position = 'afterCurrent';
                        self.wizardStepsAfter.push(step);
                    }
                }
            });

        }
    };
    return PaymentWizardSteps;
});