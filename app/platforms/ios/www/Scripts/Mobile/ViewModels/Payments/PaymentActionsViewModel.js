﻿define([
    'backbone',
  'utils',
  
  //'i18n!Mobile/nls/strings'

], function (backbone, utils) {

    function ViewPaymentViewModel(paymentKey) {

        this.editPayment = function (payment, e) {
            
            e.stopPropagation();
            var link = utils.buildLink('PaymentWizard', 'editPayment', [payment.PaymentIncarnationType, payment.ID]);
            utils.navigate(link, { trigger: true });
            return false;
           
        }

        this.deletePayment = function (payment, e) {
          
            e.stopPropagation();
            var link = utils.buildLink('ProcessPayments', 'delete', [payment.ID]);
            utils.navigate(link, { trigger: true });
            return false;
        
        }

        this.copyPayment = function (payment, e) {

            e.stopPropagation();
            var link = utils.buildLink('PaymentWizard', 'cereteLikePayment', [payment.PaymentIncarnationType, payment.PaymentIncarnationType, payment.ID]);
            utils.navigate(link, { trigger: true });
            return false;
        
        }

        this.signPayment = function (payment, e) {

            e.stopPropagation();
            var link = utils.buildLink('ProcessPayments', 'sign', [payment.ID]);
            utils.navigate(link, { trigger: true });
            return false;
           
        }
    };

    return ViewPaymentViewModel;
});