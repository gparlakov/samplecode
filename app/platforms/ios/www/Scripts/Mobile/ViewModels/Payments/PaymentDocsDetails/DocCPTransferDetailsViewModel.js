﻿define([
  'jquery',
  'underscore',
  'knockout',
  'komapping',
  'services',
  'validator',

  'Mobile/ViewModels/Payments/PaymentDocsDetails/DetailsBaseViewModel'
], function ($, _, ko, komapping, services, validator, baseViewModel) {

    function DocCPTransfer(original) {

        _.extend(this, komapping.fromJS(original));    
    }    

    function DocCPTransferDetailsViewModel(model, wizardOutputModel) {

        validator.validatableModel.call(this, function () {
            return {
                properties: {
                    "Details": {
                        properties: {
                            "DirtyMoney": {
                                rules: {
                                    NotNullOrEmpty: function () { return self.EspeciallyNeedsDirtyMoneyDeclaration; }
                                }
                            }
                        }
                    }
                }
            }
        });

        var self = this;
        
        baseViewModel.call(this, model, wizardOutputModel);

        this.initialize = function () {
            var result = self.initializeBase();
            return result;
        }

        this.Details = new DocCPTransfer(model);
        
    
    };

    return DocCPTransferDetailsViewModel;
});