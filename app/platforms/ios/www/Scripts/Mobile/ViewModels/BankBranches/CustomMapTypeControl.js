﻿define([
    'jquery',
    'knockout',
    'i18n!Mobile/nls/googleMaps',
    'mapBaseControl',
    'events',
    'text!Mobile/Templates/BankBranches/CustomMapTypeControlTemplate.html'

], function ($, ko,googleMaps, baseControl, events, template) {

    var buttons = {
        //id -tata shte ni trqbvat .
        //posle po tqh si vzimame gotovite butonite (sled self.create) 
        Branches: {
            id: 'mapTypeBankBranches',
            visible: true,
        },
        ATMs: {
            id: 'mapTypeATMs',
            visible: true,
        }
    }

    var MAP_TYPE_CHANGED_EVENT_NAME = 'mapTypeChanged';
    var ACTIVE_TYPE_BUTTON_CLASS = 'active';

    function CustomMapType(map, options) {


        var self = this;


        this.eventTypes = {};
        this.eventTypes.CURRENT_MAP_TYPE_CLICKED_EVENT_NAME = 'mapTypeCurrentClick';
        
        baseControl.call(this, template);
        this.map = map;
                
        this.brancesHandler = function (viewModel, event) {
            self.mapTypeCommonHandler(buttons.Branches);
            if (event)
                event.stopPropagation();
            return false;
        }

        this.ATMsHandler = function (viewModel, event) {
            self.mapTypeCommonHandler(buttons.ATMs);
            if (event)
                event.stopPropagation();
            return false;
        }
        
        
        this.mapTypeCommonHandler = function (button) {
            if (button != self.currentActiveButton) {

                events.trigger(self, MAP_TYPE_CHANGED_EVENT_NAME, button);
                self.currentActiveButton = button;

                for (b in self.mapTypeButtons) {
                    self.mapTypeButtons[b].uiElement.removeClass(ACTIVE_TYPE_BUTTON_CLASS);
                }
                self.currentActiveButton.uiElement.addClass(ACTIVE_TYPE_BUTTON_CLASS);
            } else {
                events.trigger(self, self.eventTypes.CURRENT_MAP_TYPE_CLICKED_EVENT_NAME, button);
            }
        }

        self.mapTypeButtons = buttons;

        //za da sprem klickaneto po edin i sashti buton :( 
        self.currentActiveButton = null;
        self.visible = ko.observable(true);

        self.show = function () {
            self.visible(true);
        }
        self.hide = function () {
            self.visible(false);
        }
        //tova e to BaseControla - sazdava HTML obektite i apply-va binding-i 
        self.create();
        
        for (b in self.mapTypeButtons) {
            self.mapTypeButtons[b].uiElement = $('#' + self.mapTypeButtons[b].id, self.control);
        }

        var mapTileLoadListener = googleMaps.event.addListenerOnce(map, 'tilesloaded', function (e) {
            //preventva eventa, a v tozi sluchai ne iskame. Zatova ne mu podavame nishto 
            self.brancesHandler();
        });

        //events.bind(self, 'rendered', function () {
        //    self.brancesHandler();
        //});
        //this.brancesHandler();
    }

    return CustomMapType;
});