﻿define([
    'jquery',
    'i18n!Mobile/nls/googleMaps',
    'mapBaseControl',
    'knockout',
    'events',
    'text!Mobile/Templates/BankBranches/FilterControl/BankStructureFilterControlTemplate.html',
    //'text!Mobile/Templates/BankBranches/FilterControl/BankBranchesFilterControlTemplate.html'

], function ($, googleMaps, baseControl, ko, events, template) {

    //moje da se napravi dinamichno da se tegli ot bazata 
    // no nqma smisal 
    var offeredBankServices = [
        {
            selected : false,
            BankService: {
                ID: 0
            }
        },
        {
            selected: false,
            BankService: {
                ID :1 
            }
        },
        {
            selected: false,
            BankService: {
                ID: 3
            }
        }
    ];

    function BankStructureFilterControl(map, options) {

        //this.currentFilter = 'BankBranchesFilter';

        var self = this;
        vm = self;
        baseControl.call(this, template);
        this.map = map;
        

        self.control.addClass('filterContainer');
        this._valueChangedTriggered = false;
        this.triggerValuesChanged = function () {
            //console.log('trigger filter changed ' + !self._valueChangedTriggered);

            if (!self._valueChangedTriggered) {
                self._valueChangedTriggered = true;
                setTimeout(function () {
                    events.trigger(self, 'filterValuesChanged', self.GetFilter());
                    self._valueChangedTriggered = false;
                }, 0);
            }
        }

        this.bind = function (memberName, hostEventObject, eventName) {
            events.bind(hostEventObject, eventName, self[memberName]);
        }
        
        this.toggleFilter = function () {

            self.visible(!self.visible());

            //e.stopPropagation();
            //e.stopImmediatePropagation();
            return false;
        }
        this.show = function () {
            self.visible(true);
        }
        this.hide = function (viewmodel, e) {
            //console.log('hide filter');
            self.visible(false);
        }
        
        this.enableHideOnBodyClick();
        //this._bodyClickHandler = function (e) {

        //    if (self.visible()) {

        //        var targetIsInFilter = $(e.target).parents('.branchFilter');

        //        if (targetIsInFilter.length == 0) {
        //            self.hide();
        //        }
        //    }
        //}

        //$('body').on('click', this._bodyClickHandler);

        this.visible = ko.observable(true);

        this.filterBranchesVisible = ko.observable(false);
        this.filterAtms = ko.observable(true);

        this.showATMsFilter = function () {
            self.clear();
            self.filterBranchesVisible(false);
            self.filterAtms(true);
            self.visible(true);
            
        }

        this.showBranchesFilter = function () {
            self.clear();
            self.filterAtms(false);
            self.filterBranchesVisible(true);
            self.visible(true);
        }

        this.clear = function () {
            self.extendedWorkingTime(false);
            self.worksWeekend(false);
            self.isInBank(false);
            self.nonStopZone(false);
        }

        this.extendedWorkingTime = ko.observable(false);
        this.extendedWorkingTime.subscribe(this.triggerValuesChanged);

        this.worksWeekend = ko.observable(false);
        this.worksWeekend.subscribe(this.triggerValuesChanged);

        this.isInBank = ko.observable(false);
        this.isInBank.subscribe(this.triggerValuesChanged);

        this.nonStopZone = ko.observable(false);
        this.nonStopZone.subscribe(this.triggerValuesChanged);

        this.GetFilter = function () {
            filter = {
                OfferedBankServices: [], 
                AdditionalProps: {} 
            };

            var extendeWorkingTime = self.extendedWorkingTime() ? filter.OfferedBankServices.push({ BankService: { ID: 0 } }) : null;
            var worksWeekend = self.worksWeekend() ? filter.OfferedBankServices.push({ BankService: { ID: 3 } }) : null;
            var nonStopZone = self.nonStopZone() ? filter.OfferedBankServices.push({ BankService: { ID: 1 } }) : null;

            var isInBank = self.isInBank() ? filter.AdditionalProps.IsInBank = true : null;

            return filter;
        }

        self.create();
        
        //this.destroy = function () {
        //    $('body').off('click', self._bodyClickHandler);
        //}
    }

    return BankStructureFilterControl;
});