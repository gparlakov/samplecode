﻿define([
  'underscore',
  'jqueryformat',
  'text!Mobile/Templates/Common/AccountLabel.html',
  ], function (_, $format, accountLabelPartialTemplate) {

    function AccountBalanceLabelHelper() {
        this.GetAccountFormattedBalanceFromAccount = function (account) {
            return account.BankAccount.BankAccountBalances ? $format.number(account.BankAccount.BankAccountBalances[0].AvailableBalance) : '';
        };

    }
    return AccountBalanceLabelHelper;
  });