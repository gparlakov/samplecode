﻿define([
  'jquery',
  'underscore',
  'knockout',
  'services',

   'Mobile/ViewModels/Base/PopupPageViewModel',
   'text!Mobile/Templates/Accounts/Movements/Partials/DatesPaymentSystem.html'

], function ($, _, ko, services, PopupPageViewModel)
{

    function MovementDocHelperPopUp(parent, options) {

        var self = this;

        //PopupPageViewModel.apply(this, arguments);
        PopupPageViewModel.apply(this, arguments);

        this.path = "Accounts/Movements/MovementDocumentTemplate";

        this.MovementDocument = ko.observable();

        //this.SelectAccount = function (account) {
        //    self["return"](account);
        //}
        
        //this.ClearAccount = function () {
        //    //a moje i s null ? 
        //    self["return"]({dummyForOptionAll: null});
        //}

        this.loadData = function () {
            var result =
                $.whenEx(
                    options.load()
                )
                .done(function (movementDoc) {
                    self.MovementDocument(movementDoc.BankProductMovementDocument);
                });

            return result;
        };
    }

    return MovementDocHelperPopUp;
});