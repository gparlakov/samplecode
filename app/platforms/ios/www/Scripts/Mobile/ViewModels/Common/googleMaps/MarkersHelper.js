﻿define([
  'jquery',
  'underscore',
  'i18n!Mobile/nls/googleMaps',
  'mapBaseControl',
  'typeVisitor',
  'gmapsInfoBox',
  'Mobile/ViewModels/Common/BankStructureHelperPopUp',
  'text!Mobile/Templates/BankBranches/BankBranchInfoWindowTemplate.html',
  'text!Mobile/Templates/BankBranches/AtmInfoWindowTemplate.html',
  'text!Mobile/Templates/BankBranches/ViewBankStructure/OfferedBankServices.html'
  //'text!Mobile/Templates/Common/googleMaps/InfoBoxTemplate.html',
  
], function ($, _, googleMaps, mapBaseControl, typevisitor, InfoBox, bankStructureHelperPopUp, branchTemplate, atmTemplate, bankServicesTemplate ) {

    var infoWindowContent = function (bankStructure, template) {
        var self = this;

        this.bankStructure = bankStructure;

        this.bankStructureHelperPopUp = new bankStructureHelperPopUp(self,
          { load: function () { return self.cuttentStructure } }
        );

        mapBaseControl.call(this, template);

        this.cuttentStructure = {};

        this.showBranch = function (viewModel) {
            console.log('show branch');
            self.cuttentStructure = viewModel.bankStructure;
            self.bankStructureHelperPopUp.Open(true);
        }

        this.showATM = function (viewModel) {

            self.cuttentStructure = viewModel.bankStructure;
            self.bankStructureHelperPopUp.Open(true);
        }

        this.create();

    }

    var infoBoxOptions = {
        disableAutoPan: false,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(-140, 0),
        zIndex: null,
        //boxClass : '',
        boxStyle: {
            background: "white"
          , opacity: 0.85
          , width: "280px"
        }
        //,closeBoxMargin: "10px 2px 2px 2px"
        , closeBoxURL: 'Content/images/close.png',
        //,infoBoxClearance: new google.maps.Size(1, 1)
        //,isHidden: false
        pane: "floatPane",
        enableEventPropagation: false

    };

    function MarkersHelper(map) {

        var self = this;
        //this.parent = parent;
        this.markerMyPosition = null;
        this.createMarkerOnMe = function (position) {
            if (!self.markerMyPosition) {
                self.markerMyPosition = new googleMaps.Marker({
                    position: position,
                    map: map
                });
            } else {
                self.markerMyPosition.setPosition(position);
            }
        }

        this.allMarkers = {};

        this.clearMarkers = function () {
            _.each(self.allMarkers, function (marker) { marker.marker.setMap(null) });
            self.allMarkers = {};
        }

        var markerTitleVisitor = {
            BankStructureItem: function (item) { return $.GetLocalizedString(item.name) },
            Other: function () { return '' }
        }

        var infoWindowContenVisitor = {
            BankStructureItem: function (bankBranch) { return new infoWindowContent(bankBranch, branchTemplate) },
            ATM: function (atm) { return new infoWindowContent(atm, atmTemplate) },
            Other: function () { return {render : function() {}  } }
        }

        self.infoBox = new InfoBox(infoBoxOptions);

        this.creatMarker = function (bankStructure, doNotShow) {

            var id = bankStructure.ID;

            if (typeof (self.allMarkers[id]) == 'undefined') {

                var markerTitle = typevisitor.visit(bankStructure, markerTitleVisitor);

                var latlngCity = new googleMaps.LatLng(bankStructure.LocationOnMap.Latitude, bankStructure.LocationOnMap.Longitude);

                var marker = new googleMaps.Marker({
                    map: (typeof(doNotShow) == 'undefined' || !doNotShow)  ? map: null,
                    position: latlngCity,
                    title: markerTitle,
                    clickable: true,
                    optimized: false,
                    icon: 'Content/images/rbb-logo-small.jpg',
                    bankStructureID: bankStructure.ID
                });

                googleMaps.event.addListener(marker, 'click', function (event) {
                    // nqma prqka vrazka mejdu markera i klona . 
                    // bankStructureID-to sme go kodnali po-gore, za da moje da gi svarjem 
                    var bankStructure = self.allMarkers[marker.bankStructureID].bankStructure

                    var contentObj = typevisitor.visit(bankStructure, infoWindowContenVisitor);
                                    
                    var renderedContent = contentObj.render();
                    self.infoBox.setContent(renderedContent);

                    //zatvarqme profilaktichno otvoreniq infoBox. (ako nqma otvoren ne e problem) 
                    self.infoBox.close();
                    self.infoBox.open(map, marker);
                    
                    //tova ne e mnogo qko . Ne se fire-va, ako natishnesh direktno na kartata 
                    $(renderedContent).on('mouseout', null, null, function () {
                            self.infoBox.close();
                        }, true);
                    //googleMaps.event.addListenerOnce(marker, 'mouseout', function () {
                    //    self.infoBox.close();
                    //});
                });
                
                self.allMarkers[id] = { marker: marker, bankStructure: bankStructure };
            }
        }
        
        this.showOnly = function (predicate) {
            _.each(self.allMarkers, function (m) {
                if (predicate(m.bankStructure)) {
                    if (!m.marker.getVisible()) {
                        m.marker.setVisible(true);
                    }
                    //if (!m.marker.getMap()) {
                    //    m.marker.setMap(map);
                    //}
                } else {
                    m.marker.setVisible(false);
                    //m.marker.setMap(null);
                }
            })
        }

        //self.creatMarker(bankBranch.ID, bankBranch.LocationOnMap.Latitude, bankBranch.LocationOnMap.Longitude, message);
        this.destroy = function () {
            self.clearMarkers();
        }
    }

    return MarkersHelper;
});