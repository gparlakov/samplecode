﻿define([
  'jquery',
  'underscore',
  'knockout',
], function ($, _, ko) {

    function AdvancedFilter(parent, options) {
        var self = this;

        self.AdvancedFilterVisible = ko.observable(false);

        self.ShowHideAdvancedFilter = function () {
            var visible = self.AdvancedFilterVisible();
            if (visible) {
                $(".advancedFilter").removeClass('open');
            } else {
                $(".advancedFilter").addClass('open');
            }
            self.AdvancedFilterVisible(!visible);
        }
    }

    return AdvancedFilter;
});