﻿define([
  'underscore',  
  'i18n!Mobile/nls/googleMaps',
  
], function (_, googleMaps) {

    function BoundHelper() {

        var self = this;

        this._visitedBounds = [];
        
        this._cotainsBaounds = function(parent, b){
            //parent.countains(b.getNorthEast());
            //parent.countains(b.getSouthWest());
            var nw = new googleMaps.LatLng(b.getNorthEast().lat(), b.getSouthWest().lng());
            var se = new googleMaps.LatLng(b.getSouthWest().lat(), b.getNorthEast().lng());

            return parent.contains(b.getNorthEast()) && parent.contains(nw) &&
                   parent.contains(b.getSouthWest()) && parent.contains(se);


        }

        this.isVisited = function (bounds) {
            //if (self._visitedBound.length == 0 ) {
            //    return false;
            //}else {

            //moqt sadarja li se v nqkoi drug 
            var parentBounds = _.find(self._visitedBounds, function (b) {
                return self._cotainsBaounds(b, bounds);
            });

            return typeof (parentBounds) != "undefined";
        }
        
        this.clearBounds = function () {
            this._visitedBounds = [];
        }

        this.visit = function (bounds) {
            // mahame vsichki koito se sadarjat v moq 
            self._visitedBounds = _.filter(self._visitedBounds, function (b) {
                return !self._cotainsBaounds(bounds, b);

            });

            self._visitedBounds.push(bounds);
        }
        
    }

    return BoundHelper;
});