﻿define([
    'knockout',
    'jsrender',
    'i18n!Mobile/nls/strings'
], function (ko, jsrender, strings) {

    UINotificationType = {
        Success: 1,
        Error: 2,
        Info: 3
    }

    function UINotification(initValues) {
        this.Type = initValues.Type ? initValues.Type : UINotificationType.Success;
        this.Types = UINotificationType;
        this.Message = initValues.Message ? initValues.Message : "???";
    }

    function UINotificationHtml(initValues) {
        UINotification.call(this, initValues);

        var resHtml = "&nbsp;";

        if (initValues.MessageStringID) {

            var strTemplate = strings[initValues.MessageStringID];
            if (initValues.MessageStringData) {

                var compiledTemplate = jsrender(strTemplate);
                resHtml = compiledTemplate.render(initValues.MessageStringData);
            }
            else {
                resHtml = strTemplate;
            }
        }

        this.MessageHtml = resHtml;
    }

    function UINotificationContainer() {
        this.Notifications = ko.observableArray([]);
        this.AddNotification = function AddNotification(notification) {
            this.Notifications.push(notification);
        }
        this.ClearNotifications = function ClearNotifications() {
            this.Notifications.removeAll();
        },
        this.ClearAndAddNotification = function CleatAndAdd(notification)
        {
            this.ClearNotifications();
            this.AddNotification(notification);
        }
    }


    return {
        Notifications: {
            UINotificationContainer: UINotificationContainer,
            UINotification: UINotification,
            UINotificationHtml : UINotificationHtml,
            UINotificationType: UINotificationType
        }
    };
});