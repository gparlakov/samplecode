﻿define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'config',
  'services',
  'encryptor',
  'validator',
  'productscache',
  'session',
  'events',
  'uimodels',
  'i18n!Mobile/nls/strings',
  'Common/nomenclatures/enumLang',
  'Common/cache/language',
  'deviceapi',
  'utils',
  'version',
  'osManager',
  'Mobile/ViewModels/CaptchaViewModel',
  'Common/nomenclatures/enumOSType',
  'pushNotifications'
], function ($, _, Backbone, ko, config, services, encryptor, validator, productscache, session, events, uimodels, strings, enumLang, languageManager, deviceapi, utils, version, osManager, CaptchaViewModel, enumOsType, pushNotifications) {

    SignOnRsStatus = [
        { Code: 0, Name: "Success", Succes: true, CanRetry: false, Text: "ID_STR_LOGIN_STATUS_SUCCESS" },
        { Code: 100, Name: "General_Error", Succes: false, CanRetry: true, Text: "ID_STR_LOGIN_STATUS_GENERAL_ERROR" },
        { Code: 1740, Name: "Authentication_Failed", Succes: false, CanRetry: true, Text: "ID_STR_LOGIN_STATUS_AUTHENTICATION_FAILED" },
        { Code: -2, Name: "Authentication_Failed_TryLater", Succes: false, CanRetry: true, Text: "ID_STR_LOGIN_STATUS_AUTHENTICATION_FAILED_TRYLATER" },
        { Code: 1880, Name: "Customer_Locked_Out", Succes: false, CanRetry: true, Text: "ID_STR_LOGIN_STATUS_INVALID_ADDTITIONALANTIBOTPROTECTIONREQUIRED" }, //ID_STR_LOGIN_STATUS_CUSTOMER_LOCKED_OUT
        { Code: 1820, Name: "Customer_Session_Already_In_Progress", Succes: false, CanRetry: false, Text: "ID_STR_LOGIN_STATUS_CUSTOMER_SESSION_ALREADY_IN_PROGRESS" },
        { Code: 1900, Name: "Password_Change_Required", Succes: false, CanRetry: false, Text: "ID_STR_LOGIN_STATUS_PASSWORD_CHANGE_REQUIRED" },
        { Code: 1930, Name: "Login_Change_Required", Succes: false, CanRetry: false, Text: "ID_STR_LOGIN_STATUS_LOGIN_CHANGE_REQUIRED" },
        { Code: 3460, Name: "Invalid_Captcha_Code", Succes: false, CanRetry: true, Text: "ID_STR_CAPTCHA_INVALID" },
        { Code: 3480, Name: "Invalid_AddtitionalAntibotProtectionRequired", Succes: false, CanRetry: true, Text: "ID_STR_LOGIN_STATUS_INVALID_ADDTITIONALANTIBOTPROTECTIONREQUIRED" },
        { Code: 3560, Name: "Access_Not_Allowed", Succes: false, CanRetry: false, Text: "ID_STR_LOGIN_STATUS_ACCESS_NOT_ALLOWED" }
    ]

    var LoginViewModel = function LoginViewModel() {
        uimodels.Notifications.UINotificationContainer.call(this);
        this.Captcha = new CaptchaViewModel();
        var self = this;
        
        validator.validatableModel.call(this, function () {
            return {
                properties: {
                    "loginName": {
                        rules: {
                            NotNullOrEmpty: true
                        }
                    },
                    "password": {
                        rules: {
                            NotNullOrEmpty: true
                        }
                    },
                    "Captcha": {
                        rules: {
                            SubModel: true
                    }
                }
            }
            }
        });

        this.loginName = ko.observable(null);
        this.password = ko.observable(null);

        if (config.Debug)
        {
            this.loginName("demo");
            this.password("demodemo");
        }

        this.ShowLoginForm = ko.observable(false);
        this.NewsFeedMessages = ko.observableArray([]);
        this.PromotionFeedMessages = ko.observableArray([]);

        this.initialize = function () {
            var checkVersionRes =
                services.VersionService.CheckVersion(
                        { 'ClinetApplicationVersion': version.ClinetApplicationVersion }
                    )
                .done(function (comp) {
                    if (!comp.Result.Compatible) {
                        self.ShowLoginForm(false);

                        var currentOS = osManager.CurrentOS;
                        var marketLink = currentOS.Consts.MarketLink;

                        self.ClearAndAddNotification(
                            new uimodels.Notifications.UINotificationHtml(
                                {
                                    MessageStringID: 'ID_STR_CLIENT_AND_SERVER_VERSIONS_NOT_COMPATIBLE',
                                    MessageStringData: { MarketLink: marketLink },
                                    Type: uimodels.Notifications.UINotificationType.Error
                                }
                            )
                        )
                    }
                    else {
                        self.ShowLoginForm(true);
                    }
                })
                .fail(function () {

                    var buttons = osManager.CurrentOS.closeAppIsAvailable ?
                        [
                            { ID: 'exit', text: strings.ID_STR_LOGOUT_BUTTON },
                            { ID: 'retry', text: strings.ID_STR_RETRY }
                        ] :
                        [
                            { ID: 'retry', text: strings.ID_STR_RETRY }
                        ];

                    //tova tryabva da se opravi zashtoto ne e zadaljitelno da garmi zashtoto nyama internet
                    deviceapi.notification.Confirm(
                        strings.ID_STR_ERROR_NOCONNECTION_TITLE,
                        strings.ID_STR_ERROR_NOCONNECTION_ERROR,
                        buttons
                    ).done(function (dialogres) {
                        if (dialogres.ID == 'exit') {
                            deviceapi.app.terminate();
                        } else {
                            self.initialize();
                        }
                    })
                });

            events.one(self, 'rendered', function () {
                var feedMessagesRes = services.FeedMessagesService.GetNewsFeedMessages(languageManager.GetCurrentLang().id)
                    .then(function (data) {
                        self.NewsFeedMessages(data.NewsFeedMessages);
                    });

                var feedPromotionsRes = services.FeedMessagesService.GetPromotionsFeedMessages(languageManager.GetCurrentLang().id)
                    .then(function (data) {
                        data.PromotionFeedMessages[0].active = true;
                        self.PromotionFeedMessages(data.PromotionFeedMessages);

                        $("#banners").swiper({
                            speed: 500,
                            pagination: ".swiper-pagination",
                            slidesPerView: 1,
                            paginationClickable: true,
                            resistanceRatio: 0.5,
                            //centeredSlides: true,
                            spaceBetween: 10,
                            loop: true, //NOT WORKING!
                            autoplay: 5000, //ms
                            autoplayDisableOnInteraction: false,
                        });
                    });
            });

            return;
        }

        this.login = function () {

            var deviceData = new services.Objects.AdditionalUserDataMobileDevice({
                UserDeviceID: deviceapi.device.DeviceID,
                UserDeviceName: deviceapi.device.DeviceName,
                UserDevicePlatform: deviceapi.device.DevicePlatform,
                UserDeviceVersion: deviceapi.device.DeviceVersion,
                UserDeviceModel : deviceapi.device.DeviceModel
            });
            
            if (validator.getValidator(self).validate()) {

                enc = new encryptor();
                enc.ClearState();

                var userAndLogin = enc.GetHashedPaswordAndSuccessStateHadler({
                    "Password": this.password(),
                    "LoginID": this.loginName(),
                });
                var request = {
                        "SignOnRq": {
                            "SignOnPassword": {
                                "Password": userAndLogin.Password,
                                "LoginID": userAndLogin.LoginID,
                                "Channel": "WEB"
                            },
                            AntiBotProtectionToken: this.Captcha.GetModel(),
                            AdditionalUserData: deviceData
                        }
                };

                services.SessionService.Login(request)
                .then(function (response) {
                        if (response.SignOnRs) {
                            var status = _.find(SignOnRsStatus, function (item) { return item.Code == response.SignOnRs.Status });
                            if (status.Succes) {
                                session.SetSession(response.SignOnRs.Session);
                                session.SetCredentialID(response.SignOnRs.CredentialID);
                                userAndLogin.Success(response.SignOnRs.Session);
                                
                                services.BankProductsService
                                    .GetProducts()
                                    .pipe(function (products) {
                                        productscache.InitProductCache(products.APerson);

                                    return utils.navigate("Products", { trigger: true });
                                    });

                                pushNotifications.Register();
                            }
                            else {
                                self.ShowLoginForm(status.CanRetry);
                                self.ClearAndAddNotification(
                                        new uimodels.Notifications.UINotificationHtml(
                                            {
                                                MessageStringID: status.Text,
                                                Type: uimodels.Notifications.UINotificationType.Error
                                            }
                                        )
                                    )
                                //antibot protection required
                                if (status.Code == 1880 || status.Code == 3480) {
                                    //maybe clear inputs?
                                    self.Captcha.LoadCaptcha();
                                }
                            }
                        }
                        else {
                            //error
                        }
                });
            }
        }
        
        this.langs = _.toArray(new enumLang());
        this.selectedLanguage = languageManager.GetCurrentLang().id;

        //this.selectedLanguage.subscribe(function (nv) { alert(nv); });

        this.toggleLanguage = function () {
            
            //vzimame parviq koito ne e nashiq. :D :D hahah 
            var newLang = _.find(self.langs, function (l) { return l.id != self.selectedLanguage });

            languageManager.SetCurrentLang(newLang.id);
            window.location = config.Index;
        }

        this.handlebackbutton = function () {
            require(["diagnostics"], function (session, services) {
                diagnostics.Debug("Back button is not allowed on login page");
            });
        }

        this.scrollToElement = function (data, event) {
            
            if ($(event.currentTarget).length > 0) {
                setTimeout(function (target) {

                    var topPosition = null;
                    
                    //ako e dolu na stranicata, pod tekushtiq scroll
                    // - 11 e mega hak - nqkoi soft claviaturi pokazvat prev i next butoni, 
                    // koito se risuvat po-kasno ot obshtata klaviatura i ne se vzimat predvid ot v razmera na prozoreca sled kato se poqvqt 
                    if (target.height() + target.position().top > $(window).height() - 11 + $(window).scrollTop()
                         //|| target.position().top < $(window).scrollTop())//ili e gore na stranicata 
                         ){
                        //sredata na elemta da se padne v sredata na prozoreca 
                        topPosition = target.position().top - target.height() / 2 - $(window).height() / 2;// -

                        $('body').animate({ scrollTop: topPosition }, 400);
                    } 
                }, 800, $(event.currentTarget));
            }
        }

        this.branches = function () {
            utils.navigate("BankBranches", { trigger: true });
        }

        this.ccyCalc = function () {
            utils.navigate("CCYRates", { trigger: true });
        }

        this.viewContacts = function (viewModel) {

            var link = utils.buildLink('Settings', 'contacts');
            utils.navigate(link, { trigger: true });
        }

        //this.openInBrowser = function ( data, e ){

        //    if (e && e.target) {
        //        window.open(e.target.href, '_system');
        //    }
        //    return false;
        //}
        
    };

    return LoginViewModel;
});;