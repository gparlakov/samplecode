define([
  'utils',
  'jquery',
  'services',
  'i18n!Mobile/nls/strings',
  'Mobile/ViewModels/Repository/ActionButtonTypes'
], function (utils, $, services, strings, buttonTypes) {
    
    var actionButtons = {};

    actionButtons.viewPayment = function (eventHandler) {
        $.extend(this, buttonTypes.gray);

        this.label = strings.ID_STR_PREVIEW;
        this.enabled = true;

        this.click = eventHandler;
    };

    actionButtons.editPayment = function (payment) {
        $.extend(this, buttonTypes.blue);

        this.label = strings.ID_STR_EDIT;
        this.enabled = payment.PaymentOrderAllowedOperations.CanIEdit
            && !payment.PaymentOrderAllowedOperations.Expired
            && payment.Incarnation.PaymentType.IsActive
            && payment.Incarnation.PaymentType.EditAllowed;

        this.click = function () {
            var link = utils.buildLink('PaymentWizard', 'editPayment', [payment.PaymentIncarnationType, payment.ID]);
            utils.navigate(link, { trigger: true });
        }
    };

    actionButtons.signPayment = function (payment) {
        $.extend(this, buttonTypes.greenLight);

        this.label = strings.ID_STR_SEND;
        this.enabled = payment.PaymentOrderAllowedOperations.ExtendedProperties.CanPerformCurrentOperation
            && payment.Incarnation.PaymentType.IsActive;

        this.click = function () {
            var link = utils.buildLink('ProcessPayments', 'sign', [payment.ID]);
            utils.navigate(link, { trigger: true });
        }
    };

    actionButtons.copyPayment = function (payment) {
        $.extend(this, buttonTypes.green);

        this.label = strings.ID_STR_CREATE_LIKE;
        this.enabled = payment.Incarnation.PaymentType.IsActive
            && payment.Incarnation.PaymentType.CreateLikeAllowed;

        this.click = function () {
            var link = utils.buildLink('PaymentWizard', 'cereteLikePayment', [payment.PaymentIncarnationType, payment.PaymentIncarnationType, payment.ID]);
            utils.navigate(link, { trigger: true });
        }
    };

    actionButtons.deletePayment = function (payment) {
        $.extend(this, buttonTypes.red);

        this.label = strings.ID_STR_DELETE;
        this.enabled = payment.PaymentOrderAllowedOperations.CanIDelete
            && payment.Incarnation.PaymentType.IsActive;

        this.click = function () {
            var link = utils.buildLink('ProcessPayments', 'delete', [payment.ID]);
            utils.navigate(link, { trigger: true });
        }
    };

    return actionButtons;
});