define([
  'Mobile/Models/Submenu/SubmenuItem',
  'Mobile/Models/Submenu/Submenu',
  'i18n!Mobile/nls/strings'

], function (submenuItem, submenu, strings) {

    var OnlineNotificationsSubmenu = function (params) {

        return {
            Items: [
                {
                    Label: strings.ID_STR_ONLINE_NOTIF,  
                    Controller: 'OnlineNotifications',
                    Action: 'onlineNotificationsList',
                    liCss: 'onlineNotifications-list',
                    Params: params
                }, {
                    Label: strings.ID_STR_ONLINE_NOTIF_SUBSCRIPTIONS,
                    Controller: 'OnlineNotifications',
                    Action: 'onlineNotificationsSubscriptionsList',
                    liCss: 'onlineNotifications-subscriptionsList',
                    Params: params
                }
            ]
        };
    }
    return OnlineNotificationsSubmenu;
});