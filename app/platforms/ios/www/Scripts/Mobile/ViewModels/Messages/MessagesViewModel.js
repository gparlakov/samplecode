define([
  'underscore',
  'knockout',
  'komapping',
  'services',
  'utils',
  'backbone',
  'validator',
  'events',
  'Common/cache/language',
  'Mobile/ViewModels/Common/HeaderViewModel',

], function (_, ko, komapping, services, utils, backbone, validator, events, language, headerViewModel) {


    var MessagesViewModel = function (filter) {

        var self = this;
        self.HeaderViewModel = new headerViewModel({ titleStringID: "ID_STR_MESSAGES" });

        validator.validatableModel.call(this, function () {
            return {
                properties: {}
            };
        });
        filter.ExtendedProperties = {};
        filter.ExtendedProperties.Lang = language.GetCurrentLang().id;

        this.Filter = komapping.fromJS(filter);

        this.Messages = ko.observableArray();

        this.GetNonobservableFilter = function () {
            return komapping.toJS(self.Filter);
        }

        this.initialize = function () {
            var loadingPromise = self.find();

            events.one(self, 'rendered', function () {
                $.whenEx(
                    loadingPromise
                )
                .done(function () {
                    $(window).daisScrollV2('register', self.findMore);
                    events.bind(self, "findresulsfinished", function () {
                        $(window).daisScrollV2('reset', self.findMore);
                    })
                })
            });
        }

        this.pipe = $.DeferredEx();
        this.pipe.resolve();


        this.find = function () {
            if (validator.getValidator(self).validate()) {
                self.Filter.Paging.CurrentPage(1);
                var promise =
                    this._findInternal(true)
                    .done(function () {
                        events.trigger(self, "findresulsfinished");
                    });

                return promise;
            }
        }

        this.findMore = function () {

            var def = new $.DeferredEx(function (def) {
                if (self.Filter.Paging.TotalPages() > self.Filter.Paging.CurrentPage() && validator.getValidator(self).validate()) {

                    self.Filter.Paging.CurrentPage(self.Filter.Paging.CurrentPage() + 1);
                    def.bindTo(self._findInternal(false));
                }
                else {
                    def.fail();
                }
            });
            return def;
        }


        this._findInternal = function (shouldClearOldResults) {

            var res = new $.DeferredEx();

            function action() {
                var actionres = 
                    services.MessageService.GetMessages(self.GetNonobservableFilter(), function (data) {

                        if (shouldClearOldResults) {
                            self.Messages.removeAll();
                        }

                        for (var msg in data.MessagesToUser) {
                            self.Messages.push(data.MessagesToUser[msg]);
                        }

                        komapping.fromJS(data.MessagesToUserFilter, self.Filter);
                    });

                res.bindTo(actionres);

                return actionres;
            }

            self.pipe.pipe(action)

            return res;
        }

        this.findresulsfinished = function () {

        }


        this.unload = function () {
            $(window).daisScrollV2('unregister', self.findMore);
        }



        this.showMessage = function (message) {
            var link = utils.buildLink("Messages", "showMessage", [message.ID]);
            utils.navigate(link, { trigger: true });
        }
    };

    return MessagesViewModel;
});