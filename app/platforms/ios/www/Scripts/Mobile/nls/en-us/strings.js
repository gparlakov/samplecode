﻿define({
    "ID_STR_CONTINUE": "Continue",
    "ID_STR_LOGIN": "User name",
    "ID_STR_PASSWORD": "Password",
    "ID_STR_BANK_CLIENT": "Bank Client",
    "ID_STR_TYPE": "Type",
    "ID_STR_CARD_NUMBER": "Number",
    "ID_STR_ACCOUNT": "Account",
    "ID_STR_IBAN": "IBAN",
    "ID_STR_CCY": "Currency",
    "ID_STR_EXPIRATION": "Expiration",
    "ID_STR_FROM": "From",
    "ID_STR_TO": "To",
    "ID_STR_DATE": "Date",
    "ID_STR_SEARCH": "Search",
    "ID_STR_MOVEMENT_TYPE": "Type",
    "ID_STR_ALL": "All",
    "ID_STR_ALL_ACCOUNTS": "All accounts",
    "ID_STR_ALL_ACCOUNTS_SUB": "Show payments from all accounts",
    "ID_STR_DT": "Debit",
    "ID_STR_KT": "Credit",
    "ID_STR_ACCOUNT_DETAILS": "Account Details",
    "ID_STR_LOAN_DETAILS": "Loan Details",
    "ID_STR_CARD_DETAILS": "Card Details",
    "ID_STR_DEPOSIT_DETAILS": "Deposit Details",
    "ID_STR_PRODUCTS": "Products",
    "ID_STR_TRANSFERS": "Transfers",
    "ID_STR_MESSAGES": "Messages",
    "ID_STR_SETTINGS": "Settings",
    "ID_STR_DEBIT_CARDS": "Debit Cards",
    "ID_STR_CREDIT_CARDS": "Credit Cards",
    "ID_STR_RAI_CARDS": "RAI Cards",
    "ID_STR_TOTAL_AMOUNTS": "Total Amounts",
    "ID_STR_TOTAL_AMOUNTS_BGN": "Total availability (BGN)",
    "ID_STR_BALANCE": "Balance",
    "ID_STR_AVAILABLE_AMOUNT": "Available amount",
    "ID_STR_CREDIT_LIMIT": "Overdraft",
    "ID_STR_RAICARD_MINPAY": "Minimal Pay",
    "ID_STR_RAICARD_TOTAL_AMOUNT_DUE": "Actual",
    "ID_STR_LOANS": "Loans",
    "ID_STR_FACILITY_APPROVE_DATE": "Approve Date",
    "ID_STR_FACILITY_LIMIT": "Limit",
    "ID_STR_FACILITY_EXPOSURE": "Exposure",
    "ID_STR_FACILITY_CURRENT_AVAILABLE": "Current Available",
    "ID_STR_FACILITY_DUE_DATE": "Due Date",
    "ID_STR_LOAN": "Loan",
    "ID_STR_LOAN_ID": "ID",
    "ID_STR_LOAN_PRINCIPAL": "Principal",
    "ID_STR_NEXT": "Next",
    "ID_STR_BACK": "Back",
    "ID_STR_FROM_ACCOUNT": "From account",
    "ID_STR_CHOOSE_FROM_ACCOUNT": "Tap to select an account",
    "ID_STR_CHOOSE_OTHER_ACCOUNT": "Select another account",
    "ID_STR_NAME": "Name",
    "ID_STR_PAYER_IBAN": "Payer account",
    "ID_STR_PAYER_NAME": "Payer Name",
    "ID_STR_PAYER_ADDRESS": "Payer Address",
    "ID_STR_DOC_DD_PAYER_NAME": "Payer IBAN",
    "ID_STR_DOC_DD_PAYER_IBAN": " Payer name",
    "ID_STR_AMOUNT": "Amount",
    "ID_STR_PAYMENY_DETAILS": "Details",
    "ID_STR_PAYMENY_DETAILS_ADDTIONAL": "Additional Details",
    "ID_STR_PENDING": "Pending",
    "ID_STR_PENDING_PAYMENT": "Pending payments",
    "ID_STR_PAYMENT_SAVE": "Save",
    "ID_STR_CREATE_DATE": "Create Date",
    "ID_STR_EXECUTE_DATE": "Execute Date",
    "ID_STR_EXPIRY_DATE": "Expiry Date",
    "ID_STR_SEND_DATE": "Send Date",
    "ID_STR_CREATE_NEW_PAYMENT": "New Transfer",
    "ID_STR_OWN_ACCOUNTS": "Own Accounts",
    "ID_STR_BENEFICIARIES": "Beneficiaries",
    "ID_STR_LOAD_BENEFICIARY": 'Load beneficiary',
    "ID_STR_PAYEE_IBAN": "Payee account",
    "ID_STR_PAYEE_BANK": "Payee Bank",
    "ID_STR_PAYEE_NAME": "Payee Name",
    "ID_STR_PAYEE_ACCOUNT_NUMBER": "Account number",
    "ID_STR_PAYEE_ADDRESS": "Payee Address",
    "ID_STR_PAYEE_BANK_SWIFT": "Payee Bank Code",
    "ID_STR_PAYEE_BANK_BIC": "Payee Bank BIC",
    "ID_STR_PAYER_BANK_BIC": "Payer Bank BIC",
    "ID_STR_PAYER_BANK": "Payer Bank",
    "ID_STR_SIGN": "Sign",
    "ID_STR_MTAN": "SMS TAN",
    "ID_STR_CANCEL": "Cancel",
    "ID_STR_OK": " OK ",
    "ID_STR_RETRY": " Retry ",
    "ID_STR_SIGN_AND_SEND": "Sign and send",
    "ID_STR_CORRBANK_NAME": "Name",
    "ID_STR_CORRBANK_ADDRESS": "Address",
    "ID_STR_CORRBANK_CITY": "City",
    "ID_STR_CORRBANK_COUNTRY": "Country",
    "ID_STR_CORRBANK_BICCODE": "BIC code",
    "ID_STR_BENEFICIARY_BANK_CORRESPONDENT": "Beneficiary's bank correspondent",
    "ID_STR_PAYEE_VALUE_DATE": "Beneficiary value date",
    "ID_STR_PAYMENT_DIRECTION": "Direction",
    "ID_STR_PAYMENT_DIRECTION_ABROAD": "Abroad",
    "ID_STR_PAYMENT_DIRECTION_DOMESTIC_LOCAL": "Domestic for local party",
    "ID_STR_PAYMENT_DIRECTION_DOMESTIC_FOREING": "Domestic for foreign party",
    "ID_STR_PAYMENT_BANK_EXPENSES_SHA": "Shared",
    "ID_STR_PAYMENT_BANK_EXPENSES_OUR": "Ord. customer",
    "ID_STR_PAYMENT_BANK_EXPENSES_BEN": "Beneficary",
    "ID_STR_PAYMENT_BANK_EXPENSES": "Bank's charges",
    "ID_STR_PAYMENT_CREATOR": "Creator",
    "ID_STR_PAYMENT_VALUE_DATE": "Value Date",
    "ID_STR_SEND": "Send",
    "ID_STR_AUTH_RETRY": "Invalid code",
    "ID_STR_AUTH_REJECTED": "Invalid code",
    "ID_STR_AUTH_REFRESH": "Generate new code",
    "ID_STR_PAYMENTS_SIGN_ALL_ERROR": "Transfers are not signed",
    "ID_STR_PAYMENTS_SIGN_OK": "Transfer is signed",
    "ID_STR_PAYMENTS_SIGN_ERROR": "Transfer is not signed",
    "ID_STR_PAYMENTS_SEND_OK": "Transfer is sent",
    "ID_STR_PAYMENTS_SEND_ERROR": "Transfer is not sent",
    "ID_STR_TOKEN": "Token",
    "ID_STR_TOKEN_CHALLENGE": "Challange:",
    "ID_STR_TOKEN_RESPONSE": "Response:",
    "ID_STR_PAYMENT_SAVE_AND_SEND": "Save and send",
    "ID_STR_LIABILITY_PERSON_HEADER": "Obliged Person",
    "ID_STR_LIABILITY_PERSON_NAME": "Full Name",
    "ID_STR_LIABILITY_PERSON_EGN": "Personal №",
    "ID_STR_LIABILITY_PERSON_PERSONAL_ID": "Personal ID",
    "ID_STR_LIABILITY_PERSON_COMPANY_NAME": "Name",
    "ID_STR_LIABILITY_PERSON_BULSTAT": "BULSTAT",
    "ID_STR_LIABILITY_PERSON_TYPE_LOCAL": " Bulgarian Citizen",
    "ID_STR_LIABILITY_PERSON_TYPE_FOREIGNER": "Foreign Citizen",
    "ID_STR_LIABILITY_PERSON_TYPE_COMPANY": "Company",
    "ID_STR_PAYMENT_PERIOD": "Payment period",
    "ID_STR_LIABILITY_DOCUMENT_NUMBER": 'Liability document number',
    "ID_STR_LIABILITY_DOCUMENT_DATE": 'Liability document date',
    "ID_STR_LIABILITY_DOCUMENT_TYPE": "Document type and number",
    "ID_STR_PAYMENT_TYPE": "Transfer Type",
    "ID_STR_PAYMENT_TYPE_BISERA_EXT": "(Standart) BISERA",
    "ID_STR_PAYMENT_TYPE_RINGS_EXT": "(Fast) RINGS",
    "ID_STR_PAYMENT_TYPE_BISERA": "BISERA",
    "ID_STR_PAYMENT_TYPE_RINGS": "RINGS",
    "ID_STR_PAYMENT_PAYEE_TYPE": "Transfer Type (payee)",
    "ID_STR_PAYMENT_PAYER_TYPE": "Transfer Type (payer)",
    "ID_STR_EDIT": "Edit",
    "ID_STR_CREATE_LIKE": "Copy",
    "ID_STR_SIGNATURES": "Signatures:",
    "ID_STR_PREVIEW": "Preview",
    "ID_STR_HOME": "Home",
    "ID_STR_HOME_SUB": "Home",
    "ID_STR_USERS_NAME" : "Username",

    "ID_STR_BENEFICIRAY_NAME": "Name",
    "ID_STR_BENEFICIRAY_ADDRESS": "Beneficiary address",
    "ID_STR_BENEFICIRAY_ACCOUNT_TYPE": "Account type",
    "ID_STR_BENEFICIRAY_ACCOUNT_TYPE_HOME": "Local account",
    "ID_STR_BENEFICIRAY_ACCOUNT_TYPE_FOREIGN": "Account abroad",
    "ID_STR_BENEFICIRAY_ACCOUNT_FOREIGN_NUMBER": "Account number",
    "ID_STR_BENEFICIRAY_ACCOUNT_FOREIGN_BANK_NAME": "Bank name",
    "ID_STR_BENEFICIRAY_ACCOUNT_FOREIGN_BANK_ADDRESS": "Bank address",
    "ID_STR_BENEFICIRAY_ACCOUNT_FOREIGN_BANK_CITY": "Bank city",
    "ID_STR_BENEFICIRAY_ACCOUNT_FOREIGN_BANK_SWIFT": "Bank SWIFT",
    "ID_STR_BENEFICIRAY_ACCOUNT_FOREIGN_BANK_CODE": "Bank code",
    "ID_STR_BENEFICIRAY_ACCOUNT_FOREIGN_BANK_COUNTRY": "Bank country",
    "ID_STR_BENEFICIRAY_ACCOUNT_HOME_IBAN": "IBAN",
    "ID_STR_NEW_PAYMENT_FROM_TEMPLATE": "Load payment from template",
    "ID_STR_CHOOSE": "Choose",
    "ID_STR_DELETE": "Delete",
    "ID_STR_TEMPLATES": "Templates",

    "ID_STR_LOGIN_STATUS_SUCCESS": "Success.",
    "ID_STR_LOGIN_STATUS_GENERAL_ERROR": "Error.",
    "ID_STR_LOGIN_STATUS_AUTHENTICATION_FAILED": "Invalid user name or password.<br/> Check for lower/upper case in user name and password fields",
    "ID_STR_LOGIN_STATUS_AUTHENTICATION_FAILED_TRYLATER": "Error. Try again later.",
    "ID_STR_LOGIN_STATUS_CUSTOMER_LOCKED_OUT": "Your account is locked. Please log in https://online.rbb.bg/ to unlock it",
    "ID_STR_LOGIN_STATUS_CUSTOMER_SESSION_ALREADY_IN_PROGRESS": "There is active session.",
    "ID_STR_LOGIN_STATUS_PASSWORD_CHANGE_REQUIRED": "You must change your password. Please, do this on https://online.rbb.bg/",
    "ID_STR_LOGIN_STATUS_LOGIN_CHANGE_REQUIRED": "You must change your user name. Please, do this on https://online.rbb.bg/",
    "ID_STR_LOGIN_STATUS_INVALID_ADDTITIONALANTIBOTPROTECTIONREQUIRED": "Enter security code",
    "ID_STR_LOGIN_STATUS_ACCESS_NOT_ALLOWED": "Access to the mobile application is not allowed for your account.",
    "ID_STR_CAPTCHA_LABEL": "Control code",
    "ID_STR_CAPTCHA_INVALID": "Invalid control code",
    "ID_STR_CAPTCHA_GENERATE_NEW": "new image",

    "ID_STR_LOGIN_BUTTON": "Login",
    "ID_STR_LOGOUT_BUTTON": "Exit",
    "ID_STR_PLEASE_ENTER_LOGIN_AND_PASSWORD": "Please enter your user name and password",
    "ID_STR_ARCHIVE": "Archive",
    "ID_STR_PAYMENT_ARCHIVE": "Payments archive",
    "ID_STR_SESSION_EXPIRED_TITILE": "Session expired",
    "ID_STR_SESSION_EXPIRED_MESSAGE": "Session expired. Please log in.",

    "ID_STR_313_DOCTYPE_1": "Declaration",
    "ID_STR_313_DOCTYPE_2": "Audit certificate",
    "ID_STR_313_DOCTYPE_3": "Penal ordinance",
    "ID_STR_313_DOCTYPE_4": "Advance instalment",
    "ID_STR_313_DOCTYPE_5": "Tax registration number of property",
    "ID_STR_313_DOCTYPE_6": "Order for execution",
    "ID_STR_313_DOCTYPE_9": "Other",

    "ID_STR_ACCOUNT_MOVEMENTS_BLOCKS": "Blocks",
    "ID_STR_ACCOUNT_MOVEMENTS": "Movements",
    "ID_STR_ACCOUNT_BALANCES": "Balances",
    "ID_STR_DETAILS": "Details",
    "ID_STR_DETAILS_ICON": "0xe882",
    "ID_STR_NO_RESULTS": "No results",
    "ID_STR_NO_PENDING_PAYMENTS": "No pending payments found",
    "ID_STR_NO_ARCHIVE_PAYMENTS": "No payments found in archive",
    "ID_STR_NO_ACCOUNTS": "You do not have any accounts",
    "ID_STR_NO_BENEFICIARIES": "You do not have any saved beneficiaries",
    "ID_STR_NO_TEMPLATES": "No saved templates found",
    "ID_STR_NO_MESSAGES": "You do not have any incoming messages",
    "ID_STR_NO_MOVEMENTS": "No movements found",
    "ID_STR_NO_DEBIT_CARDS": "There are no debit cards for this account",
    "ID_STR_NO_FACILITY_FEES": "There are no fees for this loan",

    "ID_STR_ACCOUNT": "Account",
    "ID_STR_DEBIT_CARDS": "Debit Cards",
    "ID_STR_CARDS": "Cards",
    "ID_STR_INTEREST_RATE": "Interest rate",
    "ID_STR_NEXT_PRINCIPAL_PAYMENT": "Next principal payment",
    "ID_STR_NEXT_MATURITY_PRINCIPAL": "Next maturity principal",

    "ID_STR_PAYMET_SYSTEM": "Payment System",

    "ID_STR_PAYMENT_EXPENSES_OUR_INFO": "All commissions and charges to be borne by us",
    "ID_STR_PAYMENT_EXPENSES_BEN_INFO": "All commissions and charges to be borne by beneficiary",
    "ID_STR_PAYMENT_EXPENSES_SHA_INFO": "Your commissions are for our account, those of your correspondent - for the account of the beneficiary",

    "ID_STR_MOVEMENT_DOC": "Transfer Order",
    "ID_STR_MOVEMENT_DOC_TYPE_CP": "Transfer Order for Credit Payment",
    "ID_STR_MOVEMENT_DOC_TYPE_DD": "Transfer Order for Direct Debit",
    "ID_STR_MOVEMENT_DOC_TYPE_DD_AUTH": "Direct Debit Authorization",
    "ID_STR_MOVEMENT_DOC_TYPE_PB": "Transfer Order to Budget",
    "ID_STR_PAYMENT_DATE" : "Payment Date",

    "ID_STR_PACKET_INFO": "If you want to see list of payments, please visit https://online.rbb.bg/",

    "ID_STR_PICK_ACC": "Select an account",
    "ID_STR_PICK_ACC_DESC": "Please pick an account from the list",
 
    "ID_STR_LANGUAGE": "Language",
    "ID_STR_LANGUAGE_BG": "BG",
    "ID_STR_LANGUAGE_ENG": "EN",
    "ID_STR_PRODUCTS_ACCOUNTS": "Accounts",
    "ID_STR_PRODUCTS_DEPOSITS": "Deposits",
    "ID_STR_DIRTY_MONEY_LABEL": "Origin of funds",
    "ID_STR_STATUS": "Status",
    "ID_STR_LANGUAGE_BG_ENG": "Българска версия",//narochno e taka 
    "ID_STR_OVERDRAFT": "Overdraft",

    "ID_STR_ERROR_TITLE": "Error",
    "ID_STR_ERROR_SYSTEM_ERROR": "Error. Try again later.",

    "ID_STR_ERROR_TIMEOUT_TITLE": "Timeout",
    "ID_STR_ERROR_SYSTEM_TIMEOUT_ERROR": "Request took too much time. Please check your internet connection and try again.",

    "ID_STR_ERROR_NOCONNECTION_TITLE": "Connection error.",
    "ID_STR_ERROR_NOCONNECTION_ERROR": "Error while connecting to server. Please check your internet connection and try again.",

    "ID_STR_DEPOSIT_ID": "ID",
    "ID_STR_DEPOSIT_OPENED_AT": "Opened at",
    "ID_STR_DEPOSTI_DUE_DATA": "Due Date",
    "ID_STR_DEPOSIT_INTERESET_RATE": "Basic Interest Rate",
    "ID_STR_DEPOSIT_ACCRUED_INTEREST": "Accrued Interest",
    "ID_STR_DEPOSIT_INTEREST_AT_MATURITY": "Interest at Maturity",
    "ID_STR_ENTER_ACCOUNT": "Enter a new account",

    "ID_STR_GUARANTEE_PRINCIPAL": "Amount",
    "ID_STR_GUARANTEE_VALUE_DATE_FROM": "Valid from",
    "ID_STR_GUARANTEE_VALUE_DATE_TO": "Valid to",

    "ID_STR_CREDIT_NEXT_VALUE_DATE": "Next pay date",
    "ID_STR_CREDIT_NEXT_VALUE_AMOUNT": "Next pay amount",
    "ID_STR_CREDIT_NEXT_PRINCIPAL_VALUE_AMOUNT": "Next principal payment",
    "ID_STR_CREDIT_NEXT_PRINCIPAL_VALUE_DATE": "Next maturity principal",
    "ID_STR_CREDIT_NEXT_INTEREST_VALUE_AMOUNT": "Next maturity interest",
    "ID_STR_CREDIT_NEXT_INTEREST_VALUE_DATE": "Next maturity interest date",
    "ID_STR_CREDIT_CURRENT_INTEREST": "Accrued interest to date",
    "ID_STR_LOAN_IS_BANK_ADMINISTRATED": "Your loan is administrated by Raiffeisen Services LTD",
    "ID_STR_LOAN_OVERDUED_WARNING": "You have overdued payment for this loan. Please contact your Credit inspector.",
    "ID_STR_FACILITY_FEES": "Fees",
    "ID_STR_CHARGE": "Charge",
    "ID_STR_CREDIT_DUE_AMOUNT": "Due amount",
    "ID_STR_MORE": "More",
    "ID_STR_LESS": "Less",
    "ID_STR_MORE_OPTIONS": "More options",
    "ID_STR_LESS_OPTIONS": "Less options",
    "ID_STR_MORE_FEATURES_NEXT_VERSION": "Expect more features in next versions",
    "ID_STR_STANDART_RATE": "Standard Rate",
    "ID_STR_NEGOTIATED_RATE": "Negotiated Rate",
    "ID_STR_RATIO_FIX" : "BNB exchage rate (fixing)",
    "ID_STR_RATIO": "BNB exchage rate",
    "ID_STR_NEGOTIATED_RATIO_LABEL": "Negotiated Rate",
    "ID_STR_REFERENCE": "Reference",
    "ID_STR_MOVEMENT_NOT_AVAILABLE": "There is no additional information for this movement",
    "ID_STR_CASH_MOVT_CASHIER": "Cashier",
    "ID_STR_CAHS_MOVT_CASH_OPERATION": "Cash operation",
    "ID_STR_CAHS_MOVT_CASH_CASH_WITHDRAWAL": "Cash withdrawal",
    "ID_STR_CAHS_MOVT_DEPOSIT CASH":"Deposit cash",
    "ID_STR_CAHS_MOVT_TRANSFER": "Transfer",
    "ID_STR_CONTACTS": "Contacts",
    "ID_STR_NO_ACTIVE_PRODUCTS": "You do not have active products",
    "ID_STR_PAYMENT_BENEFICIARY_DETAILS": "Beneficiary's Details",
    "ID_STR_PAYMENT_BENEFICIATY_BANK_DETAILS": "Beneficiary's Bank Details",
    "ID_STR_BRANCHES": "Branches",
    "ID_STR_BRANCH" : "Branch",
    "ID_STR_ATMS": "ATMs",
    "ID_STR_ATM": "ATM",
    "ID_STR_BRANCHES_ATMS": "Branches and ATMs",
    "ID_STR_NASELENO_MYASTO": "Populated place",
    "ID_STR_ADDRESS": "Address",
    "ID_STR_IS_IN_BANK": "Positioned in bank branch",
    "ID_STR_ATM_SITE": "Site",
    "ID_STR_PHONE": "Phone",
    "ID_STR_PHONES": "Phones",
    "ID_STR_AND": "and",
    "ID_STR_FAX": "Fax",
    "ID_STR_CONTACT_PERSON": "Contact Person",
    "ID_STR_E_MAIL": "E-mail",
    "ID_STR_WORK_TIME": "Working Time",
    "ID_STR_WORK_TIME_BREAK": "Break",
    "ID_STR_BRANCH_EXTENDED_WORKING_TIME": "Extended working hours",
    "ID_STR_BRANCH_WOKRS_WEEKED": "Opened Saturday/Sunday",
    "ID_STR_ATM_24H_ZONE": "24h banking zone",
    "ID_STR_MORE": "More",
    "ID_STR_OPCODETYPE": "Operation Type",
    "ID_STR_PAYMENT_CREDIT_NUMBER": "BNB Credit Number",

    "ID_STR_WEEKDAY_MON": "Monday",
    "ID_STR_WEEKDAY_TUE": "Tuesday",
    "ID_STR_WEEKDAY_WED": "Wednesday",
    "ID_STR_WEEKDAY_THU": "Thursday",
    "ID_STR_WEEKDAY_FRI": "Friday",
    "ID_STR_WEEKDAY_SAT": "Sataday",
    "ID_STR_WEEKDAY_SUN": "Sunday",
    "ID_STR_CURRENCY_CONVERTER": "Currency converter",
    "ID_STR_CURRENCY_RATES": "Currency Rates",
    "ID_STR_CURRENCY_RATE_BUYBOOK": "Buy Books",
    "ID_STR_CURRENCY_RATE_SELLBOOK": "Sell Books",
    "ID_STR_CURRENCY_RATE_BUYCASH": "Buy Cash",
    "ID_STR_CURRENCY_RATE_SELLCASH": "Sell Cash",
    "ID_STR_PROFILE": "My profile",
    "ID_STR_PROFILE_SUB": "More information for your profile",
    "ID_STR_IDENTIFICATION_CODE": "Identification code:",
    "ID_STR_OPERATING_SYSTEM": "Device operating system:",
    "ID_STR_DEVICE_MODEL": "Model of device:",
    "ID_STR_SCREEN_SIZE": "Screensize:",
    "ID_STR_APPLICATION_VERSION": "Application version:",
    "ID_STR_TOTAL_RESULTS": "Тotal number of results:",

    "ID_STR_ENTER_LOCATION": "Enter a location",
    "ID_STR_CCY_CALC_INFO_TEXT": "Equivalence is based upon the actual Raiffeisenbank (Bulgaria) EAD FX Bulletin",

    "ID_TMPL_PAYMENTOPERATIONSTATUS_MESSAGE": "{{if Messages }} {{for Messages}} {{>~GetLocalizedString(Message)}} {{/for}} {{else}} {{/if}}",
    "ID_TMPL_PAYMENTOPERATIONSTATUSSEND_PAYMENTORDER_SUCCESS":
        "Transfer for {{:~format.number(PaymentIncarnation.Amount)}} {{if PaymentIncarnation.CCY}} {{> PaymentIncarnation.CCY.SWIFTCode }} {{/if}} sent. {{RenderTemplateFromStrings name='ID_TMPL_PAYMENTOPERATIONSTATUS_MESSAGE' /}}",

    "ID_TMPL_PAYMENTOPERATIONSTATUSSEND_PAYMENTORDER_ERROR":
        "Transfer for {{:~format.number(PaymentIncarnation.Amount)}} {{if PaymentIncarnation.CCY}} {{> PaymentIncarnation.CCY.SWIFTCode }} {{/if}} not sent. {{RenderTemplateFromStrings name='ID_TMPL_PAYMENTOPERATIONSTATUS_MESSAGE' /}}",

    "ID_TMPL_PAYMENTOPERATIONSTATUSSIGN_PAYMENTORDER_SUCCESS": 
        "Transfer for {{:~format.number(PaymentIncarnation.Amount)}} {{if PaymentIncarnation.CCY}} {{> PaymentIncarnation.CCY.SWIFTCode }} {{/if}} signed. {{RenderTemplateFromStrings name='ID_TMPL_PAYMENTOPERATIONSTATUS_MESSAGE' /}}",

    "ID_TMPL_PAYMENTOPERATIONSTATUSSIGN_PAYMENTORDER_ERROR": 
        "Transfer for {{:~format.number(PaymentIncarnation.Amount)}} {{if PaymentIncarnation.CCY}} {{> PaymentIncarnation.CCY.SWIFTCode }} {{/if}} not signed. {{RenderTemplateFromStrings name='ID_TMPL_PAYMENTOPERATIONSTATUS_MESSAGE' /}}",

    "ID_TMPL_PAYMENTOPERATIONSTATUSSAVE_PAYMENTORDER_SUCCESS":
        "Transfer for {{:~format.number(PaymentIncarnation.Amount)}} {{if PaymentIncarnation.CCY}} {{> PaymentIncarnation.CCY.SWIFTCode }} {{/if}} saved. {{RenderTemplateFromStrings name='ID_TMPL_PAYMENTOPERATIONSTATUS_MESSAGE' /}}",

    "ID_TMPL_PAYMENTOPERATIONSTATUSSAVE_PAYMENTORDER_ERROR":
        "Transfer for {{:~format.number(PaymentIncarnation.Amount)}} {{if PaymentIncarnation.CCY}} {{> PaymentIncarnation.CCY.SWIFTCode }} {{/if}} not saved. {{RenderTemplateFromStrings name='ID_TMPL_PAYMENTOPERATIONSTATUS_MESSAGE' /}}",

    "ID_TMPL_PAYMENTOPERATIONSTATUSDELETE_PAYMENTORDER_SUCCESS":
        "Transfer  for {{:~format.number(PaymentIncarnation.Amount)}} {{if PaymentIncarnation.CCY}} {{> PaymentIncarnation.CCY.SWIFTCode }} {{/if}} deleted. {{RenderTemplateFromStrings name='ID_TMPL_PAYMENTOPERATIONSTATUS_MESSAGE' /}}",

    "ID_TMPL_PAYMENTOPERATIONSTATUSDELETE_PAYMENTORDER_ERROR":
        "Transfer for {{:~format.number(PaymentIncarnation.Amount)}} {{if PaymentIncarnation.CCY}} {{> PaymentIncarnation.CCY.SWIFTCode }} {{/if}} not deleted. {{RenderTemplateFromStrings name='ID_TMPL_PAYMENTOPERATIONSTATUS_MESSAGE' /}}",


    "ID_TMPL_PAYMENTORDERALLOWEDOPERATIONREASON[PaymentOrderAllowedOperationReasonSign]": "Payment can not be signed.",
    "ID_TMPL_PAYMENTORDERALLOWEDOPERATIONREASON[PaymentOrderAllowedOperationReasonSignAmountLimitExceed]": "Maximum payment sign amount limit exceeded.",
    "ID_TMPL_PAYMENTORDERALLOWEDOPERATIONREASON[PaymentOrderAllowedOperationReasonSignExpired]": "Payment validity period is expired.",
    "ID_TMPL_PAYMENTORDERALLOWEDOPERATIONREASON[PaymentOrderAllowedOperationReasonSignAlreadySigned]": "Payment requires more signatures to be sent.",
    "ID_TMPL_PAYMENTORDERALLOWEDOPERATIONREASON[PaymentOrderAllowedOperationReasonSignNoRight]": "You don't have rights to sing this payment.",
    "ID_TMPL_PAYMENTORDERALLOWEDOPERATIONREASON[PaymentOrderAllowedOperationReasonSignNotNeeded]": "Your signature is not needed.",
    "ID_TMPL_PAYMENTORDERALLOWEDOPERATIONREASON[PaymentOrderAllowedOperationReasonSend]": "Payment can not be sent.",
    "ID_TMPL_PAYMENTORDERALLOWEDOPERATIONREASON[PaymentOrderAllowedOperationReasonSendAmountLimitExceed]": "Send amount limit exceeded.",
    "ID_TMPL_PAYMENTORDERALLOWEDOPERATIONREASON[PaymentOrderAllowedOperationReasonSendNoRight]": "You don't have rights to send this payment.",
    "ID_TMPL_PAYMENTORDERALLOWEDOPERATIONREASON[PaymentOrderAllowedOperationReasonSendExpired]": "Payment validity period is expired.",
    "ID_TMPL_PAYMENTORDERALLOWEDOPERATIONREASON[PaymentOrderAllowedOperationReasonSendAuthRequiredNoSecurityDevice]": "You need additional security device to send this payment.",

    "ID_STR_WELCOME_1": "Welcome to the",
    "ID_STR_WELCOME_2": "Raiffeisen ONLINE",
    "ID_STR_WELCOME_3": "Mobile Application",
    "ID_STR_SEE_MORE": "See more at:",

    "ID_STR_STEP": "Step",

    "ID_STR_CONTACTS_BANK_TITLE": "Raiffeisenbank (Bulgaria) EAD",
    "ID_STR_CONTACTS_BANK_ADDRESS": "55 Nikola Vaptsarov Bvld., EXPO 2000, 1407 Sofia, Bulgaria",

    "ID_STR_CLIENT_AND_SERVER_VERSIONS_NOT_COMPATIBLE": ' <a target="_blank" style="text-decoration: none" href="market://details?id=com.raiffeisen.online.mobile">A new version of Raiffeisen ONLINE App is available.Please install it from here</a>',
    // navigation subtitles
    "ID_STR_LOGIN_BUTTON_SUB": "Login to your account",
    "ID_STR_CURRENCY_CONVERTER_SUB": "bank currency rates",
    "ID_STR_PRODUCTS_SUB": "between your accounts and to third parties",
    "ID_STR_TRANSFERS_SUB": "between your accounts and to third parties",
    "ID_STR_MESSAGES_SUB": "Notifications from the bank",
    "ID_STR_CONTACTS_SUB": "how to connect with us",
    "ID_STR_BRANCHES_SUB": "how to find our branches",
    "ID_STR_LOGOUT_BUTTON_SUB": "Exit the application",

    "ID_STR_FILTER_BY_DATE": "Search by date",

    // misc resources
    "ID_STR_BALANCE_BY_TYPE": "Availability by products",
    "ID_STR_LIST_BALANCE_BY_TYPE": "Products list",
    "ID_STR_LOGO_PATH": "Content/images/logo-en.png",
    "ID_STR_HEADLOGO_PATH": "Content/images/headlogoen.png",
    "-": "-",
    "ID_STR_ONLINE_NOTIF": "Notifications",
    "ID_STR_ONLINE_NOTIF_SUB": "Notificatins about current state of your funds",
    "ID_STR_ONLINE_NOTIF_LIST": "List notifications",
    "ID_STR_ONLINE_NOTIF_SETTING": "Notification setting",
    "ID_STR_ONLINE_NOTIF_NONE": "You don't have notifications",
    "ID_STR_ONLINE_NOTIF_SUBSCRIPTION_LIST": "Subscriptions list",
    "ID_STR_ONLINE_NOTIF_CREATE_NEW_SUBSCRIPTION": "New subscription",
    "ID_STR_ONLINE_NOTIF_ACTIVATE": "Activate",
    "ID_STR_ONLINE_NOTIF_DEACTIVATE": "Deactivate",

});