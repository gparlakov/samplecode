﻿define([
  'require',
  'jquery',
  'jsrender',
  'knockout',
  'events',
  "backbone",
  'session',
  'text!Mobile/Templates/PageTemplate.html',
  'text!Mobile/Templates/Common/PanelHeader.html',
  'text!Mobile/Templates/Common/HeaderNavigationTemplate.html',
  'text!Mobile/Templates/NavigationTemplate.html',
], function (require, $, $templates, ko, events, backbone, session, pageTmpl, panelHeader, headerNavigationTemplate, navigationTemplate) {

    var utils = {
        //format: $format,
        history: [],
        currentPage: '',

        getPath: function (name) {
            return "/Scripts/Mobile/Templates/{0}.html".format(name);
        },

        renderTemplate: function (item) {
            //var file = utils.getPath(item.name);
            $.whenEx(
                events.trigger(item.data, 'load', item.data) //tova tryabva da se mahne!!
            ).then(function () {
                $templates({ tmpl: item.tmplData });

                item.selector.html($.render.tmpl(item.data));

                events.trigger(item.data, 'rendering', { element: item.selector, data: item.data })

                $(item.selector[0]).one('remove',
                    function (e) {
                        events.trigger(item.data, 'unload', item.data);
                    });
            });
        },

        applyBinding: function (item) {
            ko.applyBindings(item.data, item.selector[0]);
            //трябва да се изчистят computed-ите на knockout-a за да се GC
            $(item.selector[0]).one("remove", function () {
                //diagnostics.Debug("calling ko.removeNode");
                ko.removeNode(this);
            });

            $(item.selector[0]).find(":data(includeUIScripts)").each(function (index, element) {
                require([$(element).data('includeUIScripts')], function (UIScriptsModule) {
                    var module = new UIScriptsModule();
                    var data = {
                        viewModel: item.data,
                        element: $(element)[0],
                        rootElement: item.selector[0]
                    };
                    module.applyUIScripts(data);
                }); 
            });

            //this.applyBindingChildren(item);
        },

        applyBindingChildren: function (item) {
            $.each(item.selector.children(), function () {
                ko.applyBindings(item.data, this);
                //трябва да се изчистят computed-ите на knockout-a за да се GC
                $(this).one("remove", function () {
                    //diagnostics.Debug("calling ko.removeNode");
                    ko.removeNode(this);
                });
            });
        },

        triggerMobileCreate: function (item) {

            $.data(item.selector[0], "data", item.data);
            $(item.selector[0]).trigger('create');

            return events.trigger(item.data, 'rendered', { element: item.selector[0] });
        },

        changePage: function (page) {
            var self = this;
            require(["Mobile/ViewModels/NavigationViewModel"], function (NavigationViewModel) {
                $("body").loadingpanel("show");

                $(page.el).addClass("controller-" + page.ControllerName.toLowerCase());
                $(page.el).addClass("action-" + page.ActionName.toLowerCase());
                $(page.el).attr('data-role', 'page');
                $(page.el).attr('data-url', window.location.pathname + window.location.hash);
                $(page.el).attr('id', 'mainPage');
                //$(page.el).hide();

                var layout = pageTmpl;
                if (page.viewModel.Layout)
                    layout = page.viewModel.Layout;

                page.viewModel.NavigationViewModel = new NavigationViewModel();
                var pageitem = utils._renderTempalteApplyBinding(layout, page.viewModel);

                $(pageitem.selector).children().each(function () {
                    $(page.el).prepend(this);
                });

                var oldNavigation = utils.getNavigationContainer();
                oldNavigation.remove();
            
                var navigationItem = utils._renderTempalteApplyBinding(navigationTemplate, page.viewModel.NavigationViewModel);

                $(navigationItem.selector).children().each(function () {
                    $('body').append(this);
                    $(this).swipeMenu();
                });

                var element = $(page.el);

                $('body').append(element);

                $.mobile.changePage(element, { changeHash: false });

                var itemToTrigger = {
                    data: page.viewModel,
                    selector: $(page.el)
                };

                var res = self.triggerMobileCreate(itemToTrigger);

                $("body").loadingpanel("hide");

                return res;
            });
        },

        _renderTempalteApplyBinding: function (tmplate, viewModel) {
            var pageitem = {
                selector: $("<div />"),
                data: viewModel,
                tmplData: tmplate
            };

            this.renderTemplate(pageitem);
            this.applyBinding(pageitem);

            return pageitem;
        },

        getCurrentPageViewModel: function () {
            var data = utils.getCurrentPageContainer().data('data');
            return data;
        },

        getCurrentPageContainer: function () {
            return $("#mainPage");
        },

        getNavigationContainer: function () {
            return $('#menuContainer');
        },

        callBackOnCurrentViewModel: function () {
            var viewModel = utils.getCurrentPageViewModel();

            if (viewModel && events.handlers.trigger(viewModel, "handlebackbutton"))
            { }
            else
            {
                this.navigateBack();
            }
        },

        navigateBack: function () {
            if (this.getLastPage() == "Login" && session.HasSession()) {
                console.log("Going back to login page with a session is not allowed!");
                //diagnostics.Debug("Going back to login page with a session is not allowed!");
            } else {
                var fragment = this.history.pop();
                this.navigate(fragment, { trigger: true }, true);
            }
        },

        getLastPage: function() {
            return this.history[this.history.length - 1];
        },

        buildLink: function (controller, action, params) {
            var result = '/' + controller;

            if (action && action != '') {
                result += '/' + action
            }

            if (params) {
                for (ind in params) {
                    result += '/' + params[ind];
                }
            }
            return result;
        },
        navigate: function (path, options, navigateBack) {
            if (path != backbone.history.fragment) {
                var self = this;
                self.currentPage = path;
                var res = $.DeferredEx(function (res) {

                    var hide = $("body").loadingpanel("show");

                    require(["Mobile/router"], function (router) {

                        var allPathsRes =
                            router.Router.allPaths(path)
                                .done(function () {
                                    if (!navigateBack)
                                        self.history.push(backbone.history.fragment);
                                    var op = $.extend({}, options, { trigger: false });
                                    backbone.history.navigate(path, op);
                                    hide();
                                }).exception(function () {
                                    hide();
                                })
                                .fail(function () {
                                    hide();
                                });

                        res.bindTo(allPathsRes);
                    });
                });

                return res;
            }
        }

    };

    return utils;
});
