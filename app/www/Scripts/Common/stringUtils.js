﻿define([

], function () {
    var initialize = function () {
        if (typeof(String.prototype.format) != 'function') {
            String.prototype.format = function () {
                var content = this;
                for (var i = 0; i < arguments.length; i++) {
                    var replacement = '{' + i + '}';
                    content = content.replace(replacement, arguments[i]);
                }
                return content;
            };
        }
    }

    return {
        initialize: initialize
    };
});