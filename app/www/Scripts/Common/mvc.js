﻿define([
  'jquery',
  'underscore',
  'backbone',
  'utils',
  'events'
], function ($, _, Backbone, utils, events) {

    // Shared empty constructor function to aid in prototype-chain creation.
    var ctor = function () { };

    // The self-propagating extend function that Backbone classes use.
    var extend = function (protoProps, classProps) {
        var child = inherits(this, protoProps, classProps);
        child.extend = this.extend;
        return child;
    };


    // Helper function to correctly set up the prototype chain, for subclasses.
    // Similar to `goog.inherits`, but uses a hash of prototype properties and
    // class properties to be extended.
    var inherits = function (parent, protoProps, staticProps) {
        var child;

        // The constructor function for the new subclass is either defined by you
        // (the "constructor" property in your `extend` definition), or defaulted
        // by us to simply call the parent's constructor.
        if (protoProps && protoProps.hasOwnProperty('constructor')) {
            child = protoProps.constructor;
        } else {
            child = function () { parent.apply(this, arguments); };
        }

        // Inherit class (static) properties from parent.
        _.extend(child, parent);

        // Set the prototype chain to inherit from `parent`, without calling
        // `parent`'s constructor function.
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();

        // Add prototype properties (instance properties) to the subclass,
        // if supplied.
        if (protoProps) _.extend(child.prototype, protoProps);

        // Add static properties to the constructor function, if supplied.
        if (staticProps) _.extend(child, staticProps);

        // Correctly set child's `prototype.constructor`.
        child.prototype.constructor = child;

        // Set a convenience property in case the parent's prototype is needed later.
        child.__super__ = parent.prototype;

        return child;
    };

    var mvc = {
        ViewResult: function (template, viewModelFactory, afterRender) {
            var self = this;
            self.template = template;
            self.viewModelFactory =
                $.isFunction(viewModelFactory) ?
                    function () { var a = new $.DeferredEx(function (a) {  
                                                    viewModelFactory(function (viewModel) { a.resolve(viewModel); }); 
                                                });
                                                return a; } :
                    function () { return viewModelFactory };



            this.execute = function (executedViewHandler) {
                var reuslt =
                    $.whenEx(
                        self.viewModelFactory()
                    )
                    .pipe(function (viewModel) {
                        events.trigger(self, 'viewModelAvailable', viewModel);
                        self.viewModel = viewModel;

                        var backBoneView = Backbone.View.extend({
                            render: function () {

                                var item = {
                                    tmplData: self.template,
                                    data: viewModel,
                                    selector: this.$el
                                };

                                utils.renderTemplate(item);

                                utils.applyBinding(item);

                                if (afterRender != undefined)
                                    afterRender(this.$el, viewModel);

                                return this;
                            }
                        });

                        self.BackboneView = new backBoneView();
                
                        var result =
                            $.whenEx(
                                events.trigger(viewModel, 'initialize')
                            )
                            .pipe(function () {
                                self.BackboneView.viewModel = viewModel;
                                self.BackboneView.render();

                                if (executedViewHandler)
                                {
                                    executedViewHandler(self.BackboneView);
                                }

                                return self.BackboneView;
                            });
                        return result;
                    });

                return reuslt;
            }
        }
    }


    var Controller = mvc.Controller = function (options) {
        options || (options = {});
        if (options.routes) this.routes = options.routes;
        this._bindRoutes();
        this.initialize.apply(this, arguments);
    };

    // Cached regular expressions for matching named param parts and splatted
    // parts of route strings.
    var namedParam = /:\w+/g;
    var splatParam = /\*\w+/g;
    var escapeRegExp = /[-[\]{}()+?.,\\^$|#\s]/g;

    _.extend(Controller.prototype, {

        handlers: [],

        initialize: function () { },

        handlePath: function (fragment) {
            
            var handler = _.find(this.handlers, function (handler) {
                return handler.route.test(fragment);
            });

            return handler.callback(fragment);

        },

        route: function (route, callback) {

            if (!_.isRegExp(route)) route = this._routeToRegExp(route);

            this._addToHanlders(route, _.bind(function (fragment) {
                var args = this._extractParameters(route, fragment);
                if (callback)
                    return callback.apply(this, args);

            }, this));
            return this;
        },

        _routeToRegExp: function (route) {
            route = route.replace(escapeRegExp, '\\$&')
							   .replace(namedParam, '([^\/]+)')
							   .replace(splatParam, '(.*?)');

            return new RegExp('^' + route + '$');
        },

        _extractParameters: function (route, fragment) {
            return route.exec(fragment).slice(1);
        },

        _addToHanlders: function (route, callback) {
            this.handlers.unshift({ route: route, callback: callback });
        },

        _bindRoutes: function () {
            if (!this.routes) {

                this.route("*index", this.index);
                return;
            }
            var routes = [];
            for (var route in this.routes) {
                routes.unshift([route, this.routes[route]]);
            }
            for (var i = 0, l = routes.length; i < l; i++) {
                this.route(routes[i][0], this[routes[i][1]]);
            }
        }
    });

    Controller.extend = extend;

    return mvc;
});