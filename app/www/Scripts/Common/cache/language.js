define([
    'underscore',
    'Common/nomenclatures/enumLang'

],function (_, enumLang) {
        var Language = {
            
            

            _langStorageKey : "applicationSelectedLang",
            _langDefaultValue : (new enumLang()).bg,

            GetCurrentLang : function (){

                var localStorageLangSTR = window.localStorage.getItem(Language._langStorageKey);

                if (localStorageLangSTR && localStorageLangSTR != '') {

                    var localStorageLang = JSON.parse(localStorageLangSTR);

                    var tmpLang = _.find(new enumLang(), function (l) { return l.id == localStorageLang.id });

                    if (tmpLang != null && tmpLang != 'undefined')
                        return (tmpLang);
                }
                //ne sme namerili ezika . Da setnem default stoinost i da go varnem 

                window.localStorage.setItem(Language._langStorageKey, JSON.stringify(Language._langDefaultValue));

                return Language._langDefaultValue;
            },

            SetCurrentLang: function (selectedLangID) {
                var tmpLang = _.find(new enumLang(), function (l) { return l.id == selectedLangID });
                
                //tuk stoijnostite za lang sa 1 i 0. Zaradi 0 ne varvi if (tmpLang) 
                if (tmpLang != null && typeof(tmpLang) != 'undefined') {
                    
                } else {
                    tmpLang = Language._langDefaultValue;
                }
                
                var langAsStr = JSON.stringify(tmpLang);

                window.localStorage.setItem(Language._langStorageKey, langAsStr)
            }

        }

        return Language ;
    });