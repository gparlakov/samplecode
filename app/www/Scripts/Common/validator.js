﻿define([
  'require',
  'jquery',
  'jsrender',
  'knockout',
  'jsrender',
  'underscore',
  'utils',
  'diagnostics',
  'i18n!Mobile/nls/validation',
], function (require, $, $templates, ko, jsrender, _, utils, diagnostics, validationMessages) {

    Validator = {};
    
    Validator.getTemplateMessageFormater = function(template)
    {
        var validator = this;
        var compiledTemplate = jsrender(template);
        return function (value, parameters, context, result) {
            var object = {
                value: value,
                parameters: parameters,
                context: context,
                result: result
            }
            var text = compiledTemplate.render(object);
            return text;
        }
    }

    Validator.Custom = function Custom(f) {
        this.func = f;
        return this;
    }

    Validator.methods = {
        NotNull: {
            method: function (value, parameters) {
                return !parameters || (value != null);
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_NOTNULL)
        },
        NotNullOrEmpty: {
            method: function (value, parameters) {
                return !parameters || (value != null && value != '');
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_NOTNULLOREMPTY)
        },
        MaxLength: {
            method: function (value, parameters) {
                if (typeof (value) == "string")
                    return value.length <= parameters;
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_MAXLENGTH)
        },
        MinLength: {
            method: function (value, parameters) {
                if (typeof (value) == "string")
                    return value.length >= parameters;
                return true;
                },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_MINLENGTH)
        },
        DeciamlMinValue: {
            method: function (value, parameters) {
                if (value != null && parameters != null) {
                    var v = value;
                    if (typeof (v) == "string")
                        v = parseFloat(v);
                    return v > parameters.value || (parameters.inclusive && v == parameters.value);
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_DECIAMLMINVALUE)
        },
        DecimalMaxValue: {
            method: function (value, parameters) {
                if (value != null && parameters != null) {
                    var v = value;
                    if (typeof (v) == "string")
                        v = parseFloat(v);
                    return v < parameters.value || (parameters.inclusive && v == parameters.value);
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_DECIAMLMAXVALUE)
        },
        RegEx: {
            method: function (value, parameters) {
                if (parameters != null) {
                    if (typeof (parameters) != "string") {
                        throw "Validator: validator parameter must be string";
                    }
                    if (typeof (value) == "string") {
                        var regex = new RegExp(parameters);
                        return regex.test(value);
                    }
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_REGEX)
        },
        MinDate :{
            method: function (value, parameters) {
                if (value != null) {
                                                                                //tapiiii dati - ravnoto ne raboti 
                    return value > parameters.value || (parameters.inclusive && (value - parameters.value == 0));
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_EXECUTION_DATE_MUST_BE_LATER)
        },
        // saobshtenieto e za prevodi . 
        MaxDate: {
            method: function (value, parameters) {
                if (value != null) {
                    //tapiiii dati - ravnoto ne raboti 
                    return value < parameters.value || (parameters.inclusive && (value - parameters.value == 0));
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_EXECUTION_DATE_MUST_BE_SOONER)
        },
        SubModel: {
            method: function (value, parameters) {
                return !parameters || (Validator.isValidatableModel(value) && Validator.getValidator(value).validate());
            },
            message: null
        },
        EqualTo: {
            method: function (value, parameters) {
                if (value != null)
                {
                    if (parameters != null && parameters.equalToValue != null)
                    {
                        return value == parameters.equalToValue;
                    }
                }
                return true;
            },
            message: Validator.getTemplateMessageFormater(validationMessages.TMPL_VAL_EQUALTO)
            
        },
        Custom: {
            method: function (value, parameters, context) {
                return parameters.func.call(this, value, parameters, context)
            },
            message: null

        }
    }
    Validator.methods.add = function(methods) {
        _.extend(Validator.methods, methods);
    }

    Validator.RegExUtils = {
        Amount: "^[0-9]{1,11}[.|,]{0,1}[0-9]{0,2}$",
        AmountOptional: "^(([0-9]{1,11}[.|,]{0,1}[0-9]{0,2})|())$",
        HomeIBAN: "^BG{1}[0-9]{2}[A-Z0-9]{18}$"
    }

    Validator._subscriptionWrapper = function validation__subscriptionWrapper(subscription, name)
    {
        this.subscription = subscription;
        subscription.____debug = this.subscription;
        this.name = name;
    }
    Validator.properyChangeNotifier = function validation_properyChangeNotifier(propertyChangedNotification) {
        var self = this;
        var iteration = 0;

        this._callPropertyChangedNotification = function (propertyValue, context) {
            propertyChangedNotification.call(this, propertyValue, context);
        }

        this._subscriptions = [];

        this.clearState = function () {
            iteration++;
            _.each(self._subscriptions, function (sub) {
                sub.subscription.dispose();
            });
            self._subscriptions = [];
        }

        this.registerVisitedProperty = function (propertyValue, context) {
            (function (propertyValue, context, addedIteration) {
                if (addedIteration != self.iteration) {
                    throw "Validator: notified on subscription on previous iteration";
                }
                if (ko.isObservable(propertyValue)) {
                    if (ko.isComputed(propertyValue))
                    {
                        diagnostics.Debug("Validator: Attaching to computed observable: " + context.modelName);
                    }
                    var subscription = propertyValue.subscribe(function () {
                        self._callPropertyChangedNotification(propertyValue, context)
                    });
                    self._subscriptions.push(new Validator._subscriptionWrapper(subscription, context.modelName));
                }
            })(propertyValue, context, self.iteration);
        };

    }
    
    Validator.validationResultContainer = function validation_validationResultContainer()
    {
        var self = this;

        this.errors = ko.observableArray([]);


        this.GetObservableMessageFor = function (path)
        {
            diagnostics.Debug("Validating: GetObservableMessageFor " + path);
            return ko.dependentObservable({
                read: function () {
                    return _.find(self.errors(), function (item) { return item.path == path; });
                }
            });
        }

        this.claerState = function (context) {
            this.errors.removeAll();
        };

        this.registerInvalidProperty = function (context, message) {
            this.errors.push(
                    {
                        path: context.getFullPath(),
                        context: context,
                        message: message
                    }
                );
        };
    }

    
    Validator.context = function validation_context() {
        var self = this;

        this.validtor = null;
        this.modelName = null;
        this.parentContext = null;

        this._fullPath = null;

        this._childrens = {};
        
        this.getChild = function(propertyName)
        {
            if (!self._childrens[propertyName])
            {
                self._childrens[propertyName] = this._createChildContextFor(propertyName);
            }
            return self._childrens[propertyName];
        }

        this._createChildContextFor = function (propertyName) {
            var child = new Validator.context();
            child.parentContext = self;
            child.modelName = propertyName;
            child.validtor = self.validtor;

            return child;
        };

        this.getFullPath = function () {
            if (self._fullPath == null) {
                self._fullPath = self.modelName;
                if (self.parentContext != null) {
                    var parentPath = self.parentContext.getFullPath();
                    if (parentPath != '') {
                        self._fullPath = "{0}.{1}".format(self.parentContext.getFullPath(), self._fullPath);
                    }
                }
            }
            return self._fullPath;
        };

        this.getValue = function()
        {
            if (self.parentContext != null)
            {
                var parentValue = self.parentContext.getValue();
                if (parentValue[self.modelName])
                {
                    return self.validtor.unWrapValue(parentValue[self.modelName])
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }

    Validator.rootContext = function validation_rootContext(model, validtor) {
        Validator.context.call(this);
        this.validtor = validtor;
        this.getValue = function() { return model; };
        this.modelName = "";
        this.parentContext = null;
    }


    Validator.modelValidator = function validation_validatableModel(initParams, model) {
        var self = this;



        this.initParams = initParams;
        this.getProperties = function () {
            var initParamsUnwrapped = self.unWrapValue(self.initParams);

            return initParamsUnwrapped["properties"] ? initParamsUnwrapped["properties"] : [];
        }


        this._changeNotifier = new Validator.properyChangeNotifier(
            function() {
                self.validate({ autoFired: true });
            }
        );

        this._resultContainer = new Validator.validationResultContainer();
        this.GetObservableMessageFor = this._resultContainer.GetObservableMessageFor;

        this._rootContext = new Validator.rootContext(model, self);

        this.model = model;

        this._validateComplateCallbacks = $.Callbacks("unique");

        this.validate = function (options) {
            var o = options || { 
                autoFired: false //дали е запалено вътрешно от зависимост
            }
            self._changeNotifier.clearState();
            self._resultContainer.claerState();

            var res = self.validateProperties(self.model, self.getProperties(), self._rootContext, options);

            self._validateComplateCallbacks.fire(self, res, o);

            return res;
        };

        this.validateCompleted = {
            add: this._validateComplateCallbacks.add,
            remove: this._validateComplateCallbacks.remove
        }

        //razopakova stoinost i ako moje da se abonira se abonira za change
        this.unWrapValueAndRegisterOnChange = function(value, context)
        {
            
            if (value == null) {
                return null
            } else if (ko.isObservable(value)) {
                self._changeNotifier.registerVisitedProperty(value, context);
                return self.unWrapValueAndRegisterOnChange(ko.utils.unwrapObservable(value), context);
            } else if (_.isFunction(value)) {
                return self.unWrapValueAndRegisterOnChange(value.call(value), context);
            } else {
                return value;
            }
        }
        this.unWrapValue = function (value)
        {
            if (value == null) {
                return null
            } else if (ko.isObservable(value)) {
                return self.unWrapValue(ko.utils.unwrapObservable(value));
            } else if (_.isFunction(value)) {
                return self.unWrapValue(value.call(value));
            } else {
                return value;
            }
        }

        this.validateProperties = function (model, properties, context, options) {
            var boolresult = true;
            for (var property in properties) {
                var internaloptions = options;

                if (options != null) {
                    if (options.only != null && !options.only[property])
                        continue;
                    else
                        internaloptions = $.extend({}, options, { only: options.only != null ? options.only[property] : null });
                }

                

                var childContext = context.getChild(property);

                var propertiesAndRuiles = properties[property];

                //var value = Validator.propertyGetter(model, property);
                
                var value = self.unWrapValueAndRegisterOnChange(model[property], childContext);
                

                if (propertiesAndRuiles.rules) {
                    boolresult = boolresult & self.validateRules(value, propertiesAndRuiles.rules, childContext, propertiesAndRuiles);
                }

                if (propertiesAndRuiles.properties) {
                    boolresult = boolresult & self.validateProperties(value, propertiesAndRuiles.properties, childContext, internaloptions);
                }
            }
            return boolresult;
        };

        this.validateRules = function (value, rules, context, propertiesAndRuiles) {
            var boolresult = true;
            for (var rule in rules) {
                diagnostics.Debug("Validating: {0} for rule {1}".format(context.getFullPath(), rule));

                var method = Validator.methods[rule];
                var params = self.unWrapValueAndRegisterOnChange(rules[rule], context);

                var result = method.method.call(self, value, params, context);
                if (!_.isObject(result))
                {
                    result = { valid: result };
                }
                if (!result.valid)
                {
                    diagnostics.Debug("Validating: Validation Failed!");
                    boolresult = false;
                    var message = "";
                    var messageGenerator = method.message;
                    if (propertiesAndRuiles["messages"] && propertiesAndRuiles["messages"][rule])
                    {
                        if (_.isFunction(propertiesAndRuiles["messages"][rule])) {
                            messageGenerator = propertiesAndRuiles["messages"][rule];
                        } else {
                            messageGenerator = validationMessages[propertiesAndRuiles["messages"][rule]];
                        }
                    }
                    if (messageGenerator) { //za nyakoi ne iskame da imame saobshteniya
                        if (_.isFunction(messageGenerator)) {
                            message = messageGenerator.call(this, value, params, context, result);
                        }
                        else {
                            message = messageGenerator;
                        }
                        self._resultContainer.registerInvalidProperty(context, message);
                    }
                }
            }
            return boolresult;
        };
    }

    Validator.validatableModel = function validation_validatableModel(params)
    {
        var validator = new Validator.modelValidator(params, this);
        this.validator = function () { return validator; }
    }
    Validator.isValidatableModel = function (model) {
        return model != null && model.validator != null;
    }

    Validator.isValidatable = Validator.isValidatableModel;
    Validator.getValidator = function (model) {
        if (!Validator.isValidatableModel(model))
            throw "Validator: model is not validatable";
        return model.validator();
    }
    
    ko.bindingHandlers.validationMessageFor = {
        clear: function(element) {
            $(element).css("display", "none");
            $(element).parent().removeClass('has-error has-feedback');

            $(element).empty();
        },
        setError: function(element, error)
        {
            $(element).css("display", "");
            $(element).parent().addClass('has-error has-feedback');
            $(element).text(error.message);
        },
        scrollToError: function()
        {
            var list = $("span.validation-msg:visible");
            if (list.length == 0)
                return;
            var visibleMessages = list.map(function () { return { position: $(this).offset().top, element: this } });
            var sorted = _.sortBy(visibleMessages, function (item) { return item.position; });
            var first = _.first(sorted);

            var topPosition = $(first.element).offset().top -130;
            var parent = $(first.element).parent(); //ако бащата държи само една валидация
            //if (parent.children("span.validation-msg").length == 1) {
            //    topPosition = _.min([parent.position().top, topPosition]);
            //}

            $('body').animate({ scrollTop: topPosition }, 400);
        },
        globalScrollHandler: function(validator, result, options) {
            if (!result && !options.autoFired)
            {
                ko.bindingHandlers.validationMessageFor.scrollToError();
            }
        },
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            //var $innerContainer = $("<span></span>");
            //var $vtip = $("<img class='vtip' src='Content/images/validation_tip.png' alt=''></img>");

            $(element).empty().css("display", "none");

            //$(element).empty().addClass("validation")

            //$(element).append($innerContainer);

      //      $(element).append($vtip);

            if (Validator.isValidatableModel(viewModel))
            {
                var currentValidator = Validator.getValidator(viewModel);

                currentValidator.validateCompleted.add(ko.bindingHandlers.validationMessageFor.globalScrollHandler);

                var observable = currentValidator.GetObservableMessageFor(valueAccessor());
                observable.subscribe(function () {
                    newValue = observable();
                    diagnostics.Debug("Changed " + newValue);
                    if (newValue)
                    {
                        ko.bindingHandlers.validationMessageFor.setError(element, newValue)
                        //ko.bindingHandlers.validationMessageFor.scrollToError()
                    }
                    else
                    {
                        ko.bindingHandlers.validationMessageFor.clear(element)
                    }
                });
            }
            else
            {
                throw "Validator: ko binder viewModel must be validateable";
            }
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            
        }
    }

    return Validator;
});
