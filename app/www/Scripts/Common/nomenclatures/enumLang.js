﻿define([
    'i18n!Mobile/nls/strings'
], function (strings) {
        var EnumLang = function (){
           this.bg = {
                id: 1, //synced with gac EnumLang 
                value: 'bg-bg',
                text: strings.ID_STR_LANGUAGE_BG
            },
            this.en = {
                id: 2, //synced with gac EnumLang 
                value: 'en-us',
                text: strings.ID_STR_LANGUAGE_ENG
            }
        }
        return EnumLang;
    });