﻿define([
    "Common/ufnomenclatures/UFNomenclaturePair"
    ],function (ufPair) {

        var LiabilityPersonTypes = [
            new ufPair("0", "ID_STR_LIABILITY_PERSON_TYPE_LOCAL"),
            new ufPair("1", "ID_STR_LIABILITY_PERSON_TYPE_FOREIGNER"),
            new ufPair("2", "ID_STR_LIABILITY_PERSON_TYPE_COMPANY")
        ];

        return LiabilityPersonTypes;
    });