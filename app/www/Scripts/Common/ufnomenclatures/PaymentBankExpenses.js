﻿define([
    "Common/ufnomenclatures/UFNomenclaturePair"
    ],function (ufPair) {

        var PaymentBankExpenses = [
            new ufPair(2, "ID_STR_PAYMENT_BANK_EXPENSES_SHA"),
            new ufPair(0, "ID_STR_PAYMENT_BANK_EXPENSES_OUR"),
            new ufPair(1, "ID_STR_PAYMENT_BANK_EXPENSES_BEN")
        ];

        return PaymentBankExpenses;
    });