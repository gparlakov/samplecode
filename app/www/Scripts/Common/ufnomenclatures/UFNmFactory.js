﻿define([
    'underscore',
    'jquery',
    'cache',
    'services',
    'servicesObjects',

    "Common/ufnomenclatures/UFNomenclaturePair",
    "Common/cache/language",

], function (_, $, cache, services, servicesObjects, ufPair, lang) {

    var UFNmFactory = function () {

        var UFNmTypes = {
            NomenclatureCountry: {
                self : this,
                typeName: 'NomenclatureCountryFullFilter',
                idselector: function (value) {
                    return value.ISOCode;
                },

                resultValuesSelector : function(value){
                    return value.Countries;
                },

                postRetrieveAction: function (nomenclatureCollection) {
                    var currentLang = lang.GetCurrentLang();
                    nomenclatureCollection.Countries = _.sortBy(nomenclatureCollection.Countries, function (cntr) 
                    { return _.find(cntr.Name, function (name) { return name.Lang == [currentLang.id]; }).Value; });
                }
            },

            NomenclaturePaymentTypes: {
                typeName: 'NomenclaturePaymentTypesFilter',
                idselector: function (value) {
                    return value;
                },
                resultValuesSelector: function (value) {
                    return value.PaymentTypes;
                },
                postRetrieveAction: function (nomenclatureCollection) {

                }
            },

            NomenclatureDirtyMoneyReasons: {
                typeName: 'NomenclatureDirtyMoneyReasonsFilter',
                idselector: function (value) {
                    return value;
                },
                resultValuesSelector: function (value) {
                    return value.DirtyMoneyReasons;
                },
                postRetrieveAction: function (nomenclatureCollection) {

                }

            },
            MobileOperators: {
                typeName: 'NomenclatureMobileOperatorsFilter',
                idselector: function (value) {
                    return value.ID;
                },
                resultValuesSelector: function (value) {
                    return value.MobileOperators;
                },
                postRetrieveAction: function (nomenclatureCollection) {

                }
            },
            NomenclatureOpModes: {
                typeName: 'NomenclatureOpModesFilter',
                idselector: function (value) {
                    return value;
                },
                resultValuesSelector: function (value) {
                    return value.OpModes;
                },
                postRetrieveAction: function (nomenclatureCollection) {

                }
            },
            NomenclatureOnlineNotificationServiceTypes: {
                typeName: '',
                idselector: function (value) {
                    return value;
                },
                resultValuesSelector: function (value) {
                    return value.Services;
                },
                postRetrieveAction: function (nomenclatureCollection) {

                },
                // alternative retreive action
                retrieveAction: function () {
                    return services.OnlineNotificationsService.GetOnlineNotificationServices()
                        .pipe(function (data) {
                            var res = {
                                NomenclatureCollection: {}
                            };
                            res.NomenclatureCollection.Services = data.OnlineNotificationServices;
                            return res;
                        });
                }
            }
        }


        var self = this;
        this.ufNomenclatureTypes = UFNmTypes;

        var getRetreiveAction = function (nomenclatureType) {
            var result = null;

            if (nomenclatureType.retrieveAction && $.isFunction(nomenclatureType.retrieveAction)) {
                result = nomenclatureType.retrieveAction();
            }

            return result;
        }


        this.GetUfNomenclature = function (type, nomenclatureReadyCallBack) {

            var deferred = $.DeferredEx(function (deferred) { 

                var resultNomenclature = {};
            
                resultNomenclature._STORAGE_KEY = type.typeName;
                resultNomenclature._nomenclatureInt = cache.GetObject(resultNomenclature._STORAGE_KEY);

                resultNomenclature._nomenclatureFilter = {
                    $type: servicesObjects.Utils.getFullName(resultNomenclature._STORAGE_KEY)
                }

                resultNomenclature.nomenclatureToShow = function () {
                    return _.map(resultNomenclature._nomenclatureInt, function (nomenclature) {
                        return { ID: type.idselector(nomenclature), Value: $.GetLocalizedString(nomenclature.Name) };
                    });
                }
            
                resultNomenclature.GetNomenclature = function ()
                {
                    return resultNomenclature._nomenclatureInt;
                }

                if (!resultNomenclature._nomenclatureInt) {
                    var retrieveAction = getRetreiveAction(type) ||
                        services.NomenclatureService.GetNomenclature(resultNomenclature._nomenclatureFilter);

                    var request = 
                        $.whenEx(
                            retrieveAction
                        )
                        .then(function (result) {
                            type.postRetrieveAction(result.NomenclatureCollection);
                            resultNomenclature._nomenclatureInt = type.resultValuesSelector(result.NomenclatureCollection);
                            cache.SetObject(resultNomenclature._STORAGE_KEY, resultNomenclature._nomenclatureInt);
                        
                            //deferred.resolve(resultNomenclature);
                            return resultNomenclature;
                        })
                    deferred.bindTo(request);
                } else {
                    deferred.resolve(resultNomenclature);
                }
            });
            return deferred;
        }

    }      
    return UFNmFactory;
});