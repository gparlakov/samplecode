﻿define([
  'knockout',
  'komapping',
  'underscore',
  'jquery',
  'jqueryformat',
  'servicesObjects',
  'dateUtils'
], function (ko, komapping, _,$, $format, servicesObjects, dateUtils) {

    ko.bindingHandlers.datePickerValue = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            //var value = ko.utils.unwrapObservable(valueAccessor());

            //ko.utils.registerEventHandler(element, 'change', function () {
            $(element).bind('change', function (e) {
                var elementVal = $(element).val();

                var newDate = '';

                if (typeof (elementVal) == "string") {
                    if (dateUtils.isNativeDate(elementVal)) {
                        newDate = dateUtils.parseDateFromNative(elementVal);

                    } else if (dateUtils.isBrowseDate(elementVal)) {
                        newDate = dateUtils.parseDateFromBrowser(elementVal);
                    }

                }

                var value = valueAccessor();
                value(newDate);
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            if (value == null || value == '') {
                $(element).val('');
            }
            else {
                if ($(element).attr('type') != 'date') {
                    $(element).val($.format.date(value));
                } else {
                    $(element).val($.format.date(value, 'yyyy-MM-dd'));
                }
            }
        }
    };


    //preimenuval sam go na jqmSelectedValue
    //za selecti. Imat problemi kogato im se semnyat optionite ili stoinostta ot bind-ing. Stoinostta e selectnata no ui-a ne se refreshva
    ko.bindingHandlers.selectedValue = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            ko.bindingHandlers.value.init(element, valueAccessor, allBindingsAccessor, viewModel);
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            //console.log("1111elememenenenne" + element.length);
            ko.bindingHandlers.value.update(element, valueAccessor, allBindingsAccessor, viewModel);

            if ($(element).children().length > 0) { //ako ima change varhu prazen spisak, ponqkoga moje da se zanuli           
                $(element).trigger("change");  //stoinostta na computed-a, za koito e bind-nat spisaka 
            }
        }
    };

    ko.bindingHandlers.includeUIScripts = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            $(element).data('includeUIScripts', ko.utils.unwrapObservable(valueAccessor()));
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            //console.log("1111elememenenenne" + element.length);
        }
    };

    //not working as expected anymore.
    //ko.bindingHandlers.valueAfterKeyDown = {
    //    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
    //        var allBindings = allBindingsAccessor();

    //        allBindings.valueUpdate = 'afterkeydown';
    //        allBindingsAccessor(allBindings);

    //        ko.bindingHandlers.value.init(element, valueAccessor, allBindingsAccessor, viewModel);
    //    },
    //    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
    //        ko.bindingHandlers.value.update(element, valueAccessor, allBindingsAccessor, viewModel);
    //    }

    //}

    ko.bindingHandlers.partial = {
        rerender: function (element, options) {
            if (options && options.data) {
                require(
                    [
                        "servicesObjects",
                        "utils",
                        "jsrender"
                    ],
                    function (servicesObjects, utils, $templates) {

                        var tempaltePath = "text!Mobile/Templates/{0}.html".format(options.path);

                        require(
                            ["utils", tempaltePath]
                            , function (utils, tempalte) {
                                $(element).children().remove();
                                var pair = {
                                    tmplData: tempalte,
                                    data: options.data,
                                    selector: $(element)
                                };

                                utils.renderTemplate(pair);

                                utils.applyBindingChildren(pair);
                                utils.triggerMobileCreate(pair);
                            }
                        );
                    }
                );
            }
            else {
                $(element).children().remove();
            }
        },
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            return { controlsDescendantBindings: true };
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = ko.utils.unwrapObservable(valueAccessor());
            
            if (options && options.path && (options.data || !options.hasOwnProperty("data"))) {
                ko.bindingHandlers.partial.rerender(element, {
                    data: options.data ? options.data : viewModel,
                    path: options.path
                });
            }
            else {
                $(element).children().remove();
            }
        }
    }


    ko.bindingHandlers.displayFor = {
        rerender: function(element, options)
        {
            if (options && options.data)
            {
                require(
                    [
                        "servicesObjects",
                        "utils",
                        "jsrender"
                    ],
                    function (servicesObjects, utils, $templates) {
           
                        var typeName = servicesObjects.Utils.GetShortTypeNameByObject(options.data);
                        if (typeName == null)
                            typeName = options.data.constructor.name;

                        var tempaltePath = "text!Mobile/Templates/{0}{1}.html".format(options.path, typeName);

                        require(
                            ["utils", tempaltePath]
                            , function (utils, tempalte) {
                                $(element).empty();

                                var pair = {
                                    tmplData: tempalte,
                                    data: options.data,
                                    selector: $(element)
                                };

                                utils.renderTemplate(pair);
                                utils.applyBindingChildren(pair);
                                utils.triggerMobileCreate(pair);
                            }
                        );
                    }
                );
            }
            else
            {
                $(element).children().remove();
            }
        },
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            return { controlsDescendantBindings: true };
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = ko.utils.unwrapObservable(valueAccessor());

            if (options) {
                ko.bindingHandlers.displayFor.rerender(element, {
                    data: ko.utils.unwrapObservable(options.data),
                    path: ko.utils.unwrapObservable(options.path)
                });
            }
            else {
                $(element).children().remove();
            }
        }
    }

    ko.bindingHandlers.renderAction = {
        rerender: function(element, options) {
            require(
                            [
                              'underscore',
                              'backbone',
                              'Mobile/router',
                              'events'
                            ]
                            , function (_, backbone, router, events) {
                                
                                if (options == null)
                                    return;

                                var action = options["action"];
                                var controller = options["controller"];
                                var params = options["params"];

                                var old = $(element).data("renderAction_options");
                                if (old && action == old.action && controller == old.controller && params == old.params)
                                {
                                    return;
                                }
                                $(element).data("renderAction_options", options);
                                $(element).children().remove();

                                var fullControllerPath = router.Router.CONTROLLER_PATH + controller;

                                require(
                                    ["utils", fullControllerPath],
                                    function (utils, controllerType) {
                                        var ctrl = new controllerType();

                                        var viewresult = ctrl[action].apply(ctrl, params);

                                        events.bind(viewresult, 'viewModelAvailable', function (data) {
                                            events.trigger(options, 'viewModelAvailable', data);
                                        });

                                        viewresult.execute(function (executedViewResult) {
                                            $(element).append(executedViewResult.el);
                                            utils.triggerMobileCreate({ data: executedViewResult.viewModel, selector: $(element) });
                                        });
                                    }
                                )
                            }
                );
        },
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            return { controlsDescendantBindings: true };
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = ko.utils.unwrapObservable(valueAccessor());
            ko.bindingHandlers.renderAction.rerender(element, options);
        }
    }

    ko.bindingHandlers.submenu = {

        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            $(element).submenu(ko.utils.unwrapObservable(valueAccessor()));
            return { controlsDescendantBindings: true };
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            
        }

    }

    ko.bindingHandlers.widgetCall = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = ko.utils.unwrapObservable(valueAccessor());

            setTimeout(function () {
                if (options.trigger == null || options.trigger) {
                    if ($(element).data(options.widget)) {
                        if (options.args) {
                            $(element).data(options.widget)[options.method](options.args)
                        }
                        else {
                            $(element).data(options.widget)[options.method]()
                        }
                    }
                }
            }, 0);
            
        }
    }

    ko.bindingHandlers.$event = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = ko.utils.unwrapObservable(valueAccessor());

            var allBindings = allBindingsAccessor();
            
            for (var name in options)
            {
                var bubble = allBindings[name + 'Bubble'] !== false;
                $(element).bind(name, function (e) {

                    options[name].call(viewModel, viewModel);
                    if (!bubble) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        return false;
                    }
                });
            }
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        }
    }

    ko.bindingHandlers.daisClick = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var wrappedValueAccessor = function () {
                return function (data, event) {
                    //prevent event if there is an active loading panel
                    var isLoading = $(".ui-loadingpanel-active").length != 0;
                    if (!isLoading) {
                        var handle = ko.utils.unwrapObservable(valueAccessor());
                        if ($.isFunction(handle)) {
                            handle.call(viewModel, viewModel, event);
                        }
                    }
                };
            };
            ko.bindingHandlers.click.init(element, wrappedValueAccessor, allBindingsAccessor, viewModel, viewModel);
        }
    }

    ko.bindingHandlers.clickWithEffect = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var wrappedValueAccessor = function () {
                return function (data, event) {
                    //prevent event if there is an active loading panel
                    var isLoading = $(".ui-loadingpanel-active").length != 0;
                    if (!isLoading) {
                        //show effect
                        ko.bindingHandlers.clickWithEffect.preOnClick.call(viewModel, element, data, event);
                        var handle = ko.utils.unwrapObservable(valueAccessor());
                        if ($.isFunction(handle)) {
                            handle.call(viewModel, viewModel, event);
                        }
                    }
                };
            };
            ko.bindingHandlers.click.init(element, wrappedValueAccessor, allBindingsAccessor, viewModel, viewModel);
        },
        preOnClick: function (element, data, event) {
            var $div = $('<div/>'),
			btnOffset = $(element).offset(),
			  xPos = event.pageX - btnOffset.left,
			  yPos = event.pageY - btnOffset.top;

            $div.addClass('ripple-effect');
            var $ripple = $(".ripple-effect");

            $ripple.css("height", $(element).height());
            $ripple.css("width", $(element).height());
            $div.css({
                top: yPos - ($ripple.height() / 2),
                left: xPos - ($ripple.width() / 2),
                background: $(element).data("ripple-color")
            }).appendTo($(element));

            window.setTimeout(function () {
                $div.remove();
            }, 2000);
        }
    }

    ko.bindingHandlers.debugViewObject = {
        renderObject: function (parentElement, object, name)
        {
            var visited = [];
            var render = function (parentElement, object, name) {
            var val = ko.utils.unwrapObservable(object);
                if (_.any(visited, function (item) { return item == val; })) {
                    $(parentElement).append($("<div>{0}: VISITED</div>".format(name)));
                    return;
                }
                visited.push(val);

            if ($.isFunction(val)) {
                if (ko.isObservable(val)) {
                        render(
                            parentElement,
                            ko.utils.unwrapObservable(val),
                            name
                        );
                }
            }
            else if ($.isPlainObject(val) || $.type(val) == "object") {
                var inner = $("<div style='margin-left: 10px;' />")
                $(parentElement).append($("<span>{0}</span>".format(name)));
                $(parentElement).append(inner);

                for (var name in val) {
                        render(inner, val[name], name);
                }
            } else if ($.isArray(val)) {
                var value = "{0}:ARRAY:".format(name);
                $(parentElement).append($("<div>{0}</div>".format(value)));
            } else {
                var value = "{0}:{1}".format(name, val);
                $(parentElement).append($("<div>{0}</div>".format(value)));
            }

                visited.pop();
            }

            return render(parentElement, object, name);
        },
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            return { controlsDescendantBindings: true };
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var val = ko.utils.unwrapObservable(valueAccessor());
            var refreshF;
            function refresh() {
                $(element).children().remove();
                ko.bindingHandlers.debugViewObject.renderObject(element, val, "ROOT");
                $(element).append($("<a>refresh</a>").click(function () {
                    refreshF();
                }));
            }
            refreshF = refresh;
            refresh();
        }
    }

    komapping.defaultOptions({
        copy: [],
        include:[],
        ignore: []
    });

    return ko;
});