define([
  'cache'
], function (cache) {


	var Session = {
	    
	    _sessionStorageKey: 'session',
        _credentialStorageKey: 'credentialID',

	    Errors: {
	        NoSession: "NoSession"
	    },

	    GetSession : function(){
	    	return cache.GetObject(this._sessionStorageKey);
	    },
	    
	    SetSession : function(session){
	    	cache.SetObject(this._sessionStorageKey, session);
	    }, 
	    
	    CheckSession : function () {
	        if (this.GetSession() == null) {
	            throw new Error(this.Errors.NoSession);
	        }
	    },
	    
	    Clear : function(){
	    	cache.RemoveObject(this._sessionStorageKey); 
	    	
	    },
	    HasSession: function () {
	        return this.GetSession() != null;
	    },

	    GetCredentialID: function () {
	        return cache.GetObject(this._credentialStorageKey);
	    },

	    SetCredentialID: function (credentialID) {
	        cache.SetObject(this._credentialStorageKey, credentialID);
	    },
	}
	
	return Session;
});