define([
  'pushNotificationsOSSpecific'
], function (pushOSSpecific) {
  
	var PushNotifications = {
	    Register: function (options) {
	        pushOSSpecific.Register(options);
	    },

	    Unregister: function () {
	        pushOSSpecific.Unregister();
	    }
	}

	return PushNotifications;
});