﻿define([
  'jquery',
  'events',
  'config',
  'osManager',
], function ($, events, config, osManager) {

    var deviceapi = {}

    deviceapi.device = {};
    
    deviceapi.device.DeviceID = navigator.appVersion;
    deviceapi.device.DeviceName = navigator.appName;
    deviceapi.device.DevicePlatform = navigator.platform;
    deviceapi.device.DeviceVersion = navigator.appVersion;
    deviceapi.device.DeviceModel = navigator.appCodeName;
    deviceapi.useNativeCalendar = false;
    

    deviceapi.notification = {};
   
    deviceapi.notification.Alert = function (title, message, buttonName) {
        alert("{0}: {1}".format(title, message));
    };
    deviceapi.notification.Confirm = function (title, message, buttons) {
        var self = this;
        var result = new $.DeferredEx(function (result) { 
            var $dialog = $("<div class='container  flyout-message'></div>");
            $dialog.append($("<div class='title'>{0}</h2>".format(title)));
            $dialog.append($("<div class='message'>{0}</div>".format(message)));
            $linkContainer = $("<div class='btn-group btn-group-justified'></div>");
            $.each(buttons, function () {
                $link = $("<a class='btn btn-primary' href='#'>{0}</a>".format(this.text));
                var currentButton = this;
                $link.click(function (e) {
                    e.preventDefault();
                    $dialog.remove();
                    result.resolve(currentButton);
                });
                $linkContainer.append($link);
            });
            $dialog.append($linkContainer);
            $("body").prepend($dialog);
        });
        return result;
    }
    
    deviceapi.events = {};

    deviceapi.events.bind = function (name, handler, domObj) {
        var objToBind = null;
        if (domObj) {
            objToBind = $(domObj);
        } else {
            objToBind = $(document);
        }

        objToBind.bind(name, handler);
    }
    deviceapi.events.unbind = function (name, handler, domObj) {
        var objToBind = null;

        if (domObj) {
            objToBind = $(domObj);
        } else {
            objToBind = $(document);
        }

        objToBind.unbind(name, handler);
    }
    

    deviceapi.state = {};

    deviceapi.state.hasInternet = function () { return true; };

    deviceapi.app = {};
    deviceapi.app = osManager.CurrentOS.app;
    
    var onDeviceReady = function () {
        deviceapi.device.DeviceID = device.uuid;
        deviceapi.device.DeviceName = device.name;
        deviceapi.device.DevicePlatform = device.platform;
        deviceapi.device.DeviceVersion = device.version;
        deviceapi.device.DeviceModel = device.model;

        deviceapi.useNativeCalendar = true;

        deviceapi.notification.Alert = function (title, message, buttonName) {
            navigator.notification.alert(message, function () { }, title, buttonName)
        }
        deviceapi.notification.Confirm = function (title, message, buttons) {
            if (!$.isArray(buttons))
                buttons = [buttons];

            var result = new $.DeferredEx(function (result) {

                var handler = function (index) {
                    result.resolve(buttons[index - 1]);
                }

                args = [message, handler, title];
                args.push($.map(buttons, function (a) { return a.text; }));

                navigator.notification.confirm.apply(navigator.notification.alert, args);
            });
            return result;
        }

        deviceapi.events.bind = function (name, handler, domObj) {
            var objToAddListener = null;

            if (domObj) {
                objToAddListener = domObj;
            } else {
                objToAddListener = document;
            }

            objToAddListener.addEventListener(name, handler, false);
        }

        deviceapi.events.unbind = function (name, handler, domObj) {
            var objToAddListener = null;

            if (domObj) {
                objToAddListener = domObj;
            } else {
                objToAddListener = document;
            }

            objToAddListener.removeEventListener(name, handler, false);
        }

        deviceapi.state.hasInternet = function () {
            return navigator.connection.type != Connection.NONE;
        };
    }

    document.addEventListener("deviceready", onDeviceReady, false);

    return deviceapi;
});