﻿define([
  'jquery',
  'underscore'
  
], function ($, _) {
    if (require) {
        require.asDeferred = function (map) {
            var result = new $.DeferredEx();

            var libs = [];
            var names = [];
            var resolved = [];

            for (var prop in map)
            {
                names.push(prop);
                libs.push(map[prop]);
                resolved.push(false);
            }

            var libsLen = libs.length;
         
            var loadedLibs = {};
            for (index = 0; index < libsLen; index++) {
                (function () {
                    var cindex = index;
                    require([libs[cindex]], function (arg) {
                        resolved[cindex] = true;

                        loadedLibs[names[cindex]] = arg;

                        if (_.every(resolved, function (a) { return a; })) {
                            result.resolve(loadedLibs);
                        }

                    }, function (error) {
                        result.rejectWithException(error);
                    });
                })();
            }

            return result;
        }
    }
})