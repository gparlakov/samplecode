define([
    'underscore',
    'knockout',
    'dateUtils'
], function (_, ko, dateUtils) {
    var utils = {
        getFullName: function (name) {
            //return "DAIS.eBank.Contracts." + name + ", DAIS.eBank.Contracts.Extended";
            return name;
        },

        copyProps: function (source, destination)
        {
            var commonProps = _.intersection(Object.getOwnPropertyNames(destination), Object.getOwnPropertyNames(source));

            _.each(commonProps, function (propName) {
                if (!_.contains(utils.systemProps, propName)) {
                    destination[propName] = source[propName];
                }
            });
        },

        systemProps: ["$type"],

        GetShortTypeNameByType: function (strType) {
            //if (strType != null) {
            //    var t = strType.split(',');
            //    if (t[0] != null) {
            //        var splittedType = t[0].split('.');
            //        resultType = splittedType[splittedType.length - 1];
            //    }
            //    return resultType;
            //}
            //return null;
            return strType;
        },

        GetShortTypeNameByObject: function (contractObject) {
            var unwrapedType = ko.utils.unwrapObservable(contractObject["$type"]);
            return this.GetShortTypeNameByType(unwrapedType);
        }
    }
    return utils;
});
