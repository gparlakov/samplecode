﻿define([
    //'config',
    //'Common/nomenclatures/enumOSType',
    'os!currentOs'

], function (currOs) {

    var OSManager = {
                
        CurrentOS : currOs,      
    }

    return OSManager;
});