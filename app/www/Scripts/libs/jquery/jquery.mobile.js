﻿define([
// Load the original jQuery source file
  'jqmConfig',
  'jqConfig',
  
  'libs/jquery/jquery.mobile-1.3.2.min',
], function () {
    // Tell Require.js that this module returns a reference to jQuery
	return jQuery;
});