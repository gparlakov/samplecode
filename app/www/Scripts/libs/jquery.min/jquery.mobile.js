﻿define([
// Load the original jQuery source file
  'jqmConfig',
  'jqConfig',
  
  'libs/jquery.min/jquery.mobile.custom'
  //,'libs/jquery.min/jquery.mobile-1.4.2'
], function () {
    // Tell Require.js that this module returns a reference to jQuery
	return jQuery;
});