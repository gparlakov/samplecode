﻿define([
    'jquery', 
    'daisJson'
    ],
function ($, daisJson) {

    $(function () {
        $.ajaxSetup({
            converters: { "text json": daisJson.daisJsonDeserialize }
        })
    });

});



