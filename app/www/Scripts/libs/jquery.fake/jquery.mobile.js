﻿define([
    'jquery'
], function ($) {
    
    $.mobile = {};
    $.mobile.currentPage = null
    $.mobile.changePage = function (el) {
        if ($.mobile.currentPage)
            $.mobile.currentPage.remove();
        $.mobile.currentPage = el;

    }

    $.mobile.dummy = true;

    return $;
});