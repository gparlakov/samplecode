﻿define([
  'knockout',
  'komapping',
  'underscore',
  'jquery',
  'utils',
    'servicesObjectsUtils',
    'text!Controls/Templates/NameAmountCCYLabel.html',
    'text!Controls/Templates/NavigationItem.html',
    'text!Controls/Templates/LabelValueList.html',
], function (ko, komapping, _, $, utils, objectUtils, nameAmounCCYLabelTmpl, navigationItemTmpl, labelValueListTmpl) {

    var initialise = function () {
        ko.components.register('name-amount-ccy-label', {
            viewModel: function (params) {

                this.actionButtons = [];
                this.additionalInfoTmpl = null;
                this.label = null;
                this.CCY = null;
                this.value = null;
                this.type = null;
                this.css = null;
                this.secondaryLabel = null;

                this.click = function (data, event) {
                    if (params.click && $.isFunction(params.click)) {
                        params.click.call(this, data, event);
                    }
                };

                objectUtils.copyProps(params, this);
            },
            template: nameAmounCCYLabelTmpl,
            synchronous: true

        });

        ko.components.register('navigation-item', {
            // params.href = null;
            // params.icon = null;
            // params.label = null;
            // params.subLabel = null;
            viewModel: function (params) {
                var result = params;
                if ($.isFunction(params))
                    result = params();
                
                return result;
            },

            template: navigationItemTmpl
        });

        ko.components.register('label-value-list', {
            viewModel: function (params) {
                if ($.isFunction(params))
                    return params();

                //this.heading = {};
                //this.details = [];
                //objectUtils.copyProps(params, this);

                return params;
            },
            template: labelValueListTmpl,
            //synchronous: true
        });
    }

    return {
        initialise: initialise
    };
});