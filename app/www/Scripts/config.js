﻿define([

], function () {
    var config = {
        //Dani URL 
        //ServicesBaseURL: "https://192.168.2.94:8201/Contracts/Mobile",

        //niki local ts
        //ServicesBaseURL: "https://192.168.2.84:8201/Contracts/Mobile",

        //local ts
        //ServicesBaseURL: "https://localhost:8201/Contracts/Mobile",

        //v-rbb2008 Ts   
        //ServicesBaseURL: "https://192.168.2.45:8211/Mobile",

        //wifi Ts 
        //ServicesBaseURL: "https://77.85.194.68/Mobile",

        //wifi niki Ts 
        //ServicesBaseURL: "http://77.85.194.68:8200/Mobile",

        //TestEnv Bank
        //ServicesBaseURL: "http://mobile.rbb.testenv/Mobile",

        //RealEnv Bank
        //ServicesBaseURL: "https://m.online.rbb.bg/Mobile",

        ServicesTimeout: 5 * 60 * 1000,

        DeviceOSType : "Android", 
        //DeviceOSType: 'iOS',

        Index: 'index.html',
        Debug: false,
        Warning: false 
    };
    
    if (typeof (Configuration) == "undefined") {
        Configuration = "DEBUG";
        //Configuration = "TESTLOCAL"; config.Debug = true;        
        //Configuration = "TESTVRBB2008";       
    }

    if (Configuration == "TESTLOCAL") {
        config.ServicesBaseURL = "http://77.85.194.68/Mobile";
    } else if (Configuration == "TESTVRBB2008") {
        config.ServicesBaseURL = "http://192.168.2.45:8210/Mobile";
    } else if (Configuration == "TESTBANK") {
        config.ServicesBaseURL = "http://10.242.2.155:8210/Mobile";
    } else if (Configuration == "RELEASE") {
        config.ServicesBaseURL = "https://m.online.rbb.bg/Staging/Mobile";
    } else {// Configuration == "DEBUG" on not set
        config.Debug = true;
        config.Warning = true;
        config.ServicesBaseURL = "http://localhost:8200/Contracts/Mobile";
        //config.ServicesBaseURL = "http://192.168.2.196:8200/Contracts/Mobile";

        /*GP moi promeni*/
        //config.ServicesBaseURL = "http://localhost:28102/Contracts/Mobile";
        //config.ServicesBaseURL = "http://77.85.194.68:28101/Contracts/Mobile"; //- da so4i kam gateway kym \\blagoevgrad        
        /*GP */
    }

    return config;
});