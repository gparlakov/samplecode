﻿// Filename: app.js
define([
  'jquery',
  'underscore',
  'Mobile/router', // Request router.js
  'utils',
  'config',
  'diagnostics',
  'deviceapi',
  'daisJson',
  'knockoutComponents'
], function ($, _, Router, utils, config, diagnostics, deviceapi, daisJson, koComponents) {
	var initialize = function () {
		// Pass in our Router module and call it's initialize function

	    var loading = false;

	    $("body").loadingpanel(
            {
                show: false,
                onshow: function () { loading = true; },
                onhide: function () { loading = false; }
            });

	    var hide = $("body").loadingpanel("show");

	    daisJson.initialize();
	    Router.initialize();
	    koComponents.initialise();

	    hide();

		window.onerror = function (e, url, line) {
		    diagnostics.Error({
                type: "onerror window handler",
                error: e,
                stack: e.stack || 'no stack',
		        url: url,
		        line: line
		    });
		    require(['deviceapi', 'i18n!Mobile/nls/strings'], function (deviceapi, strings) {
		        deviceapi.notification.Alert(strings.ID_STR_ERROR_TITLE, strings.ID_STR_ERROR_SYSTEM_ERROR, strings.ID_STR_OK);
		    });
		}

		var loggingException = false;
		$.DeferredEx.exception(function (e, recursive) {
		    if (!recursive) {
		        if (e.stack)
		            diagnostics.Debug(e.stack);

                $.whenEx(
		            diagnostics.Error({
		                type: "DeferredEx exception",
		                error: e,
		                stack: e ? e.stack || 'no stack' : 'no stack',
		                url: '',
		                line: ''
		            })
                )
		        require(['deviceapi', 'i18n!Mobile/nls/strings'], function (deviceapi, strings) {
		            deviceapi.notification.Alert(strings.ID_STR_ERROR_TITLE, strings.ID_STR_ERROR_SYSTEM_ERROR, strings.ID_STR_OK);
		        });
		    }
		    else {
		        diagnostics.Debug('Exception in exception logging!!!!!!!: ' + JSON.stringify(e ? e.stack || 'no stack' : 'no stack'));
		    }
		});

		deviceapi.events.bind("backbutton", function () {
		    if (!loading) {
		        utils.callBackOnCurrentViewModel();
		    }
		    else {
		        diagnostics.Debug("Ignore back button when loading..");
		    }
		});

		deviceapi.events.bind("resume", function () {
            require(["session", "services"], function (session, services) {
                if (session.GetSession()) {
                    services.SessionService.CheckSession({});
                    diagnostics.Debug("Checking session on device resume");
                }
                //ne me vrashtai na login-a ako nqma sesiq . 
                //Stranicite koito ne sled login shte se pracakat. Napr. za klonovete
                //else {
                //    diagnostics.Debug("Error while trying to check session");
                //    utils.navigate("Login");
                //}
		    })
		});

		deviceapi.events.bind("pause", function () {
		    
		});

		deviceapi.events.bind("native.keyboardshow", function (e) {
		    
		    setTimeout(function () {
		        var element = $(document.activeElement);
		        $('body').animate({ scrollTop: element.offset().top - 100 }, 400);
		    }, 500);

		    //$('footer').css('display', 'none');
		}, window);

		//deviceapi.events.bind("hidekeyboard", function () {
		//    $('footer').css('display', 'block');
		//});

	}
    
	return {
	    initialize: initialize
	};
});

