﻿define([
  'Mobile/ViewModels/NavigationViewModel',
  'text!Mobile/Templates/NavigationTemplate.html',

  'jquery',
  'services',
  'utils',
  'deviceapi',
  'mvc'
], function (NavigationViewModel, template, $, services, utils, deviceapi, mvc) {

    return mvc.Controller.extend({
        routes: {
            "Navigation": "index"
        },
		index: function () {

			var self = this;
            
			var viewModel = new NavigationViewModel();
			var loginView = new mvc.ViewResult(template, viewModel);

			return loginView;
		}
	});
});
