define([
    'mvc',
    'services',
    'Mobile/ViewModels/Messages/MessagesViewModel',
    'text!Mobile/Templates/Messages/MessagesTemplate.html',
    'Mobile/ViewModels/Messages/MessageViewModel',
    'text!Mobile/Templates/Messages/MessageTemplate.html',
], function (mvc, services, MessagesViewModel, template, MessageViewModel, messageTemplate) {

    return mvc.Controller.extend({
        routes: {
            "Messages/index": "index",
            "Messages/showMessage/:messageID": "showMessage",
            
        },

        index: function () {
            var filter = new services.Objects.MessagesToUserFilter({ Valid: true });
            var messagesViewModel = new MessagesViewModel(filter);

            var messagesView = new mvc.ViewResult(template, messagesViewModel);

            return messagesView;
        },

        showMessage: function (messageid) {
            var messageToUserKey = new services.Objects.MessageToUserKey({ ID: messageid});
            var messageViewModel = new MessageViewModel(messageToUserKey);

            var viewResult = new mvc.ViewResult(messageTemplate, messageViewModel);

            return viewResult;
            //return viewResult;
        }

    });

});
