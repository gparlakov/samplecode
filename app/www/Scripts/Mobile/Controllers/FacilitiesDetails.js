define([
  'Mobile/ViewModels/Loans/FacilitiesDetailsViewModel',
  'text!Mobile/Templates/Loans/FacilitiesDetailsTemplate.html',

  'Mobile/ViewModels/Loans/FacilityOverviewViewModel',
  'text!Mobile/Templates/Loans/FacilityOverviewTemplate.html',
  'Mobile/ViewModels/Loans/FacilitiesFeesViewModel',
  'text!Mobile/Templates/Loans/FacilityFees/FacilityFees.html',
    
  'productscache',
  'utils',
  'mvc',
  'underscore',
  'events',
  'text!Mobile/Templates/Loans/LoanDetailsTemplate.html'

], function (FacilityViewModel, template, FacilityOverviewModel, facilityOverviewTemplte,
        FacilitiesFeesViewModel, facilityFeesTemplate,
        productscache, utils, mvc, _, events, loansTemplate) {

    return mvc.Controller.extend({
        routes: {
            "FacilitiesDetails/index/:bankClient/:facilityType/:facilitySequence": "index"
        },

        index: function (bankClient, facilityType, facilitySequence) {

            var apFacility = this._getFacilityFromProducts(bankClient, facilityType, facilitySequence);
            var facilityOverviewViewModel = new FacilityOverviewModel(apFacility);

            events.bind(facilityOverviewViewModel, "rendered", function (el, data) {
                var self = this;
                var res = $.DeferredEx(function (res) {

                    $(el.element).find('#submenu').submenu({
                        submenuItems: self.Submenu,
                        initialized: function () {
                            res.resolve();
                        }
                    });

                });
                return res;
            });
            return new mvc.ViewResult(facilityOverviewTemplte, facilityOverviewViewModel);
        },

        facilityDetails: function (bankClient, facilityType, facilitySequence) {

            

            var apFacility = this._getFacilityFromProducts(bankClient, facilityType, facilitySequence);

            var facilityViewModel = new FacilityViewModel(apFacility);

            var facilityView = new mvc.ViewResult(template, function (dataAvailableHandler) {
                dataAvailableHandler(facilityViewModel);
            });

            return facilityView;
        },

        _getFacilityFromProducts: function (bankClient, facilityType, facilitySequence) {

            var ap = productscache.GetAPerson();

            if (ap.ExtendedProperties && ap.ExtendedProperties.APFacilities) {
                var facilities = ap.ExtendedProperties.APFacilities;
                var apFacility = _.find(facilities,
                                        function (apFac) {
                                            return apFac.BankFacility.BankClient.BankClientID == bankClient &&
                                                   apFac.BankFacility.FacilityType.ID == facilityType &&
                                                   apFac.BankFacility.FacilitySequence == facilitySequence;
                                        });
                return apFacility;
            }
            return null;
        },

        facilityFees : function (bankClient, facilityType, facilitySequence) {
            
            var apFacility = this._getFacilityFromProducts(bankClient, facilityType, facilitySequence);

            var feesViewModel = new FacilitiesFeesViewModel(apFacility.BankFacility.BankFacilityFees);

            var facilityView = new mvc.ViewResult(facilityFeesTemplate, function (dataAvailableHandler) {
                dataAvailableHandler(feesViewModel);
            });

            return facilityView;
        }

    });

});
