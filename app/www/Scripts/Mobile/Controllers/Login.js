﻿define([
  'Mobile/ViewModels/LoginViewModel',
  'text!Mobile/Templates/LoginTemplate.html',

  'jquery',
  'services',
  'utils',
  'deviceapi',
  'mvc',
  'session',
  'i18n!Mobile/nls/strings'
], function (LoginViewModel, template, $, services, utils, deviceapi, mvc, session, strings) {

    return mvc.Controller.extend({
        routes: {
            "Login": "index",
            "Login/index": "index",
            "Login/logout": "logout"
        },
		index: function () {

			var self = this;
            
			var loginViewModel = new LoginViewModel();
			loginViewModel.strings = strings;

			var loginView = new mvc.ViewResult(template, loginViewModel);

			return loginView;
		},
		logout: function () {
		    var res = $.DeferredEx();

		    $.whenEx(function() {
                if (session.HasSession()) {
                    services.SessionService.Logout()
		        }
		    }()).done(function () {
                deviceapi.app.terminate();
            }).always(function () {
                res.reject();
            });

            return res;
		}
	});
});
