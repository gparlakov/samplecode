define([
    'mvc',
    'Mobile/ViewModels/Profile/ProfileViewModel',
    'text!Mobile/Templates/Profile/ProfileTemplate.html',
    
], function (mvc, ProfileViewModel, ProfileTemplate) {

    return mvc.Controller.extend({
        routes: {
            "Profile": "index",
            "Profile/index": "index"
        },

        index: function () {
            var viewModel = new ProfileViewModel();

            var profileView = new mvc.ViewResult(ProfileTemplate, viewModel);

            return profileView;
        }
    });

});
