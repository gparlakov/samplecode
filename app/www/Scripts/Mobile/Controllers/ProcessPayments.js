﻿define([
  'jquery',
  'utils',
  'mvc',
  'services',

  'Mobile/ViewModels/Payments/ProcessPaymentSignViewModel',
  'text!Mobile/Templates/Payments/ProcessPaymentSignView.html',

  'Mobile/ViewModels/Payments/ProcessPaymentDeleteViewModel',
  'text!Mobile/Templates/Payments/ProcessPaymentDeleteView.html'

], function (
    $, utils, mvc, services,
    ProcessPaymentSignViewModel, ProcessPaymentSignView,
    ProcessPaymentDeleteViewModel, ProcessPaymentDeleteView) {


    AuthorizationDataPayments = function () {
        this.$type = services.Objects.Utils.GetShortTypeNameByType('AuthorizationDataPayments');
        this.Payments = []; //list of SignablePayment
    }


    return mvc.Controller.extend({
        routes: {
            "ProcessPayments/index": "index",
            "ProcessPayments/sign/:paymentID": "sign",
            "ProcessPayments/delete/:paymentID": "delete"
        },

        "index": function () {
            return this.sign();
        },

        "sign": function (paymentID) {
            var self = this;

            var paymentsKeys = [
                new services.Objects.PaymentKey({ ID: paymentID, PaymentIncarnationType: services.Objects.EnumPaymentIncarnationType.PaymentOrder })
            ];

            var viewModel = new ProcessPaymentSignViewModel(paymentsKeys);

            return new mvc.ViewResult(
                        ProcessPaymentSignView,
                        function (dataAvailableHandler) {

                            viewModel.Init(function () {
                                dataAvailableHandler(viewModel);
                            });

                        },
                        function (el, data) {

                        }
                );

        },

        "delete": function (paymentID) {
            var self = this;

            var paymentsKeys = [
                new services.Objects.PaymentKey({ ID: paymentID, PaymentIncarnationType: services.Objects.EnumPaymentIncarnationType.PaymentOrder })
            ];

            var viewModel = new ProcessPaymentDeleteViewModel(paymentsKeys);

            return new mvc.ViewResult(
                        ProcessPaymentDeleteView,
                        function (dataAvailableHandler) {

                            viewModel.Init(function () {
                                dataAvailableHandler(viewModel);
                            });

                        },
                        function (el, data) {

                        }
                );

        }
    });

});
