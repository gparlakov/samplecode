﻿define(
    function () {
        var SubmenuItem = function (label, controller, action, paramsData) {

            var item = this;
            item.Label = label;
            
            item.Controller = controller;
            item.Action = action;
            item.Params = paramsData;

            return item;
        }
        return SubmenuItem;
    });