﻿define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'knockoutDais',
  'services',
  'utils',
  'Mobile/ViewModels/Payments/ViewPaymentsBaseViewModel',
  'text!Mobile/Templates/Payments/ArchivePaymentAdditionalInfoTemplate.html',
  'Mobile/ViewModels/Repository/PaymentActionButtons'

], function ($, _, backbone, ko, komapping, knockoutDais, services, utils, baseViewModel, ArchivePaymentAdditionalInfoTemplate, actionButtons) {

    function ViewPaymentsViewModel() {

        baseViewModel.call(this);

        var self = this;
      
        this.PaymentsOrdersGetFromServiceMethod = self.paymentOrderService.GetPaymentOrdersArchive;
        this.GetPaymentsFromResult = function (data) {
            return data.ArchivedPayments;
        }

        var _wrapPayment = this._wrapPayment;
        this._wrapPayment = function (p) {
            p = _wrapPayment.call(this, p);
            p.actionButtons = [];
            p.actionButtons.push(new actionButtons.viewPayment(function (data, e) {
                self.viewPayment(p, e);
            }));
            p.actionButtons.push(new actionButtons.copyPayment(p));

            return p;
        }

        this.copyPayment = function (payment, e) {
            e.stopPropagation();

            var link = utils.buildLink('PaymentWizard', 'cereteLikePayment', [payment.PaymentIncarnationType, payment.PaymentIncarnationType, payment.ID]);
            utils.navigate(link, { trigger: true });
            return false;
        }
    };

    return ViewPaymentsViewModel;
});