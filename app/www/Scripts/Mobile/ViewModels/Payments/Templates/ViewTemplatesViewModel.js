﻿define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'services',
  'servicesObjects',
  'utils',
  'events',
  'Mobile/ViewModels/Common/HeaderViewModel',
  'Common/ufnomenclatures/UFNmFactory'

], function ($, _, backbone, ko, komapping, services, servicesObjects, utils, events, headerViewModel, nmFactory) {

    var ViewPaymentsViewModel = function () {

        var self = this;
        self.HeaderViewModel = new headerViewModel({ titleStringID: "ID_STR_TEMPLATES" });

        var filter = new servicesObjects.TemplatesFilter({ Paging: new servicesObjects.Paging() });
        this.Filter = komapping.fromJS (filter);

        this.Templates = ko.observableArray([]);

        
        this.find = function () {
            self.Filter.Paging.CurrentPage(1);
            var promise =
                this._findInternal(true)
                .done(function () {
                    events.trigger(self, "findresulsfinished");
                });

            return promise;
        }

        this.PaymentTypes = null;

        this.initialize = function () {
            var factory = new nmFactory();

            events.one(self, 'rendered', function () {
                $.whenEx(
                    factory.GetUfNomenclature(factory.ufNomenclatureTypes.NomenclaturePaymentTypes)
                )
                .pipe(function (nomenclature) {
                    self.PaymentTypes = nomenclature.GetNomenclature();
                    self.find();
                })
                .done(function () {
                    $(window).daisScrollV2('register', self.findMore);
                    events.bind(self, "findresulsfinished", function () {
                        $(window).daisScrollV2('reset', self.findMore);
                    })
                })
            });
        }
        this.findresulsfinished = function()
        {
            
        }

        this.unload = function () {
            $(window).daisScrollV2('unregister', self.findMore);
        }

        this.findMore = function () {
            var def = new $.DeferredEx(function (def) {
                if (self.Filter.Paging.TotalPages() > self.Filter.Paging.CurrentPage()) {
                    self.Filter.Paging.CurrentPage(self.Filter.Paging.CurrentPage() + 1);
                    def.bindTo(self._findInternal(false));
                }
                else {
                    def.fail();
                }
            });
            return def;
        }

        this.pipe = $.DeferredEx();
        this.pipe.resolve();

        this._findInternal = function (shouldClearOldResults) {
            var res = $.DeferredEx(function (res) { 

                var action = function () {
                    services.TemplateService.GetTemplates(komapping.toJS(self.Filter), function (data) {

                        if (shouldClearOldResults) {
                            self.Templates.removeAll();
                        }

                        for (var idx in data.Templates) {
                            var tmpl = data.Templates[idx];
                            tmpl.PaymentType = _.find(self.PaymentTypes, function (type) {
                                return type.PaymentType == tmpl.PaymentType.PaymentType
                                    && type.PaymentSubType == tmpl.PaymentType.PaymentSubType;
                            });
                            self.Templates.push(tmpl);
                        }

                        komapping.fromJS(data.Filter, self.Filter);

                    })
                    .done(function () {
                        res.resolve();
                    });
                }

                self.pipe.pipe(action);

            });
            

            return res;
        }

        this.createFromTemplate = function (template) {
            var link = utils.buildLink('PaymentWizard', 'cereteLikePayment', [services.Objects.EnumPaymentIncarnationType.PaymentOrder, template.PaymentIncarnationType, template.ID]);
            utils.navigate(link, { trigger: true });
        }
    };

    return ViewPaymentsViewModel;
});