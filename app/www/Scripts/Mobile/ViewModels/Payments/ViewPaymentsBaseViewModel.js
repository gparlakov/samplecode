﻿define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'knockoutDais',
  'services',
  'utils',
  'events',
  'validator',
  'Common/ufnomenclatures/UFNmFactory',

  'Mobile/ViewModels/Common/AccountsHelperPopUp',
  'productscache',
  'Mobile/ViewModels/Common/PaymentHelperPopUp',
  'Mobile/ViewModels/Common/AdvancedFilter'

], function ($, _, backbone, ko, komapping, knockoutDais, services, utils, events, validator, nmFactory, AccountsHelperPopUp, productscache, PaymentHelperPopUp, AdvancedFilter) { //, strings

    var filterInternal = function () {

        var filter = new services.Objects.PaymentOrdersFilter();
        // mega tapnqta a . 
        var tmpDate = filter.CreatePeriod.DateStart;
        filter.CreatePeriod.DateStart = filter.CreatePeriod.DateEnd;
        filter.CreatePeriod.DateEnd = tmpDate;

        return filter;
    }

    //var staticData = {
    //    filter: filterInternal()
    //}

    function ViewPaymentsBaseViewModel() {

        var self = this;

        //veche ne e staticData, no nqma da q renamevam za sega.
        //tai kato tozi e bazov za pending i arhiv, to staticData-tata predi beshe obshta za 2 ta viewModela
        //v rezultat filtrite ot arhiva se kopiraha ponqkoga vav filtrite za chakashti ... 
        var staticData = {
            filter: filterInternal()
        }

        validator.validatableModel.call(this, function () {
            return {
                properties: {
                    "Filter": {
                        properties: {
                            "CreatePeriod": {
                                properties: {
                                    "DateStart": {
                                        rules: {
                                            NotNullOrEmpty: true
                                        }
                                    },
                                    "DateEnd": {
                                        rules: {
                                            NotNullOrEmpty: true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });

        AdvancedFilter.call(this, staticData.filter);
        this.Filter = komapping.fromJS(staticData.filter);
        this.Payments = ko.observableArray([]);
        
        this.GetNonobservableFilter = function () {
            var tmpFilter = komapping.toJS(self.Filter);

            //blqqqhhh tova ne e mnogo za tuk ... ama e udobno ;) 
            //hmm zashto li ne e computed ? 
            if (self.selectedAccount()) {
                tmpFilter.BankAccount = { BankAccountID: self.selectedAccount().BankAccountID };
            } else {
                tmpFilter.BankAccount = null;
            }

            if (self.selectedPaymentType()) {
                tmpFilter.PaymentOrderType = self.selectedPaymentType().PaymentType;
            } else {
                //ah che tapo. ako sme ibzrali tip e ok, vinagi se promenq.
                //ako sme izbrali tip, tarsili sme, posle sme izbrali vsichki tipove - vlizame tuk. Nqma koi da ni resetne tipa na vsicki
                tmpFilter.PaymentOrderType = null;
            }
            return tmpFilter;
        }
        this.isLoaded = ko.observable(false);
        this.find = function () {
            if (validator.getValidator(self).validate()) {

                self.Filter.Paging.CurrentPage(1);
                var promise =
                    this._findInternal(true)
                    .done(function () {
                        events.trigger(self, "findresulsfinished");
                        self.isLoaded(true);
                    });

                return promise;
            }

        }

        this.findMore = function () {

            var def = new $.DeferredEx(function (def) {
                var totalPages = self.Filter.Paging.TotalPages();
                var currentPage = self.Filter.Paging.CurrentPage();

                if (validator.getValidator(self).validate()) {
                    if ((!totalPages && totalPages != 0) || (totalPages > currentPage)) {
                        self.Filter.Paging.CurrentPage(self.Filter.Paging.CurrentPage() + 1);
                        def.bindTo(self._findInternal(false));
                    }
                    else {
                        def.fail();
                    }
                }
            });
            return def.promise();
        }

        this.loading = false;
        this.paymentOrderService = new services.Services.PaymentOrderService();
        this.paymentOrderService.Behaiviours.Add(new services.Services.Behaviours.Types.LoadingIndicator(function (a) { self.loading = a; }));

        this.pipe = $.DeferredEx();
        this.pipe.resolve();

        this._findInternal = function (shouldClearOldResults) {

            function action() {
                var filter = self.GetNonobservableFilter();
                var res = $.whenEx(
                    self.PaymentsOrdersGetFromServiceMethod.call(self, filter)
                   // self.paymentOrderService.GetPaymentOrdersPending(filter)
                ).then(function (data) {

                    if (shouldClearOldResults) {
                        self.Payments.removeAll();
                    }

                    var payments = self.GetPaymentsFromResult(data);

                    for (var idx in payments) {
                        self.Payments.push(self._wrapPayment(payments[idx]));
                    }

                    komapping.fromJS(data.Filter, self.Filter);

                });
                return res;
            }

            self.pipe = self.pipe.pipe(action);

            return self.pipe;

        }

        this._paymentTypes = null;
        this.paymentTypesForFilterSelect = null;
        this.selectedPaymentType = ko.observable();

        this.initialize = function () {
            var factory = new nmFactory();
            var result =

                $.whenEx(
                    factory.GetUfNomenclature(factory.ufNomenclatureTypes.NomenclaturePaymentTypes)
                )
                .pipe(function (nomenclature) {
                    self._paymentTypes = nomenclature.GetNomenclature();

                    self.paymentTypesForFilterSelect = _.filter(self._paymentTypes, function (type) {
                        return type.PaymentSubType == 0 && type.IsActive == true;
                    }).map(function (type) {
                        type.DescriptionLocalized = $.GetLocalizedString(type.Description);
                        return type;
                    });
                    self.PaymentHelperPopUp = new PaymentHelperPopUp(
                         self,
                         {
                             paymentTypes: self._paymentTypes
                         }
                     );
                })

            return result;
        }

        this._wrapPayment = function (payment) {
            payment.Incarnation.PaymentType = _.find(
                                        self._paymentTypes,
                                        function (type) {
                                            return type.PaymentType == payment.Incarnation.PaymentType.PaymentType &&
                                                type.PaymentSubType == payment.Incarnation.PaymentType.PaymentSubType;
                                        }
                                    );
            return payment;
        }

        //this.load = function () {
        //    self.find();
        //}

        this.unload = function () {
            $(utils.getCurrentPageContainer()).daisScrollV2('unregister', self.findMore);
            _.extend(staticData.filter.CreatePeriod, komapping.toJS(self.Filter.CreatePeriod));
        }


        this._getAPAccounts = function () {
            return productscache.GetAPerson().APAccounts;
        }
        
        
        this.selectedAccount = ko.observable(null);

        this.AccountsHelperPopUp = new AccountsHelperPopUp(
                                    self,
                                    {
                                        load: function() { 
                                            var res = 
                                                services.BankProductInfoService.GetProductBalances(
                                                    new services.Objects.BankAccountBalancesFilterCurrent(
                                                        { Paging: { CurrentPage: -1, ResultsForPage: -1 } })
                                                ).then(function() {
                                                    return productscache.GetAPerson().APAccounts;
                                                });

                                            return res;
                                        },
                                        showOptionAll: true
                                    }
                        );


        this.GetAPAccountsHandler = function () {
            self.AccountsHelperPopUp
                .Open()
                .done(function (account) {
                    if (account && account.BankAccount) {
                        self.selectedAccount(account.BankAccount);
                    } else {
                        self.selectedAccount(null);
                    }
                    
                });
        }

        this.viewPayment = function (payment, e) {
            e.stopPropagation();

            
            self.PaymentHelperPopUp.setPaymentKey(new services.Objects.PaymentKey({ PaymentIncarnationType: payment.PaymentIncarnationType, ID: payment.ID }));
            self.PaymentHelperPopUp.Open();

            //var link = utils.buildLink('Payments', 'view', [payment.PaymentIncarnationType, payment.ID]);
            //utils.navigate(link, { trigger: true });
            
            return false;
        }

        this.AfterRender = function () {

        }
        this.rendered = function () {
            $(window).daisScrollV2('register', self.findMore);
            events.bind(self, "findresulsfinished", function () {
                $(window).daisScrollV2('reset', self.findMore);
            })
        };
    }

    return ViewPaymentsBaseViewModel;
});