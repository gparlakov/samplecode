﻿define([
  'jquery',
  'underscore',
  'knockout',
  'services',
  'utils',
  'productscache',
  'typeVisitor',
  'diagnostics',
   'Mobile/ViewModels/Base/PopupPageViewModel',


], function (
    $, _, ko, services, utils, productscache, typeVisitor, diagnostics, PopupPageViewModel
    ) {
    
    var BeneficiariesHelperPopUp = function BeneficiariesHelperPopUp(parent, wizardState) {

        var self = this;

        PopupPageViewModel.apply(this, arguments);

        this.path = "Payments/Wizard/Beneficiaries/BeneficiariesHelperPopTemplate";

        this.Beneficiaries = ko.observable([]); 
        this.PayeeAPAccounts = ko.observable([]);

        this.PayeeAPAsBeneficiariesCommon = ko.observable([]);
        
        this.SelectItem = function (item)
        {
            self._return(item);
        }

        this.loadData = function () {

            //var pAPAsFilter = new services.Objects.PaymentWizardChoosePayeeDataAPAccountsFilter({
            //                                                State: wizardState,
            //                                                SameBankClientAsPayer: false
            //                                            });


            var benefFilter = new services.Objects.PaymentWizardChoosePayeeDataBeneficiaryAccountsFilter({
                                                            State: wizardState
                                                        });

            var progress = 
                $.whenEx(
                        //services.PaymentWizardService.PaymentWizardGetData(pAPAsFilter),
                        services.PaymentWizardService.PaymentWizardGetData(benefFilter)
                    )
                .done(function (benef) {

                    //var timerDestory = diagnostics.Time('BeneficiariesHelperPopUp list destroy');
                    //timerDestory();

                    //productscache.FillBankClientsForAPAccounts(apa.PaymentWizardDetachedStepData.Accounts);

                    var timerSet = diagnostics.Time('BeneficiariesHelperPopUp setting value to observable');

                    var tmpCombined = [];
                    tmpCombined = tmpCombined.concat(benef.PaymentWizardDetachedStepData.Accounts); //.concat(apa.PaymentWizardDetachedStepData.Accounts)

                    // tova ne obedinqva apa-tata po bankov client

                    tmpCombined =_.sortBy(tmpCombined, function (element) {
                        return typeVisitor.visit(element, {
                            //APAccount: function (apa) { return apa.BankAccount.BankClient.NAME; },
                            Beneficiary: function (benef) { return benef.BeneficiaryName.toUpperCase(); }
                        });
                    });

                    self.PayeeAPAsBeneficiariesCommon(tmpCombined);
                    timerSet();


                    var timerRefresh = diagnostics.Time('BeneficiariesHelperPopUp refresh list view');
                    

                    timerRefresh();
                });

            return progress;
        }
    }

    return BeneficiariesHelperPopUp;

});