﻿define([
  'jquery',
  'underscore',
  'knockout',
  'komapping',
  'services',
  'validator',
  'events',
  'typeVisitor',

  'Mobile/ViewModels/Payments/PaymentWizardBaseViewModel',

  'Mobile/ViewModels/Payments/PaymentDocsDetails/DocCPDetailsViewModel',
  'Mobile/ViewModels/Payments/PaymentDocsDetails/DocCPTransferDetailsViewModel',
  'Mobile/ViewModels/Payments/PaymentDocsDetails/DocFCCYFullDetailsViewModel',
  'Mobile/ViewModels/Payments/PaymentDocsDetails/DocFCCYSimpleDetailsViewModel',
  'Mobile/ViewModels/Payments/PaymentDocsDetails/DocFXDetailsViewModel',
  'Mobile/ViewModels/Payments/PaymentDocsDetails/DocPBDetailsViewModel',

  'Mobile/ViewModels/Payments/PaymentDocsDetails/PaymentOrderViewModel',

], function (
        $, _, ko, komapping, services, validator, events, typeVisitor,

        PaymentWizardBaseViewModel,

        DocCPDetailsViewModel, DocCPTransferDetailsViewModel, DocFCCYFullDetailsViewModel, DocFCCYSimpleDetailsViewModel,
        DocFXDetailsViewModel, DocPBDetailsViewModel,

        PaymentOrderViewModel
    ) {

    var DocumentMappings = {
        PaymentWizardDetailsCP: function (wizardModel) {
            return new DocCPDetailsViewModel(wizardModel.State.Details, wizardModel);
        },
        PaymentWizardDetailsCPTransfer: function (wizardModel) {
            return new DocCPTransferDetailsViewModel(wizardModel.State.Details, wizardModel);
        },
        PaymentWizardDetailsFX: function (wizardModel) {
            return new DocFXDetailsViewModel(wizardModel.State.Details, wizardModel);
        },
        PaymentWizardDetailsFCCYSimple: function (wizardModel) {
            return new DocFCCYSimpleDetailsViewModel(wizardModel.State.Details, wizardModel);
        },
        PaymentWizardDetailsFCCYFull: function (wizardModel) {
            return new DocFCCYFullDetailsViewModel(wizardModel.State.Details, wizardModel);
        },
        PaymentWizardDetailsPB: function (wizardModel) {
            return new DocPBDetailsViewModel(wizardModel.State.Details, wizardModel);
        }
    };

    function PaymentWizardFillDetailsViewModel(model) {
        var self = this;

        validator.validatableModel.call(this, function () {
            return {
                properties: {
                    "IncarnationViewModel": {
                        rules: {
                            SubModel: true
                        }
                    },
                    "DetailsViewModel": {
                        rules: {
                            SubModel: true
                        }
                    }
                }
            }
        });

        PaymentWizardBaseViewModel.call(this, model);

        this.GetRequestModelForNext = function () {

            return new services.Objects.PaymentWizardFillDetailsInput({
                State: self.Model.State,
                Details: self.DetailsViewModel().GetModel(),
                Incarnation: self.IncarnationViewModel().GetModel()
            });
        }
        this.GetRequestModelForBack = function () {
            return new services.Objects.PaymentWizardFillDetails({
                State: self.Model.State
            });
        }

        this.DetailsViewModel = ko.observable();
        this.IncarnationViewModel = ko.observable();

        this.initialize = function () {
            var detailsViewModel = typeVisitor.visit(self.Model, DocumentMappings, function (wizardModel) { return wizardModel.State.Details; });
            var incarnationViewModel = new PaymentOrderViewModel(self.Model.State.Incarnation);

            var result =
                $.whenEx(
                    events.trigger(detailsViewModel, "initialize"),
                    events.trigger(incarnationViewModel, "initialize")
                )
            .done(function () {
                self.DetailsViewModel(detailsViewModel);
                self.IncarnationViewModel(incarnationViewModel);
            });

            return result;
        }
        this.Init = function (initCompletedCallback) {
            $.whenEx(
                this.initialize()
            )
            .done(function () {
                initCompletedCallback.call(this);
            });
        }
    };

    return PaymentWizardFillDetailsViewModel;
});