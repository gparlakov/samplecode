define([
  'underscore',
  'knockout',
  'komapping',
  'services',
  'utils',
  'backbone',
  'Mobile/ViewModels/Repository/SubMenu/PaymentsSubmenu',
  'Mobile/ViewModels/Common/HeaderViewModel',

], function (_, ko, komapping, services, utils, backbone, paymentsSubmenu, headerViewModel) {

    
    var PaymentOverviewViewModel = function PaymentOverviewViewModel(model) {
        this.HeaderViewModel = new headerViewModel({ titleStringID: 'ID_STR_TRANSFERS' });
        this.Submenu = new paymentsSubmenu();

        }

    return PaymentOverviewViewModel;
});