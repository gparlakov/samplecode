﻿/// <reference path="../../../libs/require.intellisense.js" />

define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'services',
  'utils',
  'validator',
  'productscache',
   'typeVisitor',
  'Mobile/ViewModels/Payments/PaymentWizardBaseViewModel'

], function ($, _, bakcbone, ko, komapping, services, utils, validator, productscache, typeVisitor, PaymentWizardBaseViewModel) {

    function PreviewAPSubmodel(parent, model) {
        var self = this;
        this.controller = 'AccountOverview';
        this.action = 'AccountsList';
        var apa = productscache.GetAPAccountByID(parent.Model.State.Payer.BankAccount.BankAccountID);
        var options = {
            load: parent._getAPAccounts,
            SelectedAccount: model.BankAccount,
            FirstBankClient: apa.BankAccount.BankClient,
            GroupByBankClient: true
        };
        this.params = [parent, options];
        this.viewModel = null;
        this.viewModelAvailable = function (viewModel) {
            self.viewModel = viewModel;
        }
        this.GetPayeeModel = function () {
            return new services.Objects.PaymentWizardPayeeBankAccount({
                BankAccount: komapping.toJS(self.viewModel.SelectedAccount),
            });
        }
    };

    function PreviewAndEditBeneficiarySubModel(parent, model) {
        var self = this;
        this.controller = 'Beneficiary';
        this.action = 'edit';
        var options = {
            WizardState: parent.Model.State,
            SelectedBeneficiary: model
        };
        this.params = [parent, options];
        this.viewModel = null;
        this.viewModelAvailable = function (viewModel) {
            self.viewModel = viewModel;
        }
        this.GetPayeeModel = function () {
            return new services.Objects.PaymentWizardPayeeNewBeneficiary({
                Beneficiary: komapping.toJS(self.viewModel.BeneficiaryInput),
                Account: komapping.toJS(self.viewModel.BeneficiaryAccountInput),
            });
        }
    };

    function PaymentWizardChoosePayeeViewModel(model) {

        validator.validatableModel.call(this,
            {
                properties: {
                    "SubModel": {
                        rules: {
                            NotNull: true
                        },
                        messages: {
                            NotNull: "TMPL_VAL_SPC_CHOOSE_BENEFICIARY"
                        }
                    }
                }
            }
        );

        var self = this;

        PaymentWizardBaseViewModel.call(this, model);

        this.validate = function () {
            var res = validator.getValidator(this).validate();
            var submodel = self.SubModel();
            if (submodel != null && validator.isValidatable(submodel.viewModel))
                res = res & validator.getValidator(submodel.viewModel).validate();
            return res;
        }

        this.PayeeToSubmit = null;
        this.PayeeForPreview = ko.observable();

        //overrides
        this.GetRequestModelForNext = function () {

            return new services.Objects.PaymentWizardChoosePayeeInput({
                State: self.Model.State,
                Payee: self.SubModel().GetPayeeModel()
            });
        }
        this.GetRequestModelForBack = function () {
            return new services.Objects.PaymentWizardChoosePayee({
                State: self.Model.State
            });
        }
        //end of overrides

        this._getAPAccounts = function () {
            var stepDataFilter = new services.Objects.PaymentWizardChoosePayeeDataAPAccountsFilter({
                State: self.Model.State,
                //SameBankClientAsPayer: true
            });

            var result = new $.DeferredEx(function (result) {

                services.PaymentWizardService
                    .PaymentWizardGetData(stepDataFilter)
                    .done(function (data) {
                        result.resolve(data.PaymentWizardDetachedStepData.Accounts);
                    })
                    .fail(function () {
                        result.fail();
                    });
            });

            return result.promise();
        }

        this.Init = function (callback) {
            callback.call(this, this);
        }

        var newBeneficiaryTemplate = new services.Objects.PaymentWizardPayeeNewBeneficiary({
            Account: new services.Objects.BeneficiaryAccountHomeCountryInput(),
            Beneficiary: new services.Objects.BeneficiaryInput({ VisibleForAll: 1 })
        });
        var noAccountTemplate = new services.Objects.PaymentWizardPayeeBankAccount({ BankAccount: null });

        self.APAccountsSubModel = ko.observable();
        self.BeneficiariesSubModel = ko.observable();
        self.APAccountsSubModelVisible = ko.observable();
        self.BeneficiariesSubModelVisible = ko.observable();

        this.OpenAccountsTab = function (account) {
            self.BeneficiariesSubModelVisible(false);
            self.APAccountsSubModel(new PreviewAPSubmodel(self, account));
            self.APAccountsSubModelVisible(true);
        }
        this.OpenBeneficiariesTab = function (benef) {
            self.APAccountsSubModelVisible(false);
            self.BeneficiariesSubModel(new PreviewAndEditBeneficiarySubModel(self, benef));
            self.BeneficiariesSubModelVisible(true);
        }

        //initial setting of selected payee account
        if (self.Model.State.Payee) {
            typeVisitor.visit(self.Model.State.Payee, {
                PaymentWizardPayeeBankAccount: function (account) {
                    self.OpenAccountsTab(account);
                },
                PaymentWizardPayeeNewBeneficiary: function (benef) {
                    self.OpenBeneficiariesTab(benef);
                }
            });
        } else {
            self.OpenAccountsTab(noAccountTemplate);
        }

        this.SubModel = ko.computed(function () {
            if (self.APAccountsSubModelVisible()) {
                return self.APAccountsSubModel();
            } else {
                return self.BeneficiariesSubModel();
            }
        });

        this.ShowAPAccounts = function () {
            if (self.APAccountsSubModel()) {
                self.BeneficiariesSubModelVisible(false);
                //clear selected account
                self.APAccountsSubModel().viewModel.SelectedAccount(null);
                self.APAccountsSubModelVisible(true);
            } else {
                self.OpenAccountsTab(noAccountTemplate);
            }
        }

        this.ShowBeneficiaries = function () {
            if (self.BeneficiariesSubModel()) {
                self.APAccountsSubModelVisible(false);
                //clear selected beneficiary
                self.BeneficiariesSubModel().viewModel.Init();
                self.BeneficiariesSubModelVisible(true);
            } else {
                self.OpenBeneficiariesTab(newBeneficiaryTemplate);
            }
        }
    };

    return PaymentWizardChoosePayeeViewModel;
});