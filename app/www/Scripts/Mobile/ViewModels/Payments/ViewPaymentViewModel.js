﻿define([
  'jquery',
  'knockout',
  'underscore',
  'services',
  'utils',
  'Common/ufnomenclatures/UFNmFactory',
  'Mobile/ViewModels/Payments/PaymentActionsViewModel'
  //'i18n!Mobile/nls/strings'

], function ($, ko, _, services, utils, nmFactory, paymentActionsViewModel) {

    function ViewPaymentViewModel(paymentKey, paymentTypes) {

        paymentActionsViewModel.call(this);
        var self = this;

        self._paymentTypes = paymentTypes;
        this._paymentKey = paymentKey;

        this.Payment = null;

        this.initialize = function () {
        

            return result;

        }

        //this.Init = function (callback) {
        //    self._retrievePayment(callback);
        //}

        this.retrievePayment = function () {
            var res = services.PaymentService
                            .GetPayment(self._paymentKey)
                            .done(function (p) {
                                self.Payment = p.Payment;
                                self.Payment.Incarnation.PaymentType = _.find(
                                     self._paymentTypes,
                                     function (type) {
                                         return type.PaymentType == self.Payment.Incarnation.PaymentType.PaymentType &&
                                             type.PaymentSubType == self.Payment.Incarnation.PaymentType.PaymentSubType;
                                     }
                                 );
                            }).
                            
                            pipe(function (p) {
                                var res = 
                                    services.PaymentOrderService.GetPaymentOrdersPending({
                                        PaymentKeys: [self._paymentKey],
                                        Paging: {
                                            CurrentPage: 1,
                                            ResultsForPage: -1
                                        }
                                    }).done(function (pendingP) {
                                        if (pendingP && pendingP.PendingPayments && pendingP.PendingPayments[0]) {
                                            self.Payment.PaymentOrderAllowedOperations = pendingP.PendingPayments[0].PaymentOrderAllowedOperations;
                                        }
                                    });
                                return res;
                            })
                            .pipe(function () {

                                return self;
                            });
            return res;
        }
    };

    return ViewPaymentViewModel;
});