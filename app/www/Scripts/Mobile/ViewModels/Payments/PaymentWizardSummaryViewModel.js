﻿define([
  'jquery',
  'underscore',
  'knockout',
  'komapping',
  'services',
  'backbone',
  'Mobile/ViewModels/Payments/PaymentWizardBaseViewModel',
  'uimodels',
  'utils',
   'Mobile/Models/Payments/OperationStatusToMessageFormater'
], function ($, _, ko, komapping, services, backbone, PaymentWizardBaseViewModel, uimodels, utils, operationStatusFormater) {

    var ViewModelState = {
        WaitingForOperation: 1,
        Completed: 2,
        Error: 3
    };
    
    function PaymentWizardSummaryViewModel(model) {
        uimodels.Notifications.UINotificationContainer.call(this);

        var self = this;
        PaymentWizardBaseViewModel.call(this, model);
        
        this.GetRequestModelForNext = function () {
            throw "Not supported";
        }
        this.GetRequestModelForBack = function () {
            return new services.Objects.PaymentWizardSummary({
                State: self.Model.State
            });
        }

        this._operationStatusFormatter = new operationStatusFormater();

        this.ViewModelState = ko.observable(ViewModelState.WaitingForOperation);
        this.ViewModelStateType = ViewModelState;

        this.SavePayment = function () {
            self._savePayment(function (status) {
                if (status.Success) {
                    self.ViewModelState(ViewModelState.Completed);
                }
                else {
                    self.ViewModelState(ViewModelState.Error);
                }
            });
        }

        this._savePayment = function (callback) {
            
            var opration = self.Model.Operation == services.Objects.EnumPaymentWizardOperation.Save ?
                                self._savePayment_save :
                                self._savePayment_saveNew;

            opration(function (status) {
                self.AddNotification(self._operationStatusFormatter.FormatOperationsStatus(status.OperationStatus));
                if (callback)
                {
                    callback.call(self, status.OperationStatus);
                }
            });
        }

        this._savePayment_saveNew = function (callback)
        {
            var payment = self.Model.Payment;
            services.PaymentService.SaveNewPayment(payment, callback)
        }

        this._savePayment_save = function(callback)
        {
            var payment = self.Model.Payment;
            var key = self.Model.PaymentPreview;

            services.PaymentService.SavePayment(
                {
                    PaymentInput: payment,
                    PaymentKey: new services.Objects.PaymentKey(key)
                },
                callback
            );
        }


        this.SaveAndSendPayment = function () {
            self._savePayment(function (status) {
                if (status.Success) {
                    utils.navigate(
                            utils.buildLink("ProcessPayments", "sign", [status.PaymentIncarnation.ID])
                            , { trigger: true, replace: true }
                        );
                } else {
                    self.ViewModelState(ViewModelState.Error);
                }
            });
        }

        this.BackToPreviousPage = function () {
            utils.navigateBack();
        }
    };

    return PaymentWizardSummaryViewModel;
});