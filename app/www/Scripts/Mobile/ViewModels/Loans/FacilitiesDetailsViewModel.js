﻿define([
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'services',
  'productscache',
  'Mobile/ViewModels/Repository/SubMenu/FacilitiesSubmenu',
  'Mobile/ViewModels/Common/HeaderViewModel',
  'i18n!Mobile/nls/strings'
], function (_, bakcbone, ko, komapping, services, productscache, facilitiesSubmenu, headerViewModel, strings) {

    var FacilitiesDetailsViewModel = function (model) {

        var viewModel = _.extend({}, model);
        viewModel.HeaderViewModel = new headerViewModel({ titleStringValue: $.GetLocalizedString(viewModel.BankFacility.FacilityType.Description) });

       viewModel.hasOverduedLoan = _.any(viewModel.APLoans, function (loan)
        {
            return loan.BankLoan && loan.BankLoan.Overdued;
        });

        viewModel.ProductDetails = {
            heading: { label: strings.ID_STR_DETAILS },
            details: []
        };
        viewModel.ProductDetails.details.push({ label: strings.ID_STR_TYPE, value: $.GetLocalizedString(viewModel.BankFacility.FacilityType.Description) });
        viewModel.ProductDetails.details.push({ label: strings.ID_STR_CCY, value: viewModel.BankFacility.FacilityCCY.SWIFTCode });
        viewModel.ProductDetails.details.push({ label: strings.ID_STR_FACILITY_APPROVE_DATE, value: $.format.date(viewModel.BankFacility.ApprovedDate) });
        viewModel.ProductDetails.details.push({ label: strings.ID_STR_FACILITY_LIMIT, value: $.format.number(viewModel.BankFacility.FacilityAmount) });
        viewModel.ProductDetails.details.push({ label: strings.ID_STR_FACILITY_EXPOSURE, value: $.format.number(viewModel.BankFacility.FacilityCurrentExposure) });
        viewModel.ProductDetails.details.push({ label: strings.ID_STR_FACILITY_CURRENT_AVAILABLE, value: $.format.number(viewModel.BankFacility.FacilityAvailableAmount) });
        viewModel.ProductDetails.details.push({ label: strings.ID_STR_FACILITY_DUE_DATE, value: $.format.date(viewModel.BankFacility.ExpiryDate) });

        viewModel.APLoansDetails = [];
        if (viewModel.APLoans != null) {
            $.each(viewModel.APLoans, function () {
                if (!this.BankLoan.Overdued) {
                    var loanDetails = {
                        heading: { label: strings.ID_STR_LOAN },
                        details: []
                    };
                    loanDetails.details.push({ label: strings.ID_STR_LOAN_ID, value: this.BankLoan.BankLoanID });
                    loanDetails.details.push({ label: strings.ID_STR_CCY, value: this.BankLoan.CCY.SWIFTCode });
                    loanDetails.details.push({ label: strings.ID_STR_LOAN_PRINCIPAL, value: $.format.number(this.BankLoan.Principal) });
                    loanDetails.details.push({ label: strings.ID_STR_INTEREST_RATE, value: $.format.number(this.BankLoan.InterestRate, '0.0000000') });

                    if (this.BankLoan.BankLoanType && (this.BankLoan.BankLoanType.FunctionalType == 2 || this.BankLoan.BankLoanType.FunctionalType == 3)) {
                        loanDetails.details.push({ label: strings.ID_STR_GUARANTEE_PRINCIPAL, value: $.format.number(this.BankLoan.Principal) });
                        loanDetails.details.push({ label: strings.ID_STR_GUARANTEE_VALUE_DATE_FROM, value: $.format.date(this.BankLoan.OpenningDate) });
                        loanDetails.details.push({ label: strings.ID_STR_GUARANTEE_VALUE_DATE_TO, value: $.format.date(this.BankLoan.MaturityDate) });
                    } else if (this.BankLoan.RepaymentType == 3 || this.BankLoan.RepaymentType == 4) {
                        if (this.BankLoan.ExtendedProperties.BankLoanEvents[2]) {
                            loanDetails.details.push({ label: strings.ID_STR_CREDIT_NEXT_VALUE_AMOUNT, value: $.format.number(this.BankLoan.ExtendedProperties.BankLoanEvents[2].Amount) });
                            loanDetails.details.push({ label: strings.ID_STR_CREDIT_NEXT_VALUE_DATE, value: $.format.date(this.BankLoan.ExtendedProperties.BankLoanEvents[2].ValueDate) });
                        }
                    } else if (this.BankLoan.RepaymentType == 2) {
                        var hasPrincipalEvent = this.BankLoan.ExtendedProperties.BankLoanEvents[0] != null;
                        loanDetails.details.push({
                            label: strings.ID_STR_CREDIT_NEXT_PRINCIPAL_VALUE_AMOUNT,
                            value: hasPrincipalEvent ? $.format.number(this.BankLoan.ExtendedProperties.BankLoanEvents[0].Amount) : "",
                            cssClass: "list-group-item-block"
                        });
                        loanDetails.details.push({
                            label: strings.ID_STR_CREDIT_NEXT_PRINCIPAL_VALUE_DATE,
                            value: hasPrincipalEvent ? $.format.date(this.BankLoan.ExtendedProperties.BankLoanEvents[0].ValueDate) : "",
                            cssClass: "list-group-item-block"
                        });
                        loanDetails.details.push({
                            label: strings.ID_STR_CREDIT_CURRENT_INTEREST,
                            value: $.format.number(this.BankLoan.InterestToPayUntilNextDate),
                            cssClass: "list-group-item-block"
                        });
                        var hasInterestEvent = this.BankLoan.ExtendedProperties.BankLoanEvents[1] != null;
                        loanDetails.details.push({
                            label: strings.ID_STR_CREDIT_NEXT_INTEREST_VALUE_AMOUNT,
                            value: hasInterestEvent ? $.format.number(this.BankLoan.ExtendedProperties.BankLoanEvents[1].Amount) : "",
                            cssClass: "list-group-item-block"
                        });
                        loanDetails.details.push({
                            label: strings.ID_STR_CREDIT_NEXT_INTEREST_VALUE_DATE,
                            value: hasInterestEvent ? $.format.date(this.BankLoan.ExtendedProperties.BankLoanEvents[1].ValueDate) : "",
                            cssClass: "list-group-item-block"
                        });
                    }
                    //var subTypesHT = ["PR", "PM", "PH", "PS"];
                    //var loanIsAdministrated = this.BankLoan.BankLoanType.LoanSubType == "AG" || (this.BankLoan.BankLoanType.LoanType == "HT" && $.inArray(this.BankLoan.BankLoanType.LoanSubType, subTypesHT));
                    //if (loanIsAdministrated) {
                    //    loanDetails.details.push({ value: strings.ID_STR_LOAN_IS_BANK_ADMINISTRATED });
                    //}
                    viewModel.APLoansDetails.push(loanDetails);
                }
            });
        }

        return viewModel;
    };

    return FacilitiesDetailsViewModel;
});