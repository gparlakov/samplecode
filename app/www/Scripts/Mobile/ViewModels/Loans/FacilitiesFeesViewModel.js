﻿define([
  'underscore',
  'jquery',
  'i18n!Mobile/nls/strings'
], function (_, $, strings) {

    var FacilitiesDetailsViewModel = function (model) {

        //var viewModel = _.extend({}, model);
        var viewModel = { FeesDetails: [] };

        var feeGroup1 =  _.filter(model, function (fee){
            return (fee.FeeCod == "1" && fee.FeePeriodStartDate <= new Date()) || fee.FeeCod == "5" || fee.FeeCod == "10";
        });

        $.each(feeGroup1, function (index, fee) {
            var feeDetails = {
                heading: { label: $.GetLocalizedString(fee.FeeCodDescription) },
                details: []
            };
            feeDetails.details.push({ label: strings.ID_STR_AMOUNT, value: $.format.number(fee.FeeAmount) });
            feeDetails.details.push({ label: strings.ID_STR_CCY, value: fee.FeeCCY.SWIFTCode });
            feeDetails.details.push({ label: strings.ID_STR_FACILITY_DUE_DATE, value: $.format.date(fee.FeePeriodStartDate) });

            viewModel.FeesDetails.push(feeDetails);
        });

        var feeGroup2 = _.filter(model, function (fee) {
            return fee.FeeCod == '4' || fee.FeeCod == '6';
        });

        $.each(feeGroup2, function (index, fee) {
            var feeDetails = {
                heading: { label: $.GetLocalizedString(fee.FeeCodDescription) },
                details: []
            };
            feeDetails.details.push({ label: strings.ID_STR_CREDIT_DUE_AMOUNT, value: $.format.number(fee.FeePayedAmount) });
            feeDetails.details.push({ label: strings.ID_STR_CCY, value: fee.FeeCCY.SWIFTCode });
            feeDetails.details.push({ label: strings.ID_STR_FACILITY_DUE_DATE, value: $.format.date(fee.FeeNextPayDate) });

            viewModel.FeesDetails.push(feeDetails);
        });

        return viewModel;
    };

    return FacilitiesDetailsViewModel;
});