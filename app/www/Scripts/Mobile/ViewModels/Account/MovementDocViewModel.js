define([
  'jquery',
  'underscore',
  'knockout',
  'komapping',
  'services',
 
 
], function ($, _, ko, komapping, services) {

    var MovementDocViewModel = function (documentReference, archiveSuffix, bankAccountKey) {

        var self = this;

        this.bankAccountKey = bankAccountKey;

        var filter = new services.Objects.BankAccountMovementDocumentKey({ BankAccount: bankAccountKey, DocumentReference: documentReference, ArchiveSuffix: archiveSuffix });

        this.MovementDocument = null;

        this.initialize = function () {
            var result = 
                    $.whenEx(
                        services.BankProductInfoService.GetProductMovementDocument(filter)
                    )
                    .done(function(doc) {
                        self.MovementDocument = doc.BankProductMovementDocument;
                    });

            return result;
        }

        //this.bankAccountKey = bankAccountKey;

        //this.Movements = ko.observableArray([]);
        //this.Loaded = ko.observable(false);

        //this.loadMovemetns = function () {
        //    var filter = new services.Objects.BankAccountMovementsBlockUnblockFilterCurrent({ BankAccount: bankAccountKey });

        //    services.BankProductInfoService.GetProductMovements(
        //        filter,
        //        function (data) {

        //            self.Movements.removeAll();

        //            for (var movt in data.Movements) {
        //                self.Movements.push(data.Movements[movt]);
        //            }
        //            self.Loaded(true);
        //        }
        //    );
        //}

        //this.load = function () {
        //    self.loadMovemetns();
        //}

        //this.unload = function () {

        //}
       
    };

    return MovementDocViewModel;
});