define([
  'underscore',
  'knockout',
  'komapping',
  'services',
  'utils',
  'backbone',
  'productscache',
  'Mobile/ViewModels/Common/HeaderViewModel',
  'Mobile/ViewModels/Repository/SubMenu/AccountMovements'
], function (_, ko, komapping, services, utils, backbone, productscache, headerViewModel, accountsSubmenu) {


    var AccountOverviewViewModel = function (model) {
        

        this.Init = function () {
            this.APAccount = productscache.GetAPAccountByID(model.BankAccountID);
            this.HeaderViewModel = new headerViewModel({ titleStringID: "ID_STR_ACCOUNT_DETAILS" });
        }
        this.Init();

        this.Submenu = accountsSubmenu([model.BankAccountID]);

        this.newPayment = function () {
            var link = utils.buildLink(
                            'PaymentWizard',
                            'newPaymentFromBankAccount',
                             [
                                 services.Objects.EnumPaymentIncarnationType.PaymentOrder,
                                 model.BankAccountID
                             ]
                        );
            utils.navigate(link, { trigger: true });
        }

    };

    return AccountOverviewViewModel;
});