﻿define([
  'underscore',
  'i18n!Mobile/nls/strings'
  
], function (_, strings) {

    
    var CCYCalculatorViewModel = function CCYCalculatorViewModel(model) {
        var self = this;
                
        this.ccyRates = _.filter(model.CCYRates, function (ccyRate) {
            return ccyRate.CCY.SWIFTCode != 'BGN';
        });

        self.CCYRatesItems = [];
        $.each(this.ccyRates, function () {
            var item = {
                heading: { label: this.CCY.SWIFTCode, cssClass: this.CCY.SWIFTCode },
                details: []
            };
            item.details.push({ label: strings.ID_STR_DATE, value: $.format.date(this.RateUpdateTimestamp, 'dd.MM.yyyy HH:mm') });
            item.details.push({ label: strings.ID_STR_RATIO_FIX, value: $.format.number(this.FixRate, '0.0000') });
            item.details.push({ label: strings.ID_STR_CURRENCY_RATE_BUYBOOK, value: $.format.number(this.BuyBook, '0.0000') });
            item.details.push({ label: strings.ID_STR_CURRENCY_RATE_SELLBOOK, value: $.format.number(this.SellBook, '0.0000') });
            item.details.push({ label: strings.ID_STR_CURRENCY_RATE_BUYCASH, value: $.format.number(this.BuyCash, '0.0000') });
            item.details.push({ label: strings.ID_STR_CURRENCY_RATE_SELLCASH, value: $.format.number(this.SellCash, '0.0000') });

            self.CCYRatesItems.push(item);
        });
    };

    return CCYCalculatorViewModel;
});