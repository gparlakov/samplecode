﻿define([
  'underscore',
  'knockout',
  'services',
  'utils',
  'Mobile/ViewModels/Common/HeaderViewModel',
  'Mobile/ViewModels/Repository/SubMenu/CCYRatesSubmenu'
  
], function (_, ko, services, utils, headerViewModel, ccyRateSubmenu) {

    
    var CCYCalculatorViewModel = function CCYCalculatorViewModel(model)
    {
        var self = this;

        this.HeaderViewModel = new headerViewModel({ titleStringID: 'ID_STR_CURRENCY_CONVERTER' });
        //this.hasSeesion = session.GetSession() != null ;

        this.Submenu = new ccyRateSubmenu();
        
        
        this.initialize = function () {
            
            //if (!self.hasSeesion) {
            //    navigation.hide();
            //}

            var result =
                services.BankInformationService.GetCCYRatesCurrent()
                .pipe(function (rates) {
                        self.CCYRates = rates;
                    });
            return result;
        }
        
        //$(document).one('pagechange', function () {
            
        //    var s = session.GetSession();

        //    if (!s) {
        //        //ako e skrit header-a, da nqma razstoqnie otgore 
        //        //utils.getCurrentPageContainer().css('padding-top', 0);

        //        navigation.hide();
        //    }
        //});
    };

    return CCYCalculatorViewModel;
});