define([
  'Mobile/Models/Submenu/SubmenuItem',
  'Mobile/Models/Submenu/Submenu',
  'i18n!Mobile/nls/strings'
], function (submenuItem, submenu, strings) {

    var AccountMovements = function (params) {

        return { Items: [
            {
                Label: strings.ID_STR_DETAILS,
                Controller : 'AccountBalances',
                Action: 'index',
                //IsActive: false,
                liCss: 'acc-details',
                Params : params
            },{
                Label: strings.ID_STR_ACCOUNT_MOVEMENTS,
                Controller : 'AccountMovements',
                Action: 'index',
                //IsActive: true,
                liCss: 'acc-movs',
                Params: params
            },{
                Label: strings.ID_STR_ACCOUNT_MOVEMENTS_BLOCKS,
                Controller: 'AccountMovements',
                Action: 'blocks',
                //IsActive: false,
                liCss: 'acc-blocks',
                Params: params
            },{
                Label: strings.ID_STR_CARDS,
                Controller: 'AccountOverview',
                Action: 'DebitCardsList',
                //IsActive: false,
                liCss: 'acc-cards',
                Params: params
            }
        ]
        };
    }

    return AccountMovements;
});