define([
  'i18n!Mobile/nls/strings'

], function (strings) {

    var PaymentsSubmenu = function (params) {

        return {
            Items: [
                {
                    Label: strings.ID_STR_LOAN,
                    Controller: 'FacilitiesDetails',
                    Action: 'facilityDetails',
                    //IsActive: false,
                    liCss: 'fclt-details',
                    Params: params
                }, {
                    Label: strings.ID_STR_FACILITY_FEES,
                    Controller: 'FacilitiesDetails',
                    Action: 'facilityFees',
                    //IsActive: false,
                    liCss: 'fclt-fees',
                    Params: params
                }
            ]
        };
    }
    return PaymentsSubmenu;
});