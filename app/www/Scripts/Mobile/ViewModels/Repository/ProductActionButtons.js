define([
  'utils',
  'jquery',
  'services',
  'i18n!Mobile/nls/strings',
  'Mobile/ViewModels/Repository/ActionButtonTypes'
], function (utils, $,services, strings, buttonTypes) {

    var actionButtons = {};

    actionButtons.ViewAccountDetails = function (apa) {

        $.extend(this, buttonTypes.gray);
        this.enabled = true;
        this.label = strings.ID_STR_DETAILS;
        this.click = function () {
            var link = utils.buildLink("AccountOverview", "index", [apa.BankAccount.BankAccountID]);
            utils.navigate(link, { trigger: true });
        }
    }, 
    
    actionButtons.NewPaymentFromAccount = function (apa) {

        $.extend(this, buttonTypes.green);
        this.enabled = true;
        this.label = strings.ID_STR_CREATE_NEW_PAYMENT;
        this.click = function () {
            var link = utils.buildLink('PaymentWizard',
            'newPaymentFromBankAccount',
             [
                 services.Objects.EnumPaymentIncarnationType.PaymentOrder,
                 apa.BankAccount.BankAccountID
             ]);
            utils.navigate(link, { trigger: true });
        }
    },

    actionButtons.ViewCardDetails = function (apCard) {

        $.extend(this, buttonTypes.gray);
        this.enabled = true;
        this.label = strings.ID_STR_DETAILS;
        this.click = function () {
            var link = utils.buildLink("CardDetails", "overview", [apCard.BankCard.BankCardID]);
            utils.navigate(link, { trigger: true });
        }
    },

    actionButtons.ViewDepositDetails = function (apDeposit) {

        $.extend(this, buttonTypes.gray);
        this.enabled = true;
        this.label = strings.ID_STR_DETAILS;
        this.click = function () {
            var link = utils.buildLink("DepositDetails", "", [apDeposit.BankTimeDeposit.BankDepositID]);
            utils.navigate(link, { trigger: true });
        }
    },

    actionButtons.ViewFacilityDetails = function (apFacility) {

        $.extend(this, buttonTypes.gray);
        this.enabled = true;
        this.label = strings.ID_STR_DETAILS;
        this.click = function () {
            var link = utils.buildLink("FacilitiesDetails", "index", [apFacility.BankFacility.BankClient.BankClientID, apFacility.BankFacility.FacilityType.ID, apFacility.BankFacility.FacilitySequence]);
            utils.navigate(link, { trigger: true });
        }
    }

    return actionButtons;
});