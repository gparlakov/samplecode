define([
  'utils',
  'jquery',
  'services',
  'i18n!Mobile/nls/strings',
  'Mobile/ViewModels/Repository/ActionButtonTypes',
  'servicesObjects',
  'knockout',
  'deviceapi'

], function (utils, $, services, strings, buttonTypes, servicesObjects, ko, deviceapi) {

    var actionButtons = {};

    var statusActive = servicesObjects.EnumNotificationCorrespondentSubscriptionStatus.Active;
    var statusInactive = servicesObjects.EnumNotificationCorrespondentSubscriptionStatus.Inactive;

    actionButtons.ActivateSubscription = function (subscription) {

        $.extend(this, buttonTypes.green);

        this.enabled = ko.pureComputed(function () {
            return subscription.Status() == statusInactive;
        });

        this.label = strings.ID_STR_ONLINE_NOTIF_ACTIVATE;
        this.click = function () {
            changeStatus(subscription, statusActive);
        }
    };

    // only one dialog object
    var CancelID = "NO";
    var OKID = "OK";
    var deactivateDialog = null;
    var getDeactivateDialog = function () {
        if (deactivateDialog === null) {
            deactivateDialog = deviceapi.notification.Confirm(
                strings.ID_STR_ONLINE_NOTIF_DEACTIVATE_CONFIRMATION_TITLE,
                strings.ID_STR_ONLINE_NOTIF_DEACTIVATE_CONFIRMATION_MESSAGE,
                [
                    { ID: OKID, text: strings.ID_STR_OK },
                    { ID: CancelID, text: strings.ID_STR_CANCEL }
                ]
            ).done(function () {
                deactivateDialog = null;
            });
        }

        return deactivateDialog;
    }

    actionButtons.DeactivateSubscription = function (subscription) {

        $.extend(this, buttonTypes.red);

        this.enabled = ko.pureComputed(function () {
            return subscription.Status() == statusActive;
        });
           
        this.label = strings.ID_STR_ONLINE_NOTIF_DEACTIVATE;
        this.click = function () {
           
            var dialog = getDeactivateDialog();
            $.whenEx(
                dialog
            )
            .then(function (dialogres) {
                if (dialogres.ID == OKID) {
                    changeStatus(subscription, statusInactive);
                }
            });
        }
    };

    var changeStatus = function (subscription, newStatus) {

        var changeStatusRequest = new servicesObjects.ChangeSubscriptionStatus({
            Subscription: subscription,
            SubscriptionStatus: newStatus
        });
        
        services.OnlineNotificationsService.ChangeSubscriptionStatus(changeStatusRequest)
            .then(function () {
                subscription.Status(newStatus);
            });
    }

    return actionButtons;  
});