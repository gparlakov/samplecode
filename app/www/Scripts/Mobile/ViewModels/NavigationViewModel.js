﻿define([
  'jquery',
  'knockout',
  'session',
  'utils',
  'i18n!Mobile/nls/strings',
  'osManager'
], function ($, ko, session, utils, strings, osManager) {

    var navigationItemsNoSession = [
        {
            href: 'Login',
            icon: "i-right",
            label: strings.ID_STR_LOGIN_BUTTON,
            subLabel: strings.ID_STR_LOGIN_BUTTON_SUB
        },
        {
            href: 'BankBranches',
            icon: "i-location",
            label: strings.ID_STR_BRANCHES_ATMS,
            subLabel: strings.ID_STR_BRANCHES_SUB
        },
        {
            href: 'CCYRates',
            icon: "i-chart-line",
            label: strings.ID_STR_CURRENCY_CONVERTER,
            subLabel: strings.ID_STR_CURRENCY_CONVERTER_SUB
        },
        {
            href: 'Settings/contacts',
            icon: "i-mobile",
            label: strings.ID_STR_CONTACTS,
            subLabel: strings.ID_STR_CONTACTS_SUB
        }
    ];
    if (osManager.CurrentOS.closeAppIsAvailable) {
        navigationItemsNoSession.push({
            href: 'Login/logout',
            icon: "i-cancel-1",
            label: strings.ID_STR_LOGOUT_BUTTON,
            subLabel: strings.ID_STR_LOGOUT_BUTTON_SUB
        });
    }

    var navigationItemsSession = [
        {
            href: 'Products',
            icon: "i-archive",
            label: strings.ID_STR_PRODUCTS,
            subLabel: strings.ID_STR_PRODUCTS_SUB
        },
        {
            href: 'ViewPayments/index',
            icon: "i-switch",
            label: strings.ID_STR_TRANSFERS,
            subLabel: strings.ID_STR_TRANSFERS_SUB
        },
        {
            href: 'OnlineNotifications/index',
            icon: "i-th-list",
            label: strings.ID_STR_ONLINE_NOTIF,
            subLabel: strings.ID_STR_ONLINE_NOTIF_SUB
        },
        {
            href: 'Messages/index',
            icon: "i-paper-plane",
            label: strings.ID_STR_MESSAGES,
            subLabel: strings.ID_STR_MESSAGES_SUB
        },
        {
            href: 'Settings/contacts',
            icon: "i-mobile",
            label: strings.ID_STR_CONTACTS,
            subLabel: strings.ID_STR_CONTACTS_SUB
        },   
        {
            href: 'BankBranches',
            icon: "i-location",
            label: strings.ID_STR_BRANCHES_ATMS,
            subLabel: strings.ID_STR_BRANCHES_SUB
        },
        {
            href: 'CCYRates',
            icon: "i-chart-line",
            label: strings.ID_STR_CURRENCY_CONVERTER,
            subLabel: strings.ID_STR_CURRENCY_CONVERTER_SUB
        },
        {
            href: 'Profile',
            icon: "i-user",
            label: strings.ID_STR_PROFILE,
            subLabel: strings.ID_STR_PROFILE_SUB
        },
        {
            href: 'Login/logout',
            icon: "i-cancel-1",
            label: strings.ID_STR_LOGOUT_BUTTON,
            subLabel: strings.ID_STR_LOGOUT_BUTTON_SUB
        }

    ];

    function NavigationViewModel() {
        var self = this;
        this.HasSession = session.HasSession();

        if (this.HasSession) {
            this.NavigationItems = navigationItemsSession;
        } else {
            this.NavigationItems = navigationItemsNoSession;
        }

        this.PageReachableFromMenu = ko.observable(false);

        $.each(self.NavigationItems, function (index, navigationItem) {
            navigationItem.isActive = navigationItem.href == utils.currentPage;
            if (navigationItem.isActive)
                self.PageReachableFromMenu(true);
            navigationItem.navigate = function () {
                utils.getNavigationContainer().swipeMenu("hide");
                utils.navigate(this.href, { trigger: true });
            };
        });
    }
    return NavigationViewModel;
});