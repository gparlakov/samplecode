define([
  'jquery',
  'services',
  'utils',
  'events',
  'Mobile/ViewModels/Common/HeaderViewModel'
], function ($, services, utils, events, headerViewModel) {


    var MessageViewModel = function (messageKey) {

        var self = this;
        
        self.MessageToUser = null;

        self.HeaderViewModel = new headerViewModel({ titleStringID: 'ID_STR_MESSAGES' });

        this.initialize = function () {
            var result =
                $.whenEx(
                   services.MessageService.GetMessage(messageKey)
                )
                .done(function (msg) {
                    self.MessageToUser = msg.MessageToUser;
                }).then(function (msg) {
                    if (!msg.MessageToUser.Read)
                        return services.MessageService.MarkAsRead(messageKey);   
                });

            return result;
        }   
    };

    return MessageViewModel;
});