﻿define([
  'underscore',
  'jquery',
  'utils',
  'knockout',
  'komapping',
  'services',
  'typeVisitor',
  'productscache',
  'i18n!Mobile/nls/strings',
  'dxChart',
  'Mobile/ViewModels/Common/HeaderViewModel',
  'Mobile/ViewModels/Repository/ProductActionButtons',
], function (_, $, utils, ko, komapping, services, typeVisitor, productscache, strings, dxChart, headerViewModel, productActionButtons) {

    var OverviewViewModel = function OverviewViewModel() {

        this.HeaderViewModel = new headerViewModel({ titleStringID: "ID_STR_PRODUCTS" });
        this.productsListLabel = ko.observable();;

        this.sum = function (collection) {
            return collection.reduce(function (memo, element) {
                return memo + element.value;
            }, 0.001); //shte se pokazva 0.00 zaradi string.format, no shte go ima v legendata, zashtoto e > 0
        }

        this.GetAccountsAvailable = function (ap) {
            var accountsAvailable = ap.APAccounts
                .filter(function (apa) {
                    return apa.BankAccount.BankAccountBalances != null
                })
                .map(function (apa) {
                    return {
                        argument : apa.BankAccount.BankAccountID,
                        argumentText: apa.ShortName,
                        value: apa.BankAccount.BankAccountBalances[0].AvailableBalance,
                        ccy: apa.BankAccount.CCY.SWIFTCode,
                        click: function () {
                            var details = new productActionButtons.ViewAccountDetails(apa);
                            details.click();
                        }
                    }
                });

            return _.sortBy(accountsAvailable, function (item) {
                return item.value;
            });
        }

        this.GetDepositsAvailable = function (ap) {
            var depositsAvailable = ap.APTimeDeposits
                .map(function (apDeposit) {
                    return {
                        argument: apDeposit.BankTimeDeposit.BankDepositID,
                        argumentText : $.GetLocalizedString(apDeposit.BankTimeDeposit.DepositType.FunctionalType.Description),
                        value: apDeposit.BankTimeDeposit.Principal,
                        ccy: apDeposit.BankTimeDeposit.BankDepositCCY,
                        click: function () {
                            var details = new productActionButtons.ViewDepositDetails(apDeposit);
                            details.click();
                        }
                    }
                });

            return _.sortBy(depositsAvailable, function (item) {
                return item.value;
            });
        }

        this.GetCreditCardsAvailable = function (ap) {
            var creditCardsAvailable = _.compact(ap.APCards
                .filter(function (apCard) {
                    if (apCard.BankCard.BankCardStatus != null && apCard.BankCard.BankCardStatus.ID != 0 && !apCard.BankCard.BankCardStatus.isActive) {
                        return false;
                    } else {
                        if (apCard.BankCard.BankAccount != null && apCard.BankCard.BankAccount != 'undefined') {
                            productscache.SetBankAccountToBankCard(apCard.BankCard);

                            if (apCard.BankCard.BankAccount.BankAccountBalances != null)
                                return true;
                            else
                                return false;
                        }
                    }
                })
                .map(function (apCard) {
                    return typeVisitor.visit(apCard.BankCard, {
                        BankCardCredit: function (creditCard) {
                            return {
                                argument : creditCard.BankCardID,
                                argumentText: creditCard.BankCardNumber,
                                value: creditCard.BankAccount.BankAccountBalances[0].AvailableBalance,
                                ccy: creditCard.CCY.SWIFTCode,
                                click: function () {
                                    var details = new productActionButtons.ViewCardDetails(apCard);
                                    details.click();
                                }
                            }
                        },
                        BankCardRaiCard: function (raiCard) {
                            return {
                                argument: raiCard.BankCardID,
                                argumentText: raiCard.BankCardNumber,
                                value: raiCard.BankAccount.BankAccountBalances[0].AvailableBalance,
                                ccy: raiCard.CCY.SWIFTCode,
                                click: function () {
                                    var details = new productActionButtons.ViewCardDetails(apCard);
                                    details.click();
                                }
                            }
                        },
                        Other: function () {
                            return;
                        }
                    });
                }));

            return _.sortBy(creditCardsAvailable, function (item) {
                return item.value;
            });
        }

        this.BarChartVisible = ko.observable(false);
        this.TotalAmount = 0;
        this.Data = [];
        this.SetData = function () {
            var self = this;
            var ap = productscache.GetAPerson();
            var accountsAvailable = self.GetAccountsAvailable(ap);
            var depositsAvailable = self.GetDepositsAvailable(ap);
            var creditCardsAvailable = self.GetCreditCardsAvailable(ap);
            
            self.Data = [
                {
                    argument: strings.ID_STR_PRODUCTS_ACCOUNTS,
                    value: self.sum(accountsAvailable),
                    color: "#6B8F00",
                    click: function () {
                        self.showDetails(accountsAvailable, this);
                    }
                },
                {
                    argument: strings.ID_STR_CARDS,
                    value: self.sum(creditCardsAvailable),
                    color: "#00c7b1",
                    click: function () {
                        self.showDetails(creditCardsAvailable, this);
                    }
                },
                {
                    argument: strings.ID_STR_PRODUCTS_DEPOSITS,
                    value: self.sum(depositsAvailable),
                    color: "#0092bc",
                    click: function () {
                        self.showDetails(depositsAvailable, this);
                    }
                },
            ];
            self.FindDataItem = function (items, argument) {
                return _.find(items, function (dataItem) {
                    return argument == dataItem.argument;
                });
            }
            self.TotalAmount = _.map(self.Data, function(item){
                return item.value;
            }).reduce(function (memo, element) {
                return memo + element;
            }, 0);
        }

        this.initialize = function () {
            var self = this;
            var filter = new services.Objects.BankAccountBalancesFilterCurrent({ Paging: { CurrentPage: -1, ResultsForPage: -1 } });
            var balancesRes = services.BankProductInfoService.GetProductBalances(filter);

            return balancesRes.pipe(function () { self.SetData(); });
        }

        this.rendered = function () {
            var self = this;

            $("#overviewDonutChart").dxPieChart({
                //adaptiveLayout: {
                //    keepLabels: true
                //},
                commonSeriesSettings: {
                    hoverStyle: {
                        border: {
                            visible: true,
                        },
                        hatching: {
                            direction: 'none',
                        }
                    }
                },
                dataSource: self.Data,
                diameter: 0.8,
                palette: 'Harmony Light',
                animation: {
                    duration: 500
                },
                resolveLabelOverlapping: "shift",
                tooltip: {
                    font: {
                        family: "RobotoCondensed-Bold",
                        size: 12,
                    },
                    
                    enabled: true,
                    customizeTooltip: function (arg) {
                        var item = _.find(self.Data, function (dataItem) {
                            return arg.argument == dataItem.argument;
                        });
                        return {
                            text: "{0}\r\n{1} BGN".format(arg.argument, $.format.number(arg.value)).toUpperCase(),
                            fontColor: item.color
                        };
                    },
                },
                legend: {
                    //orientation: 'vertical',
                    font: {
                        family: "RobotoCondensed-Bold",
                        size: 12,
                        color: '#454545'
                    },
                    horizontalAlignment: "center",
                    verticalAlignment: "bottom",
                    markerSize: 20,
                    customizeText: function (pointInfo) {
                        var item = self.FindDataItem(self.Data, pointInfo.pointName);
                        return "{0}\r\n{1} BGN".format(item.argument, $.format.number(item.value)).toUpperCase();
                    },
                },
                customizePoint: function (point) {
                    //console.log(point);
                    var item = _.find(self.Data, function (dataItem) {
                        return point.argument == dataItem.argument;
                    });
                    if (item) {
                        return {
                            color: item.color
                        };
                    }
                },
                series: [{
                    type: "doughnut",
                    argumentField: "argument",
                    valueField: "value",
                    hoverStyle: {
                        border: {
                            visible: true,
                            color: self.Data.color,
                            width: 1
                        },

                        hatching: {
                            direction: 'none',
                            //opacity: 0.6,
                            //step: 6,

                        },
                    },
                    innerRadius: 0.58,
                    border: {
                        visible: true,
                        color: '#ffffff',
                        width: 6
                    },
                    label: {
                        //visible: true,
                        //connector: {
                        //    visible: true
                        //},
                        //customizeText: function () {
                        //    var percent = Math.round(this.value / self.TotalAmount * 10000) / 100;
                        //    return "{0}%".format($.format.number(percent, "0.00"));
                        //}
                    }
                }],
                onPointClick: function (e) {
                    var item = self.FindDataItem(self.Data, e.target.originalArgument);
                    if ($.isFunction(item.click)) {
                        item.click();
                        $('body').animate({ scrollTop: 480 }, 1000);
                    }
                }
            });
        }

        this.showDetails = function (productsAvailableAmount, dataItem) {
            var self = this;
            var barChart = $("#overviewBarChart");
            barChart.empty();
            barChart.removeData();
            self.productsListLabel(dataItem.argument);

            if (productsAvailableAmount.length) {
                self.BarChartVisible(true);
                var barWidth = 40;
                var barSpacing = 20;
                barChart.dxChart({
                    dataSource: productsAvailableAmount,
                    rotated: true,
                    size: {
                        height: (productsAvailableAmount.length == 1 ? 2 : productsAvailableAmount.length) * (barWidth + barSpacing)
                    },
                    animation: {
                        duration: 500
                    },
                    equalBarWidth: {
                        width: barWidth,
                        spacing: barSpacing
                    },
                    tooltip: {
                        font: {
                            family: "RobotoCondensed-Bold",
                            size: 12,
                        },
                        enabled: false,
                        customizeTooltip: function (arg) {
                            var item = self.FindDataItem(productsAvailableAmount, arg.originalArgument);
                            return {
                                text: "{0}\r\n{1} {2}".format(item.argument, $.format.number(item.value), item.ccy)
                            };
                        }
                    },
                    legend: {
                        visible: false,
                    },
                    series: [{
                        type: "bar",
                        argumentField: "argument",
                        valueField: "value",
                        color: dataItem.color,
                        cornerRadius: 1,
                        hoverStyle: {
                            border: {
                                visible: true,
                                color: self.Data.color,
                                width: 1
                            },

                            hatching: {
                                direction: 'none',
                                //opacity: 0.6,
                                //step: 6,

                            },
                        },
                        label: {
                            visible: true,
                            position: 'inside',
                            alignment: 'left',
                            horizontalOffset:-800,
                            backgroundColor: "transparent",
                            connector: {
                                visible: false
                            },
                            customizeText: function (pointInfo) {
                                var item = self.FindDataItem(productsAvailableAmount, pointInfo.originalArgument);
                                return "{0}\r\n{1} {2}".format(item.argumentText, $.format.number(item.value), item.ccy).toUpperCase();
                            },
                            font: {
                                family: "RobotoCondensed-Bold",
                                size: 12,
                                color:'#444444',
                            }
                        }
                    }],
                    argumentAxis: {
                        visible: true,
                        grid: { visible: false },
                        label: {
                            visible: true,
                            font: {
                                family: "mobile-set",
                                size: 24,
                                color: '#ababab'
                            },
                            customizeText: function (pointInfo) {
                                //return strings.ID_STR_DETAILS;//.toUpperCase();
                                return '&#xE882';
                            },
                        },
                        placeholderSize: 60
                    },
                    valueAxis: {
                        visible: false,
                        grid: { visible: false },
                        label: { visible: false },
                    },
                    onArgumentAxisClick: function (e) {
                        var item = self.FindDataItem(productsAvailableAmount, e.argument);
                        if ($.isFunction(item.click)) {
                            item.click();
                        }
                    },
                    //onPointClick: function (e) {
                    //    console.log(e.target);
                    //}
                });
            } else {
                self.BarChartVisible(false);
                barChart.html('<div class="alert alert-info nodata">{0}</div>'.format(strings.ID_STR_NO_ACTIVE_PRODUCTS));
            }
        }
    };

    return OverviewViewModel;

});