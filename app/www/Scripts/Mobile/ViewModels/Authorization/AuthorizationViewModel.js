﻿define([
  'jquery',
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'services',
  'utils',
  'servicesObjects',
  'events',
  'uimodels',
  'i18n!Mobile/nls/strings',
  'validator',
  'typeVisitor'
], function ($, _, backbone, ko, komapping, services, utils, servicesObjects, events, uimodels, strings, validator, typeVisitor) {

    var State = {
        InProcess: 1,
        Completed: 2,
        Error: 3
    }


    function WrappedAuthorizationBase(cleanAuth)
    {
        this.Auth = komapping.fromJS(cleanAuth);
        this.OriginalAuth = cleanAuth;

        this.GetAuth = function () {
            return komapping.toJS(this.Auth);
        }

        this.Process = function () {
            return { AuthorizationProcessStatus : { Status : servicesObjects.EnumAuthorizationProcessStatus.OK } };
        }
    }

    

    function WrappedAuthorizationOTC()
    {
        WrappedAuthorizationBase.apply(this, arguments);

        this.Process = function () {
            return services.AuthorizationService.ProcessAuthorization(this.GetAuth());
        }

    }

    function WrappedAuthorizationDummy()
    {
        this.Auth = null;
    }
    function WrappedAuthorizationMTAN() {
        WrappedAuthorizationOTC.apply(this, arguments);
        validator.validatableModel.call(this,
            {
                properties: {
                    "Auth": {
                        properties: {
                            "Response": {
                                rules: {
                                    NotNullOrEmpty: true,
                                }
                            }
                        }
                    }
                }
            });
    }
    function WrappedAuthorizationToken() {
        WrappedAuthorizationOTC.apply(this, arguments);
        validator.validatableModel.call(this,
            {
                properties: {
                    "Auth": {
                        properties: {
                            "Response": {
                                rules: {
                                    NotNullOrEmpty: true,
                                }
                            }
                        }
                    }
                }
            });
    }
    function WrappedAuthorizationUserUserNameAndPassword() {
        WrappedAuthorizationBase.apply(this, arguments);

        this.ProcessNotNeeded = true;
    }


    function WrappedAuthorizationObjectFactory(cleanAuthObject)
    {
        return typeVisitor.visit(cleanAuthObject, {
            'AuthorizationMTAN': function (auth) { return new WrappedAuthorizationMTAN(auth); },
            'AuthorizationToken': function (auth) { return new WrappedAuthorizationToken(auth); },
            'AuthorizationUserNameAndPassword': function (auth) { return new WrappedAuthorizationUserUserNameAndPassword(auth); }
        });
    }


    var AuthorizationViewModel = function (payments) {
        uimodels.Notifications.UINotificationContainer.call(this);

        

        this.Types = {
            State: State
        }

        var self = this;

        this.State = ko.observable(State.InProcess);

        this.Payments = payments;
        this.OriginalAuthorization = null;
        this.Authorization = ko.observable(null);

        this.initialize = function () {
            return self._refreshAuthorization();
        };

        this.RefreshAuthorization = function () {
            self._refreshAuthorization();
        };

        this._refreshAuthorization = function () {
            var result = (
                self._createAuthorization()
                .done(function (auth) {
                    var a = auth.Authorization;
                    var wrapped = WrappedAuthorizationObjectFactory(a);
                    self.Authorization(wrapped);
                    self.OriginalAuthorization = a;

                    if (wrapped.ProcessNotNeeded)
                    {
                        events.trigger(self, "success", State.Completed);
                    }
                })
            );

            return result;
        };

        this.Process = function () {
            this._processAuthorization();
        };

        this.Cancel = function () {
            events.trigger(this, "cancel", null);
        };

        //Private

        this._STATUSES_OK = [servicesObjects.EnumAuthorizationProcessStatus.OK]
        this._STATUSES_REJECTED = [servicesObjects.EnumAuthorizationProcessStatus.Rejected,
                                    servicesObjects.EnumAuthorizationProcessStatus.RejectedInvalidCode,
                                    servicesObjects.EnumAuthorizationProcessStatus.RejectedTooManyRetries];
        this._STATUSES_RETRY = [servicesObjects.EnumAuthorizationProcessStatus.RejectedRetry];

        this._createAuthorization = function () {
            return (
                services.AuthorizationService.RequestAuthorization(
                    new services.Objects.AuthorizationDataPayments({
                        Payments: _.map(
                            ko.utils.unwrapObservable(self.Payments),
                            function (payment) {
                                return new services.Objects.SignablePayment(payment.Incarnation);
                            }
                        )
                    })
                )
            );

        };

        this._processAuthorization = function () {
            var self = this;

            var auth = ko.utils.unwrapObservable(self.Authorization);

            
            if (validator.isValidatable(auth) && !validator.getValidator(auth).validate())
                return;

            var cleanAuth = auth.GetAuth();

            return (
                $.whenEx(
                    auth.Process()
                )
                .done(function (authStatusResp) {
                    var authStatus = authStatusResp.AuthorizationProcessStatus;

                    if (_.contains(self._STATUSES_RETRY, authStatus.Status))
                        events.trigger(self, "retry", authStatus.Status);

                    if (_.contains(self._STATUSES_REJECTED, authStatus.Status))
                        events.trigger(self, "rejected", authStatus.Status);

                    if (_.contains(self._STATUSES_OK, authStatus.Status))
                        events.trigger(self, "success", authStatus.Status);
                })
            )            
        }

        
        this.retry = function () {
            self.ClearAndAddNotification(new uimodels.Notifications.UINotification({
                Message: strings.ID_STR_AUTH_RETRY,
                Type: uimodels.Notifications.UINotificationType.Error
            }));
        };

        this.rejected = function () {
            self.ClearAndAddNotification(new uimodels.Notifications.UINotification({
                Message: strings.ID_STR_AUTH_REJECTED,
                Type: uimodels.Notifications.UINotificationType.Error
            }));
            self.Authorization(null);
        };

        this.success = function () {
            this.State(State.Completed);
        };

        this.cancel = function () {
        };
    };

    return AuthorizationViewModel;
});