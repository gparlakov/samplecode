﻿define([
  'deviceapi',
  'utils',
  'Common/cache/language',
  'Mobile/ViewModels/Common/HeaderViewModel'
], function (deviceapi, utils, language, headerViewModel) {

    

    var SettingsViewModel = function SettingsViewModel(model) {
        var self = this;
        self.HeaderViewModel = new headerViewModel({ titleStringID: 'ID_STR_CONTACTS' })

        this.viewContacts = function (viewModel) {

            var link = utils.buildLink('Settings', 'contacts');
            utils.navigate(link, { trigger: true });
        }
        
        this.viewBranches = function (viewModel) {
            var link = utils.buildLink('BankBranches');
            utils.navigate(link, { trigger: true });
        }

        this.viewCcyCalc = function () {
            utils.navigate("CCYRates", { trigger: true });
        }

        this.logout = function (viewModel)
        {
            return utils.navigate("Login/logout", { trigger: true });
        }

        this.lang = language.GetCurrentLang()
    };

    return SettingsViewModel;
});