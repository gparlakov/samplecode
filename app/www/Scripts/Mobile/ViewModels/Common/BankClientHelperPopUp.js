﻿define([
    'jquery',
    'knockout',
    'underscore',

    'Mobile/ViewModels/Base/PopupPageViewModel',

], function ($, ko, _, PopupPageViewModel) {

    // takes:
    //  parent: viewModel calling this one,
    //  options: { 
    //      load: function() { returns a promise resolved with bank clients },
    //      selectedBankClient : { to set class selected }
    //  }
    function BankClientHelperPopUp(parent, options) {
        var self = this;

        PopupPageViewModel.apply(this, arguments);

        this.BankClients = ko.observableArray();
        this.SelectedBankClient = ko.observable();
        if (options.selectedBankClient) {
            this.SelectedBankClient(options.selectedBankClient);
        }

        this.SelectBankClient = function (bankClient) {
            self.SelectedBankClient(bankClient);
            self["return"](bankClient);
        }        

        this.loadData = function () {
            var result =
                $.whenEx(
                    options.load()
                )
                .done(function (bankClients) {
                    _.each(bankClients, function(apBankClient){
                        self.BankClients.push(apBankClient.BankClient);
                    })
                });

            return result;
        };


        this.path = 'Common/BankClientPopUpTemplate';
    };

    return BankClientHelperPopUp;
    
});