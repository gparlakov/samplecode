﻿define([
    'i18n!Mobile/nls/strings',
    'utils',
    'Mobile/ViewModels/Common/HeaderViewModel',
    'text!Mobile/Templates/Common/PanelHeaderWithOverviewLink.html'
], function (strings, utils, headerViewModel, headerWithOverviewLinkTemplate) {

    function HeaderWithOverviewLinkViewModel(headerArgs) {
        headerArgs.headerTemplate = "text!Mobile/Templates/Common/PanelHeaderWithOverviewLink.html";
        headerViewModel.call(this, headerArgs);

        this.navigateToOverview = function () {
            utils.navigate("Overview", { trigger: true });
        };
    };

    return HeaderWithOverviewLinkViewModel;
})