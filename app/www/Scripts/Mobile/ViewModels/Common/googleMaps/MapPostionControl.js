﻿define([
    'jquery',
    'knockout',
    'i18n!Mobile/nls/googleMaps',
    'mapBaseControl',
    'events',

    'text!Mobile/Templates/Common/googleMaps/MapPostionControl.html',

], function ($, ko, googleMaps, baseControl, events, template) {

    function MapPostionControl(map, markersHelper) {

        var self = this;

        baseControl.call(this, template);

        this.map = map;

        

        this.centerMapOnPosition = function (position) {

            var newPosition = new googleMaps.LatLng(position.coords.latitude, position.coords.longitude);

            markersHelper.createMarkerOnMe(newPosition);

            map.setZoom(13);
            map.panTo(newPosition);

        }

        this.centerMapOnMe = function () {
            navigator.geolocation.getCurrentPosition(self.centerMapOnPosition);
        }

        this.create();
    }

    return MapPostionControl;
});