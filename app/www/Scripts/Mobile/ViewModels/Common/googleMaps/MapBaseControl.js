﻿define([
    'jquery',
    'jsrender',
    'utils',
    'events'
    
], function ($, $templates, utils, events) {

    function MapBaseControl(template, data) {

        var self = this;

        self.control = $('<div>');
        self.viewModel = {};
        
        
        this.create = function (forceRefresh) {
            var item = {
                tmplData: template,
                data: data? data: self,
                selector: self.control
            };

            utils.renderTemplate(item);
            utils.applyBinding(item);

            //if (forceRefresh) {
            //    self.control.trigger("create");
            //}
        }
        
        this.render = function () {
            events.trigger(self, 'rendered', { element: self.control[0] });
            return self.control[0];
        }

        this.enableHideOnBodyClick = function () {
            $('body').on('click', this._bodyClickHandler);

            
            self.destroy = function () {
                $('body').off('click', self._bodyClickHandler);
            }
        }


        this._bodyClickHandler = function (e) {

            if (self.visible()) {
                var targetIsInFilter = self.control.find($(e.target));

                //var targetIsInFilter = $(e.target).parents('.branchFilter');
                if (targetIsInFilter.length == 0) {
                    self.hide();
                }
            }
        }

        

    }

    return MapBaseControl;
});