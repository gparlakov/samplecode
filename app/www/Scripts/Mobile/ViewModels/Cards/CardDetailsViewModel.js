define([
  'underscore',
  'backbone',
  'knockout',
  'komapping',
  'services',
  'utils',
  'backbone',
  'productscache',
  'i18n!Mobile/nls/strings',
  'typeVisitor'
], function (_, bakcbone, ko, komapping, services, utils, backbone, productscache, strings, typeVisitor) {

    var CardDetailsViewModel = function (model) {
        
        var self = this;

        _.extend(this, model);

        var apa = productscache.GetAPAccountByID(model.BankCard.BankAccount.BankAccountID);
        if (apa != null)
            this.APAccountShortName = apa.ShortName;

            //vij mapping na online zashto e taka . Premesteno vav View-to . Kriem balansite, ako ne e activna 
        //if (!(this.BankCard.BankCardStatus != null && this.BankCard.BankCardStatus.ID != 0 && !this.BankCard.BankCardStatus.isActive)) {

        productscache.SetBankAccountToBankCard(this.BankCard);
        //}

        self.ProductDetails = {
            heading: { label: strings.ID_STR_CARD_DETAILS },
            details: []
        };
        self.ProductDetails.details.push({ label: strings.ID_STR_TYPE, value: $.GetLocalizedString(self.BankCard.BankCardType.Description) });
        self.ProductDetails.details.push({ label: strings.ID_STR_CARD_NUMBER, value: self.BankCard.BankCardNumber });
        self.ProductDetails.details.push({ label: strings.ID_STR_ACCOUNT, value: self.APAccountShortName });
        self.ProductDetails.details.push({ label: strings.ID_STR_IBAN, value: self.BankCard.BankAccount.IBAN });
        self.ProductDetails.details.push({ label: strings.ID_STR_CCY, value: self.BankCard.CCY.SWIFTCode });
        self.ProductDetails.details.push({ label: strings.ID_STR_EXPIRATION, value: $.format.date(self.BankCard.ExpiryDate, 'MM.yyyy') });

        var hasBalances = self.BankCard.BankAccount && self.BankCard.BankAccount.BankAccountBalances && self.BankCard.BankAccount.BankAccountBalances[0];
        self.CardBalances = {
            heading: { label: strings.ID_STR_TOTAL_AMOUNTS },
            details: []
        };

        typeVisitor.visit(self.BankCard, {
            BankCardCredit: function () {
                if (hasBalances) {
                    self.CardBalances.details.push({ label: strings.ID_STR_CREDIT_LIMIT, value: $.format.number(self.BankCard.BankAccount.BankAccountBalances[0].Overdraft) });
                } else {
                    self.CardBalances.heading.hidden = true;
                }
            },
            BankCardRaiCard: function () {
                if (hasBalances) {
                    self.CardBalances.details.push({ label: strings.ID_STR_CREDIT_LIMIT, value: $.format.number(self.BankCard.BankAccount.BankAccountBalances[0].Overdraft) });
                    self.CardBalances.details.push({ label: strings.ID_STR_AVAILABLE_AMOUNT, value: $.format.number(self.BankCard.BankAccount.BankAccountBalances[0].AvailableBalance) });
                    self.CardBalances.details.push({ label: strings.ID_STR_RAICARD_TOTAL_AMOUNT_DUE, value: $.format.number(self.BankCard.BankAccount.BankAccountBalances[0].ActualBalance) });
                }
                self.CardBalances.details.push({ label: strings.ID_STR_RAICARD_MINPAY, value: $.format.number(self.BankCard.MinPay) });
            },
            BankCardDebit: function () {
                self.ProductDetails.details.push({ label: strings.ID_STR_STATUS, value: $.GetLocalizedString(self.BankCard.BankCardStatus.Description) });
                var isActive = !(self.BankCard.BankCardStatus != null && self.BankCard.BankCardStatus.ID != 0 && !self.BankCard.BankCardStatus.isActive);
                if (isActive && hasBalances) {
                    self.CardBalances.details.push({ label: strings.ID_STR_BALANCE, value: $.format.number(self.BankCard.BankAccount.BankAccountBalances[0].ActualBalance) });
                    self.CardBalances.details.push({ label: strings.ID_STR_AVAILABLE_AMOUNT, value: $.format.number(self.BankCard.BankAccount.BankAccountBalances[0].AvailableBalance) });
                } else {
                    self.CardBalances.heading.hidden = true;
                }
            },
            Other: {
                //?
            }
        });

        this.strings = strings;

        this.showMovements = function showMovements() {
            var link = utils.buildLink(
                  'CardDetails',
                  'Movements',
                   [
                       this.BankCard.BankCardID
                   ]
              );
            utils.navigate(link, { trigger: true });
        }
    };

    return CardDetailsViewModel;
});