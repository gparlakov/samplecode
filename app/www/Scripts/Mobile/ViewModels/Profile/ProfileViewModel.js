﻿define([
  'session',
  'deviceapi',
  'productscache',
  'version',
  'utils',
  'Common/cache/language',
  'Mobile/ViewModels/Common/HeaderViewModel'
], function (session, deviceapi, productscache, version, utils, language, HeaderViewModel) {

    var ProfileViewModel = function ProfileViewModel(model) {
        var self = this;
        self.HeaderViewModel = new HeaderViewModel({ titleStringID: 'ID_STR_PROFILE' })
         
        var pixelRatio = 1;
        if (window.devicePixelRatio)
            pixelRatio = window.devicePixelRatio;

        var width = window.screen.width * devicePixelRatio;
        var height = window.screen.height * devicePixelRatio;

        var size = "{0} x {1}".format(isNaN(width) ? ' ': width, isNaN(height)? ' ' : height);
        self.Items = {
            heading: { hidden: true },
            details: [
                { label: jQuery.GetResString('ID_STR_IDENTIFICATION_CODE'), value: session.GetCredentialID() },
                { label: jQuery.GetResString('ID_STR_USERS_NAME'), value: productscache.GetAPerson().Name },
                { label: jQuery.GetResString('ID_STR_OPERATING_SYSTEM'), value: "{0} {1}".format(deviceapi.device.DevicePlatform, deviceapi.device.DeviceVersion) },
                { label: jQuery.GetResString('ID_STR_DEVICE_MODEL'), value: deviceapi.device.DeviceModel },
                { label: jQuery.GetResString('ID_STR_SCREEN_SIZE'), value: size },
                { label: jQuery.GetResString('ID_STR_APPLICATION_VERSION'), value: version.ClinetApplicationVersion.Version }
            ]
        }
    };

    return ProfileViewModel;
});