﻿define([   
    'jquery',
    'knockout',
    'komapping',
    'services',
    'validator',
    'events',
    'Mobile/ViewModels/OnlineNotifications/OnlineNotificationHelperPopUp'

], function ($, ko, komapping, services, validator, events, notificationPopUpHelper) {
    function OnlineNotificationsListViewModel(filter) {
        var self = this;

        validator.validatableModel.call(this, function () {
            return {
                properties: {}
            };
        });

        this.Filter = komapping.fromJS(filter);
        this.GetNonobservableFilter = function () {
            return komapping.toJS(self.Filter);
        }

        this.Notifications = ko.observableArray();

        this.NotificationPopUpHelper = new notificationPopUpHelper(self, {});

        this.initialize = function () {
            var loadingPromise = self.find();

            events.one(self, 'rendered', function () {
                $.whenEx(
                    loadingPromise
                )
                .done(function () {
                    $(window).daisScrollV2('register', self.findMore);
                    events.bind(self, "findresulsfinished", function () {
                        $(window).daisScrollV2('reset', self.findMore);
                    })
                })
            });
        }

        this.pipe = $.DeferredEx();
        this.pipe.resolve();


        this.find = function () {
            if (validator.getValidator(self).validate()) {
                self.Filter.Paging.CurrentPage(1);
                var promise =
                    this._findInternal(true)
                    .done(function () {
                        events.trigger(self, "findresulsfinished");
                    });

                return promise;
            }
        }

        this.findMore = function () {

            var def = new $.DeferredEx(function (def) {
                if (self.Filter.Paging.TotalPages() > self.Filter.Paging.CurrentPage() && validator.getValidator(self).validate()) {

                    self.Filter.Paging.CurrentPage(self.Filter.Paging.CurrentPage() + 1);
                    def.bindTo(self._findInternal(false));
                }
                else {
                    def.fail();
                }
            });
            return def;
        }


        this._findInternal = function (shouldClearOldResults) {

            var res = new $.DeferredEx();

            function action() {
                var onlineNotifService = new services.Services.OnlineNotificationsService();
                var actionres =                
                        onlineNotifService.GetOnlineNotifications(self.GetNonobservableFilter(), function (data) {

                        if (shouldClearOldResults) {
                            self.Notifications.removeAll();
                        }

                        for (var notif in data.OnlineNotifications) {
                            self.Notifications.push(data.OnlineNotifications[notif]);
                        }

                        komapping.fromJS(data.NotificationsFilter, self.Filter);
                    });

                res.bindTo(actionres);

                return actionres;
            }

            self.pipe.pipe(action);

            return res;
        }

        this.findresulsfinished = function () {

        }

        this.unload = function () {
            $(window).daisScrollV2('unregister', self.findMore);
        }

        this.showNotification = function (notification) {
            // will check if all data is is set and won't call to server if it is
            self.NotificationPopUpHelper.setNotification(notification);

            // set data to list item
            self.NotificationPopUpHelper.Open()            
                .done(function (data) {  
                    var length = self.Notifications().length;                    
                    for (var i = 0; i < length; i++) {                     
                        if (self.Notifications()[i].ID === data.OnlineNotification.ID) {
                          
                            self.Notifications.replace(self.Notifications()[i], data.OnlineNotification);
                            break;
                        }
                    }                    
                });
        }
    }

    return OnlineNotificationsListViewModel;
});