﻿define([
    'knockout',
    'jquery',
    'underscore',
    'productscache',
    'services',
    'validator',
    'Common/cache/language',
    'utils',

    'Mobile/ViewModels/Common/HeaderViewModel',
    'Mobile/ViewModels/OnlineNotifications/Helpers/ServiceTypeOptionsHelper',
    'Mobile/ViewModels/OnlineNotifications/Helpers/ValidatorHelper',
    'Mobile/ViewModels/Common/BankClientHelperPopUp',
    'Mobile/ViewModels/Common/AccountsHelperPopUp'

], function (ko, $, _, productscache, services, validator, language, utils,
     headerViewModel, ServiceTypeOptionsHelper, ValidatorHelper, BankClientHelperPopUp, AccountsHelperPopUp) {

    function OnlineNotificationsNewSubscription(bankCorrespondent) {

        var self = this;
        self.BankCorrespondent = bankCorrespondent;

        this.ProductType_Account = 1; // notification service product type
        this.ProductType_BankClient = 5; // notification service product type

        this.HeaderViewModel = new headerViewModel({ titleStringID: "ID_STR_ONLINE_NOTIF_NEW_SUBSCRIPTION" });
        this.Services = ko.observableArray();
        this.SelectedService = ko.observable();
        this.SelectedAccount = ko.observable();
        this.SelectedBankClient = ko.observable();
        //this.isValid = ko.observable(false);

        var mapServiceObservable = function (service) {
            var serv = {};
            _.extend(serv, service)
            serv.ServiceNameLocalized = $.GetLocalizedString(service.ServiceName);
            if (service.Options) {
                serv.Options.AvailableOptions = ServiceTypeOptionsHelper.GetOptionsData(service.Options);
                ServiceTypeOptionsHelper.SetOptionsEmptyValueArrays(service.Options);
            }
            self.Services.push(serv);
        };

        this.initialize = function () {

            ValidatorHelper.intialize(self.ProductType_Account, self.ProductType_BankClient);
            ValidatorHelper.setInitialValidation(self);

            services.OnlineNotificationsService.GetOnlineNotificationServices()
                .done(function (data) {
                    _.each(data.OnlineNotificationServices, function (service) {
                        mapServiceObservable(service);
                    });
                });
        }       

        this.SelectedServiceHasOptions = ko.pureComputed(function () {
            return self.SelectedService() != null && self.SelectedService().Options != null;
        });

        this.SelectedService.subscribe(function (selectedService_newValue) {
            ValidatorHelper.setValidationsOnServiceChange(self, selectedService_newValue);
        });

       
        this.BankClientHelperPopUp = new BankClientHelperPopUp(
            self,
            {
                load: function () {
                    var res = $.DeferredEx(function (res) {
                        res.resolve(productscache.GetAPerson().APBankClients);
                    });

                    return res;
                },
                selectedBankClient: self.SelectedBankClient()
            });

        this.SelectBankClient = function () {
            self.BankClientHelperPopUp.Open()
                .done(function (selectedBankClient) {
                    self.SelectedBankClient(selectedBankClient);                   
                })
        };

        this.AccountsHelperPopUp = new AccountsHelperPopUp(
            self,
            {
                load: function () {
                    var res =
                        services.BankProductInfoService.GetProductBalances(
                            new services.Objects.BankAccountBalancesFilterCurrent(
                                { Paging: { CurrentPage: -1, ResultsForPage: -1 } })
                        ).then(function () {
                            return productscache.GetAPerson().APAccounts;
                        });

                    return res;
                },
                showOptionAll: false
            }
        );


        this.SelectAccount = function () {
            self.AccountsHelperPopUp
                .Open()
                .done(function (account) {
                    if (account && account.BankAccount) {
                        self.SelectedAccount(account.BankAccount);                       
                    } else {
                        self.SelectedAccount(null);
                    }
                });
        }

        this.saveSubscription = function () {
            var mainViewModelIsValid = validator.getValidator(self).validate();
            
            if (self.SelectedServiceHasOptions()) {               
                mainViewModelIsValid = mainViewModelIsValid &&
                    validator.getValidator(self.SelectedService().Options).validate();
            }

            if (mainViewModelIsValid) {

                var bankProduct = null
                if (self.SelectedService().ProductType == self.ProductType_Account) {
                    bankProduct = new services.Objects.BankProductBase({
                        REFID: self.SelectedAccount().BankAccountID,
                        ProductType: self.SelectedService().ProductType,
                        BankClient: new services.Objects.BankClientKey(
                        {
                            BankClientId : self.SelectedAccount().BankClient.BankClientID
                        })
                    })
                }; 
                

                if (self.SelectedService().ProductType == self.ProductType_BankClient) {
                    bankProduct = new services.Objects.BankProductBase({
                        REFID: self.SelectedBankClient().BankClientID,
                        ProductType: self.SelectedService().ProductType,
                        BankClient: new services.Objects.BankClientKey(
                        {
                            BankClientId: self.SelectedBankClient().BankClientID
                        })
                    })
                }
                
                var options = null;
                if (self.SelectedServiceHasOptions()) {
                    options = self.SelectedService().Options;                    
                }

                services.OnlineNotificationsService.RetreiveBankCorrespondentForAP()
                    .then(function (bankCorrespondentResponse) {
                        var newSubscription = new services.Objects.Subscription({
                            BankCorrespondent: bankCorrespondentResponse.BankCorrespondent,
                            NotificationService: new services.Objects.OnlineNotificationServiceBase(self.SelectedService()),
                            BankProduct: bankProduct,
                            Language: language.GetCurrentLang().id,
                            Options: options
                        });

                        return services.OnlineNotificationsService.SaveNewCorrespondentSubscription(newSubscription);
                    })
                    .done(function (response) {

                        // todo add response fail 
                        var link = utils.buildLink(
                                  'OnlineNotifications',
                                  'subsriptionsList'
                                  );
                        utils.navigate(link, { trigger: true });
                    });
                
            }
        };
    }

    return OnlineNotificationsNewSubscription;
});