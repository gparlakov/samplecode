﻿define([
    'underscore',
    'validator',
    'i18n!Mobile/nls/strings',    
    'Mobile/ViewModels/OnlineNotifications/Helpers/ServiceTypeOptionsHelper',
], function (_, validator, strings, ServiceTypeOptionsHelper) {

    var ValidatorHelper = {};        
    var ProductType_Account;
    var ProductType_BankClient;

    ValidatorHelper.intialize = function (productType_Account, productType_BankClient) {
        ProductType_Account = productType_Account;
        ProductType_BankClient = productType_BankClient

        addToValidator_NotNullOrEmptyFromArray();
    }; 

    function addToValidator_NotNullOrEmptyFromArray() {
        validator.methods.add({
            NotNullOrEmptyFromArray: {
                method: function (value, parameters) {
                    if (!$.isArray(value)) {
                        return false;
                    }
                    var val = value.join(',')
                    return !parameters || (val != null && val.trim() != '');
                },
                message: strings.ID_STR_ONLINE_NOTIF_SELECT_OPTION
            }
        });
    }

    var validatorSelectedService = {
        properties: {
            "SelectedService": {
                rules: {
                    NotNullOrEmpty: true
                }
            },
        }
    };

    var validatorSelectedBankClient = {
        properties: {
            "SelectedBankClient": {
                rules: {
                    NotNullOrEmpty: true
                }
            },
        }
    };

    var validatorSelectedAccount = {
        properties: {
            "SelectedAccount": {
                rules: {
                    NotNullOrEmpty: true
                }
            },
        }
    };

    ValidatorHelper.setInitialValidation = function (viewModel) {
        validator.validatableModel.call(viewModel, function () { return validatorSelectedService; });
    }

    ValidatorHelper.setValidationsOnServiceChange = function (viewModel, selectedService) {
        // reset validation to only service type 
        ValidatorHelper.setInitialValidation(viewModel);

        if (selectedService && selectedService.ProductType == ProductType_Account) {
            validator.validatableModel.call(viewModel, function () {
                var validator = {};

                _.extend(validator, validatorSelectedService);
                _.extend(validator, validatorSelectedAccount);

                return validator;
            });
        }

        if (selectedService && selectedService.ProductType == ProductType_BankClient) {
            validator.validatableModel.call(viewModel, function () { 
                var validator = {};

                _.extend(validator, validatorSelectedService);
                _.extend(validator, validatorSelectedBankClient);

                return validator;
            });
        }

        if (selectedService && selectedService.Options) {
            var validationOptions = ServiceTypeOptionsHelper.GetOptionsValidatior(selectedService.Options);
            validator.validatableModel.call(selectedService.Options, validationOptions);
        }
    }

    return ValidatorHelper;
});