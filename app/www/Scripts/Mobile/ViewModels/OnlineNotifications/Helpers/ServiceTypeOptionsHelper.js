﻿define([
    'typeVisitor',
    'servicesObjects',
    'validator',
    'underscore',
    'knockout'
], function (visitor, servicesObjects, validator, _, ko) {

    var dayMultiLanguageString = [{ Lang: 1, Value: 'ден' }, { Lang: 2, Value: 'day' }]
    var daysMultiLanguageString = [{ Lang: 1, Value: 'дни' }, { Lang: 2, Value: 'days' }];

    var hours = [
        { Value: 9, Text: '9:00h' },
        { Value: 13, Text: '13:00h' },
        { Value: 17, Text: '17:00h' }, ];

    var currencies = [
        { Value: 'EUR', Text: 'EUR' },
        { Value: 'USD', Text: 'USD' },
        { Value: 'CHF', Text: 'CHF' },
        { Value: 'GBP', Text: 'GBP' }];

    var days = [
        { Value: 1, Text: '1 ' + $.GetLocalizedString(dayMultiLanguageString) },
        { Value: 3, Text: '3 ' + $.GetLocalizedString(daysMultiLanguageString) },
        { Value: 5, Text: '5 ' + $.GetLocalizedString(daysMultiLanguageString) }, ];


    var optionTypes = {
        AmountLimitOptions: function () {
            return new servicesObjects.AmountLimitOptions();
        },
        CurrencyRateOptions: function () {
            return new servicesObjects.CurrencyRateOptions({ MessageHours: hours, CurrencyCode: currencies });
        },
        FutureInstallmentOptions: function () {

            return new servicesObjects.FutureInstallmentOptions({ DaysPriorToWarning: days });
        },
        CurrentBalanceOptions: function () {

            return new servicesObjects.CurrentBalanceOptions({ MessageHours: hours });
        },
        Other: function () {
            throw "Unknown options type";
        }
    };

    var optionTypesValidatorOptions = {
        AmountLimitOptions: function () {
            return {
                properties: {
                    "Limit": {
                        rules: {
                            NotNull: true,
                            RegEx: validator.RegExUtils.Amount
                        }
                    },
                }
            };
        },
        CurrencyRateOptions: function () {
            return {
                properties: {
                    "MessageHours": {
                        rules: {
                            NotNullOrEmptyFromArray: true
                        }
                    },
                    "CurrencyCode": {
                        rules: {
                            NotNullOrEmptyFromArray: true
                        }
                    },
                }
            };
        },
        FutureInstallmentOptions: function () {
            return {
                properties: {
                    "DaysPriorToWarning": {
                        rules: {
                            NotNullOrEmptyFromArray: true
                        }
                    },
                }
            };
        },
        CurrentBalanceOptions: function () {
            return {
                properties: {
                    "MessageHours": {
                        rules: {
                            NotNullOrEmptyFromArray: true
                        }
                    }
                }
            };
        },
        Other: function () {
            throw "Unknown options type";
        }
    };

    var optionTypesEmptyValueArrays = {
        AmountLimitOptions: function () {
            return null;
        },
        CurrencyRateOptions: function () {
            return {
                MessageHours: [],
                CurrencyCode: []
            };
        },
        FutureInstallmentOptions: function () {
            return {
                DaysPriorToWarning: [],
            };
        },
        CurrentBalanceOptions: function () {
            return {
                MessageHours: [],
            };
        },
        Other: function () {
            throw "Unknown options type";
        }
    };

    ServiceTypeOptionsHelper = {
        GetOptionsData: function (options) {
            if (options && options.$type) {
                return visitor.visit(options, optionTypes);
            }
        },
        GetOptionsValidatior: function (options) {
            if (options && options.$type) {
                return visitor.visit(options, optionTypesValidatorOptions);
            }
        },
        SetOptionsEmptyValueArrays: function (options) {
            if (options && options.$type) {
                var optionsEmptyValueArrays = visitor.visit(options, optionTypesEmptyValueArrays);
                if (optionsEmptyValueArrays) {
                    _.extend(options, optionsEmptyValueArrays);
                }
            }
        }
    }

    return ServiceTypeOptionsHelper;
})