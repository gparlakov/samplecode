﻿define([
    'knockout',
    'Mobile/ViewModels/OnlineNotifications/OnlineNotificationViewModel',
    'Mobile/ViewModels/Base/PopupPageViewModel',
], function (ko, onlineNotificationViewModel, PopupPageViewModel) {

    function OnlineNotificationHelperPopUp(parent, options) {
        var self = this;

        PopupPageViewModel.apply(this, arguments);

        this.path = "OnlineNotifications/OnlineNotificationHelperPopUpTemplate";       
        
        this.notificationVM = options.notification != null ? new onlineNotificationViewModel(options.notification) : null;
        this.setNotification = function (notification) {
            self.notificationVM = new onlineNotificationViewModel(notification);
        }

        this.NotificationViewModel = ko.observable();

        this.loadData = function () {

            if (self.notificationVM.FullDataIsSet) {

                self.NotificationViewModel(self.notificationVM.Notification);

                var res = new $.DeferredEx(function () {
                    return self.notificationVM;
                });
                
                res.resolve();

                return res;
            }

            else {

                var res = self.notificationVM.getNotification()
                    .done(function (notification) {
                        self.NotificationViewModel(notification.OnlineNotification);
                        self["return"](notification);
                        //res.resolve(notification);
                    });

                return res;
            }
        }        
    }

    return OnlineNotificationHelperPopUp;
});