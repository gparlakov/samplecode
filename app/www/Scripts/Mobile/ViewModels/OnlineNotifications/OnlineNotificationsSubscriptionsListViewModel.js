﻿define([   
    'jquery',
    'knockout',
    'services',
    'Mobile/ViewModels/Repository/SubscriptionsActionButtons',
    'servicesObjects',
    'underscore',
    'Common/ufnomenclatures/UFNmFactory',
    'utils',   

], function ($, ko, services, subscriptionsActionButtons, servicesObjects, _, ufNomenclatureFactory, utils) {
    var OnlineNotificationsListViewModel = function () {
        var self = this;

        this.Subscriptions = ko.observableArray();
        this.Correspondent = null;
        this.ServiceTypesDictionary = {};

        this.initialize = function () {                            

            var retreiveSubscriptionsRequest = services.OnlineNotificationsService.RetreiveBankCorrespondentForAP()
                .pipe(function (data) {
                    self.Correspondent = data.BankCorrespondent;

                    return services.OnlineNotificationsService.RetrieveSubscriptions(data.BankCorrespondent);
                });

            var ufFactory = new ufNomenclatureFactory();
            var serviceTypesNomeclatureRequest = ufFactory
                .GetUfNomenclature(ufFactory.ufNomenclatureTypes.NomenclatureOnlineNotificationServiceTypes)
                .done(mapServiceTypesDictionary);
           
            $.whenEx(
                retreiveSubscriptionsRequest,
                serviceTypesNomeclatureRequest
                )
                .done(function (data, serviceTypesNomeclature) {
                    _.each(data.Subscriptions, function(subscription) 
                    {
                        var mappedSubscription = mapSubscription(subscription);
                        self.Subscriptions.push(mappedSubscription);
                    })
                });
        };

        var mapSubscription = function (subscription) {

            var subscr = {};
            _.extend(subscr, subscription);
       
            subscr.ServiceTypeName = self.ServiceTypesDictionary[subscr.NotificationService.ServiceID];

            subscr.Status = ko.observable(subscription.Status);
            subscr.IsActive = ko.pureComputed(function () {
                return subscr.Status() == servicesObjects.EnumNotificationCorrespondentSubscriptionStatus.Active;
            });

            subscr.actionButtons = [];
            subscr.actionButtons.push(
                new subscriptionsActionButtons.ActivateSubscription(subscr));

            subscr.actionButtons.push(
                 new subscriptionsActionButtons.DeactivateSubscription(subscr));

            return subscr;
        };

        var mapServiceTypesDictionary = function (serviceTypesNomeclature) {
            var nomenclatureServTypes = serviceTypesNomeclature.GetNomenclature();
            _.each(nomenclatureServTypes, function (serviceType) {
                self.ServiceTypesDictionary[serviceType.ServiceID] = $.GetLocalizedString(serviceType.ServiceName);
            });
        }


        this.newSubscription = function () {
            var link = utils.buildLink(
                            'OnlineNotifications',
                            'newSubscription'
                        );
            utils.navigate(link, { trigger: true });
        }
    }

    return OnlineNotificationsListViewModel;
});