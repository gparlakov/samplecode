﻿define([
  'jquery',
  'underscore',
  'i18n!Mobile/nls/googleMaps',
  'services',
  'servicesObjects',
  'events',
  'utils',
  'Mobile/ViewModels/Common/googleMaps/BoundsHelpers',
  'Mobile/ViewModels/Common/googleMaps/MarkersHelper',
  'Mobile/ViewModels/Common/googleMaps/AddressSearchControl',
  'Mobile/ViewModels/Common/googleMaps/MapPostionControl',
  'Mobile/ViewModels/Common/HeaderViewModel',
  'Mobile/ViewModels/BankBranches/CustomMapTypeControl',
  'Mobile/ViewModels/BankBranches/BankStructureFilterControl'

], function ($, _, googleMaps, services, servicesObjects, events, utils,
             BoundsHelper, MarkersHelper, AddressSearchControl, MapPostionControl, headerViewModel, CustomMapType, BankStructureFilter) {

    var mapTypeID = 'mapBranches';
    var mapDefaults = {
        
        styles : [
        {
            elementType:"geometry",
            featureType:"administrative.province",
            stylers:[
           { saturation: 75 }
            ]
        },
        {
            elementType:"geometry",
            featureType:"administrative.country",
            stylers:[
            {invert_lightness: true}
            ]
        }],

        styledMapOptions:{
            name : 'Branches'
        },

        mapOptions :{
            zoom: 13,
            center: new googleMaps.LatLng(42.6982708, 23.3213718),
            streetViewControl: false,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP

            //mapTypeControlOptions: {
            //    mapTypeIds: [google.maps.MapTypeId.ROADMAP],
            //    style:googleMaps.MapTypeControlStyle.DEFAULT
            //}
            //mapTypeId: mapTypeID,
            //,navigationControlOptions: {
            //    style: googleMaps.NavigationControlStyle.DEFAULT
            //}
        },

    }

    function BankBranchesViewModel() {

        var self = this;
        self.HeaderViewModel = new headerViewModel({ titleStringID: "ID_STR_BRANCHES" });
        map = null;

        var boundsHelper = new BoundsHelper();
        var markersHelper = null; // initva se, chak kogato se narisuva kartata
        
        var mapElement = null;

        this.rendered = function () {

            mapElement = $('#bankBranches');
            
            map = new googleMaps.Map(mapElement[0], mapDefaults.mapOptions);
            map.setOptions({ styles: mapDefaults.styles });

            markersHelper = new MarkersHelper(map, this);

            var mapPositionControl = new MapPostionControl(map, markersHelper);
            mapPositionControl.centerMapOnMe();
            //navigator.geolocation.getCurrentPosition(self.centerMapOnMe);


            setTimeout(function () {
                //console.log('1 ' + $('#mainPage').height());
                //console.log('2 ' + $('body').height());
                //mahame padding-a na stranicata za da moje kartata da zaeme celiq ekran 
                // i da nqma scroll 
                //utils.getCurrentPageContainer().css('padding-top', 0);
                mapElement.height($('#mainPage').height());
                //mapElement.offset({ top: 0 });

                googleMaps.event.trigger(map, 'resize');

            }, 0);

            var addressSearch = new AddressSearchControl(map);

            self.customMapType = new CustomMapType(map);
            events.bind(self.customMapType, 'mapTypeChanged', self.mapTypeChanged);
            events.bind(addressSearch, addressSearch.eventTypes.showSearch, self.customMapType.hide);
            events.bind(addressSearch, addressSearch.eventTypes.hideSearch, self.customMapType.show);

            self.bankStructureFilter = new BankStructureFilter(map);
            //abonirame filter za klonove i atm-a da se skriva i pokazva pri cakane varhu butonite za na kartata
            self.bankStructureFilter.bind('toggleFilter', self.customMapType, self.customMapType.eventTypes.CURRENT_MAP_TYPE_CLICKED_EVENT_NAME);
            events.bind(addressSearch, addressSearch.eventTypes.hideSearch, self.bankStructureFilter.show);

            events.bind(self.bankStructureFilter, 'filterValuesChanged', self._filterBankStructures);
            //events.bind(self.customMapType, 'mapTypeChanged', self.bankStructureFilter.changeFilterType);

            map.controls[googleMaps.ControlPosition.RIGHT_BOTTOM].push(mapPositionControl.render());

            map.controls[googleMaps.ControlPosition.TOP_CENTER].push(self.customMapType.render());
            map.controls[googleMaps.ControlPosition.CENTER].push(self.bankStructureFilter.render());
            map.controls[googleMaps.ControlPosition.TOP_RIGHT].push(addressSearch.render());
            
        },

        //this.centerMapOnMe = function(position){

        //    var newPosition = new googleMaps.LatLng(position.coords.latitude, position.coords.longitude);

        //    markersHelper.createMarkerOnMe(newPosition);

        //    map.setZoom(13);
        //    map.panTo(newPosition);

        //}

        this.mapIdleListener = null;
        
        this.mapTypeChanged = function (maptype) {

            if (self.mapIdleListener) {
                googleMaps.event.removeListener(self.mapIdleListener);
            }

            if (maptype == self.customMapType.mapTypeButtons.Branches) {

                self.findBranches(true);
                self.bankStructureFilter.showBranchesFilter();

                self.mapIdleListener = googleMaps.event.addListener(map, 'idle', self.findBranches);

            } else if (maptype == self.customMapType.mapTypeButtons.ATMs) {

                self.findAtms(true);
                self.bankStructureFilter.showATMsFilter();

                self.mapIdleListener = googleMaps.event.addListener(map, 'idle', self.findAtms);
            }
        }

        var allMarkers = {};

        $(window).bind('resize', function (e) {
           
            mapElement.height(window.innerHeight); // moje da se zameni s $.mobile.ScreeHeight()
            //mapElement.offset({ top: 0 });
            googleMaps.event.trigger(map, 'resize');// }, 0);
        });

        this.creatMarker = function (id, lat, lng, infoWindowText) {

            if (typeof(allMarkers[id]) == 'undefined'){

                var latlngCity = new googleMaps.LatLng(lat, lng);
                var marker = new googleMaps.Marker({
                    map: map,
                    position: latlngCity,
                    title: "",
                    clickable: true
                    //, icon: '/images/logo_small.jpg'
                });

                infowindow = new googleMaps.InfoWindow({
                    content: infoWindowText
                });

                //TODO - da se zakachi info window-a na eventi na markera 
                allMarkers[id] = marker; 
            }
        }

        this.findBranches = function (clearMarkers) {
            if (clearMarkers) {
                markersHelper.clearMarkers();
                boundsHelper.clearBounds();
            }

            self._findInternal(servicesObjects.BankBranchesBoundsFilter, services.BankStructureItemsService.GetBankBranchesInBounds);
        }

        this.findAtms = function (clearMarkers) {

            if (clearMarkers) {
                markersHelper.clearMarkers();
                boundsHelper.clearBounds();
            }

            self._findInternal(servicesObjects.ATMsBoundsFilter, services.BankStructureItemsService.GetATMsInBounds);
        }

        this._findInternal = function (rqclass, findFunc) {

            var bounds = map.getBounds();

            if (boundsHelper.isVisited(bounds)) {
                return ;
            }
            var ne = bounds.getNorthEast();
            var sw = bounds.getSouthWest();
            
            var intObj = {
                //LatLngBoundsFilter: new servicesObjects.LatLngBounds({
                NorthEast: new servicesObjects.LatLngPoint({
                    Latitude: ne.lat(),
                    Longitude: ne.lng(),
                }),
                SouthWest: new servicesObjects.LatLngPoint({
                    Latitude: sw.lat(),
                    Longitude: sw.lng(),
                })
                //})
            };
            var reqObj = new rqclass(intObj);

            $.when(
                findFunc.call(intObj, intObj)
            ).then(function (data) {

                var filter = self.bankStructureFilter.GetFilter();

                for (bankStruct in data.BankStructureItems) {
                    var bankBranch = data.BankStructureItems[bankStruct];
                    
                    var showOnMap = self._allServicesSupported(bankBranch);
                    
                    markersHelper.creatMarker(bankBranch, !showOnMap);

                }
                boundsHelper.visit(bounds);
            });
        }

        this._allServicesSupported = function (structure) {
            var allServicesSuppoted = _.all(filter.OfferedBankServices, function (filterService) {
                return _.any(structure.OfferedBankServices, function (structService) {
                    return structService.BankService.ID == filterService.BankService.ID;
                });
            });

            var allAdditonal = true;
            for (var prop in filter.AdditionalProps) {
                allAdditonal &= structure.hasOwnProperty(prop) && structure[prop] == filter.AdditionalProps[prop];
            }

            return allServicesSuppoted && allAdditonal;
        }
        this._filterBankStructures = function (filter) {
            markersHelper.showOnly(self._allServicesSupported);
        }

        this.unload = function () {
            self.bankStructureFilter.destroy();
        }
    };

    return BankBranchesViewModel;
});