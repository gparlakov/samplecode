define([
    'underscore',
    'knockout',
    'servicesObjectsUtils',
    'dateUtils'
], function (_, ko, utils, dateUtils) {
    var utils = {
        getFullName: function (name) {
            //return "DAIS.eBank.Contracts." + name + ", DAIS.eBank.Contracts.Extended";
            return name;
        },

        copyProps: function (source, destination)
        {
            var commonProps = _.intersection(Object.getOwnPropertyNames(destination), Object.getOwnPropertyNames(source));

            _.each(commonProps, function (propName) {
                if (!_.contains(utils.systemProps, propName)) {
                    destination[propName] = source[propName];
                }
            });
        },

        systemProps: ["$type"],

        GetShortTypeNameByType: function (strType) {
            //if (strType != null) {
            //    var t = strType.split(',');
            //    if (t[0] != null) {
            //        var splittedType = t[0].split('.');
            //        resultType = splittedType[splittedType.length - 1];
            //    }
            //    return resultType;
            //}
            //return null;
            return strType;
        },

        GetShortTypeNameByObject: function (contractObject) {
            var unwrapedType = ko.utils.unwrapObservable(contractObject["$type"]);
            return this.GetShortTypeNameByType(unwrapedType);
        }
    }

    var Objects = {
        DiagnosticsError: function (copyFrom) {
            this.$type = utils.getFullName("DiagnosticsError");

            this.Session = null;
            this.AdditionalUserData = null;
            this.Message = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        AdditionalUserDataMobileDevice: function (copyFrom) {
            this.$type = utils.getFullName("AdditionalUserDataMobileDevice");

            this.UserDeviceID = null;
            this.UserDeviceName = null;
            this.UserDevicePlatform = null; 
            this.UserDeviceVersion = null;
            this.UserDeviceModel = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        AntiBotProtectionCaptcha: function (copyFrom) {
            this.$type = utils.getFullName("AntiBotProtectionCaptcha");

            this.CaptchaKey = null;
            this.CaptchaValue = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        CountryKey: function (copyFrom) {
            this.$type = utils.getFullName("CountryKey");

            this.Code = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        Paging: function (copyFrom) {
            this.$type = utils.getFullName("Paging");

            this.CurrentPage = null;// BankAccountKey
            this.ResultsForPage = 10;
            this.TotalPages = 0;
            this.TotalResults = 0;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },


        BankAccountKey: function (copyFrom) {
            this.$type = utils.getFullName("BankAccountKey");

            this.BankAccountID = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentKey: function (copyFrom) {
            this.$type = utils.getFullName("PaymentKey");

            this.ID = null;
            this.PaymentIncarnationType = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        SignablePayment: function (copyFrom) {
            this.$type = utils.getFullName("SignablePayment");

            this.ID = null;
            this.PaymentIncarnationType = null;
            this.Digest = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        AuthorizationDataPayments: function(copyFrom) {
            this.$type = utils.getFullName("AuthorizationDataPayments");

            //SignablePayments
            this.Payments = null;
                
            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        BeneficiaryKey: function (copyFrom)
        {
            this.$type = utils.getFullName("BeneficiaryKey");

            this.ID = null;
            
            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        BeneficiaryInput: function (copyFrom)
        {
            this.$type = utils.getFullName("BeneficiaryInput");

            this.VisibleForAll = null;
            this.BeneficiaryName = null;
            this.BeneficiaryAddress = null;
            this.BeneficiaryGroup = null;
            this.BankClient = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        BeneficiaryAccountKey: function(copyFrom)
        {
            this.$type = utils.getFullName("BeneficiaryAccountKey");

            this.ID = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        BeneficiaryAccountHomeCountryInput: function(copyFrom)
        {
            this.$type = utils.getFullName("BeneficiaryAccountHomeCountryInput");

            this.Name = null;
            this.CCY = null;
            this.Beneficiary = null;

            this.IBAN = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        BeneficiaryAccountForeignCountryInput: function (copyFrom) {
            this.$type = utils.getFullName("BeneficiaryAccountForeignCountryInput");

            this.Name = null;
            this.CCY = null;
            this.Beneficiary = null;

            this.AccountNumber = null;
            this.BankName = null;
            this.BankAddress = null;
            this.BankCity = null;
            this.BankSWIFT = null;
            this.BankCode = null;
            this.BankCountry = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentWizardPayeeNewBeneficiary: function (copyFrom) {
            this.$type = utils.getFullName("PaymentWizardPayeeNewBeneficiary");

            this.Beneficiary = null;
            this.Account = null;
            
            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentWizardPayeeBankAccount: function (copyFrom) {
            this.$type = utils.getFullName("PaymentWizardPayeeBankAccount");

            this.BankAccount = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentWizardChoosePayee: function (copyFrom) {
            this.$type = utils.getFullName("PaymentWizardChoosePayee");

            this.State = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentWizardChoosePayeeInput: function (copyFrom) {
            this.$type = utils.getFullName("PaymentWizardChoosePayeeInput");

            //unchanged payment state
            this.State = null;
            this.Payee = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentWizardInitDataNewPayment: function(copyFrom)  {
            this.$type = utils.getFullName("PaymentWizardInitDataNewPayment");

            this.PaymentIncarnation = null;
            this.BankAccount = null; //bankaccountkey

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentWizardInitDataCreateLikePayment: function (copyFrom) {
            this.$type = utils.getFullName("PaymentWizardInitDataCreateLikePayment");

            this.PaymentIncarnation = null;
            this.Payment = null; //PaymentKey

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentWizardInitDataEditPayment: function (copyFrom) {
            this.$type = utils.getFullName("PaymentWizardInitDataEditPayment");

            this.Payment = null; //PaymentKey

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentWizardChoosePayeeDataAPAccountsFilter: function (copyFrom) {
            this.$type = utils.getFullName("PaymentWizardChoosePayeeDataAPAccountsFilter");

            this.State = null;
            this.SameBankClientAsPayer = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentWizardChoosePayeeDataBeneficiaryAccountsFilter: function (copyFrom) {
            this.$type = utils.getFullName("PaymentWizardChoosePayeeDataBeneficiaryAccountsFilter");

            this.State = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentWizardChoosePayerAndAmount: function (copyFrom) {
            this.$type = utils.getFullName("PaymentWizardChoosePayerAndAmount");

            this.State = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentWizardSummary: function (copyFrom) {
            this.$type = utils.getFullName("PaymentWizardSummary");

            this.State = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentWizardFillDetails: function (copyFrom) {
            this.$type = utils.getFullName("PaymentWizardFillDetails");

            this.State = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentWizardFillDetailsInput: function (copyFrom) {
            this.$type = utils.getFullName("PaymentWizardFillDetailsInput");

            this.State = null;
            this.Details = null;
            this.Incarnation = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        PaymentWizardChoosePayerAndAmountInput: function (copyFrom) {
            this.$type = utils.getFullName("PaymentWizardChoosePayerAndAmountInput");

            this.State = null;
            this.Payer = null;
            this.Amount = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        EnumPaymentWizardOperation: {
            Save: 1,
            SaveNew: 2
        },

        NomenclatureNOIParagraphFilterByIBAN: function (copyFrom) {
            this.$type = utils.getFullName("NomenclatureNOIParagraphFilterByIBAN");

            this.IBAN = null;
            this.Type = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        NOIParagraphKey : function (copyFrom){
            this.$type = utils.getFullName("NOIParagraphKey");

            this.ID = null;
            if (copyFrom) {utils.copyProps(copyFrom, this);}
        },

        OpCodeKey: function (copyFrom) {
            this.$type = utils.getFullName("OpCodeKey");

            this.Code = null;
            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        TemplatesFilter :function (copyFrom) {
            this.$type = utils.getFullName("TemplatesFilter");

            this.BankAccount = null;// BankAccountKey
            this.TemplateNameContains = null;
            this.Paging = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },
        
        BankAccountBalancesFilterCurrent: function (copyFrom) {
            this.$type = utils.getFullName('BankAccountBalancesFilterCurrent');

            this.Paging = null;
            this.Period = null;
            this.BankAccount = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },
        
        AccountMovementsFilterForPeriod :function(copyFrom) {
            this.$type = utils.getFullName('BankAccountMovementsFilterForPeriod');
            
            this.Paging = new Objects.Paging();

            this.Period = new Objects.Period();
            this.MovementType = '';

            //this.BankAccount = { BankAccountID: accountID } // neshto takova 
            this.BankAccount = null;
            
            if (copyFrom) { utils.copyProps(copyFrom, this); }
            
        },
    
        PaymentOrdersFilter: function (copyFrom) {
            
            this.$type = utils.getFullName('PaymentOrdersFilter');

            this.Paging = new Objects.Paging();
            this.CreatePeriod = new Objects.Period();
            this.PaymentOrderType = null;
         
            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },
        
        BankCardKey: function (copyFrom) {
            this.$type = utils.getFullName("BankCardKey");

            this.BankCardID = null;
            if (copyFrom) { utils.copyProps(copyFrom, this); }

        },

        BankCardMovementsFilterForPeriod: function (copyFrom) {
            this.$type = utils.getFullName("BankCardMovementsFilterForPeriod");

            this.BankCard = null;
            this.Period = null;
            this.Paging = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }

        },

        Period : function (copyFrom) {

            this.DateStart = dateUtils.monthStart();
            this.DateEnd = dateUtils.today();

            if (copyFrom) {
                if (copyFrom) { utils.copyProps(copyFrom, this); }
            }
        },

        BankAccountMovementsBlockUnblockFilterCurrent: function (copyFrom) {
            this.$type = utils.getFullName("BankAccountMovementsBlockUnblockFilterCurrent");

            this.BankAccount = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        BankAccountMovementDocumentKey: function (copyFrom) {
            this.$type = utils.getFullName("BankAccountMovementDocumentKey");

            this.BankAccount = null;
            this.DocumentReference = null;
            this.ArchiveSuffix = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        NomenclaturePaymentTypesFilter: function (copyFrom) {
            this.$type = utils.getFullName("NomenclaturePaymentTypesFilter");
        
            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        MessagesToUserFilter: function (copyFrom) {
            this.$type = utils.getFullName("MessagesToUserFilter");

            this.Paging = new Objects.Paging();
            this.Period = new Objects.Period();

            this.Valid = false;

            if (copyFrom) { utils.copyProps(copyFrom, this); }

        },

        MessageToUserKey: function (copyFrom) {
            this.$type = utils.getFullName("MessageToUserKey");

            this.ID = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }

        },

        BankBranchesFilter: function(copyFrom) {
            this.$type = utils.getFullName("BankStructureItemsFilter");

            if (copyFrom) { utils.copyProps(copyFrom, this); }
            
        },

        LatLngPoint: function(copyFrom) {
            this.$type = utils.getFullName("LatLngPoint");

            this.Latitude = '';
            this.Longitude = '';
            
            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        BankBranchesBoundsFilter: function (copyFrom) {
            this.$type = utils.getFullName("BankBranchesBoundsFilter");

            this.NorthEast = null;
            this.SouthWest = null;
            
            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        ATMsBoundsFilter: function (copyFrom) {
            this.$type = utils.getFullName("ATMsBoundsFilter");

            this.NorthEast = null;
            this.SouthWest = null;

            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        //LatLngBoundsFilter: function (copyFrom) {
        //    this.$type = utils.getFullName("LatLngBoundsFilter");
        //    this.LatLngBounds = null;
        //    //this.lat = '';
        //    //this.lnt = '';

        //    if (copyFrom) { utils.copyProps(copyFrom, this); }

        //},
        //ClinetApplicationVersion: function (copyfrom)
        //{
        //    this.$type = utils.getFullName("ClinetApplicationVersion");
        
        //    this.Platform = null;
        //    this.Version = null;

        //    if (copyFrom) { utils.copyProps(copyFrom, this); }
        //},

        PushRegistrationData: function (copyFrom) {
            this.$type = utils.getFullName("PushRegistrationData");

            this.RegistrationToken = null;
            this.DeviceType = null;

            this.APerson = null;
            if (copyFrom) { utils.copyProps(copyFrom, this); }
        },

        NotificationsFilter: function (copyFrom) {
            this.$type = utils.getFullName("NotificationsFilter");

            this.Paging = new Objects.Paging();

            this.Valid = false;

            if (copyFrom) {
                utils.copyProps(copyFrom, this);
            }
        },
        OnlineNotificationKey: function (copyFrom) {
            this.$type = utils.getFullName("OnlineNotificationKey");

            this.ID = null;
            
            if (copyFrom) {
                utils.copyProps(copyFrom, this);
            }
        },
        BankCorrespondent: function (copyFrom) {
            this.$type = utils.getFullName("BankCorrespondent");

            this.ID = null;
            this.Name = null;
            this.APerson = null;

            if (copyFrom) {
                utils.copyProps(copyFrom, this);
            }
        },

        Subscription: function (copyFrom) {
            this.$type = utils.getFullName("Subscription");

            this.ID = null;
            this.BankCorrespondent = null;
            this.NotificationService = null; //notificationServiceBase
            this.Status = null;
            this.BankProduct = null; // BankProductBase
            this.Language = null;
            this.Options = null;

            if (copyFrom) {
                utils.copyProps(copyFrom, this);
            }
        },

        BankClientKey: function (copyFrom) {
            this.$type = utils.getFullName("BankClientKey");

            this.BankClientId = null;

            if (copyFrom) {
                utils.copyProps(copyFrom, this);
            }
        },
        BankProductBase: function (copyFrom) {
            this.$type = utils.getFullName("BankProductBase");

            this.REFID = null;
            this.ProductType = null;
            this.BankClient = null;

            if (copyFrom) {
                utils.copyProps(copyFrom, this);
            }
        },
        OnlineNotificationServiceBase: function (copyFrom) {
            this.$type = utils.getFullName("OnlineNotificationServiceBase");

            this.ServiceID = null;
            this.ProductType = null;

            if (copyFrom) {
                utils.copyProps(copyFrom, this);
            }
        },
        ChangeSubscriptionStatus: function(copyFrom){
            this.$type = utils.getFullName("ChangeSubscriptionStatus");

            this.Subscription = null; // subscriptionKey
            this.SubscriptionStatus = null; // EnumNotificationCorrespondentSubscriptionStatus

            if (copyFrom) {
                utils.copyProps(copyFrom, this);
            }
        },
        SubscriptionKey : function(copyFrom){
            this.$type = utils.getFullName("SubscriptionKey");

            this.ID = null; 

            if (copyFrom) {
                utils.copyProps(copyFrom, this);
            }
        },

        AmountLimitOptions: function(copyFrom){
            this.$type = utils.getFullName("AmountLimitOptions");

            this.Limit = null;
            this.NotificationServiceID = null;

            if (copyFrom) {
                utils.copyProps(copyFrom, this);
            }
        },
        CurrencyRateOptions: function(copyFrom){
            this.$type = utils.getFullName("CurrencyRateOptions");

            this.MessageHours = null;
            this.CurrencyCode = null;
            this.NotificationServiceID = null;

            if (copyFrom) {
                utils.copyProps(copyFrom, this);
            }

        },
        CurrentBalanceOptions: function (copyFrom) {
            this.$type = utils.getFullName("CurrentBalanceOptions");

            this.MessageHours = null;
            this.NotificationServiceID = null;

            if (copyFrom) {
                utils.copyProps(copyFrom, this);
            }
        },
        FutureInstallmentOptions: function (copyFrom) {
            this.$type = utils.getFullName("FutureInstallmentOptions");

            this.DaysPriorToWarning = null;
            this.NotificationServiceID = null;

            if (copyFrom) {
                utils.copyProps(copyFrom, this);
            }
        },


        EnumAuthorizationProcessStatus:  {
            OK : 10,
            Rejected : 20,
            RejectedInvalidCode : 21,
            RejectedRetry : 30,
            RejectedTooManyRetries : 40
        },

        EnumPaymentIncarnationType: {
            PaymentOrder: 1,
            PaymentOrderTemplate: 2
        },

        EnumStatusCode: {
            Success: 0,
            Error: 1
        },

        EnumNOIParagraphDirection: {
            NONE: 0,
            IN: 1,
            OUT: 2,
            DD: 3
        },

        EnumPaymentOrderPendingOperation: {
            Sign: 1,
            Send: 2
        },

        EnumBankClientSegment: {
            None: 0,
            Individual: 1,
            Corporate: 2
        },
        EnumNotificationCorrespondentSubscriptionStatus: {
            Inactive: 0,
            Active: 1,
        },

        Utils: utils
    };

    return Objects;
});
