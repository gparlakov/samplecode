﻿define({
    "TMPL_VAL_NOTNULL": "Полето е задължително.",
    "TMPL_VAL_NOTNULLOREMPTY": "Полето е задължително.",
    "TMPL_VAL_MAXLENGTH": "Моля попълнете стойност с дължина не по-голяма от {{:parameters}}",
    "TMPL_VAL_MINLENGTH": "Моля попълнете стойност с дължина по-голяма от {{:parameters}} characters.",
    "TMPL_VAL_REGEX": "Невалиден формат.",
    "TMPL_VAL_DECIAMLMINVALUE": "Стойността трябва да е по-голяма от {{:parameters.value}}",
    "TMPL_VAL_HOMEIBAN": "Невалиден IBAN.",
    "TMPL_VAL_INTERNATIONALIBAN": "Невалиден IBAN за {{:~GetLocalizedString(parameters.Name)}}",
    "TMPL_VAL_INTERNATIONALSWIFT": "Невалиден SWIFT код за {{:~GetLocalizedString(parameters.Name)}}",
    "TMPL_VAL_INVALIDSWIFTSYMBOLS": "Невалидни SWIFT символи: {{:result.invalidString}}.",
    "TMPL_VAL_INVALIDBANKSWIFT": "Невалиден SWIFT.",
    "TMPL_VAL_INVALIDBISERASYMBOLS": "Невалидни SWIFT символи: {{:result.invalidString}}.",
    "TMPL_VAL_INVALIDEGN": "Невалидно ЕГН.",
    "TMPL_VAL_INVALIDTAXNUMBER": "Невалиден номер",
    "TMPL_VAL_INVALIDBULSTAT": "Невалиден булстат",
    "TMPL_VAL_EXECUTION_DATE_MUST_BE_LATER": "Не се приемат нареждания с дата на изпълнение преди текущата дата",
    "TMPL_VAL_EXECUTION_DATE_MUST_BE_SOONER": "Максималният срок за изпълнение на плащане е 30 календарни дни, считано от текущата дата",
    

    "TMPL_VAL_SPC_AMOUNT": "Моля въведете правилна сума. Позволени са сума от 1 до 11 цифри, десетична точка, от 0 до 2 цифри. Пример '6.15', '0.1'.",
    "TMPL_VAL_SPC_CHOOSE_BENEFICIARY": "Моля изберете сметка на получател",
    "TMPL_VAL_SPC_FOREIGNBENEFACCOUN_SWIFT_OR_BANKINFO": "Въведете име, адрес, град и държава на банка на бенефициента или BIC код и държава",
    "TMPL_VAL_SPC_INVALIDACCOUNTNUMBER": "Моля въведете валиден номер на сметка",
    "TMPL_VAL_SPC_RATIO": "Моля въведете правилен курс. Позволени са курсове от 1 до 5 цифри, десетична точка или запетая, от 1 до 6 цифри. Пример '123,456789', '6.15', '0.1234'.",

    "-": "-"
});