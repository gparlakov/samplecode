﻿define({
    "TMPL_VAL_NOTNULL": "Field is required",
    "TMPL_VAL_NOTNULLOREMPTY": "Field is required",
    "TMPL_VAL_MAXLENGTH": "Please enter value shorter than {{:parameters}} characters.",
    "TMPL_VAL_MINLENGTH": "Please enter value with length more than {{:parameters}} characters.",
    "TMPL_VAL_REGEX": "Invalid format.",
    "TMPL_VAL_DECIAMLMINVALUE": "The value must be greater than {{>~format.number(parameters.value)}}",
    "TMPL_VAL_HOMEIBAN": "Invalid IBAN.",
    "TMPL_VAL_INTERNATIONALIBAN": "Invalid IBAN for {{:~GetLocalizedString(parameters.Name)}}",
    "TMPL_VAL_INTERNATIONALSWIFT": "Invalid Bank SWIFT for {{:~GetLocalizedString(parameters.Name)}}",
    "TMPL_VAL_INVALIDSWIFTSYMBOLS": "Invalid SWIFT symbols: {{:result.invalidString}}.",
    "TMPL_VAL_INVALIDBANKSWIFT": "Invalid SWIFT.",
    "TMPL_VAL_INVALIDBISERASYMBOLS": "Invalid symbols: {{:result.invalidString}}.",
    "TMPL_VAL_INVALIDEGN": "Invalid EGN.",
    "TMPL_VAL_INVALIDTAXNUMBER": "Invalid tax number",
    "TMPL_VAL_INVALIDBULSTAT": "Invalid BULSTAT",
    "TMPL_VAL_EXECUTION_DATE_MUST_BE_LATER": "Execute date cannot be before today",
    "TMPL_VAL_EXECUTION_DATE_MUST_BE_SOONER" :"The maximum period a payment to be sent is 30 calendar days from current date",

    "TMPL_VAL_SPC_AMOUNT": "Please enter a valid amount. Valid amounts are from 1 to 11 digits, decimal point, from 0 to 2 digits. Examples: '6.15', '0.1'.",
    "TMPL_VAL_SPC_CHOOSE_BENEFICIARY": "Please choose beneficiary account.",
    "TMPL_VAL_SPC_FOREIGNBENEFACCOUN_SWIFT_OR_BANKINFO": "Enter name, address, city and country of the beneficiary's bank or BIC code and country",
    "TMPL_VAL_SPC_INVALIDACCOUNTNUMBER": "Please enter a valid account number.",
    "TMPL_VAL_SPC_RATIO": "Please enter a valid rate. Valid rates are from 1 to 5 digits, decimal point or comma, from 1 to 6 digits. Examples: '123,456789', '6.15', '0.1234'.",

    "-": "-"
});