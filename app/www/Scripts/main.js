﻿/// <reference path="libs/plugins/datePickerPluginEnhanser.js" />
require.config({
    paths: {
        servicesObjectsUtils : 'Common/servicesObjectsUtils',
        servicesObjects: 'Mobile/Common/servicesObjects',
        //trqbva da ne se kazva cordova, za da raboti uglify-a 
        _cordova: '../cordova',

        //jquery: 'libs/jquery/jquery',
        //jqueryformat: 'libs/jquery/jqueryformat',
        //jqueryui: 'libs/jquery/jquery-ui-1.8.18.custom.min',
        //jqConfig: 'libs/jquery/jquery-config',
        //jqmConfig: 'libs/jquery/jqm-config',
        //jquerymobile: 'libs/jquery/jquery.mobile',

        jquery: 'libs/jquery.fake/jquery',
        jqueryformat: 'libs/jquery.fake/jqueryformat',
        jqueryui: 'libs/jquery.fake/jquery-ui',
        jqConfig: 'libs/jquery.fake/jquery-config',
        jqmConfig: 'libs/jquery.fake/jqm-config',
        jquerymobile: 'libs/jquery.fake/jquery.mobile',

        bootstrap: 'libs/bootstrap/js/bootstrap',

        underscore: 'libs/underscore/underscore',
        backbone: 'libs/backbone/backbone',
        knockout: 'libs/knockout/knockout-3.3.0',
        komapping: 'libs/knockout/knockout.mapping-2.4.1',
        jsrender: 'libs/jsrender/jsrender-1.0beta',
        //datePickerPlugin: 'libs/plugins/datePickerPlugin',
        datePickerPluginEnhanser:'libs/plugins/datePickerPluginEnhanser',
        utils: 'Common/utils',
        services: 'Mobile/Common/services',
        stringUtils: 'Common/stringUtils',
        encryptor: 'Common/security/encryptor',
        diagnostics: 'Common/diagnostics',
        deviceapi: 'Common/deviceapi',
        mvc: 'Common/mvc',
        cache: 'Common/cache/cache',
        productscache: 'Mobile/Common/cache/productscache',
        session : 'Common/session',
        daisJson: 'Common/daisJson',
        text: 'libs/text',
        i18n: 'libs/i18n',
        os: 'libs/os',
        async: 'libs/async',

        jqueryDais: 'Common/jquery-dais',
        knockoutDais: 'Common/knockout-dais',
        knockoutComponents: 'Controls/knockout-components',
        jsrenderDais: 'Common/jsrender-dais',
        requireDais: 'Common/require-dais',
        bootstrapDais: 'Common/bootstrap-dais',
		
        validator: 'Common/validator',

        typeVisitor: 'Common/visitors/typeVisitor',
        events: 'Common/objectEvents',
        jqueryUnobtrusiveKnockout: 'libs/knockout/jquery.unobtrusive-knockout',
        uimodels: 'Mobile/ViewModels/Common/UIModels',
        config: 'config',

        osManager: 'Common/OSspecific/osManager',
        dateUtils: 'Common/dateUtils',
        cryptoCore: 'libs/crypto/core',
		
        mapBaseControl: 'Mobile/ViewModels/Common/googleMaps/MapBaseControl',
        gmapsInfoBox: 'libs/googleMaps/InfoBox',
        fastClick: 'libs/fastClick',

        swiperjs: 'libs/swiper/swiper.min',
        jquerySwiper: 'libs/swiper/swiper.jquery.min',
        swipeMenu: 'Common/swipeMenu',

        globalize: 'libs/devexpress/globalize.min',
        dxChart: 'libs/devexpress/dx.chartjs',

        pushNotifications: 'Common/pushNotifications',
        pushNotificationsOSSpecific: 'Common/pushNotificationsOSSpecific'
    },

    shim: {
        '_cordova': {
            exports: 'cordova'

        },

        'jquery': {
            exports: 'jQuery'
        },

        'underscore': {
            exports: '_'
        },

        'backbone': {
            //These script dependencies should be loaded before loading
            //backbone.js
            deps: ['underscore', 'jquery'],
            //Once loaded, use the global 'backbone' as the
            //module value.
            exports: 'Backbone'
        },

        //'Mobile/router' : {
        //    deps: ['backbone']
        //},

        'servicesObjects' : {
            deps: ['underscore', 'dateUtils']
        },

        'knockoutDais': {
            deps: ['knockout', 'servicesObjects']
        },

        'requestSigner': {
            deps: ['diagnostics', 'session']
        },

        'knockout': {
            exports: 'ko'
        },


        'komapping': {
            deps: ['knockout'],
            exports: 'ko.mapping'
        },

        'jquerymobile': {
            deps: ['jquery', 'jqueryui', 'jqConfig', 'jqmConfig'],
            exports: 'jQuery.mobile'
        },

        'jsrender': {
            deps: ['jquery'],
            exports: 'jQuery.templates'
        },

        'jqueryformat': {
            deps: ['jquery'],
            exports: 'jQuery.format'
        },
        'jqueryDais':{
            deps:['jquery', 'jqueryui']
        },
        'jqueryui':{
            deps:['jquery']
        },

        'daisJson': {
            deps:['dateUtils']
        },

        'jqueryUnobtrusiveKnockout': {
            deps: ['jquery', 'knockout']
        },

        'datePickerPluginEnhanser': {
            deps: ['jquery', 'dateUtils']
        },
        'cryptoCore': {
            exports: 'sjcl'
        },
        'gmapsInfoBox': {
            deps: ['i18n!Mobile/nls/googleMaps']
        },
        'bootstrap': {
            deps: ['jquery', 'dateUtils']
        },
        'bootstrapDais': {
            deps: ['bootstrap']
        },
        'jquerySwiper': {
            deps: ['jquery', 'swiperjs']
        },
        'swipeMenu': {
            deps: ['jquerySwiper']
        },
        'dxChart': {
            deps: ['jquery', 'knockout', 'globalize']
        },
	}
});
        
require.config({
	config: {
	    //Set the config for the i18n
	    //module ID
	    i18n: {
            //ujasnooo 
            locale: window.localStorage.getItem('applicationSelectedLang') ? (JSON.parse(window.localStorage.getItem('applicationSelectedLang'))).value: 'bg-bg'
	    }
	}

});

requirejs.onError = function (err) {

    try
    {
        require(["diagnostics"], function (diagnostics) {
            diagnostics.Error({
                type: "requirejs onError",
                error: err,
                url: '',
                line: ''
            });
        });
    }
    catch (e)
    { }
    
    console.log(err.requireType);
    
    console.log('modules: ' + err.requireModules);
    
	
    throw err;
};

require([

// Load our app module and pass it to our definition function
  'app',

// Some plugins have to be loaded in order due to there non AMD compliance
// Because these scripts are not "modules" they do not pass any values to the definition function below
	'_cordova',
	'jquery',
    'stringUtils',
	'underscore',
	'backbone',
	'knockout',
	'jqmConfig',
	'jsrender',
    'jquerymobile',
    'mvc',
    'cryptoCore',
    'jqConfig',

    'jqueryDais', //tova loadva jqueryUI. Imam problem mejdu jqu
    'jsrenderDais', //add helpers to jsRender
    'knockoutDais',
    'bootstrapDais',
    'swiperjs',
    'jquerySwiper',
    'swipeMenu',

    'servicesObjects',

    'Common/validatorMethods',
    'config',
	
    'datePickerPluginEnhanser'
	
], function (App,
    _cordova,
    jquery,
    stringUtils,
    backbone,
    underscore,
    knockout,
    jqmConfig,
    jsrender,
    jquerymobile,
    mvc,
    cryptoCore,
    jquery_config,
    jqueryDais, 
    jsrenderDais, 
    knockoutDais,
    bootstrapDais,
    swiperjs,
    jquerySwiper,
    swipeMenu,
    servicesObjects,
    validatorMethods,
    config,
    datePickerPluginEnhanser
    ) {
	// The "app" dependency is passed in as "App"
    // Again, the other dependencies passed in are not "AMD" therefore don't pass a parameter to this function
    stringUtils.initialize();
    App.initialize();
    $ = jquery;
    ___ = underscore;
    __backbone = backbone;
    __mvc = mvc;
    __ko = knockout;
    __cryptoCore = cryptoCore;

    //__codrova = cordova;

});
