﻿//iOS

define([
    'jquery',
    'services',
    'servicesObjects',
    'underscore',
    'diagnostics',
    'session',
    'osManager'
], function ($, services, servicesObjects, _, diagnostics, session, osManager) {

    var pushNotificationsOSSpecific = {
        Register: function () { },
        Unregister: function () { },
    };

    function register(options) {
        var defaults = {
            "badge": "false",
            "sound": "false",
            "alert": "true",
            "ecb": "window.onNotification"
        };

        options = _.extend(defaults, options);

        window.plugins.pushNotification.register(onRegisteredSuccess, onRegisteredFail, options);
    }

    function onRegisteredSuccess(token) {
        diagnostics.Debug('Push Notifications -> regID = ' + token);

        var apId = session.GetSession().UserID;

        var request = new (servicesObjects.PushRegistrationData)(
        {
            APerson: {
                APersonID: apId
            },
            RegistrationToken: token,
            DeviceType: osManager.CurrentOS.osTypeName.id
        });

        services.PushNotificationsService.SavePushRegistration(request);
    }

    function onRegisteredFail(error) {
        diagnostics.Error(error);
    }

    function onNotification(event) {
        diagnostics.Debug('Push Notifications -> Event RECEIVED:' + event.alert || event.sound || event.badge);

        if (event.alert) {
            //navigator.notification.alert(event.alert);
        }

        if (event.sound) {
            var snd = new Media(event.sound);
            snd.play();
        }
        
        if (event.badge) {
            pushNotification.setApplicationIconBadgeNumber(addBadge_successHandler, addBadge_errorHandler, event.badge);
        }
    }

    function addBadge_successHandler() {

    }

    function addBadge_errorHandler() {

    }

    function onDeviceReady() {
        //when an event is received it does not know about this scope so it needs a global var
        window.onNotification = onNotification;

        pushNotificationsOSSpecific.Register = register;

        pushNotificationsOSSpecific.Unregister = window.plugins.pushNotification.unregister;
    }                                                     

    document.addEventListener("deviceready", onDeviceReady, false);

    return pushNotificationsOSSpecific;
});